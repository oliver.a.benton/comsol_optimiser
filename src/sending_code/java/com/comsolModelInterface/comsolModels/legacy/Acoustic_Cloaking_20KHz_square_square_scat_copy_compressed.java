package com.comsolModelInterface.comsolModels.legacy;/*
 * Acoustic_Cloaking_20KHz_square_square_scat_copy_compressed.java
 */

import com.comsol.model.*;
import com.comsol.model.util.*;

/** Model exported on Jul 3 2020, 09:30 by COMSOL 5.4.0.346. */
public class Acoustic_Cloaking_20KHz_square_square_scat_copy_compressed {

  public static Model run() {
    Model model = ModelUtil.create("Model");

    model.modelPath("D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\java\\com\\comsolModels");

    model.label("Acoustic_Cloaking_20KHz_square_square_scat-2020-02-05.mph");

    model.param().set("c0", "343[m/s]");
    model.param().set("rhoA", "1.2041[kg/m^3]");
    model.param().set("freq", "20000[1/s]");
    model.param().set("lam", "c0/freq");
    model.param().set("kk", "2*pi/lam");
    model.param().set("radius_central_pipe", "lam");
    model.param().set("scatterers_max_distance", "2.52*lam");
    model.param().set("radius_scatterer", "lam/15");
    model.param().set("init_scatterer_pos_x", "(-(radius_central_pipe ) - 0.1)");
    model.param().set("init_scatterer_pos_y", "0 [m]");
    model.param().set("far_field_r", "3*scatterers_max_distance");
    model.param().set("fitness_w_o_cloak", "0.056080");
    model.param().set("scatterer_x_1", "0.018672933");
    model.param().set("scatterer_x_2", "0.015870206");
    model.param().set("scatterer_x_3", "0.010199162");
    model.param().set("scatterer_x_4", "0.021537355");
    model.param().set("scatterer_x_5", "0.020983");
    model.param().set("scatterer_x_6", "0.02006962");
    model.param().set("scatterer_x_7", "0.023965142");
    model.param().set("scatterer_x_8", "0.026840077");
    model.param().set("scatterer_x_9", "0.025067413");
    model.param().set("scatterer_x_10", "0.022199184");
    model.param().set("scatterer_x_11", "0.020391671");
    model.param().set("scatterer_x_12", "0.015949842");
    model.param().set("scatterer_x_13", "0.013504981");
    model.param().set("scatterer_x_14", "0.005567243");
    model.param().set("scatterer_x_15", "0.033030031");
    model.param().set("scatterer_x_16", "0.030049722");
    model.param().set("scatterer_x_17", "0.002889753");
    model.param().set("scatterer_x_18", "0.032089154");
    model.param().set("scatterer_x_19", "0.025466097");
    model.param().set("scatterer_x_20", "0.016350241");
    model.param().set("scatterer_x_21", "0.005633909");
    model.param().set("scatterer_x_22", "0.038773657");
    model.param().set("scatterer_x_23", "0.0311192");
    model.param().set("scatterer_x_24", "0.029378026");
    model.param().set("scatterer_x_25", "0.02748715");
    model.param().set("scatterer_x_26", "0.025456205");
    model.param().set("scatterer_x_27", "0.023295541");
    model.param().set("scatterer_x_28", "0.040183453");
    model.param().set("scatterer_x_29", "0.024065418");
    model.param().set("scatterer_x_30", "0.013974844");
    model.param().set("scatterer_y_1", "0.002684762");
    model.param().set("scatterer_y_2", "0.010199162");
    model.param().set("scatterer_y_3", "0.015870206");
    model.param().set("scatterer_y_4", "0.002835447");
    model.param().set("scatterer_y_5", "0.005622378");
    model.param().set("scatterer_y_6", "0.008313109");
    model.param().set("scatterer_y_7", "0.005469887");
    model.param().set("scatterer_y_8", "0.005705034");
    model.param().set("scatterer_y_9", "0.011160731");
    model.param().set("scatterer_y_10", "0.016128651");
    model.param().set("scatterer_y_11", "0.018360743");
    model.param().set("scatterer_y_12", "0.025759879");
    model.param().set("scatterer_y_13", "0.027121657");
    model.param().set("scatterer_y_14", "0.029782119");
    model.param().set("scatterer_y_15", "0.002889753");
    model.param().set("scatterer_y_16", "0.014012416");
    model.param().set("scatterer_y_17", "0.033030031");
    model.param().set("scatterer_y_18", "0.016350241");
    model.param().set("scatterer_y_19", "0.025466097");
    model.param().set("scatterer_y_20", "0.032089154");
    model.param().set("scatterer_y_21", "0.035571102");
    model.param().set("scatterer_y_22", "0.002773147");
    model.param().set("scatterer_y_23", "0.023295541");
    model.param().set("scatterer_y_24", "0.025456205");
    model.param().set("scatterer_y_25", "0.02748715");
    model.param().set("scatterer_y_26", "0.029378026");
    model.param().set("scatterer_y_27", "0.031119199");
    model.param().set("scatterer_y_28", "0.011258876");
    model.param().set("scatterer_y_29", "0.03409293");
    model.param().set("scatterer_y_30", "0.039321443");
    model.param().set("fitness_for_squared_metrics", "0.048905");
    model.param().set("fitness_object_cloak", "0.15197");

    model.component().create("comp1", false);

    model.component("comp1").geom().create("geom1", 2);

    model.result().table().create("tbl1", "Table");
    model.result().table().create("tbl7", "Table");
    model.result().table().create("tbl8", "Table");
    model.result().table().create("tbl9", "Table");
    model.result().table().create("tbl10", "Table");
    model.result().table().create("evl2", "Table");

    model.component("comp1").mesh().create("mesh1");

    model.component("comp1").geom("geom1").selection().create("csel1", "CumulativeSelection");
    model.component("comp1").geom("geom1").selection("csel1").label("central_object");
    model.component("comp1").geom("geom1").selection().create("csel2", "CumulativeSelection");
    model.component("comp1").geom("geom1").selection("csel2").label("square_cumulative");
    model.component("comp1").geom("geom1").selection().create("csel3", "CumulativeSelection");
    model.component("comp1").geom("geom1").selection("csel3").label("circular_cumulative");
    model.component("comp1").geom("geom1").selection().create("csel4", "CumulativeSelection");
    model.component("comp1").geom("geom1").selection("csel4").label("main_area");
    model.component("comp1").geom("geom1").create("c1", "Circle");
    model.component("comp1").geom("geom1").feature("c1").active(false);
    model.component("comp1").geom("geom1").feature("c1").label("central_pipe");
    model.component("comp1").geom("geom1").feature("c1").set("contributeto", "csel1");
    model.component("comp1").geom("geom1").feature("c1").set("r", "radius_central_pipe");
    model.component("comp1").geom("geom1").create("c3", "Circle");
    model.component("comp1").geom("geom1").feature("c3").active(false);
    model.component("comp1").geom("geom1").feature("c3").label("max_radial_distance_scatterers");
    model.component("comp1").geom("geom1").feature("c3").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c3").set("r", "scatterers_max_distance");
    model.component("comp1").geom("geom1").create("c123", "Circle");
    model.component("comp1").geom("geom1").feature("c123").active(false);
    model.component("comp1").geom("geom1").feature("c123").label("Circle 2_Far_Field_1");
    model.component("comp1").geom("geom1").feature("c123").set("pos", new double[]{2.2898349882894E-16, 0});
    model.component("comp1").geom("geom1").feature("c123").set("r", "far_field_r");
    model.component("comp1").geom("geom1").create("sq1", "Square");
    model.component("comp1").geom("geom1").feature("sq1").label("Central square");
    model.component("comp1").geom("geom1").feature("sq1").set("contributeto", "csel1");
    model.component("comp1").geom("geom1").feature("sq1").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq1").set("size", "radius_central_pipe*sqrt(2)");
    model.component("comp1").geom("geom1").create("r1", "Rectangle");
    model.component("comp1").geom("geom1").feature("r1").set("contributeto", "csel4");
    model.component("comp1").geom("geom1").feature("r1").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("r1").set("base", "center");
    model.component("comp1").geom("geom1").feature("r1").set("layername", new String[]{"Layer 1"});
    model.component("comp1").geom("geom1").feature("r1").setIndex("layer", "0.01", 0);
    model.component("comp1").geom("geom1").feature("r1").set("layerleft", true);
    model.component("comp1").geom("geom1").feature("r1").set("layerright", true);
    model.component("comp1").geom("geom1").feature("r1").set("layertop", true);
    model.component("comp1").geom("geom1").feature("r1").set("size", new double[]{0.3, 0.3});
    model.component("comp1").geom("geom1").create("sca1", "Scale");
    model.component("comp1").geom("geom1").feature("sca1").set("type", "anisotropic");
    model.component("comp1").geom("geom1").feature("sca1").set("factor", new double[]{0.9999997236511904, 1});
    model.component("comp1").geom("geom1").feature("sca1").set("pos", new double[]{0.11495, 0});
    model.component("comp1").geom("geom1").feature("sca1").selection("input").set("r1");
    model.component("comp1").geom("geom1").create("sq2", "Square");
    model.component("comp1").geom("geom1").feature("sq2").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq2").set("pos", new String[]{"scatterer_x_1", "scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("sq2").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq2").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq3", "Square");
    model.component("comp1").geom("geom1").feature("sq3").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq3").set("pos", new String[]{"scatterer_x_2", "scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("sq3").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq3").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq4", "Square");
    model.component("comp1").geom("geom1").feature("sq4").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq4").set("pos", new String[]{"scatterer_x_3", "scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("sq4").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq4").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq5", "Square");
    model.component("comp1").geom("geom1").feature("sq5").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq5").set("pos", new String[]{"scatterer_x_4", "scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("sq5").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq5").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq6", "Square");
    model.component("comp1").geom("geom1").feature("sq6").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq6").set("pos", new String[]{"scatterer_x_5", "scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("sq6").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq6").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq7", "Square");
    model.component("comp1").geom("geom1").feature("sq7").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq7").set("pos", new String[]{"scatterer_x_6", "scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("sq7").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq7").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq8", "Square");
    model.component("comp1").geom("geom1").feature("sq8").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq8").set("pos", new String[]{"scatterer_x_7", "scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("sq8").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq8").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq9", "Square");
    model.component("comp1").geom("geom1").feature("sq9").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq9").set("pos", new String[]{"scatterer_x_8", "scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("sq9").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq9").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq10", "Square");
    model.component("comp1").geom("geom1").feature("sq10").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq10")
         .set("pos", new String[]{"scatterer_x_9", "scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("sq10").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq10").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq11", "Square");
    model.component("comp1").geom("geom1").feature("sq11").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq11")
         .set("pos", new String[]{"scatterer_x_10", "scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("sq11").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq11").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq12", "Square");
    model.component("comp1").geom("geom1").feature("sq12").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq12")
         .set("pos", new String[]{"scatterer_x_11", "scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("sq12").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq12").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq13", "Square");
    model.component("comp1").geom("geom1").feature("sq13").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq13")
         .set("pos", new String[]{"scatterer_x_12", "scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("sq13").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq13").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq14", "Square");
    model.component("comp1").geom("geom1").feature("sq14").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq14")
         .set("pos", new String[]{"scatterer_x_13", "scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("sq14").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq14").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq15", "Square");
    model.component("comp1").geom("geom1").feature("sq15").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq15")
         .set("pos", new String[]{"scatterer_x_14", "scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("sq15").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq15").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq16", "Square");
    model.component("comp1").geom("geom1").feature("sq16").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq16")
         .set("pos", new String[]{"scatterer_x_15", "scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("sq16").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq16").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq17", "Square");
    model.component("comp1").geom("geom1").feature("sq17").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq17")
         .set("pos", new String[]{"scatterer_x_16", "scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("sq17").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq17").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq18", "Square");
    model.component("comp1").geom("geom1").feature("sq18").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq18")
         .set("pos", new String[]{"scatterer_x_17", "scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("sq18").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq18").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq19", "Square");
    model.component("comp1").geom("geom1").feature("sq19").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq19")
         .set("pos", new String[]{"scatterer_x_18", "scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("sq19").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq19").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq20", "Square");
    model.component("comp1").geom("geom1").feature("sq20").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq20")
         .set("pos", new String[]{"scatterer_x_19", "scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("sq20").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq20").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq21", "Square");
    model.component("comp1").geom("geom1").feature("sq21").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq21")
         .set("pos", new String[]{"scatterer_x_20", "scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("sq21").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq21").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq22", "Square");
    model.component("comp1").geom("geom1").feature("sq22").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq22")
         .set("pos", new String[]{"scatterer_x_21", "scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("sq22").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq22").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq23", "Square");
    model.component("comp1").geom("geom1").feature("sq23").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq23")
         .set("pos", new String[]{"scatterer_x_22", "scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("sq23").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq23").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq24", "Square");
    model.component("comp1").geom("geom1").feature("sq24").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq24")
         .set("pos", new String[]{"scatterer_x_23", "scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("sq24").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq24").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq25", "Square");
    model.component("comp1").geom("geom1").feature("sq25").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq25")
         .set("pos", new String[]{"scatterer_x_24", "scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("sq25").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq25").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq26", "Square");
    model.component("comp1").geom("geom1").feature("sq26").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq26")
         .set("pos", new String[]{"scatterer_x_25", "scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("sq26").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq26").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq27", "Square");
    model.component("comp1").geom("geom1").feature("sq27").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq27")
         .set("pos", new String[]{"scatterer_x_26", "scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("sq27").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq27").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq28", "Square");
    model.component("comp1").geom("geom1").feature("sq28").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq28")
         .set("pos", new String[]{"scatterer_x_27", "scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("sq28").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq28").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq29", "Square");
    model.component("comp1").geom("geom1").feature("sq29").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq29")
         .set("pos", new String[]{"scatterer_x_28", "scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("sq29").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq29").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq30", "Square");
    model.component("comp1").geom("geom1").feature("sq30").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq30")
         .set("pos", new String[]{"scatterer_x_29", "scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("sq30").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq30").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq31", "Square");
    model.component("comp1").geom("geom1").feature("sq31").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq31")
         .set("pos", new String[]{"scatterer_x_30", "scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("sq31").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq31").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq32", "Square");
    model.component("comp1").geom("geom1").feature("sq32").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq32")
         .set("pos", new String[]{"-scatterer_x_1", "scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("sq32").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq32").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq33", "Square");
    model.component("comp1").geom("geom1").feature("sq33").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq33")
         .set("pos", new String[]{"-scatterer_x_2", "scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("sq33").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq33").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq34", "Square");
    model.component("comp1").geom("geom1").feature("sq34").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq34")
         .set("pos", new String[]{"-scatterer_x_3", "scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("sq34").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq34").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq35", "Square");
    model.component("comp1").geom("geom1").feature("sq35").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq35")
         .set("pos", new String[]{"-scatterer_x_4", "scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("sq35").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq35").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq36", "Square");
    model.component("comp1").geom("geom1").feature("sq36").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq36")
         .set("pos", new String[]{"-scatterer_x_5", "scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("sq36").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq36").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq37", "Square");
    model.component("comp1").geom("geom1").feature("sq37").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq37")
         .set("pos", new String[]{"-scatterer_x_6", "scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("sq37").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq37").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq38", "Square");
    model.component("comp1").geom("geom1").feature("sq38").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq38")
         .set("pos", new String[]{"-scatterer_x_7", "scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("sq38").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq38").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq39", "Square");
    model.component("comp1").geom("geom1").feature("sq39").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq39")
         .set("pos", new String[]{"-scatterer_x_8", "scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("sq39").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq39").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq40", "Square");
    model.component("comp1").geom("geom1").feature("sq40").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq40")
         .set("pos", new String[]{"-scatterer_x_9", "scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("sq40").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq40").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq41", "Square");
    model.component("comp1").geom("geom1").feature("sq41").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq41")
         .set("pos", new String[]{"-scatterer_x_10", "scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("sq41").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq41").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq42", "Square");
    model.component("comp1").geom("geom1").feature("sq42").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq42")
         .set("pos", new String[]{"-scatterer_x_11", "scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("sq42").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq42").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq43", "Square");
    model.component("comp1").geom("geom1").feature("sq43").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq43")
         .set("pos", new String[]{"-scatterer_x_12", "scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("sq43").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq43").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq44", "Square");
    model.component("comp1").geom("geom1").feature("sq44").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq44")
         .set("pos", new String[]{"-scatterer_x_13", "scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("sq44").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq44").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq45", "Square");
    model.component("comp1").geom("geom1").feature("sq45").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq45")
         .set("pos", new String[]{"-scatterer_x_14", "scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("sq45").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq45").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq46", "Square");
    model.component("comp1").geom("geom1").feature("sq46").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq46")
         .set("pos", new String[]{"-scatterer_x_15", "scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("sq46").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq46").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq47", "Square");
    model.component("comp1").geom("geom1").feature("sq47").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq47")
         .set("pos", new String[]{"-scatterer_x_16", "scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("sq47").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq47").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq48", "Square");
    model.component("comp1").geom("geom1").feature("sq48").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq48")
         .set("pos", new String[]{"-scatterer_x_17", "scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("sq48").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq48").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq49", "Square");
    model.component("comp1").geom("geom1").feature("sq49").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq49")
         .set("pos", new String[]{"-scatterer_x_18", "scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("sq49").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq49").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq50", "Square");
    model.component("comp1").geom("geom1").feature("sq50").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq50")
         .set("pos", new String[]{"-scatterer_x_19", "scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("sq50").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq50").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq51", "Square");
    model.component("comp1").geom("geom1").feature("sq51").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq51")
         .set("pos", new String[]{"-scatterer_x_20", "scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("sq51").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq51").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq52", "Square");
    model.component("comp1").geom("geom1").feature("sq52").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq52")
         .set("pos", new String[]{"-scatterer_x_21", "scatterer_y_21"});

    return model;
  }

  public static Model run2(Model model) {
    model.component("comp1").geom("geom1").feature("sq52").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq52").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq53", "Square");
    model.component("comp1").geom("geom1").feature("sq53").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq53")
         .set("pos", new String[]{"-scatterer_x_22", "scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("sq53").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq53").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq54", "Square");
    model.component("comp1").geom("geom1").feature("sq54").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq54")
         .set("pos", new String[]{"-scatterer_x_23", "scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("sq54").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq54").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq55", "Square");
    model.component("comp1").geom("geom1").feature("sq55").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq55")
         .set("pos", new String[]{"-scatterer_x_24", "scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("sq55").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq55").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq56", "Square");
    model.component("comp1").geom("geom1").feature("sq56").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq56")
         .set("pos", new String[]{"-scatterer_x_25", "scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("sq56").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq56").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq57", "Square");
    model.component("comp1").geom("geom1").feature("sq57").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq57")
         .set("pos", new String[]{"-scatterer_x_26", "scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("sq57").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq57").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq58", "Square");
    model.component("comp1").geom("geom1").feature("sq58").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq58")
         .set("pos", new String[]{"-scatterer_x_27", "scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("sq58").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq58").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq59", "Square");
    model.component("comp1").geom("geom1").feature("sq59").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq59")
         .set("pos", new String[]{"-scatterer_x_28", "scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("sq59").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq59").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq60", "Square");
    model.component("comp1").geom("geom1").feature("sq60").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq60")
         .set("pos", new String[]{"-scatterer_x_29", "scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("sq60").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq60").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq61", "Square");
    model.component("comp1").geom("geom1").feature("sq61").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq61")
         .set("pos", new String[]{"-scatterer_x_30", "scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("sq61").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq61").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq122", "Square");
    model.component("comp1").geom("geom1").feature("sq122").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq122")
         .set("pos", new String[]{"-scatterer_x_1", "-scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("sq122").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq122").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq123", "Square");
    model.component("comp1").geom("geom1").feature("sq123").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq123")
         .set("pos", new String[]{"-scatterer_x_2", "-scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("sq123").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq123").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq124", "Square");
    model.component("comp1").geom("geom1").feature("sq124").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq124")
         .set("pos", new String[]{"-scatterer_x_3", "-scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("sq124").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq124").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq125", "Square");
    model.component("comp1").geom("geom1").feature("sq125").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq125")
         .set("pos", new String[]{"-scatterer_x_4", "-scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("sq125").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq125").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq126", "Square");
    model.component("comp1").geom("geom1").feature("sq126").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq126")
         .set("pos", new String[]{"-scatterer_x_5", "-scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("sq126").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq126").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq127", "Square");
    model.component("comp1").geom("geom1").feature("sq127").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq127")
         .set("pos", new String[]{"-scatterer_x_6", "-scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("sq127").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq127").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq128", "Square");
    model.component("comp1").geom("geom1").feature("sq128").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq128")
         .set("pos", new String[]{"-scatterer_x_7", "-scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("sq128").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq128").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq129", "Square");
    model.component("comp1").geom("geom1").feature("sq129").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq129")
         .set("pos", new String[]{"-scatterer_x_8", "-scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("sq129").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq129").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq130", "Square");
    model.component("comp1").geom("geom1").feature("sq130").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq130")
         .set("pos", new String[]{"-scatterer_x_9", "-scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("sq130").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq130").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq131", "Square");
    model.component("comp1").geom("geom1").feature("sq131").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq131")
         .set("pos", new String[]{"-scatterer_x_10", "-scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("sq131").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq131").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq132", "Square");
    model.component("comp1").geom("geom1").feature("sq132").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq132")
         .set("pos", new String[]{"-scatterer_x_11", "-scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("sq132").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq132").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq133", "Square");
    model.component("comp1").geom("geom1").feature("sq133").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq133")
         .set("pos", new String[]{"-scatterer_x_12", "-scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("sq133").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq133").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq134", "Square");
    model.component("comp1").geom("geom1").feature("sq134").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq134")
         .set("pos", new String[]{"-scatterer_x_13", "-scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("sq134").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq134").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq135", "Square");
    model.component("comp1").geom("geom1").feature("sq135").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq135")
         .set("pos", new String[]{"-scatterer_x_14", "-scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("sq135").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq135").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq136", "Square");
    model.component("comp1").geom("geom1").feature("sq136").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq136")
         .set("pos", new String[]{"-scatterer_x_15", "-scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("sq136").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq136").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq137", "Square");
    model.component("comp1").geom("geom1").feature("sq137").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq137")
         .set("pos", new String[]{"-scatterer_x_16", "-scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("sq137").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq137").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq138", "Square");
    model.component("comp1").geom("geom1").feature("sq138").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq138")
         .set("pos", new String[]{"-scatterer_x_17", "-scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("sq138").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq138").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq139", "Square");
    model.component("comp1").geom("geom1").feature("sq139").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq139")
         .set("pos", new String[]{"-scatterer_x_18", "-scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("sq139").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq139").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq140", "Square");
    model.component("comp1").geom("geom1").feature("sq140").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq140")
         .set("pos", new String[]{"-scatterer_x_19", "-scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("sq140").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq140").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq141", "Square");
    model.component("comp1").geom("geom1").feature("sq141").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq141")
         .set("pos", new String[]{"-scatterer_x_20", "-scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("sq141").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq141").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq142", "Square");
    model.component("comp1").geom("geom1").feature("sq142").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq142")
         .set("pos", new String[]{"-scatterer_x_21", "-scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("sq142").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq142").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq143", "Square");
    model.component("comp1").geom("geom1").feature("sq143").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq143")
         .set("pos", new String[]{"-scatterer_x_22", "-scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("sq143").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq143").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq144", "Square");
    model.component("comp1").geom("geom1").feature("sq144").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq144")
         .set("pos", new String[]{"-scatterer_x_23", "-scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("sq144").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq144").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq145", "Square");
    model.component("comp1").geom("geom1").feature("sq145").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq145")
         .set("pos", new String[]{"-scatterer_x_24", "-scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("sq145").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq145").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq146", "Square");
    model.component("comp1").geom("geom1").feature("sq146").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq146")
         .set("pos", new String[]{"-scatterer_x_25", "-scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("sq146").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq146").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq147", "Square");
    model.component("comp1").geom("geom1").feature("sq147").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq147")
         .set("pos", new String[]{"-scatterer_x_26", "-scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("sq147").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq147").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq148", "Square");
    model.component("comp1").geom("geom1").feature("sq148").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq148")
         .set("pos", new String[]{"-scatterer_x_27", "-scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("sq148").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq148").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq149", "Square");
    model.component("comp1").geom("geom1").feature("sq149").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq149")
         .set("pos", new String[]{"-scatterer_x_28", "-scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("sq149").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq149").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq150", "Square");
    model.component("comp1").geom("geom1").feature("sq150").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq150")
         .set("pos", new String[]{"-scatterer_x_29", "-scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("sq150").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq150").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq151", "Square");
    model.component("comp1").geom("geom1").feature("sq151").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq151")
         .set("pos", new String[]{"-scatterer_x_30", "-scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("sq151").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq151").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq152", "Square");
    model.component("comp1").geom("geom1").feature("sq152").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq152")
         .set("pos", new String[]{"scatterer_x_1", "-scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("sq152").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq152").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq153", "Square");
    model.component("comp1").geom("geom1").feature("sq153").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq153")
         .set("pos", new String[]{"scatterer_x_2", "-scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("sq153").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq153").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq154", "Square");
    model.component("comp1").geom("geom1").feature("sq154").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq154")
         .set("pos", new String[]{"scatterer_x_3", "-scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("sq154").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq154").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq155", "Square");
    model.component("comp1").geom("geom1").feature("sq155").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq155")
         .set("pos", new String[]{"scatterer_x_4", "-scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("sq155").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq155").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq156", "Square");
    model.component("comp1").geom("geom1").feature("sq156").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq156")
         .set("pos", new String[]{"scatterer_x_5", "-scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("sq156").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq156").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq157", "Square");
    model.component("comp1").geom("geom1").feature("sq157").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq157")
         .set("pos", new String[]{"scatterer_x_6", "-scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("sq157").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq157").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq158", "Square");
    model.component("comp1").geom("geom1").feature("sq158").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq158")
         .set("pos", new String[]{"scatterer_x_7", "-scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("sq158").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq158").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq159", "Square");
    model.component("comp1").geom("geom1").feature("sq159").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq159")
         .set("pos", new String[]{"scatterer_x_8", "-scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("sq159").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq159").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq160", "Square");
    model.component("comp1").geom("geom1").feature("sq160").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq160")
         .set("pos", new String[]{"scatterer_x_9", "-scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("sq160").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq160").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq161", "Square");
    model.component("comp1").geom("geom1").feature("sq161").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq161")
         .set("pos", new String[]{"scatterer_x_10", "-scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("sq161").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq161").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq162", "Square");
    model.component("comp1").geom("geom1").feature("sq162").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq162")
         .set("pos", new String[]{"scatterer_x_11", "-scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("sq162").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq162").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq163", "Square");
    model.component("comp1").geom("geom1").feature("sq163").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq163")
         .set("pos", new String[]{"scatterer_x_12", "-scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("sq163").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq163").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq164", "Square");
    model.component("comp1").geom("geom1").feature("sq164").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq164")
         .set("pos", new String[]{"scatterer_x_13", "-scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("sq164").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq164").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq165", "Square");
    model.component("comp1").geom("geom1").feature("sq165").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq165")
         .set("pos", new String[]{"scatterer_x_14", "-scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("sq165").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq165").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq166", "Square");
    model.component("comp1").geom("geom1").feature("sq166").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq166")
         .set("pos", new String[]{"scatterer_x_15", "-scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("sq166").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq166").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq167", "Square");
    model.component("comp1").geom("geom1").feature("sq167").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq167")
         .set("pos", new String[]{"scatterer_x_16", "-scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("sq167").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq167").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq168", "Square");
    model.component("comp1").geom("geom1").feature("sq168").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq168")
         .set("pos", new String[]{"scatterer_x_17", "-scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("sq168").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq168").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq169", "Square");
    model.component("comp1").geom("geom1").feature("sq169").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq169")
         .set("pos", new String[]{"scatterer_x_18", "-scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("sq169").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq169").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq170", "Square");
    model.component("comp1").geom("geom1").feature("sq170").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq170")
         .set("pos", new String[]{"scatterer_x_19", "-scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("sq170").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq170").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq171", "Square");
    model.component("comp1").geom("geom1").feature("sq171").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq171")
         .set("pos", new String[]{"scatterer_x_20", "-scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("sq171").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq171").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq172", "Square");
    model.component("comp1").geom("geom1").feature("sq172").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq172")
         .set("pos", new String[]{"scatterer_x_21", "-scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("sq172").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq172").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq173", "Square");
    model.component("comp1").geom("geom1").feature("sq173").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq173")
         .set("pos", new String[]{"scatterer_x_22", "-scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("sq173").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq173").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq174", "Square");
    model.component("comp1").geom("geom1").feature("sq174").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq174")
         .set("pos", new String[]{"scatterer_x_23", "-scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("sq174").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq174").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq175", "Square");
    model.component("comp1").geom("geom1").feature("sq175").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq175")
         .set("pos", new String[]{"scatterer_x_24", "-scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("sq175").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq175").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq176", "Square");
    model.component("comp1").geom("geom1").feature("sq176").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq176")
         .set("pos", new String[]{"scatterer_x_25", "-scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("sq176").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq176").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq177", "Square");
    model.component("comp1").geom("geom1").feature("sq177").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq177")
         .set("pos", new String[]{"scatterer_x_26", "-scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("sq177").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq177").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq178", "Square");
    model.component("comp1").geom("geom1").feature("sq178").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq178")
         .set("pos", new String[]{"scatterer_x_27", "-scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("sq178").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq178").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq179", "Square");
    model.component("comp1").geom("geom1").feature("sq179").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq179")
         .set("pos", new String[]{"scatterer_x_28", "-scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("sq179").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq179").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq180", "Square");
    model.component("comp1").geom("geom1").feature("sq180").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq180")
         .set("pos", new String[]{"scatterer_x_29", "-scatterer_y_29"});

    return model;
  }

  public static Model run3(Model model) {
    model.component("comp1").geom("geom1").feature("sq180").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq180").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq181", "Square");
    model.component("comp1").geom("geom1").feature("sq181").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq181")
         .set("pos", new String[]{"scatterer_x_30", "-scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("sq181").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq181").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq182", "Square");
    model.component("comp1").geom("geom1").feature("sq182").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq182").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq182").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq182").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq183", "Square");
    model.component("comp1").geom("geom1").feature("sq183").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq183").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq183").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq183").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq184", "Square");
    model.component("comp1").geom("geom1").feature("sq184").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq184").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq184").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq184").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq185", "Square");
    model.component("comp1").geom("geom1").feature("sq185").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq185").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq185").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq185").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq186", "Square");
    model.component("comp1").geom("geom1").feature("sq186").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq186").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq186").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq186").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq187", "Square");
    model.component("comp1").geom("geom1").feature("sq187").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq187").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq187").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq187").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq188", "Square");
    model.component("comp1").geom("geom1").feature("sq188").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq188").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq188").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq188").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq189", "Square");
    model.component("comp1").geom("geom1").feature("sq189").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq189").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq189").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq189").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq190", "Square");
    model.component("comp1").geom("geom1").feature("sq190").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq190").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq190").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq190").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq191", "Square");
    model.component("comp1").geom("geom1").feature("sq191").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq191").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq191").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq191").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq192", "Square");
    model.component("comp1").geom("geom1").feature("sq192").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq192").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq192").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq192").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq193", "Square");
    model.component("comp1").geom("geom1").feature("sq193").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq193").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq193").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq193").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq194", "Square");
    model.component("comp1").geom("geom1").feature("sq194").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq194").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq194").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq194").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq195", "Square");
    model.component("comp1").geom("geom1").feature("sq195").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq195").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq195").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq195").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq196", "Square");
    model.component("comp1").geom("geom1").feature("sq196").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq196").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq196").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq196").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq197", "Square");
    model.component("comp1").geom("geom1").feature("sq197").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq197").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq197").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq197").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq198", "Square");
    model.component("comp1").geom("geom1").feature("sq198").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq198").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq198").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq198").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq199", "Square");
    model.component("comp1").geom("geom1").feature("sq199").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq199").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq199").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq199").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq200", "Square");
    model.component("comp1").geom("geom1").feature("sq200").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq200").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq200").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq200").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("sq201", "Square");
    model.component("comp1").geom("geom1").feature("sq201").set("contributeto", "csel2");
    model.component("comp1").geom("geom1").feature("sq201").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("sq201").set("base", "center");
    model.component("comp1").geom("geom1").feature("sq201").set("size", "radius_scatterer*sqrt(2)");
    model.component("comp1").geom("geom1").create("c2", "Circle");
    model.component("comp1").geom("geom1").feature("c2").active(false);
    model.component("comp1").geom("geom1").feature("c2").label("scatterer_1");
    model.component("comp1").geom("geom1").feature("c2").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c2").set("pos", new String[]{"scatterer_x_1", "scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("c2").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c4", "Circle");
    model.component("comp1").geom("geom1").feature("c4").active(false);
    model.component("comp1").geom("geom1").feature("c4").label("scatterer_2");
    model.component("comp1").geom("geom1").feature("c4").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c4").set("pos", new String[]{"scatterer_x_2", "scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("c4").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c5", "Circle");
    model.component("comp1").geom("geom1").feature("c5").active(false);
    model.component("comp1").geom("geom1").feature("c5").label("scatterer_3");
    model.component("comp1").geom("geom1").feature("c5").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c5").set("pos", new String[]{"scatterer_x_3", "scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("c5").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c6", "Circle");
    model.component("comp1").geom("geom1").feature("c6").active(false);
    model.component("comp1").geom("geom1").feature("c6").label("scatterer_4");
    model.component("comp1").geom("geom1").feature("c6").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c6").set("pos", new String[]{"scatterer_x_4", "scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("c6").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c7", "Circle");
    model.component("comp1").geom("geom1").feature("c7").active(false);
    model.component("comp1").geom("geom1").feature("c7").label("scatterer_5");
    model.component("comp1").geom("geom1").feature("c7").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c7").set("pos", new String[]{"scatterer_x_5", "scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("c7").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c8", "Circle");
    model.component("comp1").geom("geom1").feature("c8").active(false);
    model.component("comp1").geom("geom1").feature("c8").label("scatterer_6");
    model.component("comp1").geom("geom1").feature("c8").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c8").set("pos", new String[]{"scatterer_x_6", "scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("c8").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c9", "Circle");
    model.component("comp1").geom("geom1").feature("c9").active(false);
    model.component("comp1").geom("geom1").feature("c9").label("scatterer_7");
    model.component("comp1").geom("geom1").feature("c9").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c9").set("pos", new String[]{"scatterer_x_7", "scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("c9").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c10", "Circle");
    model.component("comp1").geom("geom1").feature("c10").active(false);
    model.component("comp1").geom("geom1").feature("c10").label("scatterer_8");
    model.component("comp1").geom("geom1").feature("c10").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c10").set("pos", new String[]{"scatterer_x_8", "scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("c10").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c11", "Circle");
    model.component("comp1").geom("geom1").feature("c11").active(false);
    model.component("comp1").geom("geom1").feature("c11").label("scatterer_9");
    model.component("comp1").geom("geom1").feature("c11").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c11").set("pos", new String[]{"scatterer_x_9", "scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("c11").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c12", "Circle");
    model.component("comp1").geom("geom1").feature("c12").active(false);
    model.component("comp1").geom("geom1").feature("c12").label("scatterer_10");
    model.component("comp1").geom("geom1").feature("c12").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c12")
         .set("pos", new String[]{"scatterer_x_10", "scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("c12").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c13", "Circle");
    model.component("comp1").geom("geom1").feature("c13").active(false);
    model.component("comp1").geom("geom1").feature("c13").label("scatterer_11");
    model.component("comp1").geom("geom1").feature("c13").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c13")
         .set("pos", new String[]{"scatterer_x_11", "scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("c13").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c14", "Circle");
    model.component("comp1").geom("geom1").feature("c14").active(false);
    model.component("comp1").geom("geom1").feature("c14").label("scatterer_12");
    model.component("comp1").geom("geom1").feature("c14").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c14")
         .set("pos", new String[]{"scatterer_x_12", "scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("c14").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c15", "Circle");
    model.component("comp1").geom("geom1").feature("c15").active(false);
    model.component("comp1").geom("geom1").feature("c15").label("scatterer_13");
    model.component("comp1").geom("geom1").feature("c15").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c15")
         .set("pos", new String[]{"scatterer_x_13", "scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("c15").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c16", "Circle");
    model.component("comp1").geom("geom1").feature("c16").active(false);
    model.component("comp1").geom("geom1").feature("c16").label("scatterer_14");
    model.component("comp1").geom("geom1").feature("c16").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c16")
         .set("pos", new String[]{"scatterer_x_14", "scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("c16").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c17", "Circle");
    model.component("comp1").geom("geom1").feature("c17").active(false);
    model.component("comp1").geom("geom1").feature("c17").label("scatterer_15");
    model.component("comp1").geom("geom1").feature("c17").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c17")
         .set("pos", new String[]{"scatterer_x_15", "scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("c17").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c18", "Circle");
    model.component("comp1").geom("geom1").feature("c18").active(false);
    model.component("comp1").geom("geom1").feature("c18").label("scatterer_16");
    model.component("comp1").geom("geom1").feature("c18").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c18")
         .set("pos", new String[]{"scatterer_x_16", "scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("c18").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c19", "Circle");
    model.component("comp1").geom("geom1").feature("c19").active(false);
    model.component("comp1").geom("geom1").feature("c19").label("scatterer_17");
    model.component("comp1").geom("geom1").feature("c19").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c19")
         .set("pos", new String[]{"scatterer_x_17", "scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("c19").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c20", "Circle");
    model.component("comp1").geom("geom1").feature("c20").active(false);
    model.component("comp1").geom("geom1").feature("c20").label("scatterer_18");
    model.component("comp1").geom("geom1").feature("c20").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c20")
         .set("pos", new String[]{"scatterer_x_18", "scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("c20").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c21", "Circle");
    model.component("comp1").geom("geom1").feature("c21").active(false);
    model.component("comp1").geom("geom1").feature("c21").label("scatterer_19");
    model.component("comp1").geom("geom1").feature("c21").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c21")
         .set("pos", new String[]{"scatterer_x_19", "scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("c21").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c22", "Circle");
    model.component("comp1").geom("geom1").feature("c22").active(false);
    model.component("comp1").geom("geom1").feature("c22").label("scatterer_20");
    model.component("comp1").geom("geom1").feature("c22").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c22")
         .set("pos", new String[]{"scatterer_x_20", "scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("c22").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c23", "Circle");
    model.component("comp1").geom("geom1").feature("c23").active(false);
    model.component("comp1").geom("geom1").feature("c23").label("scatterer_21");
    model.component("comp1").geom("geom1").feature("c23").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c23")
         .set("pos", new String[]{"scatterer_x_21", "scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("c23").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c24", "Circle");
    model.component("comp1").geom("geom1").feature("c24").active(false);
    model.component("comp1").geom("geom1").feature("c24").label("scatterer_22");
    model.component("comp1").geom("geom1").feature("c24").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c24")
         .set("pos", new String[]{"scatterer_x_22", "scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("c24").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c25", "Circle");
    model.component("comp1").geom("geom1").feature("c25").active(false);
    model.component("comp1").geom("geom1").feature("c25").label("scatterer_23");
    model.component("comp1").geom("geom1").feature("c25").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c25")
         .set("pos", new String[]{"scatterer_x_23", "scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("c25").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c26", "Circle");
    model.component("comp1").geom("geom1").feature("c26").active(false);
    model.component("comp1").geom("geom1").feature("c26").label("scatterer_24");
    model.component("comp1").geom("geom1").feature("c26").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c26")
         .set("pos", new String[]{"scatterer_x_24", "scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("c26").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c27", "Circle");
    model.component("comp1").geom("geom1").feature("c27").active(false);
    model.component("comp1").geom("geom1").feature("c27").label("scatterer_25");
    model.component("comp1").geom("geom1").feature("c27").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c27")
         .set("pos", new String[]{"scatterer_x_25", "scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("c27").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c28", "Circle");
    model.component("comp1").geom("geom1").feature("c28").active(false);
    model.component("comp1").geom("geom1").feature("c28").label("scatterer_26");
    model.component("comp1").geom("geom1").feature("c28").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c28")
         .set("pos", new String[]{"scatterer_x_26", "scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("c28").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c29", "Circle");
    model.component("comp1").geom("geom1").feature("c29").active(false);
    model.component("comp1").geom("geom1").feature("c29").label("scatterer_27");
    model.component("comp1").geom("geom1").feature("c29").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c29")
         .set("pos", new String[]{"scatterer_x_27", "scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("c29").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c30", "Circle");
    model.component("comp1").geom("geom1").feature("c30").active(false);
    model.component("comp1").geom("geom1").feature("c30").label("scatterer_28");
    model.component("comp1").geom("geom1").feature("c30").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c30")
         .set("pos", new String[]{"scatterer_x_28", "scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("c30").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c31", "Circle");
    model.component("comp1").geom("geom1").feature("c31").active(false);
    model.component("comp1").geom("geom1").feature("c31").label("scatterer_29");
    model.component("comp1").geom("geom1").feature("c31").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c31")
         .set("pos", new String[]{"scatterer_x_29", "scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("c31").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c32", "Circle");
    model.component("comp1").geom("geom1").feature("c32").active(false);
    model.component("comp1").geom("geom1").feature("c32").label("scatterer_30");
    model.component("comp1").geom("geom1").feature("c32").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c32")
         .set("pos", new String[]{"scatterer_x_30", "scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("c32").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c33", "Circle");
    model.component("comp1").geom("geom1").feature("c33").active(false);
    model.component("comp1").geom("geom1").feature("c33").label("scatterer_31");
    model.component("comp1").geom("geom1").feature("c33").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c33")
         .set("pos", new String[]{"-scatterer_x_1", "scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("c33").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c34", "Circle");
    model.component("comp1").geom("geom1").feature("c34").active(false);
    model.component("comp1").geom("geom1").feature("c34").label("scatterer_32");
    model.component("comp1").geom("geom1").feature("c34").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c34")
         .set("pos", new String[]{"-scatterer_x_2", "scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("c34").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c35", "Circle");
    model.component("comp1").geom("geom1").feature("c35").active(false);
    model.component("comp1").geom("geom1").feature("c35").label("scatterer_33");
    model.component("comp1").geom("geom1").feature("c35").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c35")
         .set("pos", new String[]{"-scatterer_x_3", "scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("c35").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c36", "Circle");
    model.component("comp1").geom("geom1").feature("c36").active(false);
    model.component("comp1").geom("geom1").feature("c36").label("scatterer_34");
    model.component("comp1").geom("geom1").feature("c36").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c36")
         .set("pos", new String[]{"-scatterer_x_4", "scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("c36").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c37", "Circle");
    model.component("comp1").geom("geom1").feature("c37").active(false);
    model.component("comp1").geom("geom1").feature("c37").label("scatterer_35");
    model.component("comp1").geom("geom1").feature("c37").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c37")
         .set("pos", new String[]{"-scatterer_x_5", "scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("c37").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c124", "Circle");
    model.component("comp1").geom("geom1").feature("c124").active(false);
    model.component("comp1").geom("geom1").feature("c124").label("scatterer_36");
    model.component("comp1").geom("geom1").feature("c124").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c124")
         .set("pos", new String[]{"-scatterer_x_6", "scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("c124").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c125", "Circle");
    model.component("comp1").geom("geom1").feature("c125").active(false);
    model.component("comp1").geom("geom1").feature("c125").label("scatterer_37");
    model.component("comp1").geom("geom1").feature("c125").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c125")
         .set("pos", new String[]{"-scatterer_x_7", "scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("c125").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c126", "Circle");
    model.component("comp1").geom("geom1").feature("c126").active(false);
    model.component("comp1").geom("geom1").feature("c126").label("scatterer_38");
    model.component("comp1").geom("geom1").feature("c126").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c126")
         .set("pos", new String[]{"-scatterer_x_8", "scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("c126").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c127", "Circle");
    model.component("comp1").geom("geom1").feature("c127").active(false);
    model.component("comp1").geom("geom1").feature("c127").label("scatterer_39");
    model.component("comp1").geom("geom1").feature("c127").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c127")
         .set("pos", new String[]{"-scatterer_x_9", "scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("c127").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c128", "Circle");
    model.component("comp1").geom("geom1").feature("c128").active(false);
    model.component("comp1").geom("geom1").feature("c128").label("scatterer_40");
    model.component("comp1").geom("geom1").feature("c128").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c128")
         .set("pos", new String[]{"-scatterer_x_10", "scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("c128").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c129", "Circle");
    model.component("comp1").geom("geom1").feature("c129").active(false);
    model.component("comp1").geom("geom1").feature("c129").label("scatterer_41");
    model.component("comp1").geom("geom1").feature("c129").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c129")
         .set("pos", new String[]{"-scatterer_x_11", "scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("c129").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c130", "Circle");
    model.component("comp1").geom("geom1").feature("c130").active(false);
    model.component("comp1").geom("geom1").feature("c130").label("scatterer_42");
    model.component("comp1").geom("geom1").feature("c130").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c130")
         .set("pos", new String[]{"-scatterer_x_12", "scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("c130").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c131", "Circle");
    model.component("comp1").geom("geom1").feature("c131").active(false);
    model.component("comp1").geom("geom1").feature("c131").label("scatterer_43");
    model.component("comp1").geom("geom1").feature("c131").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c131")
         .set("pos", new String[]{"-scatterer_x_13", "scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("c131").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c132", "Circle");
    model.component("comp1").geom("geom1").feature("c132").active(false);

    return model;
  }

  public static Model run4(Model model) {
    model.component("comp1").geom("geom1").feature("c132").label("scatterer_44");
    model.component("comp1").geom("geom1").feature("c132").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c132")
         .set("pos", new String[]{"-scatterer_x_14", "scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("c132").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c133", "Circle");
    model.component("comp1").geom("geom1").feature("c133").active(false);
    model.component("comp1").geom("geom1").feature("c133").label("scatterer_45");
    model.component("comp1").geom("geom1").feature("c133").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c133")
         .set("pos", new String[]{"-scatterer_x_15", "scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("c133").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c134", "Circle");
    model.component("comp1").geom("geom1").feature("c134").active(false);
    model.component("comp1").geom("geom1").feature("c134").label("scatterer_46");
    model.component("comp1").geom("geom1").feature("c134").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c134")
         .set("pos", new String[]{"-scatterer_x_16", "scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("c134").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c135", "Circle");
    model.component("comp1").geom("geom1").feature("c135").active(false);
    model.component("comp1").geom("geom1").feature("c135").label("scatterer_47");
    model.component("comp1").geom("geom1").feature("c135").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c135")
         .set("pos", new String[]{"-scatterer_x_17", "scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("c135").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c136", "Circle");
    model.component("comp1").geom("geom1").feature("c136").active(false);
    model.component("comp1").geom("geom1").feature("c136").label("scatterer_48");
    model.component("comp1").geom("geom1").feature("c136").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c136")
         .set("pos", new String[]{"-scatterer_x_18", "scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("c136").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c137", "Circle");
    model.component("comp1").geom("geom1").feature("c137").active(false);
    model.component("comp1").geom("geom1").feature("c137").label("scatterer_49");
    model.component("comp1").geom("geom1").feature("c137").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c137")
         .set("pos", new String[]{"-scatterer_x_19", "scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("c137").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c138", "Circle");
    model.component("comp1").geom("geom1").feature("c138").active(false);
    model.component("comp1").geom("geom1").feature("c138").label("scatterer_50");
    model.component("comp1").geom("geom1").feature("c138").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c138")
         .set("pos", new String[]{"-scatterer_x_20", "scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("c138").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c139", "Circle");
    model.component("comp1").geom("geom1").feature("c139").active(false);
    model.component("comp1").geom("geom1").feature("c139").label("scatterer_51");
    model.component("comp1").geom("geom1").feature("c139").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c139")
         .set("pos", new String[]{"-scatterer_x_21", "scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("c139").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c140", "Circle");
    model.component("comp1").geom("geom1").feature("c140").active(false);
    model.component("comp1").geom("geom1").feature("c140").label("scatterer_52");
    model.component("comp1").geom("geom1").feature("c140").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c140")
         .set("pos", new String[]{"-scatterer_x_22", "scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("c140").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c141", "Circle");
    model.component("comp1").geom("geom1").feature("c141").active(false);
    model.component("comp1").geom("geom1").feature("c141").label("scatterer_53");
    model.component("comp1").geom("geom1").feature("c141").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c141")
         .set("pos", new String[]{"-scatterer_x_23", "scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("c141").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c142", "Circle");
    model.component("comp1").geom("geom1").feature("c142").active(false);
    model.component("comp1").geom("geom1").feature("c142").label("scatterer_54");
    model.component("comp1").geom("geom1").feature("c142").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c142")
         .set("pos", new String[]{"-scatterer_x_24", "scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("c142").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c143", "Circle");
    model.component("comp1").geom("geom1").feature("c143").active(false);
    model.component("comp1").geom("geom1").feature("c143").label("scatterer_55");
    model.component("comp1").geom("geom1").feature("c143").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c143")
         .set("pos", new String[]{"-scatterer_x_25", "scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("c143").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c144", "Circle");
    model.component("comp1").geom("geom1").feature("c144").active(false);
    model.component("comp1").geom("geom1").feature("c144").label("scatterer_56");
    model.component("comp1").geom("geom1").feature("c144").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c144")
         .set("pos", new String[]{"-scatterer_x_26", "scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("c144").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c145", "Circle");
    model.component("comp1").geom("geom1").feature("c145").active(false);
    model.component("comp1").geom("geom1").feature("c145").label("scatterer_57");
    model.component("comp1").geom("geom1").feature("c145").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c145")
         .set("pos", new String[]{"-scatterer_x_27", "scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("c145").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c146", "Circle");
    model.component("comp1").geom("geom1").feature("c146").active(false);
    model.component("comp1").geom("geom1").feature("c146").label("scatterer_58");
    model.component("comp1").geom("geom1").feature("c146").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c146")
         .set("pos", new String[]{"-scatterer_x_28", "scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("c146").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c147", "Circle");
    model.component("comp1").geom("geom1").feature("c147").active(false);
    model.component("comp1").geom("geom1").feature("c147").label("scatterer_59");
    model.component("comp1").geom("geom1").feature("c147").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c147")
         .set("pos", new String[]{"-scatterer_x_29", "scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("c147").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c148", "Circle");
    model.component("comp1").geom("geom1").feature("c148").active(false);
    model.component("comp1").geom("geom1").feature("c148").label("scatterer_60");
    model.component("comp1").geom("geom1").feature("c148").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c148")
         .set("pos", new String[]{"-scatterer_x_30", "scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("c148").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c149", "Circle");
    model.component("comp1").geom("geom1").feature("c149").active(false);
    model.component("comp1").geom("geom1").feature("c149").label("scatterer_61");
    model.component("comp1").geom("geom1").feature("c149").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c149")
         .set("pos", new String[]{"-scatterer_x_1", "-scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("c149").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c150", "Circle");
    model.component("comp1").geom("geom1").feature("c150").active(false);
    model.component("comp1").geom("geom1").feature("c150").label("scatterer_62");
    model.component("comp1").geom("geom1").feature("c150").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c150")
         .set("pos", new String[]{"-scatterer_x_2", "-scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("c150").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c151", "Circle");
    model.component("comp1").geom("geom1").feature("c151").active(false);
    model.component("comp1").geom("geom1").feature("c151").label("scatterer_63");
    model.component("comp1").geom("geom1").feature("c151").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c151")
         .set("pos", new String[]{"-scatterer_x_3", "-scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("c151").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c152", "Circle");
    model.component("comp1").geom("geom1").feature("c152").active(false);
    model.component("comp1").geom("geom1").feature("c152").label("scatterer_64");
    model.component("comp1").geom("geom1").feature("c152").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c152")
         .set("pos", new String[]{"-scatterer_x_4", "-scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("c152").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c153", "Circle");
    model.component("comp1").geom("geom1").feature("c153").active(false);
    model.component("comp1").geom("geom1").feature("c153").label("scatterer_65");
    model.component("comp1").geom("geom1").feature("c153").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c153")
         .set("pos", new String[]{"-scatterer_x_5", "-scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("c153").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c154", "Circle");
    model.component("comp1").geom("geom1").feature("c154").active(false);
    model.component("comp1").geom("geom1").feature("c154").label("scatterer_66");
    model.component("comp1").geom("geom1").feature("c154").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c154")
         .set("pos", new String[]{"-scatterer_x_6", "-scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("c154").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c155", "Circle");
    model.component("comp1").geom("geom1").feature("c155").active(false);
    model.component("comp1").geom("geom1").feature("c155").label("scatterer_67");
    model.component("comp1").geom("geom1").feature("c155").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c155")
         .set("pos", new String[]{"-scatterer_x_7", "-scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("c155").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c156", "Circle");
    model.component("comp1").geom("geom1").feature("c156").active(false);
    model.component("comp1").geom("geom1").feature("c156").label("scatterer_68");
    model.component("comp1").geom("geom1").feature("c156").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c156")
         .set("pos", new String[]{"-scatterer_x_8", "-scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("c156").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c157", "Circle");
    model.component("comp1").geom("geom1").feature("c157").active(false);
    model.component("comp1").geom("geom1").feature("c157").label("scatterer_69");
    model.component("comp1").geom("geom1").feature("c157").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c157")
         .set("pos", new String[]{"-scatterer_x_9", "-scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("c157").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c158", "Circle");
    model.component("comp1").geom("geom1").feature("c158").active(false);
    model.component("comp1").geom("geom1").feature("c158").label("scatterer_70");
    model.component("comp1").geom("geom1").feature("c158").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c158")
         .set("pos", new String[]{"-scatterer_x_10", "-scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("c158").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c159", "Circle");
    model.component("comp1").geom("geom1").feature("c159").active(false);
    model.component("comp1").geom("geom1").feature("c159").label("scatterer_71");
    model.component("comp1").geom("geom1").feature("c159").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c159")
         .set("pos", new String[]{"-scatterer_x_11", "-scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("c159").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c160", "Circle");
    model.component("comp1").geom("geom1").feature("c160").active(false);
    model.component("comp1").geom("geom1").feature("c160").label("scatterer_72");
    model.component("comp1").geom("geom1").feature("c160").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c160")
         .set("pos", new String[]{"-scatterer_x_12", "-scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("c160").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c161", "Circle");
    model.component("comp1").geom("geom1").feature("c161").active(false);
    model.component("comp1").geom("geom1").feature("c161").label("scatterer_73");
    model.component("comp1").geom("geom1").feature("c161").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c161")
         .set("pos", new String[]{"-scatterer_x_13", "-scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("c161").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c162", "Circle");
    model.component("comp1").geom("geom1").feature("c162").active(false);
    model.component("comp1").geom("geom1").feature("c162").label("scatterer_74");
    model.component("comp1").geom("geom1").feature("c162").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c162")
         .set("pos", new String[]{"-scatterer_x_14", "-scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("c162").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c163", "Circle");
    model.component("comp1").geom("geom1").feature("c163").active(false);
    model.component("comp1").geom("geom1").feature("c163").label("scatterer_75");
    model.component("comp1").geom("geom1").feature("c163").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c163")
         .set("pos", new String[]{"-scatterer_x_15", "-scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("c163").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c164", "Circle");
    model.component("comp1").geom("geom1").feature("c164").active(false);
    model.component("comp1").geom("geom1").feature("c164").label("scatterer_76");
    model.component("comp1").geom("geom1").feature("c164").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c164")
         .set("pos", new String[]{"-scatterer_x_16", "-scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("c164").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c165", "Circle");
    model.component("comp1").geom("geom1").feature("c165").active(false);
    model.component("comp1").geom("geom1").feature("c165").label("scatterer_77");
    model.component("comp1").geom("geom1").feature("c165").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c165")
         .set("pos", new String[]{"-scatterer_x_17", "-scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("c165").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c166", "Circle");
    model.component("comp1").geom("geom1").feature("c166").active(false);
    model.component("comp1").geom("geom1").feature("c166").label("scatterer_78");
    model.component("comp1").geom("geom1").feature("c166").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c166")
         .set("pos", new String[]{"-scatterer_x_18", "-scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("c166").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c167", "Circle");
    model.component("comp1").geom("geom1").feature("c167").active(false);
    model.component("comp1").geom("geom1").feature("c167").label("scatterer_79");
    model.component("comp1").geom("geom1").feature("c167").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c167")
         .set("pos", new String[]{"-scatterer_x_19", "-scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("c167").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c168", "Circle");
    model.component("comp1").geom("geom1").feature("c168").active(false);
    model.component("comp1").geom("geom1").feature("c168").label("scatterer_80");
    model.component("comp1").geom("geom1").feature("c168").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c168")
         .set("pos", new String[]{"-scatterer_x_20", "-scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("c168").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c169", "Circle");
    model.component("comp1").geom("geom1").feature("c169").active(false);
    model.component("comp1").geom("geom1").feature("c169").label("scatterer_81");
    model.component("comp1").geom("geom1").feature("c169").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c169")
         .set("pos", new String[]{"-scatterer_x_21", "-scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("c169").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c170", "Circle");
    model.component("comp1").geom("geom1").feature("c170").active(false);
    model.component("comp1").geom("geom1").feature("c170").label("scatterer_82");
    model.component("comp1").geom("geom1").feature("c170").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c170")
         .set("pos", new String[]{"-scatterer_x_22", "-scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("c170").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c171", "Circle");
    model.component("comp1").geom("geom1").feature("c171").active(false);
    model.component("comp1").geom("geom1").feature("c171").label("scatterer_83");
    model.component("comp1").geom("geom1").feature("c171").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c171")
         .set("pos", new String[]{"-scatterer_x_23", "-scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("c171").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c172", "Circle");
    model.component("comp1").geom("geom1").feature("c172").active(false);
    model.component("comp1").geom("geom1").feature("c172").label("scatterer_84");
    model.component("comp1").geom("geom1").feature("c172").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c172")
         .set("pos", new String[]{"-scatterer_x_24", "-scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("c172").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c173", "Circle");
    model.component("comp1").geom("geom1").feature("c173").active(false);
    model.component("comp1").geom("geom1").feature("c173").label("scatterer_85");
    model.component("comp1").geom("geom1").feature("c173").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c173")
         .set("pos", new String[]{"-scatterer_x_25", "-scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("c173").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c174", "Circle");
    model.component("comp1").geom("geom1").feature("c174").active(false);
    model.component("comp1").geom("geom1").feature("c174").label("scatterer_86");
    model.component("comp1").geom("geom1").feature("c174").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c174")
         .set("pos", new String[]{"-scatterer_x_26", "-scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("c174").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c175", "Circle");
    model.component("comp1").geom("geom1").feature("c175").active(false);
    model.component("comp1").geom("geom1").feature("c175").label("scatterer_87");
    model.component("comp1").geom("geom1").feature("c175").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c175")
         .set("pos", new String[]{"-scatterer_x_27", "-scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("c175").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c176", "Circle");
    model.component("comp1").geom("geom1").feature("c176").active(false);
    model.component("comp1").geom("geom1").feature("c176").label("scatterer_88");
    model.component("comp1").geom("geom1").feature("c176").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c176")
         .set("pos", new String[]{"-scatterer_x_28", "-scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("c176").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c177", "Circle");
    model.component("comp1").geom("geom1").feature("c177").active(false);
    model.component("comp1").geom("geom1").feature("c177").label("scatterer_89");
    model.component("comp1").geom("geom1").feature("c177").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c177")
         .set("pos", new String[]{"-scatterer_x_29", "-scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("c177").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c178", "Circle");
    model.component("comp1").geom("geom1").feature("c178").active(false);
    model.component("comp1").geom("geom1").feature("c178").label("scatterer_90");
    model.component("comp1").geom("geom1").feature("c178").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c178")
         .set("pos", new String[]{"-scatterer_x_30", "-scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("c178").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c179", "Circle");
    model.component("comp1").geom("geom1").feature("c179").active(false);
    model.component("comp1").geom("geom1").feature("c179").label("scatterer_91");
    model.component("comp1").geom("geom1").feature("c179").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c179")
         .set("pos", new String[]{"scatterer_x_1", "-scatterer_y_1"});
    model.component("comp1").geom("geom1").feature("c179").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c180", "Circle");
    model.component("comp1").geom("geom1").feature("c180").active(false);
    model.component("comp1").geom("geom1").feature("c180").label("scatterer_92");
    model.component("comp1").geom("geom1").feature("c180").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c180")
         .set("pos", new String[]{"scatterer_x_2", "-scatterer_y_2"});
    model.component("comp1").geom("geom1").feature("c180").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c181", "Circle");
    model.component("comp1").geom("geom1").feature("c181").active(false);
    model.component("comp1").geom("geom1").feature("c181").label("scatterer_93");
    model.component("comp1").geom("geom1").feature("c181").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c181")
         .set("pos", new String[]{"scatterer_x_3", "-scatterer_y_3"});
    model.component("comp1").geom("geom1").feature("c181").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c182", "Circle");
    model.component("comp1").geom("geom1").feature("c182").active(false);
    model.component("comp1").geom("geom1").feature("c182").label("scatterer_94");
    model.component("comp1").geom("geom1").feature("c182").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c182")
         .set("pos", new String[]{"scatterer_x_4", "-scatterer_y_4"});
    model.component("comp1").geom("geom1").feature("c182").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c183", "Circle");
    model.component("comp1").geom("geom1").feature("c183").active(false);
    model.component("comp1").geom("geom1").feature("c183").label("scatterer_95");
    model.component("comp1").geom("geom1").feature("c183").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c183")
         .set("pos", new String[]{"scatterer_x_5", "-scatterer_y_5"});
    model.component("comp1").geom("geom1").feature("c183").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c184", "Circle");
    model.component("comp1").geom("geom1").feature("c184").active(false);
    model.component("comp1").geom("geom1").feature("c184").label("scatterer_96");
    model.component("comp1").geom("geom1").feature("c184").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c184")
         .set("pos", new String[]{"scatterer_x_6", "-scatterer_y_6"});
    model.component("comp1").geom("geom1").feature("c184").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c185", "Circle");
    model.component("comp1").geom("geom1").feature("c185").active(false);
    model.component("comp1").geom("geom1").feature("c185").label("scatterer_97");
    model.component("comp1").geom("geom1").feature("c185").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c185")
         .set("pos", new String[]{"scatterer_x_7", "-scatterer_y_7"});
    model.component("comp1").geom("geom1").feature("c185").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c186", "Circle");
    model.component("comp1").geom("geom1").feature("c186").active(false);
    model.component("comp1").geom("geom1").feature("c186").label("scatterer_98");
    model.component("comp1").geom("geom1").feature("c186").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c186")
         .set("pos", new String[]{"scatterer_x_8", "-scatterer_y_8"});
    model.component("comp1").geom("geom1").feature("c186").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c187", "Circle");
    model.component("comp1").geom("geom1").feature("c187").active(false);
    model.component("comp1").geom("geom1").feature("c187").label("scatterer_99");
    model.component("comp1").geom("geom1").feature("c187").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c187")
         .set("pos", new String[]{"scatterer_x_9", "-scatterer_y_9"});
    model.component("comp1").geom("geom1").feature("c187").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c188", "Circle");
    model.component("comp1").geom("geom1").feature("c188").active(false);
    model.component("comp1").geom("geom1").feature("c188").label("scatterer_100");
    model.component("comp1").geom("geom1").feature("c188").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c188")
         .set("pos", new String[]{"scatterer_x_10", "-scatterer_y_10"});
    model.component("comp1").geom("geom1").feature("c188").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c189", "Circle");
    model.component("comp1").geom("geom1").feature("c189").active(false);
    model.component("comp1").geom("geom1").feature("c189").label("scatterer_101");
    model.component("comp1").geom("geom1").feature("c189").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c189")
         .set("pos", new String[]{"scatterer_x_11", "-scatterer_y_11"});
    model.component("comp1").geom("geom1").feature("c189").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c190", "Circle");
    model.component("comp1").geom("geom1").feature("c190").active(false);
    model.component("comp1").geom("geom1").feature("c190").label("scatterer_102");
    model.component("comp1").geom("geom1").feature("c190").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c190")
         .set("pos", new String[]{"scatterer_x_12", "-scatterer_y_12"});
    model.component("comp1").geom("geom1").feature("c190").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c191", "Circle");
    model.component("comp1").geom("geom1").feature("c191").active(false);
    model.component("comp1").geom("geom1").feature("c191").label("scatterer_103");
    model.component("comp1").geom("geom1").feature("c191").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c191")
         .set("pos", new String[]{"scatterer_x_13", "-scatterer_y_13"});
    model.component("comp1").geom("geom1").feature("c191").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c192", "Circle");
    model.component("comp1").geom("geom1").feature("c192").active(false);
    model.component("comp1").geom("geom1").feature("c192").label("scatterer_104");
    model.component("comp1").geom("geom1").feature("c192").set("contributeto", "csel3");

    return model;
  }

  public static Model run5(Model model) {
    model.component("comp1").geom("geom1").feature("c192")
         .set("pos", new String[]{"scatterer_x_14", "-scatterer_y_14"});
    model.component("comp1").geom("geom1").feature("c192").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c193", "Circle");
    model.component("comp1").geom("geom1").feature("c193").active(false);
    model.component("comp1").geom("geom1").feature("c193").label("scatterer_105");
    model.component("comp1").geom("geom1").feature("c193").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c193")
         .set("pos", new String[]{"scatterer_x_15", "-scatterer_y_15"});
    model.component("comp1").geom("geom1").feature("c193").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c194", "Circle");
    model.component("comp1").geom("geom1").feature("c194").active(false);
    model.component("comp1").geom("geom1").feature("c194").label("scatterer_106");
    model.component("comp1").geom("geom1").feature("c194").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c194")
         .set("pos", new String[]{"scatterer_x_16", "-scatterer_y_16"});
    model.component("comp1").geom("geom1").feature("c194").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c195", "Circle");
    model.component("comp1").geom("geom1").feature("c195").active(false);
    model.component("comp1").geom("geom1").feature("c195").label("scatterer_107");
    model.component("comp1").geom("geom1").feature("c195").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c195")
         .set("pos", new String[]{"scatterer_x_17", "-scatterer_y_17"});
    model.component("comp1").geom("geom1").feature("c195").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c196", "Circle");
    model.component("comp1").geom("geom1").feature("c196").active(false);
    model.component("comp1").geom("geom1").feature("c196").label("scatterer_108");
    model.component("comp1").geom("geom1").feature("c196").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c196")
         .set("pos", new String[]{"scatterer_x_18", "-scatterer_y_18"});
    model.component("comp1").geom("geom1").feature("c196").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c197", "Circle");
    model.component("comp1").geom("geom1").feature("c197").active(false);
    model.component("comp1").geom("geom1").feature("c197").label("scatterer_109");
    model.component("comp1").geom("geom1").feature("c197").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c197")
         .set("pos", new String[]{"scatterer_x_19", "-scatterer_y_19"});
    model.component("comp1").geom("geom1").feature("c197").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c198", "Circle");
    model.component("comp1").geom("geom1").feature("c198").active(false);
    model.component("comp1").geom("geom1").feature("c198").label("scatterer_110");
    model.component("comp1").geom("geom1").feature("c198").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c198")
         .set("pos", new String[]{"scatterer_x_20", "-scatterer_y_20"});
    model.component("comp1").geom("geom1").feature("c198").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c199", "Circle");
    model.component("comp1").geom("geom1").feature("c199").active(false);
    model.component("comp1").geom("geom1").feature("c199").label("scatterer_111");
    model.component("comp1").geom("geom1").feature("c199").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c199")
         .set("pos", new String[]{"scatterer_x_21", "-scatterer_y_21"});
    model.component("comp1").geom("geom1").feature("c199").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c200", "Circle");
    model.component("comp1").geom("geom1").feature("c200").active(false);
    model.component("comp1").geom("geom1").feature("c200").label("scatterer_112");
    model.component("comp1").geom("geom1").feature("c200").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c200")
         .set("pos", new String[]{"scatterer_x_22", "-scatterer_y_22"});
    model.component("comp1").geom("geom1").feature("c200").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c201", "Circle");
    model.component("comp1").geom("geom1").feature("c201").active(false);
    model.component("comp1").geom("geom1").feature("c201").label("scatterer_113");
    model.component("comp1").geom("geom1").feature("c201").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c201")
         .set("pos", new String[]{"scatterer_x_23", "-scatterer_y_23"});
    model.component("comp1").geom("geom1").feature("c201").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c202", "Circle");
    model.component("comp1").geom("geom1").feature("c202").active(false);
    model.component("comp1").geom("geom1").feature("c202").label("scatterer_114");
    model.component("comp1").geom("geom1").feature("c202").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c202")
         .set("pos", new String[]{"scatterer_x_24", "-scatterer_y_24"});
    model.component("comp1").geom("geom1").feature("c202").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c203", "Circle");
    model.component("comp1").geom("geom1").feature("c203").active(false);
    model.component("comp1").geom("geom1").feature("c203").label("scatterer_115");
    model.component("comp1").geom("geom1").feature("c203").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c203")
         .set("pos", new String[]{"scatterer_x_25", "-scatterer_y_25"});
    model.component("comp1").geom("geom1").feature("c203").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c204", "Circle");
    model.component("comp1").geom("geom1").feature("c204").active(false);
    model.component("comp1").geom("geom1").feature("c204").label("scatterer_116");
    model.component("comp1").geom("geom1").feature("c204").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c204")
         .set("pos", new String[]{"scatterer_x_26", "-scatterer_y_26"});
    model.component("comp1").geom("geom1").feature("c204").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c205", "Circle");
    model.component("comp1").geom("geom1").feature("c205").active(false);
    model.component("comp1").geom("geom1").feature("c205").label("scatterer_117");
    model.component("comp1").geom("geom1").feature("c205").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c205")
         .set("pos", new String[]{"scatterer_x_27", "-scatterer_y_27"});
    model.component("comp1").geom("geom1").feature("c205").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c206", "Circle");
    model.component("comp1").geom("geom1").feature("c206").active(false);
    model.component("comp1").geom("geom1").feature("c206").label("scatterer_118");
    model.component("comp1").geom("geom1").feature("c206").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c206")
         .set("pos", new String[]{"scatterer_x_28", "-scatterer_y_28"});
    model.component("comp1").geom("geom1").feature("c206").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c207", "Circle");
    model.component("comp1").geom("geom1").feature("c207").active(false);
    model.component("comp1").geom("geom1").feature("c207").label("scatterer_119");
    model.component("comp1").geom("geom1").feature("c207").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c207")
         .set("pos", new String[]{"scatterer_x_29", "-scatterer_y_29"});
    model.component("comp1").geom("geom1").feature("c207").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c208", "Circle");
    model.component("comp1").geom("geom1").feature("c208").active(false);
    model.component("comp1").geom("geom1").feature("c208").label("scatterer_120");
    model.component("comp1").geom("geom1").feature("c208").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c208")
         .set("pos", new String[]{"scatterer_x_30", "-scatterer_y_30"});
    model.component("comp1").geom("geom1").feature("c208").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c209", "Circle");
    model.component("comp1").geom("geom1").feature("c209").active(false);
    model.component("comp1").geom("geom1").feature("c209").label("scatterer_121");
    model.component("comp1").geom("geom1").feature("c209").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c209").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c209").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c210", "Circle");
    model.component("comp1").geom("geom1").feature("c210").active(false);
    model.component("comp1").geom("geom1").feature("c210").label("scatterer_122");
    model.component("comp1").geom("geom1").feature("c210").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c210").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c210").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c211", "Circle");
    model.component("comp1").geom("geom1").feature("c211").active(false);
    model.component("comp1").geom("geom1").feature("c211").label("scatterer_123");
    model.component("comp1").geom("geom1").feature("c211").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c211").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c211").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c212", "Circle");
    model.component("comp1").geom("geom1").feature("c212").active(false);
    model.component("comp1").geom("geom1").feature("c212").label("scatterer_124");
    model.component("comp1").geom("geom1").feature("c212").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c212").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c212").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c213", "Circle");
    model.component("comp1").geom("geom1").feature("c213").active(false);
    model.component("comp1").geom("geom1").feature("c213").label("scatterer_125");
    model.component("comp1").geom("geom1").feature("c213").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c213").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c213").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c214", "Circle");
    model.component("comp1").geom("geom1").feature("c214").active(false);
    model.component("comp1").geom("geom1").feature("c214").label("scatterer_126");
    model.component("comp1").geom("geom1").feature("c214").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c214").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c214").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c215", "Circle");
    model.component("comp1").geom("geom1").feature("c215").active(false);
    model.component("comp1").geom("geom1").feature("c215").label("scatterer_127");
    model.component("comp1").geom("geom1").feature("c215").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c215").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c215").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c216", "Circle");
    model.component("comp1").geom("geom1").feature("c216").active(false);
    model.component("comp1").geom("geom1").feature("c216").label("scatterer_128");
    model.component("comp1").geom("geom1").feature("c216").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c216").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c216").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c217", "Circle");
    model.component("comp1").geom("geom1").feature("c217").active(false);
    model.component("comp1").geom("geom1").feature("c217").label("scatterer_129");
    model.component("comp1").geom("geom1").feature("c217").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c217").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c217").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c218", "Circle");
    model.component("comp1").geom("geom1").feature("c218").active(false);
    model.component("comp1").geom("geom1").feature("c218").label("scatterer_130");
    model.component("comp1").geom("geom1").feature("c218").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c218").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c218").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c219", "Circle");
    model.component("comp1").geom("geom1").feature("c219").active(false);
    model.component("comp1").geom("geom1").feature("c219").label("scatterer_131");
    model.component("comp1").geom("geom1").feature("c219").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c219").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c219").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c220", "Circle");
    model.component("comp1").geom("geom1").feature("c220").active(false);
    model.component("comp1").geom("geom1").feature("c220").label("scatterer_132");
    model.component("comp1").geom("geom1").feature("c220").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c220").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c220").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c221", "Circle");
    model.component("comp1").geom("geom1").feature("c221").active(false);
    model.component("comp1").geom("geom1").feature("c221").label("scatterer_133");
    model.component("comp1").geom("geom1").feature("c221").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c221").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c221").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c222", "Circle");
    model.component("comp1").geom("geom1").feature("c222").active(false);
    model.component("comp1").geom("geom1").feature("c222").label("scatterer_134");
    model.component("comp1").geom("geom1").feature("c222").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c222").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c222").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c223", "Circle");
    model.component("comp1").geom("geom1").feature("c223").active(false);
    model.component("comp1").geom("geom1").feature("c223").label("scatterer_135");
    model.component("comp1").geom("geom1").feature("c223").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c223").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c223").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c224", "Circle");
    model.component("comp1").geom("geom1").feature("c224").active(false);
    model.component("comp1").geom("geom1").feature("c224").label("scatterer_136");
    model.component("comp1").geom("geom1").feature("c224").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c224").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c224").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c225", "Circle");
    model.component("comp1").geom("geom1").feature("c225").active(false);
    model.component("comp1").geom("geom1").feature("c225").label("scatterer_137");
    model.component("comp1").geom("geom1").feature("c225").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c225").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c225").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c226", "Circle");
    model.component("comp1").geom("geom1").feature("c226").active(false);
    model.component("comp1").geom("geom1").feature("c226").label("scatterer_138");
    model.component("comp1").geom("geom1").feature("c226").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c226").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c226").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c227", "Circle");
    model.component("comp1").geom("geom1").feature("c227").active(false);
    model.component("comp1").geom("geom1").feature("c227").label("scatterer_139");
    model.component("comp1").geom("geom1").feature("c227").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c227").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c227").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").create("c228", "Circle");
    model.component("comp1").geom("geom1").feature("c228").active(false);
    model.component("comp1").geom("geom1").feature("c228").label("scatterer_140");
    model.component("comp1").geom("geom1").feature("c228").set("contributeto", "csel3");
    model.component("comp1").geom("geom1").feature("c228").set("pos", new int[]{0, 0});
    model.component("comp1").geom("geom1").feature("c228").set("r", "radius_scatterer");
    model.component("comp1").geom("geom1").run("fin");
    model.component("comp1").geom("geom1").create("unisel1", "UnionSelection");
    model.component("comp1").geom("geom1").feature("unisel1").label("All_scatterers_union");
    model.component("comp1").geom("geom1").feature("unisel1").set("input", new String[]{"csel2", "csel3"});
    model.component("comp1").geom("geom1").run();

    model.component("comp1").selection().create("ball1", "Ball");
    model.component("comp1").selection("ball1").set("entitydim", 1);
    model.component("comp1").selection().create("ball2", "Ball");
    model.component("comp1").selection().create("ball3", "Ball");
    model.component("comp1").selection().create("com1", "Complement");
    model.component("comp1").selection().create("box1", "Box");
    model.component("comp1").selection().create("sel1", "Explicit");
    model.component("comp1").selection("sel1").set(59);
    model.component("comp1").selection("ball1").label("Ball 1 boundaries");
    model.component("comp1").selection("ball1").set("r", "scatterers_max_distance*0.9999");
    model.component("comp1").selection("ball1").set("condition", "inside");
    model.component("comp1").selection("ball2").label("Ball 2 domains");
    model.component("comp1").selection("ball2").set("r", "scatterers_max_distance*0.9999");
    model.component("comp1").selection("ball2").set("condition", "inside");
    model.component("comp1").selection("ball3").label("Ball 3 domains out");
    model.component("comp1").selection("ball3").set("r", "scatterers_max_distance*1.0001");
    model.component("comp1").selection("ball3").set("condition", "inside");
    model.component("comp1").selection("com1").set("input", new String[]{"ball2"});
    model.component("comp1").selection("box1").set("xmin", -0.06);
    model.component("comp1").selection("box1").set("xmax", -0.055);
    model.component("comp1").selection("box1").set("ymin", -0.06);
    model.component("comp1").selection("box1").set("ymax", 0.06);
    model.component("comp1").selection("sel1").label("scatterers1");

    model.component("comp1").variable().create("var4");
    model.component("comp1").variable("var4").set("eraser", "1");
    model.component("comp1").variable("var4").selection().named("com1");
    model.component("comp1").variable().create("var2");
    model.component("comp1").variable("var2").set("geomFinder", "1");
    model.component("comp1").variable("var2").selection().named("ball2");
    model.component("comp1").variable().create("var3");
    model.component("comp1").variable("var3").set("geomFinder", "0", "Finds the");
    model.component("comp1").variable("var3").selection().named("com1");
    model.component("comp1").variable().create("var5");
    model.component("comp1").variable("var5").set("blanking_limit2", "0.006");
    model.component("comp1").variable("var5").set("blanking_limit1", "0.06-2.5*lam");
    model.component("comp1").variable().create("var6");
    model.component("comp1").variable("var6").set("geomFinderNoCentre", "0", "Finds the");
    model.component("comp1").variable("var6").selection().geom("geom1", 2);
    model.component("comp1").variable("var6").selection().set(1, 2, 3, 4, 5, 6, 59, 68, 129, 130, 131);
    model.component("comp1").variable().create("var7");
    model.component("comp1").variable("var7").set("geomFinderNoCentre", "1");
    model.component("comp1").variable("var7").selection().geom("geom1", 2);
    model.component("comp1").variable("var7").selection()
         .set(7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 60, 61, 62, 63, 64, 65, 66, 67, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128);

    model.component("comp1").view("view1").hideObjects().create("hide1");
    model.component("comp1").view("view1").hideObjects().create("hide2");
    model.component("comp1").view().create("view2", "geom1");
    model.component("comp1").view("view2").hideObjects().create("hide1");
    model.component("comp1").view().create("view3", "geom1");
    model.view().create("view4", 2);

    model.component("comp1").coordSystem().create("pml1", "PML");
    model.component("comp1").coordSystem("pml1").selection().set(1, 2, 3, 4, 6, 129, 130, 131);

    model.component("comp1").physics().create("acpr", "PressureAcoustics", "geom1");
    model.component("comp1").physics("acpr").create("ishb1", "InteriorSoundHard", 1);
    model.component("comp1").physics("acpr").feature("ishb1").selection().named("ball1");
    model.component("comp1").physics("acpr").create("bpf1", "BackgroundPressureField", 2);
    model.component("comp1").physics("acpr").feature("bpf1").selection().set(5, 59);
    model.component("comp1").physics("acpr").create("efc1", "ExteriorFieldCalculation", 1);

    model.component("comp1").mesh("mesh1").create("ftri2", "FreeTri");
    model.component("comp1").mesh("mesh1").create("ftri3", "FreeTri");
    model.component("comp1").mesh("mesh1").create("map1", "Map");
    model.component("comp1").mesh("mesh1").feature("ftri2").selection().named("geom1_csel2_dom");
    model.component("comp1").mesh("mesh1").feature("ftri2").create("size1", "Size");
    model.component("comp1").mesh("mesh1").feature("ftri3").selection().geom("geom1", 2);
    model.component("comp1").mesh("mesh1").feature("ftri3").selection().set(5, 59);
    model.component("comp1").mesh("mesh1").feature("ftri3").create("size1", "Size");
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").selection().set(5);
    model.component("comp1").mesh("mesh1").feature("map1").selection().geom("geom1", 2);
    model.component("comp1").mesh("mesh1").feature("map1").selection().set(1, 2, 3, 4, 6, 129, 130, 131);
    model.component("comp1").mesh("mesh1").feature("map1").create("dis1", "Distribution");
    model.component("comp1").mesh("mesh1").feature("map1").feature("dis1").selection().set(4, 12, 503, 508);

    model.result().table("tbl1").label("Point matrix");
    model.result().table("tbl1").comments("Line Integration 1 ()");
    model.result().table("tbl7").comments("Line Integration 1 (abs(acpr.p_s))");
    model.result().table("tbl8").label("Sanchis metric table");
    model.result().table("tbl8").comments("SanchisMetric (abs(acpr.p_s)^2/fitness_for_squared_metrics)");
    model.result().table("tbl9").comments("CircleIntegratedValue (abs(acpr.p_s)/fitness_w_o_cloak)");
    model.result().table("tbl10").label("Le2018 table");
    model.result().table("tbl10").comments("Le2018Metric (1/(1+(abs(acpr.p_s)/fitness_object_cloak)))");
    model.result().table("evl2").label("Evaluation 2D");
    model.result().table("evl2").comments("Interactive 2D values");

    model.capeopen().label("Thermodynamics Package");

    model.component("comp1").variable("var4").label("Variables 1");
    model.component("comp1").variable("var5").label("graph_hider_vars");

    model.component("comp1").view("view1").axis().set("xmin", -0.16986723244190216);
    model.component("comp1").view("view1").axis().set("xmax", 0.16986729204654694);
    model.component("comp1").view("view1").axis().set("ymin", -0.16986726224422455);
    model.component("comp1").view("view1").axis().set("ymax", 0.16986726224422455);
    model.component("comp1").view("view1").hideObjects("hide1").init(1);
    model.component("comp1").view("view1").hideObjects("hide2").set();
    model.component("comp1").view("view2").set("locked", true);
    model.component("comp1").view("view2").axis().set("xmin", -0.08);
    model.component("comp1").view("view2").axis().set("xmax", 0.08);
    model.component("comp1").view("view2").axis().set("ymin", -0.08);
    model.component("comp1").view("view2").axis().set("ymax", 0.08);
    model.component("comp1").view("view2").hideObjects("hide1").init(1);
    model.component("comp1").view("view3").axis().set("xmin", -0.16499991714954376);
    model.component("comp1").view("view3").axis().set("xmax", 0.16499997675418854);
    model.component("comp1").view("view3").axis().set("ymin", -0.16518555581569672);
    model.component("comp1").view("view3").axis().set("ymax", 0.16518555581569672);
    model.view("view4").label("One_quarter_view");
    model.view("view4").set("showunits", false);
    model.view("view4").set("locked", true);
    model.view("view4").axis().label("One_quarter_axis");
    model.view("view4").axis().set("xmin", 0);
    model.view("view4").axis().set("xmax", 0.045);
    model.view("view4").axis().set("ymin", 0);
    model.view("view4").axis().set("ymax", 0.045);

    model.component("comp1").physics("acpr").feature("fpam1").set("rho", "rhoA");
    model.component("comp1").physics("acpr").feature("fpam1").set("c", "c0");
    model.component("comp1").physics("acpr").feature("bpf1").set("pamp", 1);
    model.component("comp1").physics("acpr").feature("bpf1").set("c", "c0");

    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("hauto", 1);
    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("custom", "on");
    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("hmax", 0.001);
    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("hmaxactive", true);
    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("hmin", 2.24E-5);
    model.component("comp1").mesh("mesh1").feature("ftri2").feature("size1").set("hminactive", false);
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("hauto", 1);
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("custom", "on");
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("hmax", 0.0015);
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("hmaxactive", true);
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("hmin", 2.24E-5);
    model.component("comp1").mesh("mesh1").feature("ftri3").feature("size1").set("hminactive", false);
    model.component("comp1").mesh("mesh1").feature("map1").feature("dis1").set("numelem", 8);
    model.component("comp1").mesh("mesh1").run();

    model.component("comp1").physics("acpr").feature("fpam1").set("rho_mat", "userdef");
    model.component("comp1").physics("acpr").feature("fpam1").set("c_mat", "userdef");

    model.study().create("std1");
    model.study("std1").create("freq", "Frequency");
    model.study("std1").feature("freq").set("activate", new String[]{"acpr", "on"});

    model.sol().create("sol1");
    model.sol("sol1").study("std1");
    model.sol("sol1").attach("std1");
    model.sol("sol1").create("st1", "StudyStep");
    model.sol("sol1").create("v1", "Variables");
    model.sol("sol1").create("s1", "Stationary");
    model.sol("sol1").feature("s1").create("pDef", "Parametric");
    model.sol("sol1").feature("s1").create("fc1", "FullyCoupled");
    model.sol("sol1").feature("s1").feature().remove("fcDef");

    model.batch().create("s1", "Sequence");
    model.batch("s1").create("so1", "Solutionseq");
    model.batch("s1").create("ex1", "Exportseq");
    model.batch("s1").study("std1");

    model.result().dataset().create("cln1", "CutLine2D");
    model.result().dataset().create("pc1", "ParCurve2D");
    model.result().dataset().create("cpt1", "CutPoint2D");
    model.result().numerical().create("int1", "IntLine");
    model.result().numerical().create("int2", "IntLine");
    model.result().numerical().create("int3", "IntLine");
    model.result().numerical().create("int4", "IntLine");
    model.result().numerical().create("pev1", "EvalPoint");
    model.result().numerical("int1").set("probetag", "none");
    model.result().numerical("int2").set("probetag", "none");
    model.result().numerical("int3").set("probetag", "none");
    model.result().numerical("int4").set("probetag", "none");
    model.result().numerical("pev1").set("probetag", "none");
    model.result().create("pg1", "PlotGroup2D");
    model.result().create("pg2", "PlotGroup2D");
    model.result().create("pg3", "PlotGroup2D");
    model.result().create("pg4", "PlotGroup2D");
    model.result().create("pg5", "PlotGroup2D");
    model.result().create("pg6", "PlotGroup2D");
    model.result("pg1").create("surf1", "Surface");
    model.result("pg2").create("surf1", "Surface");
    model.result("pg3").create("surf1", "Surface");
    model.result("pg4").create("surf1", "Surface");
    model.result("pg5").create("surf1", "Surface");
    model.result("pg5").feature("surf1").set("data", "dset1");
    model.result("pg6").create("surf1", "Surface");
    model.result().export().create("img1", "Image2D");
    model.result().export().create("img3", "Image2D");
    model.result().export().create("img2", "Image2D");
    model.result().export().create("img4", "Image2D");
    model.result().export().create("data1", "Data");

    return model;
  }

  public static Model run6(Model model) {
    model.result().export().create("img5", "Image2D");
    model.result().export().create("img6", "Image2D");
    model.result().export().create("data2", "Data");

    model.study("std1").feature("freq").set("plist", "freq");
    model.study("std1").feature("freq").set("discretization", new String[]{"acpr", "physics"});

    model.sol("sol1").attach("std1");
    model.sol("sol1").feature("v1").set("clistctrl", new String[]{"pDef"});
    model.sol("sol1").feature("v1").set("cname", new String[]{"freq"});
    model.sol("sol1").feature("v1").set("clist", new String[]{"freq"});
    model.sol("sol1").feature("s1").feature("aDef").set("complexfun", true);
    model.sol("sol1").feature("s1").feature("pDef").set("pname", new String[]{"freq"});
    model.sol("sol1").feature("s1").feature("pDef").set("plistarr", new String[]{"freq"});
    model.sol("sol1").feature("s1").feature("pDef").set("punit", new String[]{"Hz"});
    model.sol("sol1").feature("s1").feature("pDef").set("pcontinuationmode", "no");
    model.sol("sol1").feature("s1").feature("pDef").set("preusesol", "auto");
    model.sol("sol1").feature("s1").feature("pDef").set("uselsqdata", false);
    model.sol("sol1").runAll();

    model.batch("s1").label("solve_and_export_sequence");
    model.batch("s1").feature("so1").label("run_main_solution_sequence");
    model.batch("s1").feature("so1").set("seq", "sol1");

    model.result().dataset("cln1")
         .set("genpoints", new String[][]{{"far_field_r", "-0.08"}, {"far_field_r", "0.08"}});
    model.result().dataset("pc1").set("par1", "thet");
    model.result().dataset("pc1").set("parmax1", "2*pi");
    model.result().dataset("pc1").set("exprx", "far_field_r * cos(thet)");
    model.result().dataset("pc1").set("expry", "far_field_r * sin(thet)");
    model.result().dataset("cpt1").label("360_points");
    model.result().dataset("cpt1").set("pointx", "far_field_r * cos(range(0, 2*pi/359, 2*pi))");
    model.result().dataset("cpt1").set("pointy", "far_field_r * sin(range(0, 2*pi/359, 2*pi))");
    model.result().numerical("int1").label("LineIntegratedValue");
    model.result().numerical("int1").set("data", "cln1");
    model.result().numerical("int1").set("table", "tbl7");
    model.result().numerical("int1").set("expr", new String[]{"abs(acpr.p_s)/fitness_w_o_cloak"});
    model.result().numerical("int1").set("unit", new String[]{"N/m"});
    model.result().numerical("int1").set("descr", new String[]{""});
    model.result().numerical("int2").label("CircleIntegratedValue");
    model.result().numerical("int2").set("data", "pc1");
    model.result().numerical("int2").set("table", "tbl9");
    model.result().numerical("int2").set("expr", new String[]{"abs(acpr.p_s)/fitness_object_cloak"});
    model.result().numerical("int2").set("unit", new String[]{"N/m"});
    model.result().numerical("int2").set("descr", new String[]{""});
    model.result().numerical("int3").label("SanchisMetric");
    model.result().numerical("int3").set("data", "pc1");
    model.result().numerical("int3").set("table", "tbl8");
    model.result().numerical("int3").set("expr", new String[]{"abs(acpr.p_s)^2/fitness_for_squared_metrics"});
    model.result().numerical("int3").set("unit", new String[]{"kg^2/(m*s^4)"});
    model.result().numerical("int3").set("descr", new String[]{""});
    model.result().numerical("int4").label("Le2018Metric");
    model.result().numerical("int4").set("data", "pc1");
    model.result().numerical("int4").set("table", "tbl10");
    model.result().numerical("int4").set("expr", new String[]{"1/(1+(abs(acpr.p_s)/fitness_object_cloak))"});
    model.result().numerical("int4").set("unit", new String[]{"m^2*s^2/kg"});
    model.result().numerical("int4").set("descr", new String[]{""});
    model.result().numerical("pev1").label("scattered_pressure_of_360_points");
    model.result().numerical("pev1").set("data", "cpt1");
    model.result().numerical("pev1").set("table", "tbl1");
    model.result().numerical("pev1").set("expr", new String[]{"abs(acpr.p_s)"});
    model.result().numerical("pev1").set("unit", new String[]{"Pa"});
    model.result().numerical("pev1").set("descr", new String[]{""});
    model.result().numerical("int1").setResult();
    model.result().numerical("int2").setResult();
    model.result().numerical("int3").setResult();
    model.result().numerical("int4").setResult();
    model.result().numerical("pev1").setResult();
    model.result("pg1").label("Absolute_Scattered_Pressure");
    model.result("pg1").feature("surf1").set("expr", "abs(acpr.p_s)*eraser");
    model.result("pg1").feature("surf1").set("descr", "abs(acpr.p_s)*eraser");
    model.result("pg1").feature("surf1").set("rangecoloractive", true);
    model.result("pg1").feature("surf1").set("rangecolormax", 2);
    model.result("pg1").feature("surf1").set("resolution", "normal");
    model.result("pg2").label("Real_Scattered_Pressure");
    model.result("pg2").feature("surf1").set("expr", "real(acpr.p_s)*eraser");
    model.result("pg2").feature("surf1").set("descr", "real(acpr.p_s)*eraser");
    model.result("pg2").feature("surf1").set("resolution", "normal");
    model.result("pg3").label("Real_Total_Pressure");
    model.result("pg3").feature("surf1").set("expr", "(acpr.p_t)*eraser");
    model.result("pg3").feature("surf1").set("descr", "(acpr.p_t)*eraser");
    model.result("pg3").feature("surf1").set("resolution", "normal");
    model.result("pg4").label("Abs_Total_Pressure");
    model.result("pg4").feature("surf1").set("expr", "abs(acpr.p_t)*eraser");
    model.result("pg4").feature("surf1").set("descr", "abs(acpr.p_t)*eraser");
    model.result("pg4").feature("surf1").set("resolution", "normal");
    model.result("pg5").feature("surf1").set("expr", "if((-0.075 <= x && x<=0.075), -3, acpr.p_t)");
    model.result("pg5").feature("surf1").set("descr", "if((-0.075 <= x && x<=0.075), -3, acpr.p_t)");
    model.result("pg5").feature("surf1").set("rangecoloractive", true);
    model.result("pg5").feature("surf1").set("rangecolormin", -3);
    model.result("pg5").feature("surf1").set("rangecolormax", 3);
    model.result("pg5").feature("surf1").set("rangedataactive", true);
    model.result("pg5").feature("surf1").set("rangedatamin", -3);
    model.result("pg5").feature("surf1").set("rangedatamax", 3);
    model.result("pg5").feature("surf1").set("colortable", "Thermal");
    model.result("pg5").feature("surf1").set("smooth", "none");
    model.result("pg5").feature("surf1").set("resolution", "normal");
    model.result("pg6").label("Zoomed_in_geometry");
    model.result("pg6").set("view", "view4");
    model.result("pg6").set("edges", false);
    model.result("pg6").set("showlegends", false);
    model.result("pg6").feature("surf1").set("expr", "geomFinderNoCentre");
    model.result("pg6").feature("surf1").set("unit", "");
    model.result("pg6").feature("surf1").set("descr", "geomFinderNoCentre");
    model.result("pg6").feature("surf1").set("rangecoloractive", true);
    model.result("pg6").feature("surf1").set("rangecolormax", 1);
    model.result("pg6").feature("surf1").set("colortable", "Thermal");
    model.result("pg6").feature("surf1").set("resolution", "normal");
    model.result().export("img1").label("AbsScatPres_1");
    model.result().export("img1")
         .set("pngfilename", "D:\\Acoustic Cloaking - Raghu\\cloaking_ga_sa_working\\Images_Save_dataAbsScatPres_1.png");
    model.result().export("img1").set("options", false);
    model.result().export("img1").set("printunit", "mm");
    model.result().export("img1").set("webunit", "px");
    model.result().export("img1").set("printheight", "90");
    model.result().export("img1").set("webheight", "1200");
    model.result().export("img1").set("printwidth", "120");
    model.result().export("img1").set("webwidth", "1200");
    model.result().export("img1").set("printlockratio", "off");
    model.result().export("img1").set("weblockratio", "off");
    model.result().export("img1").set("printresolution", "300");
    model.result().export("img1").set("webresolution", "96");
    model.result().export("img1").set("size", "manualweb");
    model.result().export("img1").set("antialias", "off");
    model.result().export("img1").set("zoomextents", "off");
    model.result().export("img1").set("title", "on");
    model.result().export("img1").set("legend", "off");
    model.result().export("img1").set("logo", "off");
    model.result().export("img1").set("options", "off");
    model.result().export("img1").set("fontsize", "9");
    model.result().export("img1").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img1").set("background", "color");
    model.result().export("img1").set("axes", "on");
    model.result().export("img1").set("qualitylevel", "92");
    model.result().export("img1").set("qualityactive", "off");
    model.result().export("img1").set("imagetype", "png");
    model.result().export("img3").label("RealTotPres_1");
    model.result().export("img3").set("plotgroup", "pg3");
    model.result().export("img3")
         .set("pngfilename", "D:\\Acoustic Cloaking - Raghu\\cloaking_ga_sa_working\\Images_Save_dataRealTotPres_1.png");
    model.result().export("img3").set("options", false);
    model.result().export("img3").set("printunit", "mm");
    model.result().export("img3").set("webunit", "px");
    model.result().export("img3").set("printheight", "90");
    model.result().export("img3").set("webheight", "1200");
    model.result().export("img3").set("printwidth", "120");
    model.result().export("img3").set("webwidth", "1200");
    model.result().export("img3").set("printlockratio", "off");
    model.result().export("img3").set("weblockratio", "off");
    model.result().export("img3").set("printresolution", "300");
    model.result().export("img3").set("webresolution", "96");
    model.result().export("img3").set("size", "manualweb");
    model.result().export("img3").set("antialias", "off");
    model.result().export("img3").set("zoomextents", "off");
    model.result().export("img3").set("title", "on");
    model.result().export("img3").set("legend", "off");
    model.result().export("img3").set("logo", "off");
    model.result().export("img3").set("options", "off");
    model.result().export("img3").set("fontsize", "9");
    model.result().export("img3").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img3").set("background", "color");
    model.result().export("img3").set("axes", "on");
    model.result().export("img3").set("qualitylevel", "92");
    model.result().export("img3").set("qualityactive", "off");
    model.result().export("img3").set("imagetype", "png");
    model.result().export("img2").label("RealScatPres_1");
    model.result().export("img2").set("plotgroup", "pg2");
    model.result().export("img2")
         .set("pngfilename", "D:\\Acoustic Cloaking - Raghu\\cloaking_ga_sa_working\\Images_Save_dataRealScatPres_1.png");
    model.result().export("img2").set("options", false);
    model.result().export("img2").set("printunit", "mm");
    model.result().export("img2").set("webunit", "px");
    model.result().export("img2").set("printheight", "90");
    model.result().export("img2").set("webheight", "1200");
    model.result().export("img2").set("printwidth", "120");
    model.result().export("img2").set("webwidth", "1200");
    model.result().export("img2").set("printlockratio", "off");
    model.result().export("img2").set("weblockratio", "off");
    model.result().export("img2").set("printresolution", "300");
    model.result().export("img2").set("webresolution", "96");
    model.result().export("img2").set("size", "manualweb");
    model.result().export("img2").set("antialias", "off");
    model.result().export("img2").set("zoomextents", "off");
    model.result().export("img2").set("title", "on");
    model.result().export("img2").set("legend", "off");
    model.result().export("img2").set("logo", "off");
    model.result().export("img2").set("options", "off");
    model.result().export("img2").set("fontsize", "9");
    model.result().export("img2").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img2").set("background", "color");
    model.result().export("img2").set("axes", "on");
    model.result().export("img2").set("qualitylevel", "92");
    model.result().export("img2").set("qualityactive", "off");
    model.result().export("img2").set("imagetype", "png");
    model.result().export("img4").label("AbsTotalPres_1");
    model.result().export("img4").set("plotgroup", "pg4");
    model.result().export("img4")
         .set("pngfilename", "D:\\Acoustic Cloaking - Raghu\\cloaking_ga_sa_working\\Images_Save_dataAbsTotalPres_1.png");
    model.result().export("img4").set("options", false);
    model.result().export("img4").set("printunit", "mm");
    model.result().export("img4").set("webunit", "px");
    model.result().export("img4").set("printheight", "90");
    model.result().export("img4").set("webheight", "1200");
    model.result().export("img4").set("printwidth", "120");
    model.result().export("img4").set("webwidth", "1200");
    model.result().export("img4").set("printlockratio", "off");
    model.result().export("img4").set("weblockratio", "off");
    model.result().export("img4").set("printresolution", "300");
    model.result().export("img4").set("webresolution", "96");
    model.result().export("img4").set("size", "manualweb");
    model.result().export("img4").set("antialias", "off");
    model.result().export("img4").set("zoomextents", "off");
    model.result().export("img4").set("title", "on");
    model.result().export("img4").set("legend", "off");
    model.result().export("img4").set("logo", "off");
    model.result().export("img4").set("options", "off");
    model.result().export("img4").set("fontsize", "9");
    model.result().export("img4").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img4").set("background", "color");
    model.result().export("img4").set("axes", "on");
    model.result().export("img4").set("qualitylevel", "92");
    model.result().export("img4").set("qualityactive", "off");
    model.result().export("img4").set("imagetype", "png");
    model.result().export("data1").label("AbTotalPres_Blanked");
    model.result().export("data1").set("expr", new String[]{"if((-0.075 <= x && x<=0.075), 0, acpr.p_t+1.5)"});
    model.result().export("data1").set("unit", new String[]{"Pa"});
    model.result().export("data1").set("descr", new String[]{""});
    model.result().export("img5").label("real_total_blanked");
    model.result().export("img5").set("plotgroup", "pg5");
    model.result().export("img5").set("size", "current");
    model.result().export("img5").set("antialias", true);
    model.result().export("img5").set("options", false);
    model.result().export("img5").set("printunit", "mm");
    model.result().export("img5").set("webunit", "px");
    model.result().export("img5").set("printheight", "90");
    model.result().export("img5").set("webheight", "1200");
    model.result().export("img5").set("printwidth", "120");
    model.result().export("img5").set("webwidth", "1200");
    model.result().export("img5").set("printlockratio", "off");
    model.result().export("img5").set("weblockratio", "off");
    model.result().export("img5").set("printresolution", "300");
    model.result().export("img5").set("webresolution", "96");
    model.result().export("img5").set("size", "current");
    model.result().export("img5").set("antialias", "on");
    model.result().export("img5").set("zoomextents", "off");
    model.result().export("img5").set("title", "on");
    model.result().export("img5").set("legend", "off");
    model.result().export("img5").set("logo", "off");
    model.result().export("img5").set("options", "off");
    model.result().export("img5").set("fontsize", "9");
    model.result().export("img5").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img5").set("background", "color");
    model.result().export("img5").set("axes", "on");
    model.result().export("img5").set("qualitylevel", "92");
    model.result().export("img5").set("qualityactive", "off");
    model.result().export("img5").set("imagetype", "png");
    model.result().export("img6").label("geom_quarter_pixel_per_square");
    model.result().export("img6").set("plotgroup", "pg6");
    model.result().export("img6").set("webwidth", 354);
    model.result().export("img6").set("webheight", 354);
    model.result().export("img6").set("webresolution", 100);
    model.result().export("img6").set("sizedesc", "90 x 90 mm");
    model.result().export("img6").set("antialias", true);
    model.result().export("img6")
         .set("pngfilename", "C:\\Users\\oliver\\Desktop\\acoustics\\geom_tests\\geom_118_pixels-100DPI.png");
    model.result().export("img6").set("options", false);
    model.result().export("img6").set("printunit", "mm");
    model.result().export("img6").set("webunit", "px");
    model.result().export("img6").set("printheight", "90");
    model.result().export("img6").set("webheight", "354");
    model.result().export("img6").set("printwidth", "120");
    model.result().export("img6").set("webwidth", "354");
    model.result().export("img6").set("printlockratio", "off");
    model.result().export("img6").set("weblockratio", "off");
    model.result().export("img6").set("printresolution", "300");
    model.result().export("img6").set("webresolution", "100");
    model.result().export("img6").set("size", "manualweb");
    model.result().export("img6").set("antialias", "on");
    model.result().export("img6").set("zoomextents", "off");
    model.result().export("img6").set("title", "on");
    model.result().export("img6").set("legend", "off");
    model.result().export("img6").set("logo", "off");
    model.result().export("img6").set("options", "off");
    model.result().export("img6").set("fontsize", "9");
    model.result().export("img6").set("customcolor", new double[]{1, 1, 1});
    model.result().export("img6").set("background", "color");
    model.result().export("img6").set("axes", "on");
    model.result().export("img6").set("qualitylevel", "92");
    model.result().export("img6").set("qualityactive", "off");
    model.result().export("img6").set("imagetype", "png");
    model.result().export("data2").label("Geometry_text_export");
    model.result().export("data2").set("expr", new String[]{"geomFinder"});
    model.result().export("data2").set("unit", new String[]{""});
    model.result().export("data2").set("descr", new String[]{""});
    model.result().export("data2")
         .set("filename", "C:\\Users\\oliver\\Desktop\\acoustics\\geom_tests\\exported_geometry_text.txt");
    model.result().export("data2").set("location", "regulargrid");
    model.result().export("data2").set("gridstruct", "grid");
    model.result().export("data2").set("regulargridx2", 400);
    model.result().export("data2").set("regulargridy2", 400);
    model.result().export("data2").set("fullprec", false);

    return model;
  }

  public static void main(String[] args) {
    Model model = run();
    model = run2(model);
    model = run3(model);
    model = run4(model);
    model = run5(model);
    run6(model);
  }

}
