package com.comsolModelInterface;

public class ComsolParameters implements java.io.Serializable {
    double scattererRadius = 0.0006;
    double absoluteMinValue = 0.008575+scattererRadius;
    double suggestedMaxValue = 0.042875 + scattererRadius;
    double absoluteMaxValue = 0.042875;

    public ComsolParameters(double scattererRadius, double radius_central_pipe, double maxScattererDistance) {
        double constantRadiusMultiplier = 0.6; // multiplier for how far the scatterers can be from the extremes. Minimum of 0.5, preferably 0.6
        this.scattererRadius = scattererRadius;
        this.absoluteMinValue = radius_central_pipe + (constantRadiusMultiplier*scattererRadius);
        this.suggestedMaxValue = maxScattererDistance - (+constantRadiusMultiplier*scattererRadius);
        this.absoluteMaxValue = maxScattererDistance;
    }

    public double getScattererRadius() {
        return scattererRadius;
    }

    public double getAbsoluteMinValue() {
        return absoluteMinValue;
    }

    public double getSuggestedMaxValue() {
        return suggestedMaxValue;
    }

    public double getAbsoluteMaxValue() {
        return absoluteMaxValue;
    }
}
