package com.comsolModelInterface;

import com.comsol.model.BatchFeature;
import com.comsol.model.Model;
import com.Variables;
import com.ga.nsaga.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A "fake" comsol model, which allows us to use
 */
public class EmptyModel implements ComsolModelInterface {

	Random rand = new Random();

	@Override
	public Model generateFreshModel() {
		return null;
	}

	@Override
	public void updateParams(String[] inputParamNames, String[] paramValues, boolean displayDebug) {

	}

	@Override
	public double runModel(int combinationNumber) throws Exception {
		return rand.nextDouble();
	}

	@Override
	public String getDoubleParameter(String s) {
		double c0 = 343;
		double rhoA = 1.2041;
		double freq = 20000;
		double lam = c0/freq;
		switch (s) {
			case "c0":
				return Double.toString(343); // ("c0", "343[m/s]");
			case "rhoA":
				return Double.toString(1.2041); // ("rhoA", "1.2041[kg/m^3]");
			case "freq":
				return Double.toString(20000); // ("freq", "20000[1/s]");
			case "lam":
				return Double.toString(lam); // ("lam", "c0/freq");
			case "kk":
				return Double.toString(2*Math.PI/lam); // ("kk", "2*pi/lam");
			case "radius_central_pipe":
				return Double.toString(lam * Double.parseDouble(Variables.getCentralObjectRadisMultiplier())); // ("radius_scatterer", "lam/15");
			case "scatterers_max_distance":
				return Double.toString(2.52*lam); // ("scatterers_max_distance", "2.52*lam");
			case "far_field_r":
				return Double.toString(3*2.52*lam); // ("far_field_r", "3*scatterers_max_distance");
			case "radius_scatterer":
				return Double.toString(lam/15); //("radius_scatterer", "lam/15");
			case "fitness_for_squared_metrics":
				return "0.048905";
			default:
				return null;
		}
	}

	@Override
	public void saveImagesInt(String filename, int iteration, Integer[] combination, String startDate, double[][] model360Points) {

	}

	@Override
	public void saveImagesDouble(String filename, int iteration, Double[] combination, String startDate, double[][] model360Points) {

	}

	@Override
	public void deleteAllScatterers() {

	}

	@Override
	public void addNewScatterer(int circleNumber, double xPosition, double yPosition) {

	}

	@Override
	public List<BatchFeature> getBatchJobs() {
		return null;
	}

	@Override
	public void setRawDataGridSize(int size, Model model) {

	}

	@Override
	public ArrayList<String> getAllSquareScatterers() {
		return null;
	}

	@Override
	public ArrayList<String> getAllCircularScatterers() {
		return null;
	}

	@Override
	public double getResult() {
		return rand.nextDouble();
	}

	@Override
	public void setAllSquareScatterers(Integer[] testedIndividual, Position[] scattererMap) {

	}

	@Override
	public void setAllCircularScatterers(Integer[] testedIndividual, Position[] scattererMap) {

	}

	@Override
	public void setAllSquareScatterersDouble(Double[] testedIndividual, Position[] scattererMap) {

	}

	@Override
	public void setAllCircularScatterersDouble(Double[] testedIndividual, Position[] scattererMap) {

	}


	@Override
	public void switchToCircularScatterers() {

	}

	@Override
	public void switchToSquareScatterers() {

	}

	@Override
	public void switchToSquareCentralObject() {

	}

	@Override
	public void switchToCircleCentralObject() {

	}

	@Override
	public double[][] getModels360Points() {
		double[][] x = new double[1][360];
		int i;
		for (i=0; i<360;i++) {
			x[0][i] = rand.nextDouble();
		}
		return x;
	}

    @Override
    public void runBlankModel() throws Exception {

    }

	@Override
	public void saveImagesOneOff(String filename, int iteration, Integer[] combination, double[][] model360Points) {

	}

	@Override
	public void saveModel(String filePath) {

	}

	@Override
	public void setFitnessWithoutCloak(double fitnessWithoutCloak) {

	}

	@Override
	public void setFitnessForSquareMetrics(double fitnessForSquareMetrics) {

	}


	@Override
	public void setCoreOnOff(boolean setCoreOn){

	}

}
