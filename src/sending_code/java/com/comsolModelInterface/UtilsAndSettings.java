package com.comsolModelInterface;
/**
 * GNU Terry Pratchett
 */

import ch.qos.logback.classic.Logger;
import com.Variables;
import com.comsol.model.Model;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class UtilsAndSettings {

	static final Logger logger = (Logger) LoggerFactory.getLogger(UtilsAndSettings.class.getName());
	public static final int MAX_GENERATIONS = 200;
	public static final int SIGNIFICANT_FIGURES_USED = 5;
	public static final int MAX_SCATTERERS_PER_LAYER = 60;
	public static final double MAX_SCATTER_DISTANCE_MULTIPLIER = 2.5;

	public void showResultsTableDialogWithBest(Object[][] printTable, int bestIndex) {
		int height = 1000;
		int width = 1400;
		JTable table = new JTable(Arrays.copyOfRange(printTable, 1, printTable.length), printTable[0]);
		JTable bestResult = new JTable(Arrays.copyOfRange(printTable, bestIndex, bestIndex + 1), printTable[0]);
		JButton button = new JButton("Export Results");
		button.addActionListener(e -> saveResults(table));
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(button, BorderLayout.PAGE_START);
		JScrollPane mainTableScrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		mainTableScrollPane.setPreferredSize(new Dimension(width,height));
		panel.add(mainTableScrollPane, BorderLayout.CENTER);
		logger.debug("Initial results table set up");
		// construct result panel to be used separately - shows best result table and title for it
		JScrollPane bestResultScrollPane = new JScrollPane(bestResult, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		Dimension d = table.getPreferredSize();
		bestResultScrollPane.setPreferredSize(new Dimension(d.width, bestResult.getRowHeight() * 4 + 1));
		JPanel resultPanel = new JPanel();
		resultPanel.setLayout(new BorderLayout());
		resultPanel.add(new JLabel("Best result:"), BorderLayout.PAGE_START);
		resultPanel.add(bestResultScrollPane, BorderLayout.CENTER);
		panel.add(resultPanel, BorderLayout.PAGE_END);
		logger.debug("Full results table set up");
		final JOptionPane optionPane = new JOptionPane(panel,
				JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
		final JDialog dialog = new JDialog();
		dialog.setTitle("Results");
		dialog.setModal(true);
		dialog.setContentPane(optionPane);
		dialog.pack();
		dialog.setPreferredSize(new Dimension(width, height));
		dialog.pack();
		logger.debug("Showing results window");
		dialog.setVisible(true);
	}

	public void saveResults(JTable table) {
		try {
			JFileChooser fileSaver = new JFileChooser();
			toExcel(table, fileSaver.getSelectedFile());
			logger.info("Attempted to save the results table at " + fileSaver.getSelectedFile().getAbsolutePath() + ".csv");
		} catch (Exception e) {
			logger.error(e.getClass().getName() + " - " + e.getMessage());
		}
	}

	private void toExcel(JTable table, File file) {
		FileWriter excel = null;
		try {
			TableModel model = table.getModel();
			excel = new FileWriter(file + ".csv");
			for (int i = 0; i < model.getColumnCount(); i++) {
				excel.write(model.getColumnName(i) + ", ");
			}
			excel.write("\n");
			int j;
			for (int i = 0; i < model.getRowCount(); i++) {
				for (j = 0; j < model.getColumnCount()-1; j++) {
					try {
						excel.write(model.getValueAt(i, j).toString() + ", ");
					} catch (Exception e) {
						logger.trace("Caught an exception {} while writing to excel document ", e.getMessage());
					}
				}
				try {
					excel.write(model.getValueAt(i, j+1).toString());
				} catch (Exception e) {
					logger.trace("Caught an exception {} while writing to excel document ", e.getMessage());
				}
				excel.write("\n");
			}

			excel.close();

		} catch (IOException e) {
			logger.error("While converting to Excel: {}", e.getMessage());
		} finally {
			if (excel != null) {
				try {
					excel.close();
				} catch (IOException e) {
					logger.error("Error while closing the excel conversion");
					logger.error("CONTEXT", e);
				}
			}
		}
	}

	public void saveResultsAsFile(JTable table, String filePath) {
		try {
			String ext = "csv";
			//		fileSaver.getSelectedFile()
			toExcel(table, new File(filePath));
			logger.info("Attempted to save the results table at {}{}", filePath, ext);
		} catch (Exception e) {
			logger.error(e.getClass().getName() + " - " + e.getMessage());
		}
	}

	public String getGeneBankResultsDirectory() {
		String userDir = System.getProperty("user.dir");
		Path path = Paths.get(userDir, "target","out", "geneBankOutput");
		return path.toAbsolutePath().toString();
	}

	/**
	 * used to find the folder for the resources that contain the gene bank files
	 *
	 * @return
	 */
	public String getGeneBankStorage() {
		String userDir = System.getProperty("user.dir");
		Path path = Paths.get(userDir, "src", "main", "resources", "geneBankStorage");
		return path.toAbsolutePath().toString();
	}

	/**
	 * Used to load the parameters that the comsolModel is using from a csv file read in
	 * as the absolute csvPath - returns a 2d string array, where [0][all] is the
	 */
	public String[][] loadParamsFromCSV(String csvPath) {
		String[][] returnArray = new String[countCsvRows(csvPath)][countCsvColumns(csvPath)];
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(Paths.get(csvPath));
			// Reading Records One by One in a String array
			String line = reader.readLine();
			// loop until all lines are read
			int i = 0;
			while (line != null) {
				returnArray[i] = line.split(",");
				i++;
				line = reader.readLine();
			}
		} catch (IOException ioe) {
			logger.error("Input Output exception while updating params {} - {}", ioe.getClass().getName(), ioe.getMessage());
			logger.error("CONTEXT", ioe);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error while loading parameters from a csv");
					logger.error("CONTEXT", e);
				}
			}
		}

		return returnArray;
	}

	/**
	 * Method to count the number of columns in a csv
	 *
	 * @param csvPath the path to the csv in question
	 * @return the number of columns
	 */
	private int countCsvColumns(String csvPath) {
		int inputCounter = 0;
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(Paths.get(csvPath));
			String line = reader.readLine();
			String[] returnArray = line.split(",");
			inputCounter = returnArray.length;
		} catch (IOException ioe) {
			logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
			logger.error("CONTEXT", ioe);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error while counting CSV columns");
					logger.error("CONTEXT", e);
				}
			}
		}
		return inputCounter;
	}

	/**
	 * Method to count the number of rows in a csv
	 *
	 * @param csvPath the path to the csv in question
	 * @return the number of rows
	 */
	private int countCsvRows(String csvPath) {
		int rowCounter = 0;
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(Paths.get(csvPath));
			String line = reader.readLine();
			while (line != null) {
				rowCounter++;
				line = reader.readLine();
			}
		} catch (IOException ioe) {
			logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
			logger.error("CONTEXT", ioe);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("IO exception while counting the number of CSV rows");
					logger.error("CONTEXT", e);
				}
			}
		}
		return rowCounter;
	}

	/**
	 * used to combine the arrays of the parameter names, gene bank shown and results array
	 *
	 * @param parameterNames
	 * @param geneBank
	 * @param results
	 * @return one large 2d array where array[0] = parameter names, array[1-end][last] = results values and the rest is the in order genebank
	 */
	public Object[][] combineArrays(String[] parameterNames, Object[][] geneBank, double[] results) {
		try {
			Object[][] returnValue = new Object[geneBank.length + 1][parameterNames.length + 1];
			int i;
			for (i = 0; i < parameterNames.length; i++) {
				returnValue[0][i] = parameterNames[i];
			}
			returnValue[0][i] = "Result";
			int j;
			for (i = 0; i < geneBank.length; i++) {
				for (j = 0; j < parameterNames.length; j++) {
					// treat all as double
					returnValue[i + 1][j] = getSignificant(Double.parseDouble((String) geneBank[i][j]), SIGNIFICANT_FIGURES_USED);
					double value = Double.parseDouble((String) geneBank[i][j]);
					// check if integer by checking if rounded value is same as actual value
					if (value == Math.floor(value) && !Double.isInfinite(value)) {
						// integer type
						returnValue[i + 1][j] = (int) value + "";
					}
				}
				returnValue[i + 1][j] = getSignificant(results[i], 5);
			}

			return returnValue;
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("CONTEXT", e);
			return null;
		}
	}

	/**
	 * Used to combine results and the original gene bank together, no adding the parameter names
	 * @param geneBank
	 * @param results
	 * @return
	 */
	public String[][] combine2Arrays(Object[][] geneBank, double[] results) {
		int i =0;
		int j =0;
		try {
			String[][] returnValue = new String[geneBank.length][2];
			for (i = 0; i < geneBank.length; i++) {
				j = 0;
				while (j < geneBank[i].length) {
					String value1 = getSignificant(Double.parseDouble((String) geneBank[i][j]), SIGNIFICANT_FIGURES_USED);
					returnValue[i][j] = value1;
					double value = Double.parseDouble((String) geneBank[i][j]);
					if (value == Math.floor(value) && !Double.isInfinite(value)) {
						// integer type
						returnValue[i][j] = (int) value + "";
					}
					j++;
				}
				returnValue[i][j] = getSignificant(results[i], 5);
			}
			return returnValue;
		} catch (Exception e) {
			logger.error("Error while combining the arrays. Happened at point [{}][{}].  CONTEXT",i,j, e);
			return null;
		}
	}

	/**
	 * Method to convert a double into a string that only contains the number of significant figures passed in
	 * e.g. 4.567891E-5 with 3 S.F. = 4.57E-5
	 *
	 * @param value   the double value to be changed
	 * @param sigFigs the number of significant figures it should show
	 * @return the String value of the shortened value
	 */
	public String getSignificant(double value, int sigFigs) {
		MathContext mc = new MathContext(sigFigs, RoundingMode.DOWN);
		BigDecimal bigDecimal = new BigDecimal(value, mc);
		double d = bigDecimal.doubleValue();
		if (d > 0) {
			return " " + d;
		}
		return d + "";
	}

	public void showErrorMessageAndQuit(String message) {
		JDialog dialog = new JDialog();
		dialog.setTitle("Quitting");
		dialog.setModal(true);
		dialog.setContentPane(new JLabel(message));
		dialog.pack();
		dialog.setVisible(true);
		System.exit(0);
	}

	/**
	 *
	 * @param genome
	 * @param parameterNames
	 * @param useComsol
	 * @param comsolModel
	 * @param useError used to decide if errors should debug log (true) or trace log (false)
	 * @return
	 */
	public boolean doubleCheckSolution(String[] genome, String[] parameterNames, boolean useComsol, ComsolModelInterface comsolModel, boolean useError) {
		int i;
		double hiddenObjectRadius;
		double scattererDistanceOut;
		if (useComsol) {
			hiddenObjectRadius = Double.parseDouble(comsolModel.getDoubleParameter("R"));
			scattererDistanceOut = Double.parseDouble(comsolModel.getDoubleParameter("r"));
		} else {
			hiddenObjectRadius = 0.025;
			scattererDistanceOut = 0.0006;
		}
		String valueOf = "Value of ";
		for (i = 0; i < parameterNames.length; i++) {
			// if it's an n value
			if (parameterNames[i].startsWith("a") && !((Integer.parseInt(genome[i]) == 0) || (Integer.parseInt(genome[i]) == 1))) {
				if (useError) {
					logger.debug(valueOf + parameterNames[i] + " is neither 0 nor 1: {} ", genome[i]);
				} else {
					logger.trace(valueOf + parameterNames[i] + " is neither 0 nor 1: {} ", genome[i]);
				}
				return false;
			}
			// if it's an n value
			else if (parameterNames[i].startsWith("n") && Integer.parseInt(genome[i]) < 0) {
				if (useError) {
					logger.debug(valueOf + parameterNames[i] + " is less than 0 - rejection of it");
				} else {
					logger.trace(valueOf + parameterNames[i] + " is less than 0 - rejection of it");
				}
				return false;
			}
			// if its an scattererRadius value
			else if (parameterNames[i].startsWith("R")) {
				// if minScatterDistance < scattererRadius+cloakRadius - layer distance < scatterer radius + object radius,
				if (Double.parseDouble(genome[i]) < hiddenObjectRadius + scattererDistanceOut) {
					if (useError) {
						logger.debug(valueOf + parameterNames[i]
								+ " is lower than the central radius cloakRadius + scatter layer " + parameterNames[i].charAt(1)
								+ "'s radius : " + scattererDistanceOut + "<" + hiddenObjectRadius + "+" + scattererDistanceOut);
					} else {
						logger.trace(valueOf + parameterNames[i]
								+ " is lower than the central radius cloakRadius + scatter layer " + parameterNames[i].charAt(1)
								+ "'s radius : " + scattererDistanceOut + "<" + hiddenObjectRadius + "+" + scattererDistanceOut);
					}
					return false;
				}
				if (Double.parseDouble(genome[i]) > 2.5 *hiddenObjectRadius ) {
					if (useError) {
						logger.debug("Value of {}, gene set {} is higher than the allowed maximum of 2.5*cloakRadius(R)=={}", parameterNames[i], i, 2.5 * hiddenObjectRadius);
					} else {
						logger.trace("Value of {}, gene set {} is higher than the allowed maximum of 2.5*cloakRadius(r)=={}", parameterNames[i], i, 2.5 * hiddenObjectRadius);
					}
					return false;
				}
			}
			else if (parameterNames[i].startsWith("r")) {
				if (scattererDistanceOut < 0.0005) {
					if (useError) {
						logger.debug(valueOf + parameterNames[i] + " is smaller than 0.0005mm - 0.5/1000");
					} else {
						logger.trace(valueOf + parameterNames[i] + " is smaller than 0.0005mm - 0.5/1000");
					}
					return false;
				}
				int nx = Integer.parseInt(genome[i - 2]);
				if (scattererDistanceOut > scattererDistanceOut * Math.PI / nx) {
					if (useError) {
						logger.debug(valueOf + parameterNames[i] + " and " + parameterNames[i - 1]
								+ " is causing overlaps within the scatter Layer " + parameterNames[i].charAt(1)
								+ "': " + scattererDistanceOut + "*Pi/" + nx + "<" + scattererDistanceOut);
					} else {
						logger.trace(valueOf + parameterNames[i] + " and " + parameterNames[i - 1]
								+ " is causing overlaps within the scatter Layer " + parameterNames[i].charAt(1)
								+ "': " + scattererDistanceOut + "*Pi/" + nx + "<" + scattererDistanceOut);
					}
					return false;
				}
			}
			else if (parameterNames[i].startsWith("phi")) {
				int scattererOffset = Integer.parseInt(genome[i]);
				if (scattererOffset > 359) {
					if (useError) {
						logger.debug("Value of {} is {} which is larger than the max of 359", parameterNames[i], scattererOffset);
					} else {
						logger.trace("Value of {} is {} which is larger than the max of 359", parameterNames[i], scattererOffset);
					}
					return false;
				}
				if (scattererOffset < 0) {
					if (useError) {
						logger.debug("Value of {} is {} which is smaller than the min of 0", parameterNames[i], scattererOffset);
					} else {
						logger.trace("Value of {} is {} which is smaller than the min of 0", parameterNames[i], scattererOffset);
					}
					return false;
				}
			}
		}
		return true;
	}

	public String alternateArrays(String[] names, Object[] values) {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for (i = 0; i < names.length; i++) {
			sb.append(names[i]);
			sb.append("=");
			sb.append(values[i] + "");
			sb.append(", ");
		}
		return sb.toString();
	}

	public int wrapAroundInt(int max, int min, int value) {
		if (value > max) {
			// if larger than max, wrap around to min
			return (value % max)+min;
		} else if (value < min) {
			// if smaller than min, wrap around so max - diff(min, value)
			return max - (min - value);
		} else {
			// value in range, it's fine
			return value;
		}
	}

	public double wrapAroundDouble(double max, double min, double value) {
		if (value > max) {
			// if larger than max, wrap around to min
			logger.trace("Wrap with mod: {}%{}=={}", value, max, (value%max)+min);//todo remove log
			return (value % max)+min;
		} else if (value < min) {
			// if smaller than min, wrap around so max - diff(min, value)
			return max - (min - value);
		} else {
			// value in range, it's fine
			return value;
		}
	}

	public double newRandomScattererDistanceOut(Model model){
		return model.param().evaluate("R");
	}

	public String getImageOutputFolder() {
		String userDir = System.getProperty("user.dir");
		Path path = Paths.get(userDir, "target","out", "images");
		return path.toAbsolutePath().toString();
	}

//	public void saveImagesInt(ComsolModelInterface comsolModel) {
//		comsolModel.
//	}

	private String getBatchJobFilesFolder() {
		String userDir = System.getProperty("user.dir");
		Path path = Paths.get(userDir, "src", "main", "resources", "batchJobFiles");
		File directory = new File(path.toAbsolutePath().toString());
		if (! directory.exists()){
			directory.mkdirs();
		}
		return path.toAbsolutePath().toString();
	}

	public String getBatchJobMaxFolder() {
		Path path = Paths.get(getBatchJobFilesFolder(), "max");
		File directory = new File(path.toAbsolutePath().toString());
		if (!directory.exists()){
			directory.mkdir();
		}
		return path.toAbsolutePath().toString();
	}

	public String getBatchJobMinFolder() {
		Path path = Paths.get(getBatchJobFilesFolder(), "min");
		File directory = new File(path.toAbsolutePath().toString());
		if (!directory.exists()){
			directory.mkdir();
		}
		return path.toAbsolutePath().toString();
	}

	public String getBatchJobNamedFolder(String directoryName) {
		Path path = Paths.get(getBatchJobFilesFolder(), directoryName);
		File directory = new File(path.toAbsolutePath().toString());
		if (!directory.exists()){
			directory.mkdirs();
			directory.deleteOnExit();
		}
		return path.toAbsolutePath().toString();
	}

	public void cleanUpBatchDirectory() {
		Path path = Paths.get(getBatchJobFilesFolder());
		File directory = new File(path.toAbsolutePath().toString());
		for (File f : directory.listFiles()) {
			f.delete();
		}
	}

	public void cleanUpBatchDirectoryFull() {
		Path path = Paths.get(getBatchJobFilesFolder());
		File mainDirectory = new File(path.toAbsolutePath().toString());
		logger.debug("Preparing to delete the contents of {}", mainDirectory.getAbsolutePath().toString());
		for (File batchDirectory : mainDirectory.listFiles()) {
			for (File batchFile : batchDirectory.listFiles()) {
				batchFile.delete();
			}
			batchDirectory.delete();
		}
	}

	public String getMassImageOutputFolder(String startTime) {
		return Variables.getMassImageOutputFolder(startTime, "");
	}

	// method to get the location of the last batch job, so that it's progress can be determined
	public File getLastBatchJob(int maximumSequenceCount) {
//		Path rootDirectory = Paths.get(getBatchJobFilesFolder());
		File rootDirectory = new File(getBatchJobFilesFolder());
		File[] fileList = rootDirectory.listFiles();
		boolean foundLastCombination = false;

		int i=fileList.length-1;
		while(!foundLastCombination && i >=0) {
			// find the current file name
			String[] fileNameSplit = fileList[i].getAbsolutePath().split("\\\\");
			String fileName = fileNameSplit[fileNameSplit.length-1];
			// find the combination sequence from the name
			int numberStart = 47;
			int numberEnd = numberStart;
			while (Character.isDigit(fileName.charAt(numberEnd))) {
				numberEnd +=1;
			}
			String combination = fileName.substring(numberStart, numberEnd);
			if (Integer.parseInt(combination)==maximumSequenceCount-1) {
				foundLastCombination = true;
			}
			// check if current sequence matches the maximum

			i--;
		}
		i ++;
		return fileList[i];


	}

	public void waitForLastBatchToFinish(int maximumCombinationCount) {
		File lastBatchFolder = getLastBatchJob(maximumCombinationCount);
		File child = new File(lastBatchFolder.getAbsolutePath()+"\\batchmodel.mph.status");
		// get child.2nd line - check if it says "done"
		AtomicInteger lineCounter = new AtomicInteger();
		try (Stream<String> stream = Files.lines(Paths.get(child.getAbsolutePath()))) {
			stream.forEach(fileLine->{
				String lineToCheck = "";
				if (lineCounter.get() == 1) {
					lineToCheck = fileLine;
				}
				int loopCounter = 0;
				int loopMax = 20;
				if (lineToCheck.equals("Scheduling")) {
					logger.info("Still scheduling the batch job, 120 seconds to try and clear the image saving");
					try {
						TimeUnit.SECONDS.sleep(120);
					} catch (InterruptedException e) {
						logger.error("Error while waiting - interrupted exception");
						e.printStackTrace();
					}
				}
				while ((!lineToCheck.equals("Done") && !lineToCheck.equals("Error") && !lineToCheck.equals("")) && lineCounter.get() >=1){
					if (lineToCheck.equals("Done")) {
						logger.debug("Batch file has finished");
						// finish
					} else if (lineToCheck.equals("Error")) {
						logger.debug("Batch file has Errored, quitting");
						// finish
					} else {
						try {
							// still going, delay
							logger.info("Waiting 60 seconds to try and clear the image saving");
							TimeUnit.SECONDS.sleep(60);
						}catch (InterruptedException e) {
							logger.error("Error while waiting - interrupted exception");
							e.printStackTrace();
						}catch (Exception e) {
							logger.error("Error while waiting - regular error");
							e.printStackTrace();
						}
					}
					// exit if too long
					if (loopCounter >= loopMax) {
						lineToCheck = "";
						lineCounter.set(-1);
					}
					loopCounter ++;
				}
				lineCounter.getAndIncrement();
			});
			int breakx = 0;
		}
		catch (IOException e) {
			logger.error("IO error while reading the status file of {}", child.getAbsolutePath(), e);
		}



//		try (BufferedReader br = new BufferedReader(new FileReader(child))) {
//			String line;
//			while ((line = br.readLine()) != null) {
//
//			}
//		}
	}


	public File getLastOutputFolder(int maximumSequenceCount, String startTimeFolder) {
		try {
//		Path rootDirectory = Paths.get(getBatchJobFilesFolder());
			File rootDirectory = new File(startTimeFolder);
			File[] directoryList = rootDirectory.listFiles();
			boolean foundLastCombination = false;
			int i = directoryList.length - 1;
			while (!foundLastCombination && i >= 0) {
				// find the current file name
				String[] fileNameSplit = directoryList[i].getAbsolutePath().split("\\\\");
				String fileName = fileNameSplit[fileNameSplit.length - 1];
				String combinationString = fileName.split("_")[1].split("n-")[1];
				if (Integer.parseInt(combinationString) == maximumSequenceCount - 1) {
					foundLastCombination = true;
				}
				// check if current sequence matches the maximum

				i--;
			}
			i++;
			return directoryList[i];
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		return null;
	}

	private int calculateWaitTime(int maxNumber, int numberLeft) {
		int diff = maxNumber - numberLeft;
		if (diff < 10)
			return 60;
		else
			return diff * 6;
	}

	private String scanFolders(String outputFolder) {
		File outputDirectory = new File(outputFolder);
		String[] directories = outputDirectory.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		Path outputPath = Paths.get(outputFolder);
		String lastPlace = "";
		boolean lastPlaceFound = false;
		for (String folderName : directories) {
			File folder = new File(Paths.get(outputPath.toString(), folderName).toString());
			if (folder.list().length < 2 && !lastPlaceFound) {
				lastPlace = folder.getAbsolutePath().toString();
				lastPlaceFound = true;

			}
		}

		return lastPlace;
	}

	public void checkIfLastBatchHasFinsihed(int maximumCombinationCount, String startDate) {
		File lastOutputFolder = new File( Paths.get(getMassImageOutputFolder(startDate), "acousticCloaking_programStart-"+Variables.getProgramStartTime()).toAbsolutePath().toString() );
		File lastOutputCombinationFolder = getLastOutputFolder(maximumCombinationCount, lastOutputFolder.getAbsolutePath());
		// adjust the maximum output folder for the penultimate to be the inverse of the last - -1 * max
		File penultimateOutputCombinationFolder = getLastOutputFolder((maximumCombinationCount-2)*-1, lastOutputFolder.getAbsolutePath());

		File lastInputDataPNG = new File(lastOutputCombinationFolder.getAbsolutePath()+"\\input_image__value="+(maximumCombinationCount-1)+"__value=1.png");
		File penultimateInputDataPNG = new File(penultimateOutputCombinationFolder.getAbsolutePath()+"\\input_image__value="+((maximumCombinationCount-1)*-1)+"__value=1.png");

		// while the last file produced does not exist
		while (!lastInputDataPNG.exists() || !penultimateInputDataPNG.exists()) {
			String lastCompletedFolder = scanFolders(Paths.get(getMassImageOutputFolder(startDate), "acousticCloaking_programStart-"+Variables.getProgramStartTime()).toAbsolutePath().toString());
			String fileName = lastCompletedFolder.split("\\\\")[lastCompletedFolder.split("\\\\").length-1];
			String tagNumber = fileName.split("_")[1].split("-")[fileName.split("_")[1].split("-").length-1];
			int numberLeft = maximumCombinationCount - Integer.parseInt(tagNumber);

			if (!penultimateInputDataPNG.exists()) {
				// wait longer if the penultimate one also doesn't exist
				try {
					int waitTime = calculateWaitTime(maximumCombinationCount, numberLeft);
					// still going, delay
					logger.info("Can't find penultimate file {}, found combination {}, waiting {}", penultimateInputDataPNG, tagNumber, waitTime);
					TimeUnit.SECONDS.sleep(waitTime);
				}catch (InterruptedException e) {
					logger.error("Error while waiting - interrupted exception");
					e.printStackTrace();
				}catch (Exception e) {
					logger.error("Error while waiting - regular error");
					e.printStackTrace();
				}
			} else {
				try {
					int waitTime = calculateWaitTime(maximumCombinationCount, numberLeft);
					// still going, last one, smaller delay
					logger.info("Can't find last file, found combination {}, waiting {} seconds to try and clear the image saving", tagNumber, waitTime);
					TimeUnit.SECONDS.sleep(waitTime);
				}catch (InterruptedException e) {
					logger.error("Error while waiting - interrupted exception");
					e.printStackTrace();
				}catch (Exception e) {
					logger.error("Error while waiting - regular error");
					e.printStackTrace();
				}
			}
		}
	}


	public static String getComosolParametersFolder() {
		String userDir = System.getProperty("user.dir");
		Path path = Paths.get(userDir, "src", "main", "resources", "comosolParameters");
		File directory = new File(path.toAbsolutePath().toString());
		if (! directory.exists()){
			directory.mkdirs();
		}
		return path.toAbsolutePath().toString();
	}

	public ComsolParameters readComsolParameters() throws IOException, ClassNotFoundException{
		ComsolParameters comsolParameters = null;
		try {
			FileInputStream fileIn = new FileInputStream(getComosolParametersFolder()+"\\comsol_parameters.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			comsolParameters = (ComsolParameters) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			throw i;
		} catch (ClassNotFoundException c) {
			System.out.println("Employee class not found");
			c.printStackTrace();
			throw c;
		}
		return comsolParameters;
	}

	public void saveComsolParameters(ComsolParameters comsolParameters) {
		try {
			FileOutputStream fileOut =
					new FileOutputStream(getComosolParametersFolder()+"\\comsol_parameters.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(comsolParameters);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in /tmp/employee.ser");
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
}
