package com.ga;

import ch.qos.logback.classic.Logger;
import com.Variables;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class MainControlClass {
    final static Logger logger = (Logger) LoggerFactory.getLogger(com.ga.MainControlClass.class.getName());

    public static void main(String[] args) {
        boolean hasTitle = false;
        if (args.length>1) {
            hasTitle = Boolean.parseBoolean(args[1]);
        }
        boolean file_exists = Files.exists(Paths.get(args[0]));
        String[][] variablesLoaded = loadCSV(args[0], hasTitle);
        setUpVaraibles(variablesLoaded);

        if (Variables.isUseOldSystem()) {
            Variables.setVersionUsed("2.1");
            com.ga.nsaga.RunNSAGA.main(args);
        } else {
            Variables.setVersionUsed("3.0");
            com.ga.nsaga_3.RunNSAGA.main(args);
        }
    }

    private static void setUpVaraibles(String[][] variablesLoaded) {
        int variablesLoadedCounter = 0;
        int variablesCounted = 0;
        for (String[] row : variablesLoaded) {
            if (row.length==1 || row.length==0) {
                continue;
            } else if (row[0].equals("useGeneRandomisation")) {
                Variables.setUseGeneRandomisation(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("runCollsionChecks")) {
                Variables.setRunCollsionChecks(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("optimizerType")) {
                Variables.setOptimizerType(row[1]);
                variablesCounted +=1;
            } else if (row[0].equals("shapeOfScatterer")) {
                Variables.setShapeOfScatterer((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("shapeOfObject")) {
                Variables.setShapeOfObject((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("saveNoImages")) {
                Variables.setSaveNoImages(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("saveAllImages")) {
                Variables.setSaveAllImages(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("useOldSystem")) {
                Variables.setUseOldSystem(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("save360Points")) {
                Variables.setSave360Points(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("USED_ERROR_METRIC")) {
                Variables.setUsedErrorMetric(row[1]);
                variablesCounted +=1;
            } else if (row[0].equals("ALLOW_0_SWITCHING")) {
                Variables.setAllow0Switching(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("USE_COMSOL")) {
                Variables.setUseComsol(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("cloackThicknesssMultiplier")) {
                Variables.setCloackThicknesssMultiplier((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("centralObjectRadisMultiplier")) {
                Variables.setCentralObjectRadisMultiplier((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("scattererRadiusMultiplier")) {
                Variables.setScattererRadiusMultiplier((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("csvMapPath")) {
                Variables.setCsvMapPath((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("usePreviousMap")) {
                Variables.setUsePreviousMap(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("pixelWidth")) {
                Variables.setPixelWidth(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("regionThickness")) {
                Variables.setRegionThickness(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("previousGenerationCounter")) {
                Variables.setPreviousGenerationCounter(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("resumePreviousRun")) {
                Variables.setResumePreviousRun(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("previousRunFilePath")) {
                Variables.setPreviousRunFilePath((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("INITIAL_ANNEALING_STEP")) {
                Variables.setInitialAnnealingStep(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("initalPopSize")) {
                Variables.setInitalPopSize(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("numGenerations")) {
                Variables.setNumGenerations(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("INITIAL_TEMPERATURE")) {
                Variables.setInitialTemperature(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("MAXIMUM_TEMPERATURE_STEPS")) {
                Variables.setMaximumTemperatureSteps(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("initialMutationProbability")) {
                Variables.setInitialMutationProbability(Double.parseDouble(row[1]));
                variablesCounted +=1;
            }  else if (row[0].equals("MAXIMUM_SCATTERERS_IN_A_QUADRANT")) {
                Variables.setMaximumScatterersInAQuadrant(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("clearModelOnGenerate")) {
                Variables.setClearModelOnGenerate(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("mappedLayerDensity")) {
                Variables.setMappedLayerDensity(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("mappedScatterRadiusMultiplier")) {
                Variables.setMappedScatterRadiusMultiplier(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("massImageOutputFolder")) {
                Variables.setMassImageOUtputFolder((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("logFolder")) {
                Variables.setLogFolderUsed((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("comsolVersion")) {
                Variables.setComsolVersion((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("reannealingStep")) {
                Variables.setReannealingStep(Integer.parseInt(row[1]));
                variablesCounted +=1;
            }  else if (row[0].equals("tempRaiseAmount")) {
                Variables.setTempRaiseAmount(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("geneHistoryDir")) {
                Variables.setGeneHistoryDir((row[1]));
                variablesCounted +=1;
            }  else if (row[0].equals("numberOfCores")) {
                Variables.setNumberOfCores(Integer.parseInt(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("modelPath")) {
                Variables.setModelPath((row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("readOldMapUseNew")) {
                Variables.setReadOldMapUseNew(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("checkoutLicense")) {
                Variables.setCheckoutLicense(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("waitForLicense")) {
                Variables.setWaitForLicense(Boolean.parseBoolean(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("mutateAddtional")) {
                Variables.setMutateAddtional(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("additionalMutationDecrease")) {
                Variables.setAddtionalMutationDecrease(Double.parseDouble(row[1]));
                variablesCounted +=1;
            } else if (row[0].equals("tempFunction")) {
                Variables.setTempFunction((row[1]));
                variablesCounted +=1;
            } else {
                logger.error("Found something that is wrong with variables loaded:\n "+ row[0]);
            }
            variablesLoadedCounter+=1;
        }
        boolean variablesCountedEqualsLoaded = (variablesLoadedCounter == variablesCounted);
        if (!variablesCountedEqualsLoaded)
            logger.error("The number of variables loaded is not equal to the number of variables saved!");
        int breakx = 0;
    }

    /**
     * Used to load the parameters that the comsolModel is using from a csv file read in
     * as the absolute csvPath - returns a 2d string array, where [0][all] is the
     */
    public static String[][] loadCSV(String csvPath, boolean hasTitle) {
        String[][] returnArray = new String[countCsvRows(csvPath)][countCsvColumns(csvPath)];
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            // Reading Records One by One in a String array
            String line = reader.readLine();
            // loop until all lines are read
            int i = 0;
            while (line != null) {
                returnArray[i] = line.split(",");
                i++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while updating params {} - {}", ioe.getClass().getName(), ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while loading parameters from a csv");
                    logger.error("CONTEXT", e);
                }
            }
        }
        if (hasTitle)
            return Arrays.copyOfRange(returnArray, 1, returnArray.length);
        return Arrays.copyOfRange(returnArray, 0, returnArray.length);
    }

    /**
     * Method to count the number of rows in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of rows
     */
    private static int countCsvRows(String csvPath) {
        int rowCounter = 0;
        BufferedReader reader = null;
        try {
            // D:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\scatterer_mesh_ultrathin.csv
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            while (line != null) {
                rowCounter++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("IO exception while counting the number of CSV rows");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return rowCounter;
    }

    /**
     * Method to count the number of columns in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of columns
     */
    private static int countCsvColumns(String csvPath) {
        int inputCounter = 0;
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            String[] returnArray = line.split(",");
            inputCounter = returnArray.length;
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while counting CSV columns");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return inputCounter;
    }
}
