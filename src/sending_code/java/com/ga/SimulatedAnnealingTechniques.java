package com.ga;


public class SimulatedAnnealingTechniques {

    private double maximumTemperature;

    public double decreaseByControlAmount(double temperature, int currentAnnealingStep, int maximumTemperatureSteps, double initialTemp) {
        // used to decrease temperature by a factor of x/maxAnnealingSteps*intialTemp*control
        return temperature - (0.0645161 *currentAnnealingStep / (double)maximumTemperatureSteps)*initialTemp;
    }

    public double initialTempToStepOverMax(int currentAnnealingStep, int maximumTemperatureSteps, double initialTemp) {
        // used to decrease temperature by a factor of x/maxAnnealingSteps*intialTemp*control
        return initialTemp - (currentAnnealingStep / (double)maximumTemperatureSteps)*initialTemp;
    }

    /**
     * used to decrease temperature by a factor of 1-coolingRate e.g. temp
     * @param temperature
     * @param coolingRate
     * @return
     */
    public double decreaseByCoolingRate(double temperature, double coolingRate) {
        return temperature * (1-coolingRate);
    }

    public double decreaseByLogs(double initialTemp, int currentAnnealingStep) {
        return initialTemp/Math.log(currentAnnealingStep+1);
    }

    /**
     * 0-8 = starting temp
     *
     * @param temperature
     * @param currentStep
     * @param maxStep
     * @return
     */
    public double decreaseExpoentialByCurrentStepVsMax(double temperature, int currentStep, int maxStep) {
        return temperature - Math.exp(currentStep-maxStep);
    }

    public void runExampleCooling(int start_step, int max_steps, double startTemp, double coolingRate) {
        double temp = startTemp;
        for (int i = start_step; i < max_steps; i++) {
            System.out.println("Temp at start of step "+ i + " = "+ temp);
            temp = decreaseByCoolingRate(temp, coolingRate);

        }
    }

    public double decreaseByPurcellFactor(double initalTemp, int currentAnnealingStep, int maxAnnealingSteps) {
        // T = T0 - (T0 * i) / N where N = max num annealing steps
        return initalTemp - ((double)(initalTemp*currentAnnealingStep)/maxAnnealingSteps);
    }

}
