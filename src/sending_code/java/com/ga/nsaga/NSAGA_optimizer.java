package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.GlobalUtils;
import com.comsolModelInterface.ComsolModelInterface;
import com.ga.SimulatedAnnealingTechniques;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;

import static com.ga.nsaga.NsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT;

/**
 * Abstract class to allow optimisers to be more uniform, while also storing the consistent methods they share.
 */
public abstract class NSAGA_optimizer implements optimiserInterface {
    // recommended SA cooling rate is 0.25, recommended SA control value is 0.0645161
    ComsolModelInterface comsolModel;
    NsagaUtils utils;
    GenesAndfScoresBank currentPopulation;
    GenesAndfScoresBank nextPopulation;
    protected double currentEquilibirumScore = Double.MAX_VALUE;
    Logger logger = (Logger) LoggerFactory.getLogger(NSAGA_optimizer.class.getName());
    Position[] scattererToIndexMap;
    protected final String saveFilePath = Paths.get(Variables.getOutputFolder() +"NSAGA_out").toString();
//    protected String activeGeneLogFile;
    protected CopyOption[] copyOptions = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
    };
    private SimulatedAnnealingTechniques simulatedAnnealingTechniques = new SimulatedAnnealingTechniques();
    private String shapeOfScatterer;
    private String shapeOfObject;
    private HashSet<List<Integer>> combinationHistory;
    private HashSet<List<Integer>> rejectedHistory;
    private boolean scanRejectedHistory = true; // control variable for scanning the rejected history for duplicates.
    protected double[][] emptyModel360Pressure;
    protected double[][] centralObjectOnly360Pressure;
    protected DecimalFormat decimalFormat = new DecimalFormat("#.##");
    private int numberModelsRun = 0;
    private long totalTimeForEachModel = 0;
    protected double scattererRadius;
    protected boolean useComsol;
    protected int numberOfCheckhistoryRegjections = 0;
    private ListOfListsCheckers listOfListsChecker = new ListOfListsCheckers();
    protected String startDate;
    protected long totalTimeForEachHistoryCheck = 0;
    protected int lastReportedIndex = 0;
    protected long totalTimeForAnnealingSteps = 0;
    protected int timeSinceLastModel = 0;
    protected int historyRejections = 0;
    protected int mutationHistoryRejects = 0;
    protected int historyRejectionsFromRejectedList = 0;
    protected int totalHistoryChecks = 0;
    protected int acceptedModelIndex;
    protected int totalModelsEvaluatedByComsol = 0;
    protected int consideredAndRejected = 0;
    protected long timeStartedNsagaLoop;
    protected long totalTimeSpentSaving = 0;
    protected int collisionRejects = 0;
    protected int collisionTime = 0;
    protected long combinationGenerationTime = 0;
    protected int numberOfTimesSaved = 0;
    protected long populationTotalTime = 0;
    protected long timeSpentSavingImages = 0;
    protected int numberOfImagesSaved = 0;
    protected int combosGenerated = 0;
    protected int numCollisionChecks = 0;
    protected long timeSpentCopyingNextPopulationOver = 0;
    protected long garbageCollectionChecksTime = 0;
    protected  long timeSpentLogging = 0;
    private static int numberOfExceptions = 0;
    private static int indexModelComplete = 0;

    NSAGA_optimizer(ComsolModelInterface comsolModel, GenesAndfScoresBank genesLoaded, GenesAndfScoresBank geneHistory, boolean useComsol, Level loggerLevel, Position[] scattererToIndexMap, String shapeOfScatterer, String shapeOfObject, double scattererRadius, String startDate) throws Exception {
        this.startDate = startDate;
        this.comsolModel = comsolModel;
        currentPopulation = genesLoaded;
        this.utils = new NsagaUtils();
        this.logger.setLevel(loggerLevel);
        this.scattererToIndexMap = scattererToIndexMap;
        this.shapeOfScatterer = shapeOfScatterer;
        this.shapeOfObject = shapeOfObject;
        this.scattererRadius = scattererRadius;
        this.useComsol = useComsol;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setGroupingSize(3);
        // set up the right set of shapes.
        logger.debug("Chaning object shape 1");
        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }

        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        logger.debug("Making gene hisory csv");
        File directory = new File(Paths.get(Variables.getActiveGeneHistoryCSVLocation()).getParent().toString());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        combinationHistory = new HashSet<>();
        rejectedHistory = new HashSet<>();
        // add the entire set of genes to the current history
        for (int i = 0; i< geneHistory.getSize(); i++ ) {
            addToHistory(geneHistory.getInvididual(i));
        }

//        sortHistory(combinationHistory);

        try {
            comsolModel.runBlankModel();
            emptyModel360Pressure = comsolModel.getModels360Points();
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        comsolModel.saveImagesOneOff(Paths.get("emptyModels","blank_area").toString(), utils.empty_area_run, new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, emptyModel360Pressure);

        setAllScatterers(utils.getEmpyScattererArray(), scattererToIndexMap);
        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }
        try {
            this.comsolModel.runModel(utils.no_scatterer_run);
        } catch (Throwable e) {
            logger.error("",e);
            e.printStackTrace();
            throw e;
        }
        centralObjectOnly360Pressure = comsolModel.getModels360Points();
        comsolModel.saveImagesOneOff(Paths.get("emptyModels","only_central_object").toString(), utils.no_scatterer_run, new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, centralObjectOnly360Pressure);
        double denominator = comsolModel.getResult() * Double.parseDouble(comsolModel.getDoubleParameter("fitness_for_squared_metrics"));
        Variables.setSanchisDenominator(denominator);
        comsolModel.setFitnessForSquareMetrics(denominator);
        comsolModel.runModel(-997);

        nextPopulation = null;
        int currentModel = -70;
        int i;
        for (i = 0; i < currentPopulation.getSize(); i++) {
            setAllScatterers(currentPopulation.getInvididual(i).getGenes(), scattererToIndexMap);
            if (i % 10 == 0) {
                logger.debug("Initial run set, checking member {}", i);
            } else {
                logger.debug("Initial run set, checking member {}", i);
            }
            // evaluate
            try {
                runComsolAndGetMetrics(currentModel, currentPopulation.getInvididual(i));
            } catch (Throwable e) {
                logger.debug("Throwable detected:\n", e);
            }
            logger.debug("Run model and get metric for model {}", i);
            if (!Variables.isSaveNoImages())
                comsolModel.saveImagesInt("savedImages_" + currentPopulation.getInvididual(i).getModelNumber() + "__startTime-", currentPopulation.getInvididual(i).getModelNumber(), currentPopulation.getInvididual(i).getGenes(), startDate, comsolModel.getModels360Points());
            GenesAndfScoresBank tempBank = new GenesAndfScoresBank();
            tempBank.addNewIndividual(currentPopulation.getInvididual(i));
            saveAnIndividual(tempBank);
            currentModel++;
        }
        saveCombinationHistory(geneHistory);
//        timeDiff = System.currentTimeMillis()- annealingSetStartTime;
//        logger.info("Time to complete initial set of runs: {}s == {}m == {}h",  timeDiff/1000, timeDiff/60000, timeDiff/3600000d);
//        logger.info("Average : {}s == {}m == {}h",  timeDiff/1000, timeDiff/60000, timeDiff/3600000d);

    }


    /**
     * Method to randomly mutate the offered individual
     * @param originalIndividual
     * @return
     */
    protected Integer[] performMutation(Integer[] originalIndividual){
        Integer[] mutatedIndividual = utils.copyIntegerArray(originalIndividual);
        Random rand = new Random();
        int mutationLocation = rand.nextInt(MAXIMUM_SCATTERERS_IN_A_QUADRANT); //0-29
        int newGeneValue = 0;
        if (Variables.isAllow0Switching()) {
            newGeneValue = selectRandomGene(originalIndividual[mutationLocation].intValue(), Variables.getScattererPositionIndexMap().length);
//            newGeneValue = rand.nextInt(Variables.getScattererPositionIndexMap().length); // new random gene-- 0 to array position
        } else {
            newGeneValue = selectRandomGene(originalIndividual[mutationLocation].intValue(), Variables.getScattererPositionIndexMap().length-1);
//            newGeneValue = rand.nextInt(Variables.getScattererPositionIndexMap().length-1) + 1; // (1 to array position-1)
        }
        logger.trace("Individual is being mutated - gene {} being changed from {} to {}", mutationLocation, originalIndividual[mutationLocation], newGeneValue);
        mutatedIndividual[mutationLocation] = newGeneValue;

        ArrayList<Integer> mutatedGenes = new ArrayList<>();
        mutatedGenes.add(mutationLocation);
        // if additonal mutation chance
        double currentAddtionalMutate = Variables.getMutateAddtional();
        while (currentAddtionalMutate > 0) {
            double shouldMutate = rand.nextDouble();
            // if mutate activated
            if (shouldMutate < currentAddtionalMutate) {
                mutationLocation = rand.nextInt(MAXIMUM_SCATTERERS_IN_A_QUADRANT);
                // select a new location to mutate
                while (mutatedGenes.contains(mutationLocation)) {
                    mutationLocation = rand.nextInt(MAXIMUM_SCATTERERS_IN_A_QUADRANT);
                }
                // mutate
                if (Variables.isAllow0Switching()) {
                    newGeneValue = selectRandomGene(originalIndividual[mutationLocation].intValue(), Variables.getScattererPositionIndexMap().length);
                } else {
                    newGeneValue = selectRandomGene(originalIndividual[mutationLocation].intValue(), Variables.getScattererPositionIndexMap().length-1);
                }
                mutatedIndividual[mutationLocation] = newGeneValue;
                mutatedGenes.add(mutationLocation);
            }
            currentAddtionalMutate = currentAddtionalMutate - Variables.getAddtionalMutationDecrease();
        }
        return mutatedIndividual;
    }

    private Integer selectRandomGene(int oldValue, int max) {
        Random rand = new Random();
        int newValue = rand.nextInt(max);
        // while the mutated gene is the same as the original one
        while (oldValue == newValue) {
            newValue = rand.nextInt(max);
        }
        return newValue;
    }

    /**
     * Method to produce a new offspring via random crossover points
     * @param inputPopulation the population to select parents from
     * @param parent1Index index of parent 1 to crossover
     * @param parent2Index index of parent 2 to crossover
     * @return the chromosome of the new child
     */
    protected Integer[] produceOffspring(GenesAndfScoresBank inputPopulation, int parent1Index, int parent2Index) {
        Random rand = new Random();
        // to ensure there is at least 1 element from both parents, the crossover index has to be between 1 and n-2 inclusive.
        // croosover nextEmptyIndex has to be between 1 and the penultimate - aka in array[0: n-1], mutation in range has to be [1:n-2]
        // rand(n-3) +1 == [0:n-3] + 1 = [1:n-2]   --- ensures there will always be at least one element from both parents
        int crossoverIndex = rand.nextInt(utils.USED_SCATTERERS_IN_A_QUADRANT - 3)+1;
        logger.trace("Selected gene {} to mutate", crossoverIndex);
        Integer[] parent1Data = inputPopulation.getInvididual(parent1Index).getGenes();
        Integer[] parent2Data = inputPopulation.getInvididual(parent2Index).getGenes();;
        Integer[] offspringIndividual = new Integer[utils.USED_SCATTERERS_IN_A_QUADRANT];
        int geneIndex = 0;
        // copy parent 1 and parent 2's data
        while (geneIndex < utils.USED_SCATTERERS_IN_A_QUADRANT) {
            // if the crossover nextEmptyIndex is for parent 1
            if (geneIndex <= crossoverIndex) {
                offspringIndividual[geneIndex] = parent1Data[geneIndex];
                // else for parent 2
            } else {
                offspringIndividual[geneIndex] = parent2Data[geneIndex];
            }
            geneIndex ++;
        }
        offspringIndividual = utils.setChromosomeEndToZero(offspringIndividual);
        return offspringIndividual;
    }

    /**
     * Method to produce a new offspring via random crossover points
     * @param inputPopulation the population to select parents from
     * @param parent1Index index of parent 1 to crossover
     * @param parent2Index index of parent 2 to crossover
     * @return the chromosome of the new child
     */
    protected Integer[][] produceMultipleOffspring(GenesAndfScoresBank inputPopulation, int parent1Index, int parent2Index) {
        Random rand = new Random();
        // to ensure there is at least 1 element from both parents, the crossover index has to be between 1 and n-2 inclusive.
        // croosover nextEmptyIndex has to be between 1 and the penultimate - aka in array[0: n-1], mutation in range has to be [1:n-2]
        // rand(n-3) +1 == [0:n-3] + 1 = [1:n-2]   --- ensures there will always be at least one element from both parents
        int crossoverIndex = rand.nextInt(utils.USED_SCATTERERS_IN_A_QUADRANT - 3)+1;
        logger.trace("Selected gene {} to mutate", crossoverIndex);
        Integer[] parent1Data = inputPopulation.getInvididual(parent1Index).getGenes();
        Integer[] parent2Data = inputPopulation.getInvididual(parent2Index).getGenes();;
        Integer[] offspringIndividual = new Integer[utils.USED_SCATTERERS_IN_A_QUADRANT];
        Integer[] offspringIndividual2 = new Integer[utils.USED_SCATTERERS_IN_A_QUADRANT];
        int geneIndex = 0;
        // copy parent 1 and parent 2's data
        while (geneIndex < utils.USED_SCATTERERS_IN_A_QUADRANT) {
            // if the crossover nextEmptyIndex is for parent 1
            if (geneIndex <= crossoverIndex) {
                offspringIndividual[geneIndex] = parent1Data[geneIndex];
                offspringIndividual2[geneIndex] = parent2Data[geneIndex];
                // else for parent 2
            } else {
                offspringIndividual[geneIndex] = parent2Data[geneIndex];
                offspringIndividual2[geneIndex] = parent1Data[geneIndex];
            }
            geneIndex ++;
        }
        offspringIndividual = utils.setChromosomeEndToZero(offspringIndividual);
        offspringIndividual2 = utils.setChromosomeEndToZero(offspringIndividual2);
        return new Integer[][]{offspringIndividual, offspringIndividual2};
    }

    /**
     * Used to carry the roughly the top 10% of the previous population into the next population.
     * @param currentPoplulation
     * @return
     */
    protected GenesAndfScoresBank carryElitePopsToNextGeneration(GenesAndfScoresBank currentPoplulation) {
        GenesAndfScoresBank newPopulation = new GenesAndfScoresBank();
        currentPoplulation.sortByfScore();
        int index;
        for (index = 0; index <= utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION; index++) {
            newPopulation.addNewIndividual(currentPoplulation.getInvididual(index));
        }
        return newPopulation;
    }


    /**
     *  Method used to enact a simulated annealing gate upon a given Integer[]. This means it will take the fscore from
     *  that the testedIndividual creates, then will put it through the Simulated Annealing probability steps.
     * @param testedIndividual
     * @param temperature
     * @return null if the new individual was rejected, otherwise a new individual
     */
    protected IndividualsData simulatedAnnealingGate(Integer[] testedIndividual, double temperature, int generation, int modelNumber) throws Exception{
        Integer[] newIndividualGenes = utils.copyIntegerArray(testedIndividual);
        newIndividualGenes = utils.setChromosomeEndToZero(newIndividualGenes);
        IndividualsData newIndividual = new IndividualsData(0, newIndividualGenes, Double.MAX_VALUE, modelNumber, null, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,false, false,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE);
        // set up simulation
        getAllScatterers();
        setAllScatterers(newIndividualGenes, scattererToIndexMap);
        long time = System.currentTimeMillis();
        boolean runComsolSuccessfully = false;
        while (numberOfExceptions < 5 && !runComsolSuccessfully) {
            try {
                logger.debug("Starting to run individual {}, attempt number {}", modelNumber, numberOfExceptions+1);
                runComsolAndGetMetrics(modelNumber, newIndividual);
                runComsolSuccessfully = true;
            } catch (Exception e) {
                logger.error("Error encountered when running comsol ", e.getMessage());
                logger.error(String.valueOf(e.getStackTrace()));
                logger.error("Happened {} times", numberOfExceptions);
                numberOfExceptions++;
                if (numberOfExceptions >= 5) {
                    throw e;
                }
                if (e.getMessage().equals("There is not enough space on the disk")) {
                    throw e; // if there is a problem with memory space, no amount of retries will help, throw it
                }
            }
        }
        numberOfExceptions = 0; // reset number of exceptions to 0 after clearing
        time = System.currentTimeMillis() - time;
        numberModelsRun++;
        totalTimeForEachModel += time;
        long startTimeImages = System.currentTimeMillis();
        if (RunNSAGA.saveAllImages) {
            comsolModel.saveImagesInt("savedImages_" + newIndividual.getModelNumber() + "__startTime-", newIndividual.getModelNumber(), newIndividual.getGenes(), startDate, comsolModel.getModels360Points());
            numberOfImagesSaved++;
        }
        timeSpentSavingImages+= System.currentTimeMillis() - startTimeImages;

        // if the fscore is less than the current equilibrium, it is a better cloak, so accept
        double fscore = newIndividual.getfScore();
        GenesAndfScoresBank tempBank = new GenesAndfScoresBank();
        tempBank.addNewIndividual(newIndividual);
        saveAnIndividual(tempBank);
        indexModelComplete ++;
        if (fscore < currentEquilibirumScore) {
            logger.trace("New individual is better, score={}, equilibrium={} - temp={}", fscore, currentEquilibirumScore, temperature, Math.exp((currentEquilibirumScore - fscore) / temperature));
        } else { // else fscore is equal to or worse - consider acceptance or rejection.
            Random random = new Random();
            double randValue = random.nextDouble();
            // if random value is less than the exponential of the (current "best" - current/temp), then accept it
            if (randValue < Math.exp((currentEquilibirumScore - fscore) / temperature)) {
                if (modelNumber %100 ==0)
                    logger.debug("Accepted the new individual, score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                else
                    logger.trace("Accepted the new individual, score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
            } else {
                if (modelNumber %100 ==0)
                    logger.debug("Rejected the new individual score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                else
                    logger.trace("Rejected the new individual score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                List<Integer> sortedIndividual = sortGenome(newIndividualGenes);
                if (rejectedHistory.size() == 0)
                    rejectedHistory.add(sortedIndividual);
                else {
//                    int location = listOfListsChecker.binarySearchForLocationToAdd(rejectedHistory, sortedIndividual, 0, rejectedHistory.size() - 1);
                    rejectedHistory.add(sortedIndividual);
                }

//                rejectedHistory.add(sortedIndividual);
//                sortHistory(rejectedHistory);
                return null;
            }
        }
        // save every image if needed, but only save the accepted models
        return newIndividual;
    }

    /**
     * Used as a one off run for a comsol model individual. This allows it to bypass the Simulated annealing gate.
     * @param modelNumber
     * @param individual
     */
    protected double runComsolAndGetMetrics(int modelNumber, IndividualsData individual) throws Exception{
        double fscore = Double.MAX_VALUE;
        try {
            setAllScatterers(individual.getGenes(), scattererToIndexMap);
            logger.debug("Preparing to run model {}", modelNumber);
            comsolModel.runModel(modelNumber);
            logger.debug("Finsihed running model {}", modelNumber);
            if (individual.getModelNumber() != modelNumber) {
                individual.modelNumber = modelNumber;
            }
            double[][] model360 = comsolModel.getModels360Points();
            double sanchisMetric = comsolModel.getResult();
            double mse = calculateError(comsolModel.getModels360Points());
            double rmse = Math.sqrt(mse);
            double sanchisMetric2 = 1 - sumSquares(model360) / sumSquares(centralObjectOnly360Pressure);
            double sanchisMetric3 = sumSquares(model360) / sumSquares(centralObjectOnly360Pressure); // sum of 360 points with cloak / sum of 360 points with the central object.
            double sanchisMetric4 = sumSquares(model360);
            double sanchisMetricVsPerfect = 1 - sumSquares(model360) / sumSquares(emptyModel360Pressure);
            double sanchezMetric1 = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(centralObjectOnly360Pressure)) ), -1);
            double sanchezMetricVsperfect = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(emptyModel360Pressure)) ), -1);
            double sanchezMetric2 = sumAbsolutes(model360);
            double normalisedMSE = normalisedMSE(model360);
            double l2Norm = calculateL2Norm(model360[0]);
            individual.setArray360Points(comsolModel.getModels360Points());
            individual.setMse(calculateError(comsolModel.getModels360Points()));
            individual.setSanchisMetric(sanchisMetric);
            individual.setSanchisMetricH(sanchisMetric3);
            individual.setSanchisMetricSimple(sanchisMetric4);
            individual.setSanchezMetric(sanchezMetric1);
            individual.setSanchezMetricSimple(sanchezMetric2);
            individual.setNormalisedMSE(normalisedMSE);
            individual.setSanchisPerfectCloak(sanchisMetricVsPerfect);
            individual.setSanchezPerfectCloak(sanchezMetricVsperfect);
            individual.setfScore(utils.selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm));
            individual.setRmse(rmse);
            double[] model360SubArray = Arrays.copyOfRange(model360[0], 1, model360[0].length);
            double[][] model360SubArray2D = new double[1][360];
            model360SubArray2D[0] = model360SubArray;
            individual.setMinPressure(getMinOfModel360(model360SubArray));
            individual.setMaxPressure(getMaxOfModel360(model360SubArray));
            individual.setAveragePressure(sum(model360)/360);
            individual.setAbsoluteAveragePressure(sumAbsolutes(model360)/360);
            individual.setL2Norm(l2Norm);
//            System.out.println("    MSE metric       -> 0 (1) = " + mse);
//            System.out.println("Sanchis metric   -> 1 (2) = " + sanchisMetric2);
//            System.out.println("Sanchis metric   -> 0 (3) = " + sanchisMetric3);
//            System.out.println("Sanchis metric R -> 0 (3) = " + sanchisMetric);
//            System.out.println("Sanchis metric   -> 0 (4) = " + sanchisMetric4);
//            System.out.println("Sanchez metric   -> 1 (5) = " + sanchezMetric1);
//            System.out.println("Sanchez metric   -> 0 (6) = " + sanchezMetric2 + "\n\n");
            fscore = utils.selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm);
        } catch (Throwable e) {
            StringBuilder sb = new StringBuilder();
            for (Integer gene : individual.getGenes()) {
                sb.append(Integer.toString(gene)+", ");
            }
            logger.error("The combination that failed is: "+sb);
            e.printStackTrace();
            throw e;
        }
        return fscore;
    }

    /**
     * Method to set all the scatterer coordinates, regardless of which kind of scatterer it is.
     * @param testedIndividual
     * @param scattererToIndexMap
     */
    protected void setAllScatterers(Integer[] testedIndividual, Position[] scattererToIndexMap) {
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;
            case "Circle":
                comsolModel.setAllCircularScatterers(testedIndividual, scattererToIndexMap);
                break;
            default:
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;

        }

    }


    protected double temperatureFunction(double temperature, int currentAnnealingStep) {
         switch (Variables.getTempFunction()) {
             case "raghu":
                 return simulatedAnnealingTechniques.initialTempToStepOverMax(currentAnnealingStep, Variables.getMaximumTemperatureSteps(), Variables.getInitialTemperature());
             case "decreaseByControlAmount":
                 return simulatedAnnealingTechniques.decreaseByControlAmount(temperature, currentAnnealingStep, Variables.getMaximumTemperatureSteps(), Variables.getInitialTemperature());
             case "coolingRate":
                 return simulatedAnnealingTechniques.decreaseByCoolingRate(temperature, 0.2);
             default:
                 return simulatedAnnealingTechniques.initialTempToStepOverMax(currentAnnealingStep, Variables.getMaximumTemperatureSteps(), Variables.getInitialTemperature());
         }
//        return simulatedAnnealingTechniques.decreaseByControlAmount(temperature, currentAnnealingStep, MAXIMUM_TEMPERATURE_STEPS, utils.INITIAL_TEMPERATURE);
//        return simulatedAnnealingTechniques.decreaseByLogs(NsagaUtils.INITIAL_TEMPERATURE, currentAnnealingStep);
    }

    /**
     * Method to get all the scatterers of the right type from the comsol model.
     * @return
     */
    protected ArrayList<String> getAllScatterers(){
        ArrayList<String> returnList = null;
        switch (shapeOfScatterer) {
            case "Square":
                returnList = comsolModel.getAllSquareScatterers();
                break;
            case "Circle":
                returnList = comsolModel.getAllCircularScatterers();
                break;
            default:
                returnList = comsolModel.getAllSquareScatterers();
                break;

        }
        return returnList;
    }

    /**
     * Method to copy the current history (including the current progress of nextPopulation) into the output folders.
     * This will allow conistent and constant storing in case of any failures.
     * @param savePath
     * @param iteration
     * @throws IOException
     */
    protected void copyCurrentHistory(String savePath, int iteration) throws IOException {
        Path path = null;
        try {
            File directory = new File(savePath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            path = Paths.get(savePath, "FscoresGeneBank_" + iteration + ".csv");
            // copy the current active to the save file, and append the next population to it as well.
            Files.copy(Paths.get(Variables.getActiveGeneHistoryCSVLocation() ), path, copyOptions);
            boolean savedFile = utils.appendGeneHistoryToFile(path.toString().substring(0, path.toString().length() - 4), nextPopulation);
            if (!savedFile) {
                logger.error("Problem while appending the data");
            }
        } catch (FileSystemException e) {
            logger.error("File system exception. Trying to copy file {} to file {}\n", Variables.getActiveGeneHistoryCSVLocation() + ".csv", path, e);
            throw e;
        }
    }

    /**
     * Method to append the the current population to the end of the active history file
     * @return true if saving was successful.
     */
    protected boolean saveToActiveHistory() {
        boolean savedFile = utils.appendGeneHistoryToFile(Variables.getActiveGeneHistoryCSVLocation().substring(0, Variables.getActiveGeneHistoryCSVLocation().length()-4), currentPopulation);
        utils.runRecoveryCleanUp();
        if (!savedFile) {
            logger.error("Problem while appending the data");
        }
        return savedFile;
    }

    protected boolean saveAnIndividual(GenesAndfScoresBank population) {
        boolean savedFile = utils.appendGeneHistoryToFile(Variables.getCompleteHistoryFile().substring(0, Variables.getCompleteHistoryFile().length()-4), population);
        utils.runRecoveryCleanUp();
        if (!savedFile) {
            logger.error("Problem while appending the data");
        }
        return savedFile;
    }

    protected boolean existsInHistory(Integer[] inputCombination) {
        return true;
    }

    protected boolean addToHistory(IndividualsData individual) {
        List<Integer> sortedIndividual = sortGenome(individual.getGenes());
        boolean added = combinationHistory.add(sortedIndividual);
        return added;
//        if (combinationHistory.size() == 0)
//
//        else {
//            int location = listOfListsChecker.binarySearchForLocationToAdd(combinationHistory, sortedIndividual, 0, combinationHistory.size() - 1);
//            combinationHistory.add(location, sortedIndividual);
    }

    protected boolean saveCombinationHistory(GenesAndfScoresBank historyBank) {
        boolean savedFile = utils.appendGeneHistoryToFile(Variables.getActiveGeneHistoryCSVLocation().substring(0, Variables.getActiveGeneHistoryCSVLocation().length()-4), historyBank);
        if (!savedFile) {
            logger.error("Problem while appending the data");
        }
        return savedFile;
    }


    /**
     *
     * @param inputGenome
     * @return 0 if genome is not in any history, 1 if in combination history, 2 if in the rejected history.
     */
    protected int checkHistory(Integer[] inputGenome) {
        List<Integer> sortedGenome = sortGenome(inputGenome);

        if (combinationHistory.contains(sortedGenome)) {
            return 1;
        }

        if (rejectedHistory.contains(sortedGenome)) {
            return 2;
        }

//        int locationSorted = listOfListsChecker.searchIfMissingOrInThere(combinationHistory, sortedGenome, 0, combinationHistory.size()-1);
//        if (locationSorted >= 0) {
//            numberOfCheckhistoryRegjections ++;
//            return true;
//        }
//
//        if (rejectedHistory.size() != 0) {
//            listOfListsChecker.searchIfMissingOrInThere(rejectedHistory, sortedGenome, 0, rejectedHistory.size() - 1);
//            if (locationSorted >= 0) {
//                numberOfCheckhistoryRegjections++;
//                return true;
//            }
//        }


        return 0;
    }


    protected void sortHistory(List<List<Integer>> history) {
        listOfListsChecker.sortListsOfLists(history);
    }

    /**
     * Method to accept and possibly reject individuals based on if they exist in the previous history.
     * @param individual
     * @return true if there are no duplicates in the history, false if there are.
     */
    protected int addIndividualWithCheck(IndividualsData individual) {
        List<Integer> sortedIndividual = sortGenome(individual.getGenes());
        int historyCollision = checkHistory(individual.getGenes());
        // if  there are no duplicates found throughtout the history, then accept
        if (historyCollision == 0) {
            nextPopulation.addNewIndividual(individual);
            addToHistory(individual);
            return historyCollision;
        }

        // if any duplicates are found, reject it.
        return historyCollision;
    }

    /**
     * Method to accept and possibly reject individuals based on if they exist in the previous history.
     * @param individual
     * @return true if there are no duplicates in the history, false if there are.
     */
    protected void addIndividual(IndividualsData individual) {
        // if  there are no duplicates found throughtout the history, then accept
        nextPopulation.addNewIndividual(individual);
        addToHistory(individual);
    }

    protected List<Integer> sortGenome(Integer[] inputGenome) {
        inputGenome = utils.setChromosomeEndToZero(inputGenome);
        List<Integer> sortedIndividual = new ArrayList<>(inputGenome.length);
        // add each gene to sorted
        for (Integer integer : inputGenome) {
            sortedIndividual.add(integer);
        }
        //sort
        Collections.sort(sortedIndividual);
        return sortedIndividual;
    }

    protected double calculateError(double[][] model360) {
        int i;
        double meanSquareError = 0.0;
        double sumOfDifferences = 0.0;
        double sumOfDifferencesSquared = 0.0;//i = 1, as the first column of the table is set to the frequency, not needed. However it should be independant of it - 20k - 20k = 0, therefore should have no impact on error rate
        for (i=1; i<model360[0].length; i++) {
            sumOfDifferences += (model360[0][i] - centralObjectOnly360Pressure[0][i]);
            sumOfDifferencesSquared += Math.pow(model360[0][i] - centralObjectOnly360Pressure[0][i], 2);
        }
        meanSquareError = sumOfDifferencesSquared/model360.length;

        return meanSquareError;
    }

    protected double sum(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += (model360[0][i]);
        }
        return sum;
    }

    protected double sumAbsolutes(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.abs(model360[0][i]);
        }
        return sum;
    }

    protected double sumSquares(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.pow((model360[0][i]), 2);
        }
        return sum;
    }

    /**
     * https://rem.jrc.ec.europa.eu/RemWeb/atmes2/20b.htm
     * @param model360
     * @return
     */
    protected double normalisedMSE(double[][]model360) {
        double averageBase = sum(emptyModel360Pressure)/emptyModel360Pressure.length;
        double averageOfModel = sum(model360)/model360.length;
        double squareSumOfDifferencesOverMeans = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            // sum, [(P-m)**2 / (Paverage * MAverage)]
            squareSumOfDifferencesOverMeans += Math.pow(model360[0][i] - emptyModel360Pressure[0][i],2) / (averageBase * averageOfModel);
        }
        return (1/model360.length) * squareSumOfDifferencesOverMeans;
    }

    protected int getCombinationHistoryLength() {
        return combinationHistory.size();
    }


    protected void outputCoordinates(IndividualsData newIndividual) {
        for (Integer index : newIndividual.getGenes()) {
            double proposedXMM = Variables.getScattererPositionIndexMap()[index].getX()*1000;  // debugging purposes
            double proposedYMM = Variables.getScattererPositionIndexMap()[index].getY()*1000;
            double radiusMM =  Variables.getRadiusScatterer() * 1000;
            System.out.println("x: " + proposedXMM + "  y: "+ proposedYMM + "  rad: " + radiusMM);
        }
        int breakx = 0;
    }

    protected String outputGenes(IndividualsData newIndividual) {
        StringBuilder sb = new StringBuilder("   ");
        for (Integer index : newIndividual.getGenes()) {
            sb.append(" "+index);
        }
        return sb.toString();
    }

    protected String outputGenes(List<Integer> newIndividual) {
        StringBuilder sb = new StringBuilder("   ");
        for (Integer index : newIndividual) {
            sb.append(" "+index);
        }
        return sb.toString();
    }

    protected String outputGenes(Integer[] newIndividual) {
        StringBuilder sb = new StringBuilder("   ");
        for (Integer index : newIndividual) {
            sb.append(" "+index);
        }
        return sb.toString();
    }

    protected void historyLogCheck(int loopsEntered, long startTime) {
        totalTimeForEachHistoryCheck += System.currentTimeMillis() - startTime;
        long averageTimeForHistory = totalTimeForEachHistoryCheck / Math.max(loopsEntered,1);
        if (loopsEntered %10 == 0 && loopsEntered < 80 ){
            logger.info("    Average time to run a history check {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForHistory/1000, averageTimeForHistory/60000, averageTimeForHistory/3600000d, totalTimeForEachHistoryCheck/1000, totalTimeForEachHistoryCheck/60000, totalTimeForEachHistoryCheck/3600000d);
        } else if (loopsEntered%100 == 0 && loopsEntered >= 100 && loopsEntered < 1000) {
            logger.info("    Average time to run a history check {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForHistory/1000, averageTimeForHistory/60000, averageTimeForHistory/3600000d, totalTimeForEachHistoryCheck/1000, totalTimeForEachHistoryCheck/60000, totalTimeForEachHistoryCheck/3600000d);
        } else if (loopsEntered > 1000 &&  loopsEntered%1000 == 0) {
            logger.info("    Average time to run a history check {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForHistory/1000, averageTimeForHistory/60000, averageTimeForHistory/3600000d, totalTimeForEachHistoryCheck/1000, totalTimeForEachHistoryCheck/60000, totalTimeForEachHistoryCheck/3600000d);
        }
    }

    protected void saveLogCheck(int loopsEntered, long startTime) {
        numberOfTimesSaved +=1;
        totalTimeSpentSaving += System.currentTimeMillis() - startTime;
        long averageTimeForHistory = totalTimeSpentSaving / Math.max(loopsEntered, 1);
        logger.info("    Average time to save files {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForHistory/1000, averageTimeForHistory/60000, averageTimeForHistory/3600000d, totalTimeSpentSaving/1000, totalTimeSpentSaving/60000, totalTimeSpentSaving/3600000d);
    }

    protected void gasaTimeLogCheck(int totalModelsConsidered, long startTime, int loopsEntered) {
        totalTimeForEachModel += System.currentTimeMillis() - startTime;
        long averageTimeForModel = totalTimeForEachModel / Math.max(totalModelsConsidered,1);
        if (loopsEntered %10 == 0 && loopsEntered < 80 ){
            logger.info("    Average time to run a comsol model {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForModel/1000, averageTimeForModel/60000, averageTimeForModel/3600000d, totalTimeForEachModel/1000, totalTimeForEachModel/60000, totalTimeForEachModel/3600000d);
        } else if (loopsEntered%100 == 0 && loopsEntered >= 100 && loopsEntered < 1000) {
            logger.info("    Average time to run a comsol model {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForModel/1000, averageTimeForModel/60000, averageTimeForModel/3600000d, totalTimeForEachModel/1000, totalTimeForEachModel/60000, totalTimeForEachModel/3600000d);
        } else if (loopsEntered > 1000 &&  loopsEntered%1000 == 0) {
            logger.info("    Average time to run a comsol model {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForModel/1000, averageTimeForModel/60000, averageTimeForModel/3600000d, totalTimeForEachModel/1000, totalTimeForEachModel/60000, totalTimeForEachModel/3600000d);
        }
        long totalTime = System.currentTimeMillis() - timeStartedNsagaLoop;
        logger.info("Time taken so far is {}s == {}m == {}h", totalTime/1000, totalTime/60000, totalTime/3600000d); // continuously logged to reassure that the program is not stuck - should run every 3 minutes maximum
    }

    protected void completeTimeLogCheck(int loopsEntered) {
        long averageTimeForModel = totalTimeForEachModel / Math.max(totalModelsEvaluatedByComsol,1);
        long averageTimeForHistory = totalTimeForEachHistoryCheck / Math.max(totalHistoryChecks,1);
        long averageTimeForSaving = totalTimeSpentSaving / Math.max(numberOfTimesSaved, 1);
        long averageImageSavingTime = timeSpentSavingImages / Math.max(numberOfImagesSaved, 1);
        long averageTimeSpentGenerating = combinationGenerationTime / Math.max(combosGenerated, 1);
        long currentTimeSpent = System.currentTimeMillis() - timeStartedNsagaLoop;
        long averageCollisionTime = collisionTime/ Math.max(numCollisionChecks, 1);
        logger.info("");
        logger.info("    COMSOL TIMES:         average {}s == {}m == {}h; total time spent {}s == {}m == {}h", averageTimeForModel/1000, averageTimeForModel/60000, averageTimeForModel/3600000d, totalTimeForEachModel/1000, totalTimeForEachModel/60000, totalTimeForEachModel/3600000d);
        logger.info("    HISTORY TIMES:        average = {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForHistory/1000, averageTimeForHistory/60000, averageTimeForHistory/3600000d, totalTimeForEachHistoryCheck/1000, totalTimeForEachHistoryCheck/60000, totalTimeForEachHistoryCheck/3600000d);
        logger.info("    CSV Saving TIMES:     average = {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeForSaving/1000, averageTimeForSaving/60000, averageTimeForSaving/3600000d, totalTimeSpentSaving/1000, totalTimeSpentSaving/60000, totalTimeSpentSaving/3600000d);
        logger.info("    Image saving TIMES:   average = {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageImageSavingTime/1000, averageImageSavingTime/60000, averageImageSavingTime/3600000d, timeSpentSavingImages/1000, timeSpentSavingImages/60000, timeSpentSavingImages/3600000d);
        logger.info("    Combo gen TIMES:      average = {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageTimeSpentGenerating/1000, averageTimeSpentGenerating/60000, averageTimeSpentGenerating/3600000d, combinationGenerationTime/1000, combinationGenerationTime/60000, combinationGenerationTime/3600000d);
        logger.info("    Collison checks TIME: average = {}s == {}m == {}h, total time spent {}s == {}m == {}h", averageCollisionTime/1000, averageCollisionTime/60000, averageCollisionTime/3600000d, collisionTime/1000, collisionTime/60000, collisionTime/3600000d);
        logger.info("    Copying next population = {}s == {}m == {}h;  garbage collection time = {}s == {}m == {}h", timeSpentCopyingNextPopulationOver/1000, timeSpentCopyingNextPopulationOver/60000, timeSpentCopyingNextPopulationOver/3600000d, garbageCollectionChecksTime/1000, garbageCollectionChecksTime/60000, garbageCollectionChecksTime/3600000d);
        logger.info("    TOTAL TIME SPENT:     {}s == {}m == {}h", currentTimeSpent/1000, currentTimeSpent/60000, currentTimeSpent/3600000d);
        long summedTime = totalTimeForEachModel+ totalTimeForEachHistoryCheck + totalTimeSpentSaving + collisionTime + combinationGenerationTime;
        summedTime += timeSpentSavingImages;
//        if (false) {
//            summedTime = summedTime - timeSpentSavingImages; // if saving all images, then image saving is contained within comsol runs, so it is duplicated time. Take
//        } else {
//            summedTime += timeSpentSavingImages;
//        }
        summedTime += garbageCollectionChecksTime + timeSpentCopyingNextPopulationOver;
        logger.info("    SUMMED TIME SPENT:     {}s == {}m == {}h", summedTime/1000, summedTime/60000, summedTime/3600000d);
        long timeDiff = currentTimeSpent - summedTime;
        logger.info("    LOST TIME:     {}s == {}m == {}h", timeDiff/1000, timeDiff/60000, timeDiff/3600000d);
        logCounters(loopsEntered);
    }

    protected double getMinOfModel360(double[] model360) {
        double min = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] < min) {
                min = model360[i];
            }
        }
        return min;
    }

    protected double getMaxOfModel360(double[] model360) {
        double max = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] > max) {
                max = model360[i];
            }
        }
        return max;
    }

    /**
     *
     * @param individual
     * @param loopsEntered
     * @param temperature
     * @return -3 if rejected due to collision, -2 if dupliacte in previous rejections, -1 if duplicate in previous successes, 0 if considered but SA rejected, 1 if successfully added
     */
    protected int checkIndividual(Integer[] individual, int loopsEntered, double temperature) throws Exception{
        long startTimeForNextCounter = 0;
        startTimeForNextCounter = System.currentTimeMillis();
        if (RunNSAGA.runCollsionChecks && utils.runCollisionCheckRandomIndividual(individual, scattererRadius, scattererToIndexMap)) {
            // collision has occured and not allowed, thus reject solution, without any increment.
            logger.trace("Collision checks have failed, there are collisions, rejecting");
            timeSinceLastModel++;
            collisionRejects++;
            numCollisionChecks++;
            collisionTime += System.currentTimeMillis() - startTimeForNextCounter;
            return -3;
        }
        collisionTime += System.currentTimeMillis() - startTimeForNextCounter;
        // returns true if there is a copy already in existance
        startTimeForNextCounter = System.currentTimeMillis();
        int checkedHistory = checkHistory(individual);
        totalHistoryChecks++;
        if (checkedHistory != 0) {
            logger.trace("Copy of new offspring found in history, continuing.");
            timeSinceLastModel++;
            historyRejections ++;
            if (checkedHistory == 2)
                historyRejectionsFromRejectedList++;
            historyLogCheck(loopsEntered, startTimeForNextCounter);
            return -1 * checkedHistory;
        }
        historyLogCheck(loopsEntered, startTimeForNextCounter);

        startTimeForNextCounter = System.currentTimeMillis();
        // offspring has passed anti-collision code
        IndividualsData newIndividual = simulatedAnnealingGate(individual, temperature, acceptedModelIndex, numberModelsRun);
        totalModelsEvaluatedByComsol++;
        // if offspring is considered good by SA, then add to next population, else ignore it
        if (newIndividual == null) {
            logger.trace("Generated individual was rejected by the SA gate");
            consideredAndRejected ++;
            return 0;
        } else {
            addIndividual(newIndividual);
            timeSinceLastModel = 0;
            //outputCoordinates(newIndividual); // method to check for coordinates output
            acceptedModelIndex++;
        }
        gasaTimeLogCheck(totalModelsEvaluatedByComsol, startTimeForNextCounter, loopsEntered);
        return 1;
    }

    protected void resetTotalComsolTimes() {
        this.totalTimeForEachHistoryCheck = 0;
        this.totalTimeSpentSaving = 0;
        this.totalTimeForEachModel = 0;
        this.combinationGenerationTime = 0;
        timeSpentSavingImages = 0;
        collisionTime = 0;
    }

    protected void logCounters(int loopsEntered) {
        logger.info("  Loops entered = {}, with {} combinations created. {} were accepted.  Of the {} " +
                        "rejected, {} of them were run by comsol, {} of them failed on history, {} failed " +
                        "on collisions. -- total comsol runs = {}", loopsEntered, combosGenerated, acceptedModelIndex,
                combosGenerated-acceptedModelIndex,consideredAndRejected, historyRejections,
                collisionRejects, totalModelsEvaluatedByComsol);
    }

    public IndividualsData runSingleIndividual(Integer[] individual) throws Exception {
        return simulatedAnnealingGate(individual, 1.0, 0, 0);
    }

    private double calculateL2Norm(double[] model360) {
        // F = ||W-U||/||V-U|| where U = empty region vector, V = object field vector, W = cloak pressure vector
        // U = emptyModel360Pressure, V = Central object only 360 pressure, W = input model 360 vector
        double[] numeratorArray = subtractArrays(model360, emptyModel360Pressure[0]);
        double[] denominatorArray = subtractArrays(centralObjectOnly360Pressure[0], emptyModel360Pressure[0]);

        double numerator = abosoluteValueDoubleArray(numeratorArray);
        double denominator = abosoluteValueDoubleArray(denominatorArray);

        return numerator/denominator;
    }

    private double[] subtractArrays(double[] array1, double[] array2) {
        double[] tempArray  = new double[array1.length];
        for (int i=0; i < array1.length; i++) {
            tempArray[i] = array1[i]-array2[i];
        }
        return  tempArray;
    }

    private double abosoluteValueDoubleArray(double[] array) {
        double sum = 0;
        for (int i=0; i < array.length; i++) {
            sum += array[i] * array[i];
        }
        return Math.sqrt(sum);
    }


}
