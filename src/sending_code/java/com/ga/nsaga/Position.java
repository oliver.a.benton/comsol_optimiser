package com.ga.nsaga;

public class Position {

    private static int indexSeed = 0;
    private int index;
    private double theta, radius, x, y;
    boolean inUse;


    public Position(double theta, double radius) {
        this.theta = theta;
        this.radius = radius;
        this.x = radius * Math.cos(theta);
        this.y = radius * Math.sin(theta);
        this.inUse = false;
        this.index = indexSeed;
        indexSeed ++;
    }

    public int getIndex() {
        return index;
    }

    public double getTheta() {
        return theta;
    }

    public double getRadius() {
        return radius;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isInUse() {
        if (this.index == 0)
            // setup so that index 0 (hidden scat) is always not in use
            return false;
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }
}
