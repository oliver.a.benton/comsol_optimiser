package com.ga.nsaga;

import com.comsolModelInterface.ComsolModelInterface;
import com.Variables;

import java.util.ArrayList;

public class IndexMapGenerator {

    ComsolModelInterface comsolModel;
    double quarterOfAScatRadius;
    NsagaUtils utils = new NsagaUtils();

    public IndexMapGenerator(ComsolModelInterface comsolModel) {
        this.comsolModel = comsolModel;
        quarterOfAScatRadius = Variables.getRadiusScatterer()/4;
    }

    private void saveMap(String[][] map, String fileLocation) {
        System.out.println("Saving map");
        utils.saveToExcel(map, fileLocation, false);
        System.out.println("Map Saved");
    }

    public void generateMapAndSave(String fileLocaiton) {
        String[][] map = createMap();
        saveMap(map, fileLocaiton);
    }

    private String[][] createMap() {
        int scatIndex = 1;
        double lambda = Double.parseDouble(comsolModel.getDoubleParameter("lam"));
        double scatMaxDistance = Double.parseDouble(comsolModel.getDoubleParameter("scatterers_max_distance"));
        double scatMinDistance = Double.parseDouble(comsolModel.getDoubleParameter("radius_central_pipe")) + Variables.getRadiusScatterer()*1.2;

        int numberOfLayers = (int) Math.floor((scatMaxDistance - scatMinDistance) / (Variables.getRadiusScatterer()/Variables.getMappedLayerDensity()));
        double[] layerRadi = new double[numberOfLayers];
        int layerNumber;
        int currentScatNumber;
        ArrayList<ArrayList> wholeList = new ArrayList<>();
        ArrayList<ScattererInfo> currentList = new ArrayList<>();
        currentList.add(new ScattererInfo(0, 0, 0, 0, 0));
        wholeList.add(currentList);
        for (layerNumber = 0; layerNumber < numberOfLayers; layerNumber++) {
            double layerRadius; // =(R_c+(1.2*scat_radius))*unit_multiplier
            if (layerNumber == 0) {
                layerRadius = scatMinDistance;
            } else {
                layerRadius = layerRadi[layerNumber-1] + Variables.getRadiusScatterer()/Variables.getMappedLayerDensity(); // =(previous+ mapped layer density - default is scat_radius/4
            }
            layerRadi[layerNumber] = layerRadius;

            double radiansCheck = Math.sin(Math.PI/2);
            double degreesCheck = Math.sin(90);

            double minimumAngle = (Variables.getMappedScatterRadiusMultiplier() * Variables.getRadiusScatterer()/layerRadius) ; //(((radiusMultiplier*r_sc)/layerRadius))
            int numberOfScatterersInLayer = (int) Math.floor((Math.PI/2) / minimumAngle);  // 90/minimumAngle
//            double usedAngleDegrees = 90/numberOfScatterersInLayer;
            double usedAngleRadians = (Math.PI/2) / numberOfScatterersInLayer;
//            System.out.println("Calculated "+ numberOfLayers+ " layers and " + usedAngleRadians + " radians");
            currentList = new ArrayList<>();
            for (currentScatNumber = 0; currentScatNumber < numberOfScatterersInLayer; currentScatNumber++) {
                // =INDEX(Act_Theta_Radians, MATCH(A3,Angular_number_Layer,0))*E3 = layer radius
                ScattererInfo newScatterer = new ScattererInfo(layerNumber+1, layerRadius, scatIndex,  usedAngleRadians * currentScatNumber, currentScatNumber);
                scatIndex++;
                currentList.add(newScatterer);

            }
            //
            ScattererInfo newScatterer = new ScattererInfo(layerNumber+1, layerRadius, scatIndex,  Math.PI/2, currentScatNumber);
            scatIndex++;
            currentList.add(newScatterer);
            wholeList.add(currentList);
        }
        System.out.println("Transferrring map to array");
        String[][] map = new String[scatIndex][5];
        int currentIndex = 0;
        for (ArrayList<ScattererInfo> scatList : wholeList) {
            for (ScattererInfo scat : scatList) {
                map[currentIndex][0] = String.valueOf(scat.getLayer());
                map[currentIndex][1] = String.valueOf(scat.getRadius());
                map[currentIndex][2] = String.valueOf(scat.getIndex());
                map[currentIndex][3] = String.valueOf(scat.getAngleRadians());
                map[currentIndex][4] = String.valueOf(scat.getNumberInLayer());
                currentIndex ++;
            }
        }

        return map;
    }
}

class ScattererInfo {
    int layer;
    double radius;
    int index;
    double angleRadians;
    int numberInLayer;

    public ScattererInfo(int layer, double radius, int index, double angleRadians, int numberInLayer) {
        this.layer = layer;
        this.radius = radius;
        this.index = index;
        this.angleRadians = angleRadians;
        this.numberInLayer = numberInLayer;
    }

    public int getLayer() {
        return layer;
    }

    public double getRadius() {
        return radius;
    }

    public int getIndex() {
        return index;
    }

    public double getAngleRadians() {
        return angleRadians;
    }

    public int getNumberInLayer() {
        return numberInLayer;
    }
}


class Scatterer {

    double xPosition;
    double yPosition;
    int circleNumber;

    public Scatterer(double xPosition, double yPosition, int circleNumber) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.circleNumber = circleNumber;
    }

    public double getxPosition() {
        return xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public int getCircleNumber() {
        return circleNumber;
    }
}

