package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.GlobalUtils;
import com.Variables;
import com.comsol.model.util.ModelUtil;
import com.ga.SimulatedAnnealingTechniques;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import static com.ga.nsaga.RunNSAGA.SCATTERER_POSITION_INDEX_MAP;
import static com.ga.nsaga.RunNSAGA.stillInitialising;

/**
 * Utilities file, used to store constants and common methods that are used by all classes.
 */
public class NsagaUtils {

    public static final int INTIAL_ANNEALING_STEP = 0; // variable to control the intial starting step - can be changed to resume previous runs.
    final static Logger logger = (Logger) LoggerFactory.getLogger(NsagaUtils.class.getName());
    public final static int MAXIMUM_SCATTERERS_IN_A_QUADRANT = 35; // variable to show the maximum number of scatterers in a quadrant - 30 = 120 scat total, 35 = 140 scat total
    public final static int USED_SCATTERERS_IN_A_QUADRANT = MAXIMUM_SCATTERERS_IN_A_QUADRANT; // used to control the active number of scatterers
    public static final int MAX_POPULATION_SIZE = 20;    // DP; There should be enough diversity in our population space to form new offsprings; hence set it high; else 50/40 could be used;
    //    public static int max_no_scat_pos = 4380;  // The maximum number of scatterer positions... how is this calculated?
    public static final int totalAnnealingStepsAllowed = 30;
    public static final int maxNumPopulationGenerations = 20;
    public static final int ELITE_INDIVIDUALS_FOR_NEXT_POPULATION = (MAX_POPULATION_SIZE / 10) - 1; // set the number of "elite" entries to automatically copy over == (pop_size/10) -1
    public static final double initialMutationProbability = 0.2;
    public static final int EQUILIBRIUM_POSITION_INDEX = MAX_POPULATION_SIZE / 10;
    public static final double INITIAL_TEMPERATURE = 0.7; // the initial temperature is the starting temperature that every simulation starts at
    public static double STARTING_TEMPERATURE = initialTemperatureFinder();  // the starting temperature is the temperature that the algorithm will start at. This is the temperature to adjust when resuming a previous run.
    public static final int MAXIMUM_TEMPERATURE_STEPS = 30;
    public static final int GENERATION_SAVE_COUNTER = 100;
    public static final int empty_area_run = -999;
    public static final int no_scatterer_run = -998;
    public static final String USED_ERROR_METRIC = "sanchisMetric";
    public static final boolean ALLOW_0_SWITCHING = true; // value to control if scatterers are allowed to switch down to the hidden layer or not - set to true to allow them to
    private static final boolean save360Points = true;
    protected long timeSpentClearningRecoveries;


    public NsagaUtils() {
        logger.setLevel(Level.DEBUG);
    }

    /**
     * this function is used to retrieve all the possible scatterer positions encoding
     * and the corresponding positions in the permissible space and store it in variable called
     * data records;
     * <p>
     * This shall be called once, initially from the main function before the algorithm starts;
     */
    public Object[] readCSVScattererPolar(String input_file_name) {
        Object[] data_records = null;
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(input_file_name))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
                data_records = records.toArray();
            }
        } catch (FileNotFoundException e) {
            logger.error("No file found ", e);
        } catch (IOException e) {
            logger.error("IO Exception", e);
        }
        return data_records;
    }

    private static double initialTemperatureFinder() {
        SimulatedAnnealingTechniques simulatedAnnealingTechniques = new SimulatedAnnealingTechniques();
        double temperature = INITIAL_TEMPERATURE;
        for (int i = 0; i < INTIAL_ANNEALING_STEP; i++) {
            temperature = simulatedAnnealingTechniques.decreaseByCoolingRate(temperature, 0.25);
        }
        return temperature;
    }

    /**
     * Method that uses the value stored in USED_ERROR_METRIC to decide which of the values to return as the used fscore.
     *
     * @param sanchisMetric
     * @param mse
     * @param sanchisMetric2
     * @param sanchisMetric3
     * @param sanchisMetric4
     * @param sanchezMetric1
     * @param sanchezMetric2
     * @return
     */
    public double selectErrorMetric(double mse, double sanchisMetric, double sanchisMetric2, double sanchisMetric3, double sanchisMetric4, double sanchezMetric1, double sanchezMetric2, double l2Norm) {
        switch (Variables.getUsedErrorMetric().toLowerCase()) {
            case "mse":
                return mse;
            case "sanchis":
                return sanchisMetric;
            case "sanchisMetric2":
                return sanchisMetric2;
            case "sanchezMetric3":
                return sanchisMetric3;
            case "sanchezMetric4":
                return sanchisMetric4;
            case "sanchezMetric1":
                return sanchezMetric1;
            case "sanchezMetric2":
                return sanchezMetric2;
            case "l2Norm":
                return l2Norm;
            default:
                return mse;
        }
    }


    /**
     * Used to load the parameters that the comsolModel is using from a csv file read in
     * as the absolute csvPath - returns a 2d string array, where [0][all] is the
     */
    public String[][] loadParamsFromCSV(String csvPath, boolean hasTitle) {
        String[][] returnArray = new String[countCsvRows(csvPath)][countCsvColumns(csvPath)];
        BufferedReader reader = null;
        int i = 0;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            // Reading Records One by One in a String array
            String line = reader.readLine();
            // loop until all lines are read

            while ((line != null) && !line.equals(",,,,,,")) {
                returnArray[i] = line.split(",");
                i++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while updating params {} - {}", ioe.getClass().getName(), ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while loading parameters from a csv");
                    logger.error("CONTEXT", e);
                }
            }
        }
        if (hasTitle)
            return Arrays.copyOfRange(returnArray, 1, i);
        return Arrays.copyOfRange(returnArray, 0, i);
    }

    /**
     * Method to count the number of columns in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of columns
     */
    private int countCsvColumns(String csvPath) {
        int inputCounter = 0;
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            String[] returnArray = line.split(",");
            inputCounter = returnArray.length;
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while counting CSV columns");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return inputCounter;
    }

    /**
     * Method to count the number of rows in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of rows
     */
    private int countCsvRows(String csvPath) {
        int rowCounter = 0;
        BufferedReader reader = null;
        try {
            // D:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\scatterer_mesh_ultrathin.csv
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            while (line != null) {
                rowCounter++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("IO exception while counting the number of CSV rows");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return rowCounter;
    }

    /**
     * Used to generate a random inital population - used for the first run.
     *
     * @return the initial population
     */
    public static GenesAndfScoresBank generatePopulationSets(double radiusScatterer) {
        GenesAndfScoresBank generatedPopulation = new GenesAndfScoresBank();
        int population_counter = 0;
        Integer[] currentIndividual = new Integer[MAXIMUM_SCATTERERS_IN_A_QUADRANT];
        // while there is not enough population
        while (population_counter < MAX_POPULATION_SIZE) {
            // while the end of the individual has not been reached
            logger.trace("Generating random individual {}", population_counter);
            currentIndividual = randomIndividualCreator(new Integer[USED_SCATTERERS_IN_A_QUADRANT], radiusScatterer, population_counter);
            logger.trace("Random individual created: {}, with genes of {}", population_counter, currentIndividual);
            // set any remaining set to be 0
            currentIndividual = setChromosomeEndToZero(currentIndividual);
            logger.trace("Chromosome end set to zero of population member ", population_counter);
            clearAllUsedIndexes(SCATTERER_POSITION_INDEX_MAP);
            logger.trace("Cleaned index of scatterer map");
            // save the new member into the population
            generatedPopulation.addNewData(currentIndividual, -9999.0, population_counter - MAX_POPULATION_SIZE,
                    null, 9999.0, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
            if (population_counter % 10 == 0) {
                logger.debug("Added member {} to the population", population_counter);
            } else {
                logger.trace("Added member {} to the population", population_counter);
            }

            population_counter++;

        }
        logger.debug("Completed creating random members");
        return generatedPopulation;
    }

    private static Integer[] randomIndividualCreator(Integer[] currentIndividual, double radiusScatterer, int population_counter) {
        int geneCounter = 0;
        int randomizedGene = 0;
        Random rand = new Random();
        Integer[] labelledIndvidual = new Integer[]{865, 931, 257, 470, 911, 810, 912, 65, 260, 892, 385, 1005, 983, 914, 747, 900, 799, 1006, 20, 961, 112, 831, 542, 20, 890, 853, 555, 746, 956, 76, 0, 0, 0, 0, 0};
        int number_scat_posiotions = Variables.getScatMapLength(); // selection used for debugging, by default it should be max_no_scat_pos
        logger.trace("Max num scaterer positions available = {}", number_scat_posiotions);
        while (geneCounter < USED_SCATTERERS_IN_A_QUADRANT) {
            logger.trace("Beginning start of loop, gene counter = {}", geneCounter);
            if (ALLOW_0_SWITCHING)
                randomizedGene = rand.nextInt(number_scat_posiotions); // new random gene-- 0 to array position
            else {
                randomizedGene = rand.nextInt(number_scat_posiotions - 1) + 1; // (0 to array position-1) +1

            }
            // while random gene is in use, randomise
            logger.trace("Checking gene {} is in use", randomizedGene);
            boolean isInUse = SCATTERER_POSITION_INDEX_MAP[randomizedGene].isInUse();
            logger.trace("Gene {} {} {}", randomizedGene, " Is ", isInUse);
            while (isInUse) {
                if (ALLOW_0_SWITCHING)
                    randomizedGene = rand.nextInt(number_scat_posiotions); // new random gene
                else
                    randomizedGene = rand.nextInt(number_scat_posiotions - 1) + 1;
                logger.trace("Checking gene {} {}", randomizedGene, " Is in use");
                isInUse = SCATTERER_POSITION_INDEX_MAP[randomizedGene].isInUse();
                logger.trace("Gene {} {} {}", randomizedGene, " Is ", isInUse);
            }
            SCATTERER_POSITION_INDEX_MAP[randomizedGene].setInUse(true); // set in use
            // if running collision check, and the collision code says it's ok
            boolean collsionOccured = false;
            if (geneCounter % 10 == 0) {
                logger.trace("Checking gene {}", geneCounter);
            } else {
                logger.trace("Checking gene {}", geneCounter);
            }
            // if running collision checks, then create new one
            if (RunNSAGA.runCollsionChecks) {
                try {
                    logger.trace("Entering collsion check gene: {} value of {}", geneCounter, randomizedGene);
                    collsionOccured = collisionCheckAddNewGene(currentIndividual, randomizedGene, radiusScatterer);
                    logger.trace("Exiting collsion check gene: {} value of {}", geneCounter, randomizedGene);
                } catch (Exception e) {
                    logger.error("Error when making collision checking individual {} gene {}", population_counter, geneCounter, e);
                }
            } else {
                logger.trace("Avoiding collision check");
            }
            // if no collision occured when creating, then
            if (!collsionOccured) {
//                    temp_gene_test_2.add(randomizedGene);
                logger.trace("Setting gene to be valid");
                currentIndividual[geneCounter] = randomizedGene;
                logger.trace("Incrementing gene counter");
                geneCounter++;
                // else, if not running anti-collision code
            } else {
                logger.trace("Setting in use false for gene {}", randomizedGene);
                SCATTERER_POSITION_INDEX_MAP[randomizedGene].setInUse(false); // remove use
                logger.trace("continue");
                // skip
                continue;
            }
        }
        return currentIndividual;
    }

    /**
     * Used to generate a random inital population - used for the first run.
     *
     * @return the initial population
     */
    public static IndividualsData[] generateXRandomIndividuals(int numIndividuals, double radiusScatterer) {
        IndividualsData[] individualsData = new IndividualsData[numIndividuals];
        Random rand = new Random();
        int createdIndividuals = 0;
        Integer[] currentIndividual = new Integer[MAXIMUM_SCATTERERS_IN_A_QUADRANT];
        // while there is not enough population
        while (createdIndividuals < numIndividuals) {
            currentIndividual = randomIndividualCreator(new Integer[MAXIMUM_SCATTERERS_IN_A_QUADRANT], radiusScatterer, createdIndividuals);
            // set any remaining set to be 0
            currentIndividual = setChromosomeEndToZero(currentIndividual);
            clearAllUsedIndexes(SCATTERER_POSITION_INDEX_MAP);
            // save the new member into the population
            individualsData[createdIndividuals] = new IndividualsData(createdIndividuals,
                    currentIndividual, -9999.0,
                    createdIndividuals,
                    null, 9999.0, Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, false, false, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
            if (createdIndividuals % 10 == 0) {
                logger.debug("Added member {} to the population", createdIndividuals);
            } else {
                logger.trace("Added member {} to the population", createdIndividuals);
            }

            createdIndividuals++;

        }
        logger.debug("Completed creating random members");
        return individualsData;
    }

    /**
     * Method to check for collisions between any of the scatterers in 1 particular individual.
     *
     * @param newIndividual
     * @return true if there are collisions
     */
    public boolean runCollisionCheckRandomIndividual(Integer[] newIndividual, double radius_scatterer, Position[] scattererMap) {
        int currentExaminedGeneIndex = 0;
        int currentComparisonIndex = currentExaminedGeneIndex + 1;
        boolean collisionOccured = false;

        // run the nextEmptyIndex from 0 to n-1 (len-2), as don't need to compare the last entry against anything
        while (!collisionOccured && currentExaminedGeneIndex < newIndividual.length - 2) {
            logger.trace("Checking collision for gene {} - the ccurrentExaminedIndex", currentExaminedGeneIndex);
            // run the comparison from currentExamined + 1 to n (len-1)
            currentComparisonIndex = currentExaminedGeneIndex + 1;
            while (!collisionOccured && currentComparisonIndex < newIndividual.length - 1) {
                double currentExaminedX = scattererMap[newIndividual[currentExaminedGeneIndex]].getX();
                double currentExaminedY = scattererMap[newIndividual[currentExaminedGeneIndex]].getY();

                double currentComparisonX = scattererMap[newIndividual[currentComparisonIndex]].getX();
                double currentComparisonY = scattererMap[newIndividual[currentComparisonIndex]].getY();
                if (newIndividual[currentComparisonIndex] == 0 || newIndividual[currentExaminedGeneIndex] == 0) {
                    currentComparisonIndex++; // increase the current location - if the value of either index is 0, skip
                    continue;
                }
                // if proposed == an existing one, and they are not 0, a collision has occured
                if (newIndividual[currentComparisonIndex] == newIndividual[currentExaminedGeneIndex] && newIndividual[currentComparisonIndex] != 0) {
                    collisionOccured = true;
                    continue;
                }
                collisionOccured = checkCoordinatesForCollision(currentExaminedX, currentExaminedY, currentComparisonX, currentComparisonY, radius_scatterer);
                currentComparisonIndex++;
                logger.trace("Finished gene {} comparing to {} out of {}", currentExaminedGeneIndex, currentComparisonIndex, newIndividual.length - 1);
            }
            currentExaminedGeneIndex++;
            if (currentExaminedGeneIndex % 15 == 0) {
                logger.trace("Finished gene {} of {}", currentExaminedGeneIndex, newIndividual.length - 1);
            } else
                logger.trace("Finished gene {} of {}", currentExaminedGeneIndex, newIndividual.length - 1);

        }
        logger.trace("Left collsion check loop");
        clearUsedIndexes(newIndividual, scattererMap);
        logger.trace("Cleared indexs");
        return collisionOccured;
    }

    /**
     * Method to clear the indexs of the position maps - this keeps the map clean when checking if there has been any
     * duplicates.
     *
     * @param individual
     */
    private static void clearUsedIndexes(Integer[] individual, Position[] scattererMap) {
        for (Integer integer : individual) {
            if (integer != null && integer >= 0 && integer < scattererMap.length) {
                scattererMap[integer].setInUse(false);
            }
        }
    }

    private static void clearAllUsedIndexes(Position[] scattererMap) {
        for (Position pos : scattererMap) {
            pos.setInUse(false);
        }
    }

    /**
     * Method to check if there is a collision within the coordinates given
     *
     * @param centreX1
     * @param centreY1
     * @param centreX2
     * @param centreY2
     * @param radius_scatterer
     * @return true if there is a collision
     */
    private static boolean checkCoordinatesForCollision(double centreX1, double centreY1, double centreX2, double centreY2, double radius_scatterer) {
        boolean collsionOccured = false;
        switch (RunNSAGA.shapeOfScatterer) {
            case "Square":
                radius_scatterer = radius_scatterer * Math.sqrt(2);
                // if top of sq1 is lower than the bottom of sq 2, or if the bottom of sq2 is higher than the
                // there is a distance between the top and bottom of the rectangles.
                boolean topBottomOverlapOccured = false;
                boolean leftRightOverlap = false;
                if ((centreY1 + radius_scatterer) < (centreY2 - radius_scatterer)
                        || (centreY1 - radius_scatterer) > centreY2 + (radius_scatterer)) {
                    // pass
                } else
                    topBottomOverlapOccured = true;
                // if right side of sq1 is larger than left side of sq2, or if left side of sq1 is smaller than right sq2, overlap
                if ((centreX1 + radius_scatterer) < (centreX2 - radius_scatterer)
                        || (centreX1 - radius_scatterer) > (centreX2 + radius_scatterer)) {
                    // pass
                } else
                    leftRightOverlap = true;
                if (leftRightOverlap && topBottomOverlapOccured) {
                    collsionOccured = true;
                }
                break;
            case "Circle":
                // pythagoras - a^2 + b ^2 == c^2 in right angle - circle, if c^2 < (2*raidus) ^2  -- if (x1-x2)^2 + (y1-y2)^2 < radius^2, collision.
                if ((Math.pow((centreX1 - centreX2), 2) + Math.pow((centreY1 - centreY2), 2)) < Math.pow(2 * radius_scatterer, 2)) {
                    collsionOccured = true;
                }
                break;
            default:
                System.out.println("ERROR SYSTEM BROKEN - there is a scatterer unrecognised by the system");
                System.exit(-1);
        }
        return collsionOccured;
    }

    // todo check the pythag vs square collision code

    /**
     * Checks for a newly added gene in an individual if there are any collisions. Returns true if there are.
     *
     * @param currentIndividual
     * @param proposedGeneIndex
     * @param radius_scatterer
     * @return false if there is no collision, true if a collision occured
     */
    public static boolean collisionCheckAddNewGene(Integer[] currentIndividual, int proposedGeneIndex, double radius_scatterer) {
        boolean collsionOccured = false;
        // get the currently examined gene's x_y data:
        double proposedGeneX = SCATTERER_POSITION_INDEX_MAP[proposedGeneIndex].getX();
        double proposedGeneY = SCATTERER_POSITION_INDEX_MAP[proposedGeneIndex].getY();
//        double proposedXMM = RunNSAGA.SCATTERER_POSITION_INDEX_MAP[proposedGeneIndex].getX()*1000;  // debugging purposes
//        double proposedYMM = RunNSAGA.SCATTERER_POSITION_INDEX_MAP[proposedGeneIndex].getY()*1000;
//        double radiusMM = radius_scatterer * 1000;
//        System.out.println("X = " + proposedXMM + "  Y = "+ proposedYMM + "  Radius = "+ radiusMM);
        int geneCounter = 0;
        // exit loop if: a collision has occured, or you have gone over the length of the individual, or next item of the current individual is null (all further ones will be null too) or the proposed gene is 0 (any 0's aren't counted for collisions).
        while (!collsionOccured && geneCounter < currentIndividual.length && currentIndividual[geneCounter] != null && proposedGeneIndex != 0) {
            // examined == currently checked, exisiting value
            double examinedGeneX = SCATTERER_POSITION_INDEX_MAP[currentIndividual[geneCounter]].getX();
            double examinedGeneY = SCATTERER_POSITION_INDEX_MAP[currentIndividual[geneCounter]].getY();
            // if proposed == an existing one, and they are not 0, a collision has occured
            if (proposedGeneIndex == currentIndividual[geneCounter] && proposedGeneIndex != 0) {
                collsionOccured = true;
                continue;
            }
            logger.trace("Entering coordiante collision check");
            collsionOccured = checkCoordinatesForCollision(proposedGeneX, proposedGeneY, examinedGeneX, examinedGeneY, radius_scatterer);
            logger.trace("Exiting coordiante collision check");
            geneCounter++;

        }
        clearUsedIndexes(currentIndividual, SCATTERER_POSITION_INDEX_MAP);
        logger.trace("Clear used indexs");
        return collsionOccured;
    }

    /**
     * Method to copy an array. Acts as a shallow copy - new array object still stores the same items.
     *
     * @param original
     * @return
     */
    public Integer[] copyIntegerArray(Integer[] original) {
        Integer[] newArray = new Integer[original.length];
        int index;
        for (index = 0; index < original.length; index++) {
            newArray[index] = original[index].intValue();
        }
        return newArray;
    }

    public boolean writeResultsToFile(String path) {
        try {

        } catch (Exception e) {
            logger.error("Error writing results to file", e);
            return false;
        }
        return true;
    }

    /**
     * Method to append data to a history file.
     *
     * @param path                The history file to store in
     * @param genesAndfScoresBank The data to store
     * @return true if there were no Exceptions when appending.
     */
    public boolean appendGeneHistoryToFile(String path, GenesAndfScoresBank genesAndfScoresBank) {
        int dataLength = genesAndfScoresBank.getInvididual(0).getGenes().length + 2;
        int i = 0;
        int j = 0;
        try {
            // make a new array of columns that is the right length - number of scatterers + metric
            String[] newDataLine = new String[dataLength + IndividualsData.METRIC_COUNT + 1 + 360];
            if (!save360Points) {
                newDataLine = new String[dataLength + IndividualsData.METRIC_COUNT + 1];
            }

            ArrayList<String[]> newData = new ArrayList<>(newDataLine.length);
//            String[][] newData = new String[genesAndfScoresBank.getSize()][dataLength+6];
            for (i = 0; i < genesAndfScoresBank.getSize(); i++) {
                newDataLine = new String[dataLength + IndividualsData.METRIC_COUNT + 1 + 360];
                // if the individual is already recorded, no need to copy them into the table again.
                if (!genesAndfScoresBank.getInvididual(i).isRecoreded()) {
                    for (j = 0; j < genesAndfScoresBank.getInvididual(i).getGenes().length; j++) {
                        newDataLine[j] = genesAndfScoresBank.getInvididual(i).getGenes()[j].toString();
                    }
                    int metricCount = 0;
                    newDataLine[j] = Double.toString(genesAndfScoresBank.getInvididual(i).getfScore());  // fscore
                    newDataLine[j + 1] = Double.toString(genesAndfScoresBank.getInvididual(i).getMse()); // mse
                    newDataLine[j + 2] = Double.toString(1 - genesAndfScoresBank.getInvididual(i).getSanchisMetric()); // sanchis
                    newDataLine[j + 3] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetric());  // sanchis H comsol
                    newDataLine[j + 4] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetricH()); // sanchis H mine
                    newDataLine[j + 5] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetricSimple()); // sanchis numerator
                    newDataLine[j + 6] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezMetric());  // sanchez 2018 full
                    newDataLine[j + 7] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezMetricSimple()); // sanchez numerator
                    newDataLine[j + 8] = Double.toString(genesAndfScoresBank.getInvididual(i).getNormalisedMSE()); // normalised mse
                    newDataLine[j + 9] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisPerfectCloak()); // sanchis vs perfect cloak instead of central
                    newDataLine[j + 10] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezPerfectCloak()); // sanchez vs perfect
                    newDataLine[j + 11] = Double.toString(genesAndfScoresBank.getInvididual(i).getRmse()); // rmse
                    newDataLine[j + 12] = Double.toString(genesAndfScoresBank.getInvididual(i).getMinPressure()); // min pressure
                    newDataLine[j + 13] = Double.toString(genesAndfScoresBank.getInvididual(i).getMaxPressure()); // max pressure
                    newDataLine[j + 14] = Double.toString(genesAndfScoresBank.getInvididual(i).getAveragePressure()); // average pressure
                    newDataLine[j + 15] = Double.toString(genesAndfScoresBank.getInvididual(i).getAbsoluteAveragePressure()); // absolute average pressure
                    newDataLine[j + 16] = Double.toString(genesAndfScoresBank.getInvididual(i).getL2Norm()); // the L2 Norm
                    int startJ = j + 17;
                    if (save360Points) {
                        for (j = 0; j < 360; j++) {
                            if (genesAndfScoresBank.getInvididual(i).getArray360Points() != null)
                                newDataLine[j + startJ] = ((Double) genesAndfScoresBank.getInvididual(i).getArray360Points()[0][j]).toString();
                            else {
                                if (!stillInitialising) {
                                    // if not still initialsing, there is a problem - bad coding practice, but need to do this quickly
                                    logger.error("There is a problem with the arrays - for some reason they are empty");
                                } else {
                                    logger.trace("Still initalising - saving data from previous iterations");
                                }
                            }
                        }
                    }
                    genesAndfScoresBank.getInvididual(i).setRecoreded(true);
                    newDataLine[newDataLine.length - 1] = Integer.toString(genesAndfScoresBank.getInvididual(i).getModelNumber());
                    newData.add(newDataLine);
                } else {
                    logger.trace("Not adding element {}, model num {}, index {} to the array to save as it is null - recorded Already ==", i, genesAndfScoresBank.getInvididual(i).getModelNumber(), genesAndfScoresBank.getInvididual(i).getIndex());
                }
//                if (newDataLine[0] != null)
//
//                else {
//                    logger.trace("Not adding element {}, model num {}, index {} to the array to save as it is null", i, genesAndfScoresBank.getInvididual(i).getModelNumber(), genesAndfScoresBank.getInvididual(i).getIndexGiven());
//                }

            }
            String[][] newDataArray = new String[newData.size()][dataLength + 6];
            i = 0;
            for (String[] line : newData) {
                newDataArray[i] = line;
                i++;
            }
            // append the array to the data
            toExcel(newDataArray, path, true);
        } catch (Exception e) {
            logger.error("Error writing results to file - i = {}, j = {}", i, j, e);
            return false;
        }
        return true; // break here for stopping on file save
    }

    public void saveToExcel(String[][] table, String fileName, boolean appending) {
        toExcel(table, fileName, appending);
    }

    /**
     * Method to convert a JTable to a csv file passed in, with the option of appending to the file or just writing to it.
     *
     * @param table
     * @param fileName  file name, without the csv ending
     * @param appending
     */
    private void toExcel(String[][] table, String fileName, boolean appending) {
        FileWriter excel = null;
        File file = new File(fileName + ".csv");
        // if the file doesn't exist, then set appending to false, as it is making a new file
        if (!file.exists()) {
            appending = false;
        }
        try {

            excel = new FileWriter(file.getAbsolutePath(), appending);
            int j;
            for (int i = 0; i < table.length; i++) {
                for (j = 0; j < table[0].length; j++) {
                    try {
                        excel.append(table[i][j] + ", ");
                    } catch (Exception e) {
                        logger.trace("Caught an exception {} while writing to excel document ", e.getMessage());
                    }
                }
                excel.append("\n");
            }

            excel.close();

        } catch (IOException e) {
            logger.error("While converting to Excel: {}", e.getMessage());
        } finally {
            if (excel != null) {
                try {
                    excel.close();
                } catch (IOException e) {
                    logger.error("Error while closing the excel conversion");
                    logger.error("CONTEXT", e);
                }
            }
        }
    }

    /**
     * Method to append the right number of 0s to the end of a chromosome array - e.g. if the input has 30 numbers and the
     * maximum number of scatterers is 35, then it will add 5 0's to the end of the array.
     *
     * @param inputArray
     * @return
     */
    public static Integer[] setChromosomeEndToZero(Integer[] inputArray) {
        int i;
        // if there are less than the needed elements, then remake the array to be the right length.
        if (inputArray.length != MAXIMUM_SCATTERERS_IN_A_QUADRANT) {
            Integer[] tempArray = inputArray;
            inputArray = new Integer[MAXIMUM_SCATTERERS_IN_A_QUADRANT];
            for (i = 0; i < tempArray.length; i++) {
                inputArray[i] = tempArray[i];
            }
            tempArray = null; // clear the data from memory.
        }
        // if the number of scatters to use is less than the maximum number of scatterers
        if (USED_SCATTERERS_IN_A_QUADRANT != MAXIMUM_SCATTERERS_IN_A_QUADRANT) {
            for (i = 0; i < MAXIMUM_SCATTERERS_IN_A_QUADRANT; i++) {
                // if i is greater than the number of scatterers allowed -1 (scat_allowed = 30, therefore 0-29
                if (i > USED_SCATTERERS_IN_A_QUADRANT - 1) {
                    inputArray[i] = 0;
                }
            }
        }
        if (inputArray[inputArray.length - 1] == null) {
            for (i = 0; i < inputArray.length; i++) {
                if (inputArray[i] == null) {
                    inputArray[i] = 0;
                }
            }
        }
        return inputArray;
    }

    public void setIndividualEndToZero(IndividualsData individual) {
        int i;
        Integer[] inputArray = individual.getGenes();
        // if the number of scatters to use is less than the maximum number of scatterers
        if (USED_SCATTERERS_IN_A_QUADRANT != MAXIMUM_SCATTERERS_IN_A_QUADRANT) {
            for (i = 0; i < inputArray.length; i++) {
                // if i is greater than the number of scatterers allowed
                if (i > USED_SCATTERERS_IN_A_QUADRANT) {
                    inputArray[i] = 0;
                }
            }
        }
    }

    /**
     * Method to produce an array of scatterers that are only 0's
     *
     * @return
     */
    public Integer[] getEmpyScattererArray() {
        Integer[] array = new Integer[MAXIMUM_SCATTERERS_IN_A_QUADRANT];
        for (int i = 0; i < MAXIMUM_SCATTERERS_IN_A_QUADRANT; i++) {
            array[i] = 0;
        }
        return array;
    }

    public void runRecoveryCleanUp() {
        long startTime = System.currentTimeMillis();
        String[] fileDirectories = new String[4];
        try {
            fileDirectories[0] = ModelUtil.getPreference("tempfiles.recovery.recoverydir");
        } catch (ExceptionInInitializerError e) {
            fileDirectories[0] = " ";
        }
        fileDirectories[1] = GlobalUtils.getComsolRecoveryDirectory();
//        fileDirectories[2] = "D:\\comsol\\tempDir\\recoveries";
//        fileDirectories[3] = "D:\\comsol\\v54\\recoveries";
        // MPHRecovery8363date_Dec_19_2019_11-22_AM.mph
        LocalDate date = java.time.LocalDate.now();

        SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy_HH-mm");
        String currentStringDate = format.format(new Date());
        Date currentDate = new Date();
        currentDate.setHours(0);
        int breakx = 0;

        File dir;
        ArrayList<File> toDeleteFiles = new ArrayList<>();
        for (String fileName : fileDirectories) {
            if (fileName != null && !fileName.equals("") && !fileName.equals(" ")) {
                dir = new File(fileName);
                if (!(new File(fileName)).exists())
                    continue;
                for (File fileEntry : dir.listFiles()) {
                    String name = fileEntry.getName();
                    String dateName = "";
                    int indexForDate = name.indexOf("date") + 5;
//                    if (name.length() > 20) {
                    dateName = name.substring(indexForDate, name.length() - 7);
                    Date parsedDate = new Date();
                    try {
                        parsedDate = format.parse(dateName);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (parsedDate.before(currentDate)) {
                        toDeleteFiles.add(fileEntry);
                    }
                    int breakx1 = 0;
                }
            }
        }
        while (!toDeleteFiles.isEmpty()) {
            recursiveDelete(toDeleteFiles.get(0));
            toDeleteFiles.remove(0);
        }
        timeSpentClearningRecoveries += System.currentTimeMillis() - startTime;
        logger.debug("Time spent cleaning recoveries = {}s", timeSpentClearningRecoveries / 60);

        // Find the temporary files that are used by comsol for meshing
        ArrayList<File> tempFolders = GlobalUtils.getComsolTempFolders();
        ArrayList<Date> lastModified = new ArrayList<>(tempFolders.size());
        for (File comsolTempDir : tempFolders) {
            String name = comsolTempDir.getName();
            File[] comsolTempFolderSubFiles = comsolTempDir.listFiles();
            if (comsolTempFolderSubFiles.length == 0)
                lastModified.add(new Date(comsolTempDir.lastModified()));
            else {
                long mostRecentLong = comsolTempFolderSubFiles[0].lastModified();
                int mostRecentModifiedLocation = 0;
                for (int i = 1; i < comsolTempFolderSubFiles.length; i++) {
                    // if the current examined date is after the mostRecentModified, it is more recent, so overwrite
                    if (comsolTempFolderSubFiles[i].lastModified() > mostRecentLong) {
                        mostRecentLong = comsolTempFolderSubFiles[i].lastModified();
                        mostRecentModifiedLocation = i;
                    }
                }
                lastModified.add(new Date(comsolTempFolderSubFiles[mostRecentModifiedLocation].lastModified()));
            }
        }
        if (lastModified.size() == 0)
            return;
        Date mostRecentModified = lastModified.get(0);
        int mostRecentModifiedLocation = 0;
        for (int i = 1; i < tempFolders.size(); i++) {
            // if the current examined date is after the mostRecentModified, it is more recent, so overwrite
            if (lastModified.get(i).after(mostRecentModified)) {
                mostRecentModified = lastModified.get(i);
                mostRecentModifiedLocation = i;
            }
        }
        tempFolders.remove(mostRecentModifiedLocation);

        while (!tempFolders.isEmpty()) {
            recursiveDelete(tempFolders.get(0));
            tempFolders.remove(0);
        }
        breakx = 0;
    }

    private void recursiveDelete(File f) {
        String[] entries = f.list();
        if (entries != null) {
            for (String s : entries) {
                File currentFile = new File(f.getPath(), s);
                if (currentFile.isDirectory()) {
                    recursiveDelete(currentFile);
                }
                currentFile.delete();
            }
        }
        f.delete();
    }

    public void setComsolPreferences() {
//        File tempDir = new File("D:\\comsol\\tempDir");
//        if (!tempDir.exists()) {
//            tempDir.mkdirs();
//        }
//        File temp = new File(tempDir.getAbsolutePath() + "\\recoveries");
//        if (!temp.exists()) {
//            temp.mkdirs();
//        }
//        temp = new File(tempDir.getAbsolutePath() + "\\temp");
//        if (!temp.exists()) {
//            temp.mkdirs();
//        }
//        temp = new File(tempDir.getAbsolutePath() + "\\temp_server");
//        if (!temp.exists()) {
//            temp.mkdirs();
//        }
//        temp = new File(tempDir.getAbsolutePath() + "\\userFiles");
//        if (!temp.exists()) {
//            temp.mkdirs();
//        }
//        temp = new File(tempDir.getAbsolutePath() + "\\commonFiles");
//        if (!temp.exists()) {
//            temp.mkdirs();
//        }
        ModelUtil.setPreference("tempfiles.recovery.autosave", "off"); // turn off recovery
        if (Variables.getNumberCores() > 0) {
            ModelUtil.setPreference("cluster.processor.numberofprocessors", Integer.toString(Variables.getNumberCores()));
        }
        boolean hasRequiredComsolProducts = false;
        try {
            hasRequiredComsolProducts = ModelUtil.hasProductForFile(Variables.getModelPath());
        } catch (IOException e) {
            logger.error("Can't find the products for the comsol file");
            e.printStackTrace();
        }
        try {
            if (Variables.checkoutLicense())
                ModelUtil.checkoutLicenseForFile(Variables.getModelPath());
            logger.debug("Checked out license for file "+ Variables.getModelPath());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Can't checkout the license for the file "+ Variables.getModelPath());
            logger.error(e.getMessage());
            System.exit(-1);
        }
//        ModelUtil.setPreference("tempfiles.recovery.recoverydir", tempDir.getAbsolutePath()+"\\recoveries"); // turn off recovery
//        ModelUtil.setPreference("tempfiles.tempfiles.tmpdir", tempDir.getAbsolutePath()+"\\temp"); // change temp dirs to  D drive
//        ModelUtil.setPreference("tempfiles.tempfiles.tmpdir#server", tempDir.getAbsolutePath()+"\\temp_server"); // change temp dirs to  D drive
//        ModelUtil.setPreference("tempfiles.schemes.filerootuser", tempDir.getAbsolutePath()+"\\userFiles"); // change temp dirs to  D drive
//        ModelUtil.setPreference("tempfiles.schemes.filerootcommon", tempDir.getAbsolutePath()+"\\commonFiles"); // change temp dirs to  D drive
//        ModelUtil.setPreference("memoryconservation.parametric.save", "off"); // change temp dirs to  D drive
//        ModelUtil.savePreferences();
    }
}
