package com.ga.nsaga;

import com.comsolModelInterface.ComsolModelInterface;
import com.comsolModelInterface.ComsolModelInterface2_0;
import com.comsolModelInterface.EmptyModel;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.comsol.model.util.ModelUtil;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.ga.nsaga.RunNSAGA.setUpScattererIndexMap;

public class Run_single_entry {

//    private static Integer[] initalEntry = new Integer[] {388,81,562,650,496,481,371,985,408,779,600,588,263,708,958,295,413,81,884,421,691,257,949,565,925,437,227,139,860,964,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {704,65,642,136,264,138,846,911,912,467,723,278,89,347,799,32,162,99,292,229,102,744,680,682,249,250,314,125,829,702,0,0,0,0,0}; // 75% cloak accuracy
//    private static Integer[] initalEntry = new Integer[] {70,65,642,136,264,138,846,911,912,467,723,278,89,347,799,32,162,99,292,229,102,744,680,682,249,250,314,125,829,702,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {961,35,280,395,161,309,935,1008,150,898,5,702,275,384,310,1007,320,64,125,54,889,854,251,960,601,660,162,1034,264,957,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {70,65,642,136,264,138,846,911,912,467,723,278,89,347,799,32,162,99,292,229,102,744,680,682,249,250,314,125,829,702,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {961,35,280,395,161,309,935,1008,150,898,5,702,275,384,310,1007,320,64,125,54,889,854,251,960,601,660,162,1034,264,957,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // the empty arry, just the model
//    private static Integer[] initalEntry = new Integer[] {3020, 2064, 246, 1972, 3270, 3155, 4321, 3120, 2114, 4346, 2435, 156, 2898, 4200, 1658, 414, 653, 1557, 640, 83, 2917, 3385, 2962, 512, 3366, 3143, 191, 2500, 262, 3334, 0, 0, 0, 0, 0}; // example thin cloak one
//    private static Integer[] initalEntry = new Integer[] {294, 938, 800, 852, 310, 219, 340, 932, 163, 888, 866, 138, 257, 111, 767, 300, 486, 648, 738, 850, 88, 204, 189, 848, 504, 737, 21, 149, 910, 0, 0, 0, 0, 0 ,0}; // top 30%
//    private static Integer[] initalEntry = new Integer[] {294, 938, 800, 852, 310, 219, 340, 932, 163, 888, 866, 138, 257, 767, 300, 850, 88, 204, 848, 504, 737, 21, 149, 910, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // top 40 %
//    private static Integer[] initalEntry = new Integer[] {938, 800, 852, 219, 340, 163, 888, 866, 138, 257, 767, 300, 850, 88, 204, 504}; // top 50 %
//    private static Integer[] initalEntry = new Integer[] {}; // template
    private static Integer[] initalEntry = new Integer[] {255, 1821, 2254, 172, 1880, 2417, 1526, 228, 992, 2180, 1175, 435, 2294, 1649, 716, 1754, 87, 1168, 2000, 2375, 1624, 1952, 1384, 1616, 353, 1033, 1348, 1632, 2085, 2060, 1513, 1239, 94, 1138, 84}; // template
//    private static Integer[] initalEntry = new Integer[] {950, 1677, 356, 1487, 902, 375, 187, 643, 674, 886, 1305, 1637, 3603, 1538, 3220, 2033, 1083, 3066, 3741, 3578, 2507, 121, 3179, 1212, 3290, 353, 2814, 1195, 1858, 941, 1929, 3688, 2680, 935, 3014}; //
//    private static Integer[] initalEntry = new Integer[] {2297, 3172, 1940, 1584, 1122, 2956, 636, 2038, 519, 2749, 2080, 710, 1020, 737, 1455, 2212, 2390, 41, 3245, 2348, 3212, 2787, 147, 1220, 212, 3509, 1086, 1055, 2547, 858, 309, 1336, 1506, 1492, 2773}; // top 50 %
//    private static Integer[] initalEntry = new Integer[] {665, 2321, 1619, 2040, 3104, 278, 486, 771, 61, 1294, 2646, 3572, 2707, 2249, 3581, 283, 964, 2513, 2644, 2834, 3323, 1325, 2745, 442, 731, 2244, 2706, 1331, 1052, 2871, 2319, 2492, 3184, 2999, 1755}; // breaking example from 2020-10-22



    private static NsagaUtils utils = new NsagaUtils();
    final static Logger logger = (Logger) LoggerFactory.getLogger(Run_single_entry.class.getName());
    final static boolean USE_COMSOL = true; // control variable, where it can be switched off to give random answers,
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\2019-11-11--11-23_output.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistor__archive_2020-06-26--22-48.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"

    static String optimizerType = "SAGA"; // control variable to choose which of the optimizers to use - SAGA or NSAGAD:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\activeProcessingCSV\geneHistory_renamed.csv
    static boolean resumePreviousRun = true; // control if using a previous run. If yes, then please do use the previous generationCounter below - use the GENE_BANK_FILE_TO_USE to control which file is loaded
    static int previousGenerationCounter = 0;
    static boolean runCollsionChecks = false;
    //    final static Object[][] SCATTERER_POSITION_INDEX_MAP = utils.loadCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv");
    static Position[] SCATTERER_POSITION_INDEX_MAP;
    final static String shapeOfScatterer = "Square";
    final static String shapeOfObject = "Square";
    static double radiusScatterer;
    static String comsolReferencesFolder = System.getProperty("user.dir")+"\\src\\main\\resources\\comsolPreferences\\";
    static boolean useGeneRandomisation = false;
    static boolean saveAllImages = true;
    static boolean stillInitialising = true;
    static long programStartTime;
    static ComsolModelInterface comsolModel;
    static double[][]emptyModel360Pressure;
    static double[][]centralObjectOnly360Pressure;
    static String startDate;

    public static void main(String[] args) {
        logger.info("Beginning single individual run");
        programStartTime = System.currentTimeMillis();
        SCATTERER_POSITION_INDEX_MAP = setUpScattererIndexMap();
        Variables.setScattererPositionIndexMap(SCATTERER_POSITION_INDEX_MAP);
        long startTime = System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm"); // used in saving the data, once saving is made available.
        Date date = new Date();
        Level loggerLevel;
        if (USE_COMSOL) {
            loggerLevel = Level.DEBUG;
        } else {
            loggerLevel = Level.INFO;
        }

        // insert comsol model of choice here
        comsolModel = null;

        if (USE_COMSOL) {
            // initialise comsol model.
            ModelUtil.initStandalone(false); // method to make comsol work.

            File preferenceFile = new File(comsolReferencesFolder + "\\comsolReferences.txt");
            // if preferneces exists, load file
            if (preferenceFile.exists()) {
                ModelUtil.loadPreferences();
            } else {
                File preferencesDir = new File(comsolReferencesFolder);
                utils.setComsolPreferences();

//                try {
//                    FileWriter fw = new FileWriter(preferenceFile);
//                    fw.write("Saved preferences");
//                    fw.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
            comsolModel = new ComsolModelInterface2_0(USE_COMSOL, loggerLevel);
        } else {
            comsolModel = new EmptyModel();
        }
        utils.runRecoveryCleanUp();
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        radiusScatterer = Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer"));
        logger.info("Comsol Model initiated");

        GenesAndfScoresBank genesLoaded = null;
        GenesAndfScoresBank genesHistory = null;
        startDate = dateFormat.format(date);
        logControlVariables(startDate);

        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }

        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }

        try {
            comsolModel.runBlankModel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        emptyModel360Pressure = comsolModel.getModels360Points();
        logger.debug("Setup empty model");

        setAllScatterers(utils.getEmpyScattererArray(), SCATTERER_POSITION_INDEX_MAP);
        try {
            comsolModel.runModel(utils.no_scatterer_run);
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }
        centralObjectOnly360Pressure = comsolModel.getModels360Points();
        logger.debug("Setup central object only!");
        logger.info("Running collision checks now");
        boolean collisionOccured = utils.runCollisionCheckRandomIndividual(initalEntry, radiusScatterer, SCATTERER_POSITION_INDEX_MAP);
        initalEntry = utils.setChromosomeEndToZero(initalEntry);
        logger.info("Collision in given sample? " + Boolean.toString(collisionOccured));
        if (runCollsionChecks && collisionOccured)
            logger.error("COLLISION OCCURRED while set to prevent collision checks");
        IndividualsData data2 = Run_single_entry(initalEntry, comsolModel);
        logger.debug("Completed individual run");

        System.exit(0);
    }


    private static void logControlVariables(String startDate) {
        logger.info("Confirming before Run:");
        logger.info("    runCollisionChecks = "+ runCollsionChecks);
        logger.info("    optimiserType = "+ optimizerType);
        logger.info("    shapeOfScatterer = "+ shapeOfScatterer);
        logger.info("    shapeOfObject = "+ shapeOfObject);
        logger.info("    radiusScatterer = "+ radiusScatterer);
        logger.info("    resumePreviousRun = "+ resumePreviousRun);
        if (resumePreviousRun)
            logger.info("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
        logger.info("    useGeneRandomisation = " + useGeneRandomisation);
        logger.info("    scattererType = " + shapeOfScatterer);
        logger.info("    central Type = " + shapeOfObject);
        try{
            FileWriter detailsFile =  new FileWriter(String.valueOf(Paths.get(Variables.getMassImageOutputFolder(startDate, "")+ "\\details.txt")));
            detailsFile.write("Confirming before Run:");
            detailsFile.write("    runCollisionChecks = "+ runCollsionChecks);
            detailsFile.write("    optimiserType = "+ optimizerType);
            detailsFile.write("    shapeOfScatterer = "+ shapeOfScatterer);
            detailsFile.write("    shapeOfObject = "+ shapeOfObject);
            detailsFile.write("    radiusScatterer = "+ radiusScatterer);
            detailsFile.write("    resumePreviousRun = "+ resumePreviousRun);
            if (resumePreviousRun)
                detailsFile.write("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
            detailsFile.write("    useGeneRandomisation = " + useGeneRandomisation);
            detailsFile.write("    scattererType = " + shapeOfScatterer);
            detailsFile.write("    central Type = " + shapeOfObject);
            detailsFile.write("    Allow 0 switching = " +utils.ALLOW_0_SWITCHING );
            detailsFile.write("    Optimiser = " + utils.USED_ERROR_METRIC );
            detailsFile.write("    Inital annealing step = " + (utils.INTIAL_ANNEALING_STEP ));
            detailsFile.write("    Max scatts in a quadrant = " + Integer.toString(utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) );
            detailsFile.write("    Used scatts in a quadrant = " + Integer.toString(utils.USED_SCATTERERS_IN_A_QUADRANT) );
            detailsFile.write("    Max pop size = " + Integer.toString(utils.MAX_POPULATION_SIZE) );
            detailsFile.write("    Max scat positions = " + Integer.toString(utils.MAX_POPULATION_SIZE) );
            detailsFile.write("    Total annealing steps allowed = " + Integer.toString(utils.totalAnnealingStepsAllowed) );
            detailsFile.write("    Max num population generations= " + Integer.toString(utils.maxNumPopulationGenerations) );
            detailsFile.write("    Eliete individuals= " + Integer.toString(utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION) );
            detailsFile.write("    Equilibirum= " + Integer.toString(utils.EQUILIBRIUM_POSITION_INDEX) );
            detailsFile.write("    Mutation Prob= " + Double.toString(utils.initialMutationProbability) );
            detailsFile.write("    Inital temp= " + Double.toString(utils.STARTING_TEMPERATURE) );
            detailsFile.write("    Max temp steps= " + Integer.toString(utils.MAXIMUM_TEMPERATURE_STEPS) );
            detailsFile.write("    Gen save counter= " + Integer.toString(utils.GENERATION_SAVE_COUNTER) );
            detailsFile.close();
            int breakx = 0;
        } catch (IOException e) {
            logger.error("IO exception when writing details file", e);
        }
    }

    private static GenesAndfScoresBank[] loadGenesFromClass(GenesAndfScoresBank genesLoaded, GenesAndfScoresBank genesHistory) {
        Object[][] csvRawGenes = utils.loadParamsFromCSV((GENE_BANK_FILE_TO_USE), false);
        genesLoaded = new GenesAndfScoresBank();
        int index = 0;
        for (Object[] loadedIndividualData: csvRawGenes) {
            if (loadedIndividualData[0].equals("")) {
                logger.trace("Skip over, as found an empty first item of the array");
                continue;
            }
            logger.trace("Pre chromosome load Index = " + index);
            // copy the array from 1 end to the other, ignoring the final element.
            // todo will likely have to parse this information.
            Integer[] chromosome = new Integer[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT];
            String[] stringChromosome = (String[]) Arrays.copyOfRange(loadedIndividualData, 0, utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT);
            for (int i= 0; i< stringChromosome.length; i++) {
                // remove white space
                String s = stringChromosome[i];
                s =s.replaceAll("\\s+", "");
                chromosome[i] = Integer.parseInt(s);
            }
            logger.trace("Post chromosome load Index = " + index);

            // todo write up the code to save and load the information
            // chromosome, fscore, model number, 360 points, mse,  sanchis, sanchisH, sanchisSimple, Sanchez, SanchezSimple
            genesLoaded.addNewData(chromosome, Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]), index,
                    null,
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+1].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+3].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+4].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+5].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+6].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+7].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+8].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+9].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+10].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+11].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+12].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+13].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+14].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+15].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT+16].equals("null")? null: Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT])
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +2]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +3]),
//                        Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +1]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +4]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +5])
            );
            logger.trace("Post geneLoaded Index = " + index);
            index++;
        }
        // if the number of imported genes is larger than 70, then filter the best 70 from it
        if (genesLoaded.getSize() > utils.MAX_POPULATION_SIZE) {
            genesLoaded.sortByfScore();
            GenesAndfScoresBank tempGenes = new GenesAndfScoresBank();
            for (index = 0; index < utils.MAX_POPULATION_SIZE; index++) {
                tempGenes.addNewIndividual(genesLoaded.getInvididual(index));
            }
            genesHistory = genesLoaded;
            genesHistory.sortByIndex();
            genesLoaded = tempGenes;
        } else if (csvRawGenes.length < utils.MAX_POPULATION_SIZE) {
            // else if there are less entries than there should be, then add on some new random ones
            IndividualsData[] individualsDataSet = utils.generateXRandomIndividuals(utils.MAX_POPULATION_SIZE - genesLoaded.getSize(), radiusScatterer);
            for (IndividualsData individual : individualsDataSet) {
                genesLoaded.addNewIndividual(individual);
            }
            genesHistory = genesLoaded;
        }
        return new GenesAndfScoresBank[]{genesLoaded, genesHistory};
    }

    private static IndividualsData Run_single_entry(Integer[] testedIndividual, ComsolModelInterface comsolModel) {
        Integer[] newIndividualGenes = utils.copyIntegerArray(testedIndividual);
        newIndividualGenes = utils.setChromosomeEndToZero(newIndividualGenes);
        IndividualsData newIndividual = new IndividualsData(0, newIndividualGenes, Double.MAX_VALUE, 0, null, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,false, false,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE, Double.MAX_VALUE);
        // set up simulation
        getAllScatterers();
        setAllScatterers(newIndividualGenes, SCATTERER_POSITION_INDEX_MAP);
        long time = System.currentTimeMillis();
        runComsolAndGetMetrics(0, newIndividual);
        if (RunNSAGA.saveAllImages) {
            comsolModel.saveImagesInt("savedImages_" + newIndividual.getModelNumber() + "__startTime-", newIndividual.getModelNumber(), newIndividual.getGenes(), startDate, comsolModel.getModels360Points());
        }
        // if the fscore is less than the current equilibrium, it is a better cloak, so accept
        double fscore = newIndividual.getfScore();
        // save every image if needed, but only save the accepted models
        return newIndividual;
    }

    protected static ArrayList<String> getAllScatterers(){
        ArrayList<String> returnList = null;
        switch (shapeOfScatterer) {
            case "Square":
                returnList = comsolModel.getAllSquareScatterers();
                break;
            case "Circle":
                returnList = comsolModel.getAllCircularScatterers();
                break;
            default:
                returnList = comsolModel.getAllSquareScatterers();
                break;

        }
        return returnList;
    }

    protected static void setAllScatterers(Integer[] testedIndividual, Position[] scattererToIndexMap) {
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;
            case "Circle":
                comsolModel.setAllCircularScatterers(testedIndividual, scattererToIndexMap);
                break;
            default:
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;

        }

    }

    protected static double runComsolAndGetMetrics(int modelNumber, IndividualsData individual) {
        double fscore = Double.MAX_VALUE;
        try {
            setAllScatterers(individual.getGenes(), SCATTERER_POSITION_INDEX_MAP);
            comsolModel.runModel(modelNumber);
            if (individual.getModelNumber() != modelNumber) {
                individual.modelNumber = modelNumber;
            }
            double[][] model360 = comsolModel.getModels360Points();
            double sanchisMetric = comsolModel.getResult();
            double mse = calculateError(comsolModel.getModels360Points());
            double rmse = Math.sqrt(mse);
            double sanchisMetric2 = 1 - sumSquares(model360) / sumSquares(centralObjectOnly360Pressure);
            double sanchisMetric3 = sumSquares(model360) / sumSquares(centralObjectOnly360Pressure); // sum of 360 points with cloak / sum of 360 points with the central object.
            double sanchisMetric4 = sumSquares(model360);
            double sanchisMetricVsPerfect = 1 - sumSquares(model360) / sumSquares(emptyModel360Pressure);
            double sanchezMetric1 = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(centralObjectOnly360Pressure)) ), -1);
            double sanchezMetricVsperfect = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(emptyModel360Pressure)) ), -1);
            double sanchezMetric2 = sumAbsolutes(model360);
            double normalisedMSE = normalisedMSE(model360);
            double l2Norm = calculateL2Norm(model360[0]);
            individual.setArray360Points(comsolModel.getModels360Points());
            individual.setMse(calculateError(comsolModel.getModels360Points()));
            individual.setSanchisMetric(sanchisMetric);
            individual.setSanchisMetricH(sanchisMetric3);
            individual.setSanchisMetricSimple(sanchisMetric4);
            individual.setSanchezMetric(sanchezMetric1);
            individual.setSanchezMetricSimple(sanchezMetric2);
            individual.setNormalisedMSE(normalisedMSE);
            individual.setSanchisPerfectCloak(sanchisMetricVsPerfect);
            individual.setSanchezPerfectCloak(sanchezMetricVsperfect);
            individual.setfScore(utils.selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm));
            individual.setRmse(rmse);
            double[] model360SubArray = Arrays.copyOfRange(model360[0], 1, model360[0].length);
            double[][] model360SubArray2D = new double[1][360];
            model360SubArray2D[0] = model360SubArray;
            individual.setMinPressure(getMinOfModel360(model360SubArray));
            individual.setMaxPressure(getMaxOfModel360(model360SubArray));
            individual.setAveragePressure(sum(model360)/360);
            individual.setAbsoluteAveragePressure(sumAbsolutes(model360)/360);
//            System.out.println("    MSE metric       -> 0 (1) = " + mse);
//            System.out.println("Sanchis metric   -> 1 (2) = " + sanchisMetric2);
//            System.out.println("Sanchis metric   -> 0 (3) = " + sanchisMetric3);
//            System.out.println("Sanchis metric R -> 0 (3) = " + sanchisMetric);
//            System.out.println("Sanchis metric   -> 0 (4) = " + sanchisMetric4);
//            System.out.println("Sanchez metric   -> 1 (5) = " + sanchezMetric1);
//            System.out.println("Sanchez metric   -> 0 (6) = " + sanchezMetric2 + "\n\n");
            fscore = utils.selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fscore;
    }

    protected static double calculateError(double[][] model360) {
        int i;
        double meanSquareError = 0.0;
        double sumOfDifferences = 0.0;
        double sumOfDifferencesSquared = 0.0;//i = 1, as the first column of the table is set to the frequency, not needed. However it should be independant of it - 20k - 20k = 0, therefore should have no impact on error rate
        for (i=1; i<model360[0].length; i++) {
            sumOfDifferences += (model360[0][i] - emptyModel360Pressure[0][i]);
            sumOfDifferencesSquared += Math.pow(model360[0][i] - emptyModel360Pressure[0][i], 2);
        }
        meanSquareError = sumOfDifferencesSquared/model360.length;

        return meanSquareError;
    }

    protected static double sum(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += (model360[0][i]);
        }
        return sum;
    }

    protected static double sumAbsolutes(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.abs(model360[0][i]);
        }
        return sum;
    }

    protected static double sumSquares(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.pow((model360[0][i]), 2);
        }
        return sum;
    }

    /**
     * https://rem.jrc.ec.europa.eu/RemWeb/atmes2/20b.htm
     * @param model360
     * @return
     */
    protected static double normalisedMSE(double[][] model360) {
        double averageBase = sum(emptyModel360Pressure)/emptyModel360Pressure.length;
        double averageOfModel = sum(model360)/model360.length;
        double squareSumOfDifferencesOverMeans = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            // sum, [(P-m)**2 / (Paverage * MAverage)]
            squareSumOfDifferencesOverMeans += Math.pow(model360[0][i] - emptyModel360Pressure[0][i],2) / (averageBase * averageOfModel);
        }
        return (1/model360.length) * squareSumOfDifferencesOverMeans;
    }

    protected static double getMinOfModel360(double[] model360) {
        double min = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] < min) {
                min = model360[i];
            }
        }
        return min;
    }

    protected static double getMaxOfModel360(double[] model360) {
        double max = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] > max) {
                max = model360[i];
            }
        }
        return max;
    }

    private static double calculateL2Norm(double[] model360) {
        // F = ||W-U||/||V-U|| where U = empty region vector, V = object field vector, W = cloak pressure vector
        // U = emptyModel360Pressure, V = Central object only 360 pressure, W = input model 360 vector
        double[] numeratorArray = subtractArrays(model360, emptyModel360Pressure[0]);
        double[] denominatorArray = subtractArrays(centralObjectOnly360Pressure[0], emptyModel360Pressure[0]);

        double numerator = abosoluteValueDoubleArray(numeratorArray);
        double denominator = abosoluteValueDoubleArray(denominatorArray);

        return numerator/denominator;
    }

    private static double[] subtractArrays(double[] array1, double[] array2) {
        double[] tempArray  = new double[array1.length];
        for (int i=0; i < array1.length; i++) {
            tempArray[i] = array1[i]-array2[i];
        }
        return  tempArray;
    }

    private static double abosoluteValueDoubleArray(double[] array) {
        double sum = 0;
        for (int i=0; i < array.length; i++) {
            sum += array[i] * array[i];
        }
        return Math.sqrt(sum);
    }

}
