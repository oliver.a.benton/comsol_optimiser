package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.comsolModelInterface.ComsolModelInterface;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Random;

import static com.ga.nsaga.NsagaUtils.*;
//import static com.ga.nsaga.RunNSAGA.programStartTime;

/**
 * The class that runs the SAGA optimiser
 */
public class SAGA extends NSAGA_optimizer {

    final Logger logger = (Logger) LoggerFactory.getLogger(SAGA.class.getName());
    int numRandomsAddedToPopulation = 0;


    SAGA(ComsolModelInterface comsolModel, GenesAndfScoresBank genesLoaded, GenesAndfScoresBank geneHistory, boolean useComsol, Level loggerLevel, Position[] scattererToIndexMap, String shapeOfScatterer, String shapeOfObject, double scattererRadius, String startDate) throws Exception{
        super(comsolModel, genesLoaded, geneHistory, useComsol, loggerLevel, scattererToIndexMap, shapeOfScatterer, shapeOfObject, scattererRadius, startDate);
        logger.setLevel(loggerLevel);
        // create save file path
        File directory = new File(saveFilePath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }


    @Override
    public GenesAndfScoresBank runOptimizer(boolean resumePreviousRun, int previousGenerationCount) throws Exception {
        long timeSpendScanningDuplicateIndexes = 0;
        int completedPopulationsCounter;
        this.startDate = startDate;
        int currentAnnealingStep = utils.INTIAL_ANNEALING_STEP;
        int parent1Index, parent2Index, crossoverIndex;
        double temperature = STARTING_TEMPERATURE;
        Integer[] offspringIndividual, parent1Data, parent2Data = new Integer[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT];
        int iterationIndex; // number of times the system has completed the innermost loop


        double currentMutationRate = Variables.getInitialMutationProbability();
        Random rand = new Random();
        Path sagaSavePath = Paths.get(saveFilePath, "SAGA", startDate);
        Path path = null;
        Path originalFile = Paths.get(Variables.getMassImageOutputFolder(startDate, ""),"details.txt");
        try {
            File directory = new File(String.valueOf(sagaSavePath));
            if (!directory.exists()) {
                directory.mkdirs();
            }
            path = Paths.get(String.valueOf(sagaSavePath), "details.txt");
            // copy the current active to the save file, and append the next population to it as well.
            Path target = null;
            target = Files.copy(originalFile, path, copyOptions);
            if (target == null) {
                logger.error("Problem while copying details to excel folder");
            }
        } catch (FileSystemException e) {
            logger.error("File system exception. Trying to copy file {} to location {}\n", originalFile , path, e);
            throw e;
        }


        long annealingSetStartTime = System.currentTimeMillis();
        long optimiserStartTime = annealingSetStartTime;
        long timeDiff;

        int loopsEntered = 0;
        scanForIndexDuplicatesInAPopulation(currentPopulation);
        long startTimeForNextCounter = 0;



        try {
//            logger.debug("Running empty model");
            // run a completely empty model to get the empty array.
            // begin initial run.
            logger.info("Now initialising SAGA run");
//            double scatterRadius = Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer"));


            // get initial scatterer positions
            ArrayList<String> scattererNames = getAllScatterers();

            // get the numerical result of the f_score, assign it to the GeneMaker120... value of present equilibrium force - using the original section
            currentPopulation.sortByfScore();
            currentEquilibirumScore = currentPopulation.getInvididual(EQUILIBRIUM_POSITION_INDEX).getfScore();
            currentPopulation.sortByIndex();
//            saveCombinationHistory(); // save initial history
            // print trial run score
            logger.info("Trial run completed, equilibrium found: " + currentEquilibirumScore);
            int currentModel = getCombinationHistoryLength()+1;
            completedPopulationsCounter = 7;
            iterationIndex = 0;
            acceptedModelIndex = 0;
            int i = 0;
            int mutationsConsidered = 0;
            long timeTakenSinceInitalStart = System.currentTimeMillis() - Variables.getProgramStartTime();
            logger.info("Time spent during intial set of data:");
            timeStartedNsagaLoop = System.currentTimeMillis();
            completeTimeLogCheck(loopsEntered);
            logger.info("Time taken since start of program: {}s == {}m == {}h", timeTakenSinceInitalStart/1000, timeTakenSinceInitalStart/60000, timeTakenSinceInitalStart/3600000d);
            timeStartedNsagaLoop = System.currentTimeMillis(); // deliberatly duplicated to add give more time to it.
            logger.info("Now beginning SAGA run");
            resetTotalComsolTimes();
            long currentPopulationTimeStart;
            // while: current annealing step is less than max steps
            while (currentAnnealingStep < utils.totalAnnealingStepsAllowed) {
                annealingSetStartTime = System.currentTimeMillis();
                // while: generator counter is still less than max number of generations
                while (completedPopulationsCounter < utils.maxNumPopulationGenerations) {
                    currentPopulationTimeStart = System.currentTimeMillis();
                    nextPopulation = carryElitePopsToNextGeneration(currentPopulation);
                    logger.info("Time spent scanning duplicate indexes = {}s == {}m == {}h", timeSpendScanningDuplicateIndexes/1000, timeSpendScanningDuplicateIndexes/60000, timeSpendScanningDuplicateIndexes/3600000d);
                    // while: there are less than the max population
                    while (nextPopulation.getSize() < MAX_POPULATION_SIZE) {
                        loopsEntered +=1;
                        utils.runRecoveryCleanUp();
                        // save the current iteration data to the drive.
                        if (acceptedModelIndex % utils.GENERATION_SAVE_COUNTER == 0 && iterationIndex > 0) {
                            startTimeForNextCounter = System.currentTimeMillis();
                            copyCurrentHistory(sagaSavePath.toAbsolutePath().toString(), acceptedModelIndex);
                            saveLogCheck(loopsEntered, startTimeForNextCounter);
                        }
                        // Log the number of models scanned, accepted and rejected for various reasons
                        if (loopsEntered%10 == 0 && completedPopulationsCounter < 1 && currentAnnealingStep < 1){
                            logCounters(loopsEntered);
                        } else if (loopsEntered%100 == 0 && currentAnnealingStep < 1) {
                            logCounters(loopsEntered);
                        } else if (currentAnnealingStep > 0 && loopsEntered%500 == 0) {
                            logCounters(loopsEntered);
                        }
                        // scan for index duplicates in population - there shouldnt be any, and this might be slowing it down - take a look at logging it
                        if (currentEquilibirumScore == 0) {
                            logger.info("Found the perfect cloak!!");
                            GenesAndfScoresBank perfectBank = new GenesAndfScoresBank();
                            perfectBank.addNewIndividual(currentPopulation.getInvididual(utils.EQUILIBRIUM_POSITION_INDEX));
                            return perfectBank;
                        }

                        int currentCombinationHisotryLength = getCombinationHistoryLength()-20;
//                        if (currentCombinationHisotryLength != acceptedModelIndex) {
//                            int breakx1 = 0;
//                        }

                        // TODO test
                        // if the number of iterations since the last model is more than 400 while early enough, or more than 30% of the total models accepted, then generate a random member to add to the population
                        // only run if the control variable useGeneRandomisation is true
                        if ( RunNSAGA.useGeneRandomisation && ((timeSinceLastModel > 400 && acceptedModelIndex < 1000) || (timeSinceLastModel > 0.3 * acceptedModelIndex && acceptedModelIndex > 1000)) ) {
                            IndividualsData[] newIndviduials  = generateXRandomIndividuals(1, scattererRadius);
                            int checkedHistory = checkHistory(newIndviduials[0].getGenes());
                            if (checkedHistory != 0) {
                                logger.trace("Copy of new offspring found in history, continuing.");
                                timeSinceLastModel++;
                                historyRejections ++;
                                if (checkedHistory == 2)
                                    historyRejectionsFromRejectedList++;
                                continue;
                            }
                            addIndividual(newIndviduials[0]);
                            timeSinceLastModel = 0;
                            numRandomsAddedToPopulation ++;
                            acceptedModelIndex ++;
                            logger.info("ADDED INDIVIDUAL FROM RANDOM. THIS MAKES {} out of {}", numRandomsAddedToPopulation, acceptedModelIndex);
                        }

                        //OFFSPRING
                        // select new parents and produce offspring
                        startTimeForNextCounter = System.currentTimeMillis();
                        parent1Index = rand.nextInt(MAX_POPULATION_SIZE);
                        parent2Index = rand.nextInt(MAX_POPULATION_SIZE);
                        while (parent1Index == parent2Index) {
                            // while: the 2nd parent is the same as the first parent, select random parent 2
                            parent2Index = rand.nextInt(MAX_POPULATION_SIZE);
                        }
                        Integer[][] offspringProduced = produceMultipleOffspring(currentPopulation, parent1Index, parent2Index);
                        combosGenerated +=2;
                        combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;
                        int result = checkIndividual(offspringProduced[0], loopsEntered, temperature);

                        result = checkIndividual(offspringProduced[1], loopsEntered, temperature);

//                        offspringIndividual = produceOffspring(currentPopulation, parent1Index, parent2Index);
//                        System.out.println(outputGenes(currentPopulation.getInvididual(parent1Index)));
//                        System.out.println(outputGenes(currentPopulation.getInvididual(parent2Index)));
//                        System.out.println(outputGenes(offspringIndividual));
                        // run collision checks if needed on offspring  .

                        // every 10 models for first population, every 100 for first annealing step, every 100 after that
                        if (lastReportedIndex != acceptedModelIndex && acceptedModelIndex%10 == 0 && completedPopulationsCounter < 1 && currentAnnealingStep < 1){
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}", acceptedModelIndex, nextPopulation.getSize());
                        } else if (lastReportedIndex != acceptedModelIndex && acceptedModelIndex%100 == 0 && currentAnnealingStep < 1) {
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}/{} of population {}/{} of annealing step {}/{}",
                                    acceptedModelIndex, nextPopulation.getSize(), currentPopulation.getSize(),
                                    completedPopulationsCounter, utils.maxNumPopulationGenerations, currentAnnealingStep, utils.totalAnnealingStepsAllowed);
                        } else if (currentAnnealingStep > 0 && lastReportedIndex != acceptedModelIndex && acceptedModelIndex%500 == 0) {
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}/{} of population {}/{} of annealing step {}/{}",
                                    acceptedModelIndex, nextPopulation.getSize(), currentPopulation.getSize(),
                                    completedPopulationsCounter, utils.maxNumPopulationGenerations, currentAnnealingStep, utils.totalAnnealingStepsAllowed);
                            comsolModel.generateFreshModel();
                            System.gc(); // force GC after every population change - roughly every 63 successful additions
                            garbageCollectionChecksTime = System.currentTimeMillis() - startTimeForNextCounter;
                        }

                        // MUTATION
                        // if random number is higher than mutation, mutate
                        if (rand.nextDouble() < currentMutationRate) {
                            // random which individual to mutate
                            startTimeForNextCounter = System.currentTimeMillis();
                            int mutationPick = rand.nextInt(4);
                            Integer[] mutatedOffspring;
                            switch (mutationPick) {
                                case 0:
                                    mutatedOffspring = utils.copyIntegerArray(offspringProduced[0]);
                                    break;
                                case 1:
                                    mutatedOffspring = utils.copyIntegerArray(currentPopulation.getInvididual(parent1Index).getGenes());
                                    break;
                                case 2:
                                    mutatedOffspring = utils.copyIntegerArray(currentPopulation.getInvididual(parent2Index).getGenes());
                                    break;
                                case 3:
                                    mutatedOffspring = utils.copyIntegerArray(offspringProduced[1]);
                                    break;
                                default:
                                    logger.error("SHOULD NOT GET HERE");
                                    mutatedOffspring = null;
                                    System.exit(1);
                            }

                            // mutated individual chosen, mutate
                            mutatedOffspring = performMutation(mutatedOffspring);
                            combosGenerated +=1;
                            combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;
                            result = checkIndividual(mutatedOffspring, loopsEntered, temperature);
                            if (result >=0 )
                                mutationsConsidered ++;
                        } // end mutation

                        if (lastReportedIndex != acceptedModelIndex && acceptedModelIndex%10 == 0 && completedPopulationsCounter < 1 && currentAnnealingStep < 1){
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}", acceptedModelIndex, nextPopulation.getSize());
                        } else if (lastReportedIndex != acceptedModelIndex && acceptedModelIndex%100 == 0 && currentAnnealingStep < 1) {
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}/{} of population {}/{} of annealing step {}/{}",
                                    acceptedModelIndex, nextPopulation.getSize(), currentPopulation.getSize(),
                                    completedPopulationsCounter, utils.maxNumPopulationGenerations, currentAnnealingStep, utils.totalAnnealingStepsAllowed);
                        } else if (currentAnnealingStep > 0 && lastReportedIndex != acceptedModelIndex && acceptedModelIndex%500 == 0) {
                            lastReportedIndex = acceptedModelIndex;
                            logger.info("  Completed {} models. Currently on population member {}/{} of population {}/{} of annealing step {}/{}",
                                    acceptedModelIndex, nextPopulation.getSize(), currentPopulation.getSize(),
                                    completedPopulationsCounter, utils.maxNumPopulationGenerations, currentAnnealingStep, utils.totalAnnealingStepsAllowed);
                        }
                        saveToActiveHistory();
                        iterationIndex++;
                    }// end while nextPopSize < max - filled out the next gene pool
                    populationTotalTime += System.currentTimeMillis() - currentPopulationTimeStart;
                    logger.debug("Completed creating population {}, now adding current one to file", completedPopulationsCounter);
                    completedPopulationsCounter++;
                    long averagePopulationTime =  populationTotalTime/ (completedPopulationsCounter + maxNumPopulationGenerations * currentAnnealingStep);
                    logger.info("Average time per population = {}s == {}m == {}h", averagePopulationTime/1000, averagePopulationTime/60000, averagePopulationTime/3600000d );
                    startTimeForNextCounter = System.currentTimeMillis();
                    nextPopulation.sortByfScore();
                    // update the equilibrium value
                    if (nextPopulation.getInvididual(EQUILIBRIUM_POSITION_INDEX).getfScore() < currentEquilibirumScore)
                        currentEquilibirumScore = nextPopulation.getInvididual(EQUILIBRIUM_POSITION_INDEX).getfScore();
                    currentPopulation = nextPopulation;
                    timeSpentCopyingNextPopulationOver =  System.currentTimeMillis() - startTimeForNextCounter;
                    startTimeForNextCounter = System.currentTimeMillis();
                    saveToActiveHistory();
                    saveLogCheck(loopsEntered, startTimeForNextCounter);
                    startTimeForNextCounter = System.currentTimeMillis();
                    nextPopulation = new GenesAndfScoresBank();
                    currentPopulation.sortByfScore();
                    timeSpentCopyingNextPopulationOver =  System.currentTimeMillis() - startTimeForNextCounter;
                    int savedImages = 0;
                    i = 0;
                    startTimeForNextCounter = System.currentTimeMillis();
                    // !Runall images is because there is a a later peice of code (NSAGA_optimiser, line 324) that will save every image.
                    while (!RunNSAGA.saveAllImages && savedImages < utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION && i < currentPopulation.getSize()) {
                        IndividualsData individual = currentPopulation.getInvididual(i);
                        // if image has not been saved
                        if (!individual.isImageSaved()) {
                            setAllScatterers(individual.getGenes(), scattererToIndexMap);
                            comsolModel.runModel(individual.getModelNumber());
                            if (!Variables.isSaveNoImages())
                                comsolModel.saveImagesInt("savedImages_" + individual.getModelNumber() + "__startTime-", individual.getModelNumber(), individual.getGenes(), startDate, individual.getArray360Points());
                            individual.setImageSaved(true);
                            savedImages ++;
                            numberOfImagesSaved ++;
                        }
                        i++;
                    }
                    timeSpentSavingImages += System.currentTimeMillis()- startTimeForNextCounter;
                    startTimeForNextCounter = System.currentTimeMillis();
                    for (i = 0; i < utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION; i++) {
                        IndividualsData individual = currentPopulation.getInvididual(i);
                        nextPopulation.addNewIndividual(individual);
//                        if (!individual.isImageSaved()) {
//                            setAllScatterers(individual.getGenes(), scattererToIndexMap);
//                            comsolModel.runModel(individual.getModelNumber());
//                            comsolModel.saveImagesInt("savedImages_" + individual.getModelNumber() + "__startTime-", individual.getModelNumber(), new String[]{}, startDate);
//                            individual.setImageSaved(true);
//                        }
                    }
                    timeSpentCopyingNextPopulationOver =  System.currentTimeMillis() - startTimeForNextCounter;
                    currentPopulation.sortByIndex();
                    logger.trace("Now moving to population iteration {}", completedPopulationsCounter);
                    completeTimeLogCheck(loopsEntered); // complete log check every popoulation completed
                    comsolModel.generateFreshModel();
                    startTimeForNextCounter = System.currentTimeMillis();
                    System.gc(); // force GC after every population change - roughly every 63 successful additions
                    garbageCollectionChecksTime = System.currentTimeMillis() - startTimeForNextCounter;
                } // end while completedPopulationsCounter< max
                currentAnnealingStep++;
                double newTemperature = temperatureFunction(temperature, currentAnnealingStep);
                long endAnnealingStepTime = System.currentTimeMillis();
                timeDiff = endAnnealingStepTime- annealingSetStartTime;
                logger.trace("Taking temperature from {} to {}", temperature, newTemperature);
                logger.info("Completed annealing step {}. Time taken is {}s == {}m == {}h", currentAnnealingStep - 1, timeDiff/1000, timeDiff/60000, timeDiff/3600000d);
                logger.info("Models accepted = {} models rejected = {}", decimalFormat.format(acceptedModelIndex), decimalFormat.format(totalModelsEvaluatedByComsol - acceptedModelIndex),
                        acceptedModelIndex, nextPopulation.getSize(), currentPopulation.getSize(),
                        completedPopulationsCounter, utils.maxNumPopulationGenerations, currentAnnealingStep, utils.totalAnnealingStepsAllowed);
                totalTimeForAnnealingSteps += (endAnnealingStepTime - annealingSetStartTime); // average time across all annealing steps
                long averageAnnealingStepTime = totalTimeForAnnealingSteps/currentAnnealingStep;
                timeDiff = endAnnealingStepTime - annealingSetStartTime; // time for this annealing step.
                logger.info("Time for this annealing step: {}s == {}m == {}h", timeDiff/1000, timeDiff/60000, timeDiff/3600000d);
                logger.info("Average time for annealing steps: {}s == {}m == {}h", averageAnnealingStepTime /1000, averageAnnealingStepTime /60000, averageAnnealingStepTime /3600000d);
                temperature = newTemperature;
                completedPopulationsCounter = 0;
                comsolModel.generateFreshModel();
                System.gc(); // force gc after every annealing step

            }// end while temp
            logger.debug("End of SAGA code - completed full temperature run");
            // copy the final version of the geneBank over to the drive.
            startTimeForNextCounter = System.currentTimeMillis();
            copyCurrentHistory(sagaSavePath.toAbsolutePath().toString(), acceptedModelIndex);
            saveLogCheck(loopsEntered, startTimeForNextCounter);

            return nextPopulation;
        } catch (Exception e) {
            logger.error("Exception encountered when running system. System stats: \n    ****Simulated annealing step {}, temp {}\n    ****Current Population counter = {}", currentAnnealingStep, temperature, nextPopulation!=null? nextPopulation.getSize():"next pop not initialised");
            throw e;
        }
    }


    private void scanForIndexDuplicatesInAPopulation(GenesAndfScoresBank population) {
        int debugIndex1 = 0;
        int debugIndex2 = 0;
        for (debugIndex1 = 0; debugIndex1 < population.getSize(); debugIndex1 ++) {
            for (debugIndex2 = debugIndex1 +1; debugIndex2 < population.getSize(); debugIndex2 ++) {
                if (population.getInvididual(debugIndex1).getIndex() == population.getInvididual(debugIndex2).getIndex()) {
                    System.out.println("    Found duplicate in current population: "+debugIndex1+" is the same as "+debugIndex2);
                    String debug1RecorededAndImaged = population.getInvididual(debugIndex1).isRecoreded()? "recorded and ": "not recorded and ";
                    debug1RecorededAndImaged +=  population.getInvididual(debugIndex1).isImageSaved()? "image Saved": "not image saved";

                    String debug2RecorededAndImaged = population.getInvididual(debugIndex2).isRecoreded()? "recorded and ": "not recorded and ";
                    debug2RecorededAndImaged +=  population.getInvididual(debugIndex2).isImageSaved()? "image Saved": "not image saved";
                    System.out.println("    "+debugIndex1+"is set to "+ debug1RecorededAndImaged +" while "+debugIndex2+ " is set to " + debug2RecorededAndImaged);
                    int breakInt = 0;
                }
            }
        }
    }
}
