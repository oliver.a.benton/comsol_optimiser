package com.ga.nsaga;

/**
 * Optimiser Interface, made to ensure that all optimisers will have the same common method to call, allowing the control
 * class RunNSAGA to switch out whichever optimiser is needed, only having to change which concrete class is built.
 */
public interface optimiserInterface  {

    public GenesAndfScoresBank runOptimizer(boolean resumePreviousRun, int previousGenerationCount) throws Exception;

}
