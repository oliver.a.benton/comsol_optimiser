package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.comsol.model.util.ModelUtil;
import com.comsolModelInterface.ComsolModelInterface;
import com.comsolModelInterface.ComsolModelInterface2_0;
import com.comsolModelInterface.EmptyModel;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.ga.nsaga.Run_single_entry.comsolModel;

public class RunExistingGeneFile {

    private static NsagaUtils utils = new NsagaUtils();
    final static Logger logger = (Logger) LoggerFactory.getLogger(RunExistingGeneFile.class.getName());
    final static boolean USE_COMSOL = Variables.isUseComsol(); // control variable, where it can be switched off to give random answers,
    //    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\2019-11-11--11-23_output.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistor__archive_2020-07-30--11-50.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistory_2020-06-26.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistory_2020-06-26.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\2019-11-11--11-23_output.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistor__archive_2020-07-30--11-50.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"

    static String optimizerType = Variables.getOptimizerType(); // control variable to choose which of the optimizers to use - SAGA or NSAGAD:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\activeProcessingCSV\geneHistory_renamed.csv
    static boolean resumePreviousRun = Variables.isResumePreviousRun(); // control if using a previous run. If yes, then please do use the previous generationCounter below - use the GENE_BANK_FILE_TO_USE to control which file is loaded
    static int previousGenerationCounter = 0;
    static boolean runCollsionChecks = Variables.isRunCollsionChecks();
    //    final static Object[][] SCATTERER_POSITION_INDEX_MAP = utils.loadParamsFromCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv");
    static Position[] SCATTERER_POSITION_INDEX_MAP;
    final static String shapeOfScatterer = Variables.getShapeOfScatterer();
    final static String shapeOfObject = Variables.getShapeOfObject();
    static double radiusScatterer;
    static String comsolReferencesFolder = System.getProperty("user.dir")+"\\src\\main\\resources\\comsolPreferences\\";
    static boolean useGeneRandomisation = Variables.isUseGeneRandomisation();
    static boolean saveAllImages = Variables.isSaveAllImages();
    static boolean stillInitialising = true;
    protected static double[][] emptyModel360Pressure;
    protected static double[][] centralObjectOnly360Pressure;
//    static long programStartTime;
    protected static String startDate;
    static String fileToRun = "D:\\dev_D\\comsol_optimiser\\checking_combining_csvs.csv";
    static boolean containsACollision = false;
    static ArrayList<Integer> collisionLocations = new ArrayList<>();
    static GenesAndfScoresBank loadedGenes;
    static int number_to_start_at = 0;
    static int number_to_end ; // leave null to end at the end of data


    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm"); // used in saving the data, once saving is made available.
        Date date = new Date();
        startDate = dateFormat.format(date);
        Variables.setProgramStartTime(System.currentTimeMillis());
        SCATTERER_POSITION_INDEX_MAP = setUpScattererIndexMap();
        Variables.setScattererPositionIndexMap(SCATTERER_POSITION_INDEX_MAP);
        long startTime = System.currentTimeMillis();
        Level loggerLevel;
        if (USE_COMSOL) {
            loggerLevel = Level.DEBUG;
        } else {
            loggerLevel = Level.INFO;
        }

        // insert comsol model of choice here
        comsolModel = null;

        if (USE_COMSOL) {
            // initialise comsol model.
            ModelUtil.initStandalone(false); // method to make comsol work.

            File preferenceFile = new File (comsolReferencesFolder+"\\comsolReferences.txt");
            // if preferneces exists, load file
            if (preferenceFile.exists()) {
                ModelUtil.loadPreferences();
            } else {
                File preferencesDir = new File (comsolReferencesFolder);
                utils.setComsolPreferences();

//                try {
//                    FileWriter fw = new FileWriter(preferenceFile);
//                    fw.write("Saved preferences");
//                    fw.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
            comsolModel = new ComsolModelInterface2_0(USE_COMSOL, loggerLevel);
        } else {
            comsolModel = new EmptyModel();
        }

        File geneHistoryToRun = new File(fileToRun);

        if (!geneHistoryToRun.exists()) {
            logger.error("Trying to load target file {} - it doesn't exist", geneHistoryToRun.getAbsolutePath());
            System.exit(-1);
        }

        Variables.setRadiusScatterer(Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer")));
//        radiusScatterer = Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer"));
        logger.info("Comsol Model initiated");

        GenesAndfScoresBank genesLoaded = null;
        GenesAndfScoresBank genesHistory = null;

        loadedGenes = loadGenes(geneHistoryToRun.getAbsolutePath());

        try {
            comsolModel.runBlankModel();
            emptyModel360Pressure = comsolModel.getModels360Points();
        } catch (Exception e) {
            e.printStackTrace();
        }
        comsolModel.saveImagesOneOff("emptyModels\\blank_area", utils.empty_area_run, new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, emptyModel360Pressure);

        setAllScatterers(utils.getEmpyScattererArray(), Variables.getScattererPositionIndexMap());
        try {
            comsolModel.runModel(utils.no_scatterer_run);
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }
        centralObjectOnly360Pressure = comsolModel.getModels360Points();
        comsolModel.saveImagesOneOff("emptyModels\\only_central_object", utils.no_scatterer_run, new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, centralObjectOnly360Pressure);
        double denominator = comsolModel.getResult() * Double.parseDouble(comsolModel.getDoubleParameter("fitness_for_squared_metrics"));
        Variables.setSanchisDenominator(denominator);
        comsolModel.setFitnessForSquareMetrics(denominator);
        try {
            comsolModel.runModel(-997);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logControlVariables(startDate, comsolModel, false);

        GenesAndfScoresBank runGenes = new GenesAndfScoresBank();
        if (number_to_end == 0)
            number_to_end = loadedGenes.getSize();
        for (int i = number_to_start_at; i < number_to_end; i++) {
            if (i != 0 && i%100 ==0) {
                comsolModel.generateFreshModel();
                logger.info("Generating fresh model");
                boolean savedFile = utils.appendGeneHistoryToFile(Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date)), runGenes);
                utils.runRecoveryCleanUp();
                if (!savedFile) {
                    logger.error("Problem while appending the data");
                }
                logger.info("Model {} has been completed ", i);
            }
            runIndividual(loadedGenes.getInvididual(i));
            runGenes.addNewIndividual(loadedGenes.getInvididual(i));
        }
        logger.info("Completed running models");
        saveCombinationHistory(runGenes,  Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date)));
        logControlVariables(startDate, comsolModel, true);
        logger.info("Saved models to {}", Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date))+".csv");
        System.exit(0);


    }

    protected static boolean saveCombinationHistory(GenesAndfScoresBank historyBank, String path) {
        boolean savedFile = utils.appendGeneHistoryToFile(path, historyBank);
        if (!savedFile) {
            logger.error("Problem while appending the data");
        }
        return savedFile;
    }

    private static GenesAndfScoresBank loadGenes(String filePath) {
        Object[][] csvRawGenes = utils.loadParamsFromCSV((filePath), false);
        logger.debug("CSV Genes loaded in");

        GenesAndfScoresBank newGeneBank = new GenesAndfScoresBank();
        int index = 0;
        for (Object[] loadedIndividualData: csvRawGenes) {
            if (loadedIndividualData[0].equals("")) {
                logger.trace("Skip over, as found an empty first item of the array");
                continue;
            }
            logger.trace("Pre chromosome load Index = " + index);
            // copy the array from 1 end to the other, ignoring the final element.
            // todo will likely have to parse this information.
            Integer[] chromosome = new Integer[Variables.getMaximumScatterersInAQuadrant()];
            String[] stringChromosome = (String[]) Arrays.copyOfRange(loadedIndividualData, 0, Variables.getMaximumScatterersInAQuadrant());
            for (int i= 0; i< stringChromosome.length; i++) {
                // remove white space
                String s = stringChromosome[i];
                s =s.replaceAll("\\s+", "");
                chromosome[i] = Integer.parseInt(s);
            }
            logger.trace("Post chromosome load Index = " + index);

            // todo write up the code to save and load the information
            // chromosome, fscore, model number, 360 points, mse,  sanchis, sanchisH, sanchisSimple, Sanchez, SanchezSimple
            newGeneBank.addNewData(chromosome, -999.0, index,
                    null, Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE ,Double.MAX_VALUE);
            logger.trace("Post geneLoaded Index = " + index);
            index++;
        }

        return newGeneBank;
    }

    private static void runIndividual(IndividualsData individual) {
        try {
            containsACollision = utils.runCollisionCheckRandomIndividual(individual.getGenes(), Variables.getRadiusScatterer(), Variables.getScattererPositionIndexMap());
            if (containsACollision) {
                logger.info("Individual {} contains a collision", individual.getIndex());
                collisionLocations.add(individual.getIndex());
            }
            setAllScatterers(individual.getGenes(), Variables.getScattererPositionIndexMap());
            comsolModel.runModel(individual.getIndex());
            double[][] model360 = comsolModel.getModels360Points();
            double sanchisMetric = comsolModel.getResult();
            double mse = calculateError(comsolModel.getModels360Points());
            double rmse = Math.sqrt(mse);
            double sanchisMetric2 = 1 - sumSquares(model360) / sumSquares(centralObjectOnly360Pressure);
            double sanchisMetric3 = sumSquares(model360) / sumSquares(centralObjectOnly360Pressure); // sum of 360 points with cloak / sum of 360 points with the central object.
            double sanchisMetric4 = sumSquares(model360);
            double sanchisMetricVsPerfect = 1 - sumSquares(model360) / sumSquares(emptyModel360Pressure);
            double sanchezMetric1 = Math.pow((1 + (sumAbsolutes(model360) / sumAbsolutes(centralObjectOnly360Pressure))), -1);
            double sanchezMetricVsperfect = Math.pow((1 + (sumAbsolutes(model360) / sumAbsolutes(emptyModel360Pressure))), -1);
            double sanchezMetric2 = sumAbsolutes(model360);
            double normalisedMSE = normalisedMSE(model360);
            double l2Norm = calculateL2Norm(model360[0]);
            individual.setArray360Points(comsolModel.getModels360Points());
            individual.setMse(calculateError(comsolModel.getModels360Points()));
            individual.setSanchisMetric(sanchisMetric);
            individual.setSanchisMetricH(sanchisMetric3);
            individual.setSanchisMetricSimple(sanchisMetric4);
            individual.setSanchezMetric(sanchezMetric1);
            individual.setSanchezMetricSimple(sanchezMetric2);
            individual.setNormalisedMSE(normalisedMSE);
            individual.setSanchisPerfectCloak(sanchisMetricVsPerfect);
            individual.setSanchezPerfectCloak(sanchezMetricVsperfect);
            individual.setfScore(utils.selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm));
            individual.setRmse(rmse);
            double[] model360SubArray = Arrays.copyOfRange(model360[0], 1, model360[0].length);
            double[][] model360SubArray2D = new double[1][360];
            model360SubArray2D[0] = model360SubArray;
            individual.setMinPressure(getMinOfModel360(model360SubArray));
            individual.setMaxPressure(getMaxOfModel360(model360SubArray));
            individual.setAveragePressure(sum(model360) / 360);
            individual.setAbsoluteAveragePressure(sumAbsolutes(model360) / 360);
            if (Variables.isSaveAllImages()) {
                comsolModel.saveImagesInt("savedImages_" + individual.getModelNumber() + "__startTime-", individual.getModelNumber(), individual.getGenes(), startDate, comsolModel.getModels360Points());
            }
        } catch (Exception e) {
            logger.error("Error when running individual {}\n", individual.getIndex(), e);
            if (e.getMessage().toLowerCase().equals("there is not enough space on the disk")) {
                int breakx = 0;
            }
        }
    }

    protected List<Integer> sortGenome(Integer[] inputGenome) {
        inputGenome = utils.setChromosomeEndToZero(inputGenome);
        List<Integer> sortedIndividual = new ArrayList<>(inputGenome.length);
        // add each gene to sorted
        for (Integer integer : inputGenome) {
            sortedIndividual.add(integer);
        }
        //sort
        Collections.sort(sortedIndividual);
        return sortedIndividual;
    }

    protected static double calculateError(double[][] model360) {
        int i;
        double meanSquareError = 0.0;
        double sumOfDifferences = 0.0;
        double sumOfDifferencesSquared = 0.0;//i = 1, as the first column of the table is set to the frequency, not needed. However it should be independant of it - 20k - 20k = 0, therefore should have no impact on error rate
        for (i=1; i<model360[0].length; i++) {
            sumOfDifferences += (model360[0][i] - centralObjectOnly360Pressure[0][i]);
            sumOfDifferencesSquared += Math.pow(model360[0][i] - centralObjectOnly360Pressure[0][i], 2);
        }
        meanSquareError = sumOfDifferencesSquared/model360.length;

        return meanSquareError;
    }

    protected static double sum(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += (model360[0][i]);
        }
        return sum;
    }

    protected static double sumAbsolutes(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.abs(model360[0][i]);
        }
        return sum;
    }

    protected static double sumSquares(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.pow((model360[0][i]), 2);
        }
        return sum;
    }

    /**
     * https://rem.jrc.ec.europa.eu/RemWeb/atmes2/20b.htm
     * @param model360
     * @return
     */
    protected static double normalisedMSE(double[][] model360) {
        double averageBase = sum(centralObjectOnly360Pressure)/centralObjectOnly360Pressure.length;
        double averageOfModel = sum(model360)/model360.length;
        double squareSumOfDifferencesOverMeans = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            // sum, [(P-m)**2 / (Paverage * MAverage)]
            squareSumOfDifferencesOverMeans += Math.pow(model360[0][i] - centralObjectOnly360Pressure[0][i],2) / (averageBase * averageOfModel);
        }
        return (1/model360.length) * squareSumOfDifferencesOverMeans;
    }


    public static Position[] setUpScattererIndexMap() {
//        Object[][] initalLoadedMap = utils.loadParamsFromCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_mesh_ultrathin.csv", true); // ultrathin
        Object[][] initalLoadedMap = utils.loadParamsFromCSV(Variables.getCsvMapPath(), true); // original one
        logger.debug("Map loaded from CSV");
        Position[] positions = new Position[initalLoadedMap.length];
        int i = 0;
        DecimalFormat df = new DecimalFormat("#.#########");
        df.setRoundingMode(RoundingMode.CEILING);
        logger.debug("Setting up position index");
        for (i = 0; i< initalLoadedMap.length; i++) {
            logger.trace("Beginning to set up position {}", i);
            try {
                positions[i] = new Position(Double.parseDouble((String) initalLoadedMap[i][3]), Double.parseDouble((String) initalLoadedMap[i][1]));
            } catch (Exception e) {
                logger.error("Error when trying to setup positon {}", i);
            }
        }
//        NsagaUtils.max_no_scat_pos = positions.length;
        return positions;
    }

    protected static double getMinOfModel360(double[] model360) {
        double min = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] < min) {
                min = model360[i];
            }
        }
        return min;
    }

    protected static double getMaxOfModel360(double[] model360) {
        double max = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] > max) {
                max = model360[i];
            }
        }
        return max;
    }

    /**
     * Method to set all the scatterer coordinates, regardless of which kind of scatterer it is.
     * @param testedIndividual
     * @param scattererToIndexMap
     */
    protected static void setAllScatterers(Integer[] testedIndividual, Position[] scattererToIndexMap) {
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;
            case "Circle":
                comsolModel.setAllCircularScatterers(testedIndividual, scattererToIndexMap);
                break;
            default:
                comsolModel.setAllSquareScatterers(testedIndividual, scattererToIndexMap);
                break;

        }

    }

    private static void logControlVariables(String startDate, ComsolModelInterface comsolModel, boolean postCompletion) {
        logger.info("Confirming before Run:");
        logger.info("    runCollisionChecks = "+ runCollsionChecks);
        logger.info("    optimiserType = "+ optimizerType);
        logger.info("    shapeOfScatterer = "+ shapeOfScatterer);
        logger.info("    shapeOfObject = "+ shapeOfObject);
        logger.info("    radiusScatterer = "+ Variables.getRadiusScatterer());
        logger.info("    resumePreviousRun = "+ resumePreviousRun);
        if (resumePreviousRun)
            logger.info("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
        logger.info("    useGeneRandomisation = " + useGeneRandomisation);
        logger.info("    scattererType = " + shapeOfScatterer);
        logger.info("    central Type = " + shapeOfObject);

        try{
            String newLine = System.getProperty("line.separator");
            String fileName = "details.txt";
            if (postCompletion) {
                fileName = "details_post.txt";
            }
            FileWriter detailsFile =  new FileWriter(String.valueOf(Paths.get(Variables.getMassImageOutputFolder(startDate, "")+ "\\"+fileName)));
            detailsFile.write("Run existing gene file."+ newLine);
            detailsFile.write("File is: "+fileToRun+ newLine);
            detailsFile.write("Confirming for Run:"+ newLine);
            detailsFile.write("    Version num = "+ Variables.getVersionUsed()+newLine);
            if (postCompletion) {
                detailsFile.write("    Collisions occurred at "+ collisionLocations.size()+" out of "+ loadedGenes.getSize()+" combinations "+ newLine);


                detailsFile.write("    Collisions occurred at combinations: " + collisionLocations.toString() + newLine);
            }
            detailsFile.write("    shapeOfScatterer = "+ shapeOfScatterer+ newLine);
            detailsFile.write("    radiusScatterer = "+ Variables.getRadiusScatterer()+ newLine);
            detailsFile.write("    area examined = "+ Variables.getRegionThickness()+"m"+ newLine);
            detailsFile.write("    " );


            detailsFile.write("    shapeOfObject = "+ shapeOfObject+ newLine);
            detailsFile.write("    scattererType = " + shapeOfScatterer+ newLine);
            detailsFile.write("    " );


            detailsFile.write("Radius central pipe= " + comsolModel.getDoubleParameter("radius_central_pipe")+ newLine);
            detailsFile.write("Radius scatterer= " + comsolModel.getDoubleParameter("radius_scatterer")+ newLine);
            detailsFile.write("Scat max distance= " + comsolModel.getDoubleParameter("scatterers_max_distance")+ newLine);
            detailsFile.write("    " );


            detailsFile.write("onehot_coord_resolution= " + Variables.getPixelWidth()+ newLine);
            detailsFile.close();
            int breakx = 0;
        } catch (IOException e) {
            logger.error("IO exception when writing details file", e);
        }
    }

    private static double calculateL2Norm(double[] model360) {
        // F = ||W-U||/||V-U|| where U = empty region vector, V = object field vector, W = cloak pressure vector
        // U = emptyModel360Pressure, V = Central object only 360 pressure, W = input model 360 vector
        double[] numeratorArray = subtractArrays(model360, emptyModel360Pressure[0]);
        double[] denominatorArray = subtractArrays(centralObjectOnly360Pressure[0], emptyModel360Pressure[0]);

        double numerator = abosoluteValueDoubleArray(numeratorArray);
        double denominator = abosoluteValueDoubleArray(denominatorArray);

        return numerator/denominator;
    }

    private static double[] subtractArrays(double[] array1, double[] array2) {
        double[] tempArray  = new double[array1.length];
        for (int i=0; i < array1.length; i++) {
            tempArray[i] = array1[i]-array2[i];
        }
        return  tempArray;
    }

    private static double abosoluteValueDoubleArray(double[] array) {
        double sum = 0;
        for (int i=0; i < array.length; i++) {
            sum += array[i] * array[i];
        }
        return Math.sqrt(sum);
    }

}
