package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.comsolModelInterface.ComsolModelInterface;
import org.slf4j.LoggerFactory;

import java.io.File;

public class NSAGA extends NSAGA_optimizer {

    final Logger logger = (Logger) LoggerFactory.getLogger(NSAGA.class.getName());
    private final String saveFilePath = "D:\\dev_D\\comsol_optimiser\\target\\out\\NSAGA_out";

    NSAGA(ComsolModelInterface comsolModel, GenesAndfScoresBank genesLoaded, GenesAndfScoresBank geneHistory, boolean useComsol, Level loggerLevel, Position[] scattererToIndexMap, String shapeOfScatterer, String shapeOfObject, double scattererRadius, String startDate) throws Exception{
        super(comsolModel, genesLoaded, geneHistory, useComsol, loggerLevel, scattererToIndexMap, shapeOfScatterer, shapeOfObject, scattererRadius, startDate);
        logger.setLevel(loggerLevel);
        // create save file path
        File directory = new File(saveFilePath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }


    @Override
    public GenesAndfScoresBank runOptimizer(boolean resumePreviousRun, int previousGenerationCount) throws Exception {
        logger.debug("Now initialising NSAGA run");
        logger.debug("Now beginnning NSAGA run");
        return null;
    }
}
