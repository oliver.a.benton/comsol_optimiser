package com.ga.nsaga_3;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.Variables;
import com.ga.nsaga.Position;
import org.slf4j.LoggerFactory;
import com.comsol.model.util.ModelUtil;
import com.comsolModelInterface.ComsolModelInterface;
import com.comsolModelInterface.ComsolModelInterface2_0;
import com.comsolModelInterface.ComsolModelInterface3_0;
import com.comsolModelInterface.EmptyModel;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static java.nio.file.Paths.*;

/**
 * Control code for the optimizers. This is where you can switch out the scatterer types.
 */
public class RunNSAGA {
    /**
     * todo: add a second individual during crossover - the opposite of the one currently generated
     * todo: rate of cooling - use the exponential to increase the
     */
    private static NsagaUtils utils = new NsagaUtils();
    final static Logger logger = (Logger) LoggerFactory.getLogger(RunNSAGA.class.getName());
    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistory_2020-06-26.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"

//    final static Object[][] SCATTERER_POSITION_INDEX_MAP = utils.loadCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv");
    static String comsolReferencesFolder = System.getProperty("user.dir")+"\\src\\main\\resources\\comsolPreferences\\";
    static boolean stillInitialising = true;


    public static void main(String[] args) {
        Variables.setProgramStartTime(System.currentTimeMillis());
        long startTime = System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm"); // used in saving the data, once saving is made available.
        Date date = new Date();
        Level loggerLevel;
        if (Variables.isUseComsol()) {
            loggerLevel = Level.DEBUG;
        } else {
            loggerLevel = Level.INFO;
        }

        // insert comsol model of choice here
        ComsolModelInterface comsolModel = null;

        if (Variables.isUseComsol()) {
            // initialise comsol model.
            ModelUtil.initStandalone(false); // method to make comsol work.
            utils.setComsolPreferences();
            if (Variables.isUseOldSystem())
                comsolModel = new ComsolModelInterface2_0(Variables.isUseComsol(), loggerLevel);
            else
                comsolModel = new ComsolModelInterface3_0(Variables.isUseComsol(), loggerLevel);
        } else {
            comsolModel = new EmptyModel();
        }
        utils.runRecoveryCleanUp();
        File geneHistoryCSV = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\activeProcessingCSV\\activeHistory");
        // move the old gene history file.
        if (new File(geneHistoryCSV.getAbsolutePath()+".csv").exists()) {
            String targetFilePath = geneHistoryCSV.getAbsolutePath().substring(0, geneHistoryCSV.getAbsolutePath().length() - 4) + "__archive_created_" + dateFormat.format(date) + ".csv";
            try {
                Files.move(get(geneHistoryCSV.getPath()+".csv"), get(targetFilePath));
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("Error copying the old gene history file to archive");
            }
        }
        File completeHistoryCSV = new File(System.getProperty("user.dir") +"\\src\\main\\resources\\activeProcessingCSV\\geneHistoryAll.csv");
        if (completeHistoryCSV.exists()) {
            String targetFilePath = completeHistoryCSV.getAbsolutePath().substring(0, completeHistoryCSV.getAbsolutePath().length()-4) +"__archive_"+dateFormat.format(date)+".csv";
            try {
                Files.move(Paths.get(completeHistoryCSV.getPath()), Paths.get(targetFilePath));
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("Error copying the old gene history file to archive");
            }
        }
        Variables.setCompleteHistoryFilePath(completeHistoryCSV.getAbsolutePath());

        Variables.setRadiusScatterer(Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer")));
        Variables.setRadiusCentralObject(Double.parseDouble(comsolModel.getDoubleParameter("radius_central_pipe")));
        Variables.setMin_scatterer_placement_radius(Variables.getRadiusScatterer() + Variables.getRadiusCentralObject());
        Variables.setMax_scatterer_placement_radius(Double.parseDouble(comsolModel.getDoubleParameter("scatterers_max_distance")) - Variables.getRadiusScatterer());
        logger.info("Comsol Model initiated");
        // load in the exisiting storage for coordinates.
//        UtilsAndSettings utils = new UtilsAndSettings();
        GeneBank genesLoaded = null;
        GeneBank genesHistory = null;
        if (Variables.isResumePreviousRun()) {
            GeneBank[] loaded = null;
            if (Variables.isUseOldSystem())
                loaded = loadGenesFromClass(genesLoaded, genesHistory);
            else
                loaded = loadGenesFromClass(genesLoaded, genesHistory);
            genesLoaded = loaded[0];
            genesHistory = loaded[1];
        } else {
            genesLoaded = new GeneBank();
//            genesLoaded = utils.generatePopulationSets(Variables.getRadiusScatterer());
            Individual[] individuals = utils.generateXRandomIndividuals(Variables.getInitalPopSize());
            for (Individual individual : individuals) {
                genesLoaded.addNewIndividual(individual);
            }
            genesHistory = genesLoaded;
        }
        Variables.setActiveGeneHistoryCSVLocation(geneHistoryCSV.getAbsolutePath());

        NSAGAOptimiser optimizer = null;
        String startDate = dateFormat.format(date);

        try {
            switch (Variables.getOptimizerType()) {
                case "SAGA":
                    optimizer = new SAGA(comsolModel, genesLoaded, genesHistory, Variables.isUseComsol(), loggerLevel, null, Variables.getShapeOfScatterer(), Variables.getShapeOfObject(), Variables.getRadiusScatterer(), startDate);
                    break;
                case "NSAGA":
                    //                optimizer = new NSAGA(comsolModel, genesLoaded, genesHistory, Variables.isUseComsol(), loggerLevel, SCATTERER_POSITION_INDEX_MAP, shapeOfScatterer, shapeOfObject, radiusScatterer, startDate);
                    optimizer = null;
                    break;
                default:
                    throw new Error("Can't find the optimizer specified, quitting.");
            }
        } catch (Exception e) {
            logger.error("Error when creating Optimiser:", e);
            System.exit(1);
        }
        logger.debug("Completed initalising optimiser");
//		Optimizer optimizer = new MicrobialGeneticAlgorithm(comsolModel,genesLoaded, Variables.isUseComsol(), loggerLevel);
        GeneBank genesAndfScoresBank = null;
        logControlVariables(startDate, comsolModel);
        stillInitialising = false;
        // try to run the optimizer
        String targetFilePath = Variables.getMassImageOutputFolder(startDate, "" );
        try {
            genesAndfScoresBank = optimizer.runOptimizer(Variables.isResumePreviousRun(), Variables.getPreviousGenerationCounter());
        } catch (NullPointerException nullE) {
            if (optimizer == null) {
                logger.error("Optimiser {} is null, proceeding to quit", Variables.getOptimizerType());
                System.exit(1);
            } else {
                logger.error("Break while running optimiser, null value detected: ", nullE);
                System.exit(1);
            }
        } catch (Exception e) {
            // in the event of early finish, copy the current history file to the same place as details
            try {
                utils.runRecoveryCleanUp();
                System.gc();
                utils.appendGeneHistoryToFile(Variables.getActiveGeneHistoryCSVLocation(), genesAndfScoresBank);
                copyCSVAcross(geneHistoryCSV.getAbsolutePath(),Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date)+".csv"));
                copyCSVAcross(completeHistoryCSV.getAbsolutePath(),Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistoryALL__started_"+dateFormat.format(date)+".csv"));

            } catch (IOException ioe) {
                e.printStackTrace();
                logger.error("Error copying the active gene history to detials", ioe);
            } catch (Exception e2) {
                logger.error("Other Error detected when trying to save files", e2);
            }
            logger.error("Broke while running optimiser", e);
            System.exit(1);
        }



        // use the results to make a table, display and save it
        long simulationFinishTime = System.currentTimeMillis();
        logger.info("Time taken to complete simulation: {}s = {}m = {}h", (System.currentTimeMillis()-startTime)/1000, (System.currentTimeMillis()-startTime)/60000, (System.currentTimeMillis()-startTime)/3600000d);
        genesAndfScoresBank.sortByfScore();
        Individual bestIndividual = genesAndfScoresBank.getInvididual(0);
        logger.info("The best individual scored {}% cloak, using an fscore of {} where they used a build of {}", 1- bestIndividual.getfScore(), bestIndividual.getfScore(), bestIndividual.getGenes());

        logger.info("Time taken between end and user response: {}s = {}m = {}h", (System.currentTimeMillis()-simulationFinishTime)/1000, (System.currentTimeMillis()-simulationFinishTime)/60000, (System.currentTimeMillis()-simulationFinishTime)/3600000d);
        copyCSVAcross(geneHistoryCSV.getAbsolutePath(),Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date)+".csv"));
        copyCSVAcross(completeHistoryCSV.getAbsolutePath(),Variables.getMassImageOutputFolder(startDate, "" )+("\\geneHistoryALL__started_"+dateFormat.format(date)+".csv"));


        logger.info("Program completed, using System.exit(0)");
        // clean up the batch directory for comsol running.
        System.exit(0);

    }


    private static GeneBank loadGenes3_0(GeneBank genesLoaded, Object[][] csvRawGenes) {
        int index = 0;
        for (Object[] loadedIndividualData: csvRawGenes) {
            if (loadedIndividualData[0].equals("")) {
                logger.trace("Skip over, as found an empty first item of the array");
                continue;
            }
            logger.trace("Pre chromosome load Index = " + index);
            // copy the array from 1 end to the other, ignoring the final element.
            // todo will likely have to parse this information.
            int maxScatInQuadrant = Variables.getMaximumScatterersInAQuadrant();
            Double[] chromosome = new Double[maxScatInQuadrant*2];
            String[] stringChromosome = (String[]) Arrays.copyOfRange(loadedIndividualData, 0, maxScatInQuadrant);
            for (int i= 0; i< stringChromosome.length; i++) {
                // remove white space
                String s = stringChromosome[i];
                s =s.replaceAll("\\s+", "");
                chromosome[i] = Double.parseDouble(s);
            }
            logger.trace("Post chromosome load Index = " + index);

            // todo write up the code to save and load the information
            // chromosome, fscore, model number, 360 points, mse,  sanchis, sanchisH, sanchisSimple, Sanchez, SanchezSimple
            genesLoaded.addNewData(chromosome, Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]), index,
                    null,
                    loadedIndividualData[maxScatInQuadrant * 2+1].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2+3].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +4].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +5].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +6].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +7].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +8].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +9].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +10].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +11].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +12].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +13].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +14].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +15].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +16].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant])
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +2]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +3]),
//                        Double.parseDouble((String) loadedIndividualData[maxScatInQuadrant +1]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +4]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +5])
            );
            logger.trace("Post geneLoaded Index = " + index);
            index++;
        }
        return genesLoaded;
    }

    private static GeneBank loadGenes2_0(GeneBank genesLoaded, Object[][] csvRawGenes) {
        Variables.setCsvMapPath("D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\output\\MassImages\\2020-11-24--16-06\\index_map2020-11-24--16-06.csv");
        com.ga.nsaga.Position[] scatMap = setUpScattererIndexMap();
        int index = 0;
        for (Object[] loadedIndividualData: csvRawGenes) {
            if (loadedIndividualData[0].equals("")) {
                logger.trace("Skip over, as found an empty first item of the array");
                continue;
            }
            logger.trace("Pre chromosome load Index = " + index);
            // copy the array from 1 end to the other, ignoring the final element.
            // todo will likely have to parse this information.
            int maxScatInQuadrant = Variables.getMaximumScatterersInAQuadrant();
            Double[] chromosome = new Double[maxScatInQuadrant*2];
            Integer[] tempChromosome = new Integer[maxScatInQuadrant];
            String[] stringChromosome = (String[]) Arrays.copyOfRange(loadedIndividualData, 0, maxScatInQuadrant);
            int tempGene = 0;
            int coordsIndex = 0;
            for (int i= 0; i< stringChromosome.length; i++) {
                // remove white space
                String s = stringChromosome[i];
                s =s.replaceAll("\\s+", "");
                tempGene = Integer.parseInt(s);
                chromosome[coordsIndex] = scatMap[tempGene].getX();
                coordsIndex ++;
                chromosome[coordsIndex] = scatMap[tempGene].getY();
                coordsIndex ++;
            }
            logger.trace("Post chromosome load Index = " + index);

//            Variables.setScattererPositionIndexMap(scatMap);


            // todo write up the code to save and load the information
            // chromosome, fscore, model number, 360 points, mse,  sanchis, sanchisH, sanchisSimple, Sanchez, SanchezSimple
            genesLoaded.addNewData(chromosome, Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]), index,
                    null,
                    loadedIndividualData[maxScatInQuadrant * 2+1].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2+3].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +4].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +5].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +6].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +7].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +8].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +9].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +10].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +11].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +12].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +13].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +14].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +15].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
                    loadedIndividualData[maxScatInQuadrant * 2 +16].equals("null")? null: Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant])
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +2]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +3]),
//                        Double.parseDouble((String) loadedIndividualData[maxScatInQuadrant +1]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +4]),
//                        Double.parseDouble((String)loadedIndividualData[maxScatInQuadrant +5])
            );
            logger.trace("Post geneLoaded Index = " + index);
            index++;
        }
        return genesLoaded;
    }

    private static GeneBank[] loadGenesFromClass(GeneBank genesLoaded, GeneBank genesHistory) {
        Object[][] csvRawGenes = utils.loadCSV((Variables.getPreviousRunFilePath()), false);
        genesLoaded = new GeneBank();
       if (Double.parseDouble((String)csvRawGenes[0][34])%1 == 0) {
           genesLoaded = loadGenes2_0(genesLoaded, csvRawGenes);
       } else {
           genesLoaded = loadGenes3_0(genesLoaded, csvRawGenes);
       }
       int index = 0;
        // if the number of imported genes is larger than 70, then filter the best 70 from it
        if (genesLoaded.getSize() > Variables.getInitalPopSize()) {
            genesLoaded.sortByfScore();
            GeneBank tempGenes = new GeneBank();
            for (index = 0; index < Variables.getInitalPopSize(); index++) {
                tempGenes.addNewIndividual(genesLoaded.getInvididual(index));
            }
            genesHistory = genesLoaded;
            genesHistory.sortByIndex();
            genesLoaded = tempGenes;
        } else if (csvRawGenes.length < Variables.getInitalPopSize() ) {
            // else if there are less entries than there should be, then add on some new random ones
            Individual[] individualsDataSet = utils.generateXRandomIndividuals(Variables.maxPopMembers() - genesLoaded.getSize());
            for (Individual individual : individualsDataSet) {
                genesLoaded.addNewIndividual(individual);
            }
            genesHistory = genesLoaded;
        }
        return new GeneBank[]{genesLoaded, genesHistory};
    }

    private static void logControlVariables(String startDate, ComsolModelInterface comsolModel) {
        logger.info("Confirming before Run:");
        logger.info("    runCollisionChecks = "+ Variables.isRunCollsionChecks());
        logger.info("    optimiserType = "+ Variables.getOptimizerType());
        logger.info("    resumePreviousRun = "+ Variables.isResumePreviousRun());
        if (Variables.isResumePreviousRun())
            logger.info("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
        logger.info("    useGeneRandomisation = " + Variables.isUseGeneRandomisation());
        logger.info("    useGeneRandomisation = " + Variables.isUseGeneRandomisation());
        logger.info("    scattererType = " +  Variables.getShapeOfScatterer());
        FileWriter detailsFile = null;

        try{
            String newLine = System.getProperty("line.separator");
            detailsFile =  new FileWriter(String.valueOf(get(Variables.getMassImageOutputFolder(startDate, "")+ "\\details.txt")));
            detailsFile.write("Confirming before Run:"+newLine+newLine);
            detailsFile.write("    Version num = "+ Variables.getVersionUsed()+newLine);
            detailsFile.write("    runCollisionChecks = "+ Variables.isRunCollsionChecks()+newLine);
            detailsFile.write("    optimiserType = "+ Variables.getOptimizerType()+newLine);
            detailsFile.write("    radiusScatterer = "+ Variables.getRadiusScatterer()+newLine);
            detailsFile.write("    resumePreviousRun = "+ Variables.isResumePreviousRun()+newLine);
            if (Variables.isResumePreviousRun())
                detailsFile.write("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE+newLine);
            detailsFile.write("    useGeneRandomisation = " + Variables.isUseGeneRandomisation()+newLine);
            detailsFile.write("    scattererType = " +  Variables.getShapeOfScatterer()+newLine);
            detailsFile.write("    central Type = " + Variables.getShapeOfObject()+newLine);
            detailsFile.write("    Allow 0 switching = " + Variables.isAllow0Switching() +newLine);
            detailsFile.write("    Optimiser = " + Variables.isAllow0Switching() +newLine);
            detailsFile.write("    Inital annealing step = " + (Variables.getInitialAnnealingStep() )+newLine);
            detailsFile.write("    Max scatts in a quadrant = " + Integer.toString(Variables.getMaximumScatterersInAQuadrant()) +newLine);
            detailsFile.write("    Used scatts in a quadrant = " + Integer.toString(Variables.getUsedScatterersInAQuadrant()) +newLine);
            detailsFile.write("    Max pop size = " + Integer.toString(Variables.maxPopMembers()) +newLine);
            detailsFile.write("    Max scat positions = " + "infinite" +newLine);
            detailsFile.write("    Total annealing steps allowed = " + Integer.toString(Variables.getMaximumTemperatureSteps()) +newLine);
            detailsFile.write("    Max num population generations= " + Integer.toString(Variables.maxPopMembers()) +newLine);
//            detailsFile.write("    Eliete individuals= " + Integer.toString(utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION) +newLine);
//            detailsFile.write("    Equilibirum= " + Integer.toString(utils.EQUILIBRIUM_POSITION_INDEX) +newLine);
            detailsFile.write("    Mutation Prob= " + Double.toString(Variables.getInitialMutationProbability()) +newLine);
            detailsFile.write("    Inital temp= " + Double.toString(Variables.getInitialTemperature()) +newLine);
            detailsFile.write("    Max temp steps= " + Integer.toString(Variables.getMaximumTemperatureSteps()) +newLine);
//            detailsFile.write("    Gen save counter= " + Integer.toString(V) +newLine);
            detailsFile.write("Radius central pipe= " + comsolModel.getDoubleParameter("radius_central_pipe")+ newLine+newLine);
            detailsFile.write("Radius scatterer= " + comsolModel.getDoubleParameter("radius_scatterer")+ newLine+newLine);
            detailsFile.write("Scat max distance= " + comsolModel.getDoubleParameter("scatterers_max_distance")+ newLine+newLine);
            detailsFile.write("Radius central pipe multiplier= " + Variables.getCentralObjectRadisMultiplier()+ newLine);
            detailsFile.write("Radius scatterer multiplier= " + Variables.getScattererRadiusMultiplier()+ newLine);
            detailsFile.write("Scat max distance multiplier= " + Variables.getCloackThicknesssMultiplier()+ newLine);
            detailsFile.close();
            int breakx = 0;
        } catch (IOException e) {
            logger.error("IO exception when writing details file", e);
        }
        finally {
            if (detailsFile!= null) {
                try {
                    detailsFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void copyCSVAcross(String startPath, String targetPath) {
        try {
            utils.runRecoveryCleanUp();
            System.gc();
            // clean memory first then try to save it
//                optimizer.saveCombinationHistory(genesAndfScoresBank); // save the current implementation
            Files.move(Paths.get(startPath), Paths.get(targetPath));
            logger.info("Transferred the existing csv data to the output folder");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            logger.error("Error copying the active gene history to detials", ioe);
        } catch (Exception e2) {
            logger.error("Other Error detected when trying to save files", e2);
        }
    }

    public static com.ga.nsaga.Position[] setUpScattererIndexMap() {
//        Object[][] initalLoadedMap = utils.loadParamsFromCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_mesh_ultrathin.csv", true); // ultrathin
        Object[][] initalLoadedMap = utils.loadCSV(Variables.getCsvMapPath(), false); // original one
        logger.debug("Map loaded from CSV");
        com.ga.nsaga.Position[] positions = new com.ga.nsaga.Position[initalLoadedMap.length];
        int i = 0;
        DecimalFormat df = new DecimalFormat("#.#########");
        df.setRoundingMode(RoundingMode.CEILING);
        logger.debug("Setting up position index");
        for (i = 0; i< initalLoadedMap.length; i++) {
            logger.trace("Beginning to set up position {}", i);
            try {
                positions[i] = new Position(Double.parseDouble((String) initalLoadedMap[i][3]), Double.parseDouble((String) initalLoadedMap[i][1]));
            } catch (Exception e) {
                logger.error("Error when trying to setup positon {}", i);
            }
        }
//        NsagaUtils.max_no_scat_pos = positions.length;
        return positions;
    }


}
