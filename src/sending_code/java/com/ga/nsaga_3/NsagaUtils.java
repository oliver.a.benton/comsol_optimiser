package com.ga.nsaga_3;

import ch.qos.logback.classic.Logger;
import com.GlobalUtils;
import com.comsol.model.util.ModelUtil;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class NsagaUtils {
    final static Logger logger = (Logger) LoggerFactory.getLogger(com.ga.nsaga.NsagaUtils.class.getName());



    public void runRecoveryCleanUp() {
        long startTime = System.currentTimeMillis();
        String[] fileDirectories = new String[4];
        try {
            fileDirectories[0] = ModelUtil.getPreference("tempfiles.recovery.recoverydir");
        } catch (ExceptionInInitializerError e) {
            fileDirectories[0] = " ";
        }
        fileDirectories[1] = GlobalUtils.getComsolRecoveryDirectory();
        fileDirectories[2] = "D:\\comsol\\tempDir\\recoveries";
        fileDirectories[3] = "D:\\comsol\\v54\\recoveries";
        // MPHRecovery8363date_Dec_19_2019_11-22_AM.mph
        LocalDate date = java.time.LocalDate.now();

        SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy_HH-mm");
        String currentStringDate = format.format(new Date());
        Date currentDate = new Date();
        currentDate.setHours(0);
        int breakx = 0;

        File dir;
        ArrayList<File> toDeleteFiles = new ArrayList<>();
        for (String fileName : fileDirectories) {
            if (!fileName.equals("") && !fileName.equals(" ")) {
                dir = new File(fileName);
                for (File fileEntry : dir.listFiles()) {
                    String name = fileEntry.getName();
                    String dateName = "";
                    int indexForDate = name.indexOf("date") + 5;
//                    if (name.length() > 20) {
                    dateName = name.substring(indexForDate, name.length() - 7);
                    Date parsedDate = new Date();
                    try {
                        parsedDate = format.parse(dateName);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (parsedDate.before(currentDate)) {
                        toDeleteFiles.add(fileEntry);
                    }
                    int breakx1 = 0;
                }
            }
        }
        while (!toDeleteFiles.isEmpty()) {
            recursiveDelete(toDeleteFiles.get(0));
            toDeleteFiles.remove(0);
        }
        Variables.increseTimeSpentCleaningRevoveries(System.currentTimeMillis()-startTime);
        logger.debug("Time spent cleaning recoveries = {}s", Variables.getTimeSpentClearingRecoveries()/60);

        // Find the temporary files that are used by comsol for meshing
        ArrayList<File> tempFolders = GlobalUtils.getComsolTempFolders();
        ArrayList<Date> lastModified = new ArrayList<>(tempFolders.size());
        for (File comsolTempDir : tempFolders) {
            String name = comsolTempDir.getName();
            File[] comsolTempFolderSubFiles = comsolTempDir.listFiles();
            if (comsolTempFolderSubFiles.length == 0)
                lastModified.add( new Date(comsolTempDir.lastModified()));
            else {
                long mostRecentLong = comsolTempFolderSubFiles[0].lastModified();
                int mostRecentModifiedLocation = 0;
                for (int i = 1; i< comsolTempFolderSubFiles.length; i++) {
                    // if the current examined date is after the mostRecentModified, it is more recent, so overwrite
                    if (comsolTempFolderSubFiles[i].lastModified() > mostRecentLong) {
                        mostRecentLong = comsolTempFolderSubFiles[i].lastModified();
                        mostRecentModifiedLocation = i;
                    }
                }
                lastModified.add( new Date(comsolTempFolderSubFiles[mostRecentModifiedLocation].lastModified()));
            }
        }
        if (lastModified.size() == 0)
            return;
        Date mostRecentModified=lastModified.get(0);
        int mostRecentModifiedLocation = 0;
        for (int i = 1; i< tempFolders.size(); i++) {
            // if the current examined date is after the mostRecentModified, it is more recent, so overwrite
            if (lastModified.get(i).after(mostRecentModified)) {
                mostRecentModified = lastModified.get(i);
                mostRecentModifiedLocation = i;
            }
        }
        tempFolders.remove(mostRecentModifiedLocation);

        while (!tempFolders.isEmpty()) {
            recursiveDelete(tempFolders.get(0));
            tempFolders.remove(0);
        }
        breakx = 0;
    }

    public void setComsolPreferences() {
        File tempDir = new File ("D:\\comsol\\tempDir");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        File temp = new File(tempDir.getAbsolutePath()+"\\recoveries");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(tempDir.getAbsolutePath()+"\\temp");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(tempDir.getAbsolutePath()+"\\temp_server");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(tempDir.getAbsolutePath()+"\\userFiles");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(tempDir.getAbsolutePath()+"\\commonFiles");
        if (!temp.exists()) {
            temp.mkdirs();
        }
        ModelUtil.setPreference("tempfiles.recovery.autosave", "off"); // turn off recovery
        ModelUtil.setPreference("tempfiles.recovery.recoverydir", tempDir.getAbsolutePath()+"\\recoveries"); // turn off recovery
        ModelUtil.setPreference("tempfiles.tempfiles.tmpdir", tempDir.getAbsolutePath()+"\\temp"); // change temp dirs to  D drive
        ModelUtil.setPreference("tempfiles.tempfiles.tmpdir#server", tempDir.getAbsolutePath()+"\\temp_server"); // change temp dirs to  D drive
        ModelUtil.setPreference("tempfiles.schemes.filerootuser", tempDir.getAbsolutePath()+"\\userFiles"); // change temp dirs to  D drive
        ModelUtil.setPreference("tempfiles.schemes.filerootcommon", tempDir.getAbsolutePath()+"\\commonFiles"); // change temp dirs to  D drive
//        ModelUtil.setPreference("memoryconservation.parametric.save", "off"); // change temp dirs to  D drive
//        ModelUtil.savePreferences();
    }

    private void recursiveDelete(File f) {
        String[] entries = f.list();
        if (entries != null) {
            for (String s : entries) {
                File currentFile = new File(f.getPath(), s);
                if (currentFile.isDirectory()) {
                    recursiveDelete(currentFile);
                }
                currentFile.delete();
            }
        }
        f.delete();
    }

    /**
     * Method to count the number of rows in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of rows
     */
    private int countCsvRows(String csvPath) {
        int rowCounter = 0;
        BufferedReader reader = null;
        try {
            // D:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\scatterer_mesh_ultrathin.csv
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            while (line != null) {
                rowCounter++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("IO exception while counting the number of CSV rows");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return rowCounter;
    }

    /**
     * Method to count the number of columns in a csv
     *
     * @param csvPath the path to the csv in question
     * @return the number of columns
     */
    private int countCsvColumns(String csvPath) {
        int inputCounter = 0;
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            String line = reader.readLine();
            String[] returnArray = line.split(",");
            inputCounter = returnArray.length;
        } catch (IOException ioe) {
            logger.error("Input Output exception while counting number of rows: " + ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while counting CSV columns");
                    logger.error("CONTEXT", e);
                }
            }
        }
        return inputCounter;
    }

    /**
     * Used to load the parameters that the comsolModel is using from a csv file read in
     * as the absolute csvPath - returns a 2d string array, where [0][all] is the
     */
    public String[][] loadCSV(String csvPath, boolean hasTitle) {
        String[][] returnArray = new String[countCsvRows(csvPath)][countCsvColumns(csvPath)];
        BufferedReader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(csvPath));
            // Reading Records One by One in a String array
            String line = reader.readLine();
            // loop until all lines are read
            int i = 0;
            while (line != null) {
                returnArray[i] = line.split(",");
                i++;
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            logger.error("Input Output exception while updating params {} - {}", ioe.getClass().getName(), ioe.getMessage());
            logger.error("CONTEXT", ioe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error("Error while loading parameters from a csv");
                    logger.error("CONTEXT", e);
                }
            }
        }
        if (hasTitle)
            return Arrays.copyOfRange(returnArray, 1, returnArray.length);
        return Arrays.copyOfRange(returnArray, 0, returnArray.length);
    }

    /**
     * Method to copy an array. Acts as a shallow copy - new array object still stores the same items.
     * @param original
     * @return
     */
    public Integer[] copyIntegerArray(Integer[] original) {
        Integer[] newArray = new Integer[original.length];
        int index;
        for (index = 0; index< original.length; index++) {
            newArray[index] = original[index].intValue();
        }
        return newArray;
    }

    /**
     * Method to copy an array. Acts as a shallow copy - new array object still stores the same items.
     * @param original
     * @return
     */
    public Double[] copyDoubleArray(Double[] original) {
        Double[] newArray = new Double[original.length];
        int index;
        for (index = 0; index< original.length; index++) {
            newArray[index] = original[index];
        }
        return newArray;
    }

    /**
     * Method to append the right number of 0s to the end of a chromosome array - e.g. if the input has 30 numbers and the
     * maximum number of scatterers is 35, then it will add 5 0's to the end of the array.
     * @param inputArray
     * @return
     */
    public static Double[] setChromosomeEndToZero(Double[] inputArray) {
        int i;
        if (inputArray.length != Variables.getMaximumScatterersInAQuadrant() * 2 ) {
            Double[] tempArray = inputArray;
            inputArray = new Double[Variables.getMaximumScatterersInAQuadrant()*2];
            for (i=0; i < tempArray.length; i++ ) {
                inputArray[i] = tempArray[i];
            }
            tempArray = null; // clear the data from memory
        }
        for (i=0; i < inputArray.length; i++ ) {
            if (inputArray[i] == null)
                inputArray[i] = 0.0;
        }
        return inputArray;
    }

    public Individual[] generateXRandomIndividuals(int numIndividuals) {
        Individual[] individualsData = new Individual[numIndividuals];
        Random rand = new Random();
        int createdIndividuals = 0;
        Double[] currentIndividual = new Double[Variables.getMaximumScatterersInAQuadrant()];
        // while there is not enough population
        while (createdIndividuals < numIndividuals) {
            currentIndividual = randomIndividualCreator(new Double[Variables.getMaximumScatterersInAQuadrant()*2], Variables.getRadiusScatterer(), createdIndividuals);
            // set any remaining set to be 0
            currentIndividual = setChromosomeEndToZero(currentIndividual);
//            clearAllUsedIndexes(RunNSAGA.SCATTERER_POSITION_INDEX_MAP);
            // save the new member into the population
            individualsData[createdIndividuals] = new Individual(createdIndividuals,
                    currentIndividual, -9999.0,
                    createdIndividuals,
                    null, 9999.0, Double.MAX_VALUE, Double.MAX_VALUE,
                    Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, false, false,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE);
            if (createdIndividuals % 10 == 0) {
                logger.debug("Added member {} to the population", createdIndividuals);
            } else {
                logger.trace("Added member {} to the population", createdIndividuals);
            }

            createdIndividuals++;

        }
        logger.debug("Completed creating random members");
        return individualsData;

    }

    private Double[] randomIndividualCreator(Double[] genes, double radiusScatterer, int createdIndividuals) {
        // TODO double check that this runs smoothly with returning 70 members
        int geneCounter = 0;
        int randomizedGene = 0;
        Random rand = new Random();
//        Integer[] labelledIndvidual = new Integer[] {865,931,257,470,911,810,912,65,260,892,385,1005,983,914,747,900,799,1006,20,961,112,831,542,20,890,853,555,746,956,76,0,0,0,0,0};

        while (geneCounter < Variables.getUsedScatterersInAQuadrant()*2) {
            logger.trace("Beginning start of loop, gene counter = ", geneCounter);
            Double min = Variables.getMin_scatterer_placement_radius();  // Set To Your Desired Min Value
            Double max = Variables.getMax_scatterer_placement_radius(); // Set To Your Desired Max Value
            double x = 0.0;
            double y = 0.0;
            if (rand.nextDouble() > 0.15) {
                // if random is > 0.15, then set it to be an actual number. 0.15 selected by using 0.15*35 ~ 5, which places an average of 5 out of 35 scatterers will be 0
//                x = (Math.random() * ((max - min))) + min;
//                y = (Math.random() * ((max - min))) + min;
                x = (Math.random()/ 1.0) * (max-min) + min;
                y = (Math.random()/ 1.0) * (max-min) + min;
            }
            genes[geneCounter] = x;
            genes[geneCounter+1] = y;
            geneCounter +=2;
        }
        return genes;
    }

    public Double[][] convert1DCoordsTo2D(Double[] newIndividual) {
        Double[][] coordianteIndividual = new Double[Variables.getMaximumScatterersInAQuadrant()][2];
        boolean isXCoord = true;
        int i = 0;
        // TODO TEST
        for (Double coord : newIndividual) {
            if (isXCoord) {
                coordianteIndividual[i][0] = coord;
            } else {
                coordianteIndividual[i][1] = coord;
                i++;
            }
            isXCoord = !isXCoord;
        }
        return coordianteIndividual;
    }

    public boolean runCollisionCheckRandomIndividual(Double[] newIndividual, double radius_scatterer) {
        int currentOuterGene = 0;
        int currentInnerGene =currentOuterGene;
        boolean collisionOccured = false;

        Double[][] coordianteIndividual = convert1DCoordsTo2D(newIndividual);

        double outerLoopGeneX = coordianteIndividual[currentOuterGene][0];
        double outerLoopGeneY = coordianteIndividual[currentOuterGene][1];

        double innerLoopGeneX = coordianteIndividual[currentInnerGene][0];
        double innerLoopGeneY = coordianteIndividual[currentInnerGene][1];
        // run the nextEmptyIndex from 0 to n-1 (len-2), as don't need to compare the last entry against anything
        while (!collisionOccured && currentOuterGene < coordianteIndividual.length-2) {
            // run the comparison from currentExamined + 1 to n (len-1)
            outerLoopGeneX = coordianteIndividual[currentOuterGene][0];
            outerLoopGeneY = coordianteIndividual[currentOuterGene][1];
            currentInnerGene = currentOuterGene + 1;
            while (!collisionOccured && currentInnerGene < coordianteIndividual.length-1) {
                innerLoopGeneX = coordianteIndividual[currentInnerGene][0];
                innerLoopGeneY = coordianteIndividual[currentInnerGene][1];
                // if either gene is set to coordinate (0,0) ignore the comparison, it doesnt count for overlap
                if (outerLoopGeneX + outerLoopGeneY == 0 || innerLoopGeneX + innerLoopGeneY == 0) {
                    currentInnerGene++; // increase the current location - if the value of either indexGiven is 0, skip
                    continue;
                }
                collisionOccured = checkCoordinatesForCollision(outerLoopGeneX, outerLoopGeneY, innerLoopGeneX, innerLoopGeneY, radius_scatterer);
                currentInnerGene++;
                logger.trace("Finished gene {} comparing to {} out of {}", currentOuterGene, currentInnerGene, newIndividual.length-1);
            }
            currentOuterGene ++;
            if (currentOuterGene % 15 ==  0) {
                logger.debug("Finished gene {} of {}", currentOuterGene,newIndividual.length-1);
            } else
                logger.trace("Finished gene {} of {}", currentOuterGene,newIndividual.length-1);

        }
        logger.trace("Left collsion check loop");
        return collisionOccured;
    }

    /**
     * Method to check if there is a collision within the coordinates given
     * @param centreX1
     * @param centreY1
     * @param centreX2
     * @param centreY2
     * @param radius_scatterer
     * @return true if there is a collision
     */
    private static boolean checkCoordinatesForCollision(double centreX1, double centreY1, double centreX2, double centreY2, double radius_scatterer) {
        boolean collsionOccured = false;
        switch (Variables.getShapeOfScatterer()) {
            case "Square":
                radius_scatterer = radius_scatterer * Math.sqrt(2);
                // if top of sq1 is lower than the bottom of sq 2, or if the bottom of sq2 is higher than the
                // there is a distance between the top and bottom of the rectangles.
                boolean topBottomOverlapOccured = false;
                boolean leftRightOverlap = false;
                if ((centreY1 + radius_scatterer) < (centreY2 - radius_scatterer)
                        || (centreY1 - radius_scatterer) > centreY2 + (radius_scatterer)) {
                    // pass
                } else
                    topBottomOverlapOccured = true;
                // if right side of sq1 is larger than left side of sq2, or if left side of sq1 is smaller than right sq2, overlap
                if ((centreX1 + radius_scatterer) < (centreX2 - radius_scatterer)
                        || (centreX1 - radius_scatterer) > (centreX2 + radius_scatterer)) {
                    // pass
                } else
                    leftRightOverlap = true;
                if (leftRightOverlap && topBottomOverlapOccured) {
                    collsionOccured = true;
                }
                break;
            case "Circle":
                // pythagoras - a^2 + b ^2 == c^2 in right angle - circle, if c^2 < (2*raidus) ^2  -- if (x1-x2)^2 + (y1-y2)^2 < radius^2, collision.
                if ((Math.pow((centreX1 - centreX2), 2) + Math.pow((centreY1 - centreY2), 2)) < Math.pow(2*radius_scatterer, 2)) {
                    collsionOccured = true;
                }
                break;
            default:
                System.out.println("ERROR SYSTEM BROKEN - there is a scatterer unrecognised by the system");
                System.exit(-1);
        }
        return collsionOccured;
    }

    public Double[] getEmpyScattererArray() {
        Double[] array = new Double[Variables.getMaximumScatterersInAQuadrant()*2];
        for (int i = 0; i < Variables.getMaximumScatterersInAQuadrant()*2; i++) {
            array[i] = 0.0;
        }
        return array;
    }

    public int getEmptyAreaRunIndex() {
        return -999;
    }

    public int getNoScattererRunIndex() {
        return -998;
    }


    public boolean appendGeneHistoryToFile(String path, GeneBank genesAndfScoresBank) throws Exception {
        int dataLength = genesAndfScoresBank.getInvididual(0).getGenes().length+2;
        int metricCount = genesAndfScoresBank.getInvididual(0).getLength();
        int i = 0;
        int j = 0;
        try {
            // make a new array of columns that is the right length - number of scatterers + metric
            String[] newDataLine = new String[dataLength+metricCount+1+360];
            if (!Variables.isSave360Points()) {
                newDataLine = new String[dataLength+metricCount+1];
            }

            ArrayList<String[]> newData = new ArrayList<>(newDataLine.length);
//            String[][] newData = new String[genesAndfScoresBank.getSize()][dataLength+6];
            for (i=0; i< genesAndfScoresBank.getSize(); i++) {
                newDataLine = new String[dataLength+metricCount+1+360];
                // if the individual is already recorded, no need to copy them into the table again.
                if (!genesAndfScoresBank.getInvididual(i).isRecoreded()) {
                    for (j = 0; j < genesAndfScoresBank.getInvididual(i).getGenes().length; j++) {
                        newDataLine[j] = genesAndfScoresBank.getInvididual(i).getGenes()[j].toString();
                    }
                    newDataLine[j] = Double.toString(genesAndfScoresBank.getInvididual(i).getfScore());  // fscore
                    newDataLine[j + 1] = Double.toString(genesAndfScoresBank.getInvididual(i).getMse()); // mse
                    newDataLine[j + 2] = Double.toString(1 - genesAndfScoresBank.getInvididual(i).getSanchisMetric()); // sanchis
                    newDataLine[j + 3] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetric());  // sanchis H comsol
                    newDataLine[j + 4] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetricH()); // sanchis H mine
                    newDataLine[j + 5] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisMetricSimple()); // sanchis numerator
                    newDataLine[j + 6] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezMetric());  // sanchez 2018 full
                    newDataLine[j + 7] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezMetricSimple()); // sanchez numerator
                    newDataLine[j + 8] = Double.toString(genesAndfScoresBank.getInvididual(i).getNormalisedMSE()); // normalised mse
                    newDataLine[j + 9] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchisPerfectCloak()); // sanchis vs perfect cloak instead of central
                    newDataLine[j + 10] = Double.toString(genesAndfScoresBank.getInvididual(i).getSanchezPerfectCloak()); // sanchez vs perfect
                    newDataLine[j + 11] = Double.toString(genesAndfScoresBank.getInvididual(i).getRmse()); // rmse
                    newDataLine[j + 12] = Double.toString(genesAndfScoresBank.getInvididual(i).getMinPressure()); // min pressure
                    newDataLine[j + 13] = Double.toString(genesAndfScoresBank.getInvididual(i).getMaxPressure()); // max pressure
                    newDataLine[j + 14] = Double.toString(genesAndfScoresBank.getInvididual(i).getAveragePressure()); // average pressure
                    newDataLine[j + 15] = Double.toString(genesAndfScoresBank.getInvididual(i).getAbsoluteAveragePressure()); // absolute average pressure
                    newDataLine[j + 16] = Double.toString(genesAndfScoresBank.getInvididual(i).getL2Norm()); // absolute average pressure
                    int startJ = j+16;
                    if (Variables.isSave360Points()) {
                        for (j = 0; j < 360; j++) {
                            if (genesAndfScoresBank.getInvididual(i).getArray360Points() != null)
                                newDataLine[j + startJ] = ((Double) genesAndfScoresBank.getInvididual(i).getArray360Points()[0][j]).toString();
                            else
                                logger.error("Error in finding array - for individual {} foound {}", genesAndfScoresBank.getInvididual(i).getIndexGiven(), ((Double) genesAndfScoresBank.getInvididual(i).getArray360Points()[0][j]).toString());
//                                if (!stillInitialising) {
//                                    // if not still initialsing, there is a problem - bad coding practice, but need to do this quickly
//                                    logger.error("There is a problem with the arrays - for some reason they are empty");
//                                } else {
//                                    logger.trace("Still initalising - saving data from previous iterations");
//                                }
//                            }
                        }
                    }
                    genesAndfScoresBank.getInvididual(i).setRecoreded(true);
                    newDataLine[newDataLine.length-1] = Integer.toString(genesAndfScoresBank.getInvididual(i).getModelNumber());
                    newData.add(newDataLine);
                } else {
                    logger.trace("Not adding element {}, model num {}, index {} to the array to save as it is null - recorded Already ==", i, genesAndfScoresBank.getInvididual(i).getModelNumber(), genesAndfScoresBank.getInvididual(i).getIndexGiven());
                }
//                if (newDataLine[0] != null)
//
//                else {
//                    logger.trace("Not adding element {}, model num {}, index {} to the array to save as it is null", i, genesAndfScoresBank.getInvididual(i).getModelNumber(), genesAndfScoresBank.getInvididual(i).getIndexGiven());
//                }

            }
            String[][] newDataArray = new String[newData.size()][dataLength+6];
            i = 0;
            for (String[] line: newData) {
                newDataArray[i] = line;
                i++;
            }
            // append the array to the data
            toExcel(newDataArray, path, true);
        } catch (Exception e) {
            logger.error("Error writing results to file - i = {}, j = {}", i, j,e);
            return false;
        }
        return true; // break here for stopping on file save
    }


    private void toExcel(String[][] table, String fileName, boolean appending) {
        FileWriter excel = null;
        File file = new File(fileName+".csv");
        // if the file doesn't exist, then set appending to false, as it is making a new file
        if (!file.exists()) {
            appending = false;
        }
        try {

            excel = new FileWriter(file.getAbsolutePath(), appending);
            int j;
            for (int i = 0; i < table.length; i++) {
                for (j = 0; j < table[0].length; j++) {
                    try {
                        excel.append(table[i][j]+ ", ");
                    } catch (Exception e) {
                        logger.trace("Caught an exception {} while writing to excel document ", e.getMessage());
                    }
                }
                excel.append("\n");
            }

            excel.close();

        } catch (IOException e) {
            logger.error("While converting to Excel: {}", e.getMessage());
        } finally {
            if (excel != null) {
                try {
                    excel.close();
                } catch (IOException e) {
                    logger.error("Error while closing the excel conversion");
                    logger.error("CONTEXT", e);
                }
            }
        }
    }
}
