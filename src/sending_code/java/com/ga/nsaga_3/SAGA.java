package com.ga.nsaga_3;

import com.comsolModelInterface.ComsolModelInterface;
import com.Variables;
import com.ga.nsaga.Position;

import java.util.Random;
import ch.qos.logback.classic.Level;

public class SAGA extends NSAGAOptimiser {
    Random rand = new Random();
    GeneBank populationHistory = new GeneBank();


    SAGA(ComsolModelInterface comsolModel, GeneBank genesLoaded, GeneBank geneHistory, boolean useComsol, Level loggerLevel, Position[] scattererToIndexMap, String shapeOfScatterer, String shapeOfObject, double scattererRadius, String startDate) throws  Exception {
        super(comsolModel, genesLoaded, geneHistory, useComsol, loggerLevel, shapeOfScatterer, shapeOfObject, startDate);
//        super(comsolModel, genesLoaded, geneHistory, useComsol, loggerLevel, scattererToIndexMap, shapeOfScatterer, shapeOfObject, scattererRadius, startDate);
    }

//    SAGA(ComsolModelInterface comsolModel, GenesAndfScoresBank genesLoaded, GenesAndfScoresBank geneHistory, boolean useComsol, Level loggerLevel, Position[] scattererToIndexMap, String shapeOfScatterer, String shapeOfObject, double scattererRadius, String startDate) {
//        super(comsolModel, genesLoaded, geneHistory, useComsol, loggerLevel, scattererToIndexMap, shapeOfScatterer, shapeOfObject, scattererRadius, startDate);
//        logger.setLevel(loggerLevel);
//        // create save file path
//        File directory = new File(saveFilePath);
//        if (!directory.exists()) {
//            directory.mkdirs();
//        }
//    }

    @Override
    public GeneBank runOptimizer(boolean resumePreviousRun, int previousGenerationCount) throws Exception {
       return fromPurcellDescription(resumePreviousRun, previousGenerationCount);
    }


    private GeneBank convertedRaghuCode(boolean resumePreviousRun, int previousGenerationCount) throws Exception {
        int annealingStep =0;
        double temperature = Variables.getInitialTemperature();
        double timeDiff = 0;
        long annealingSetStartTime = System.currentTimeMillis();
        long startTimeForNextCounter = 0;
        int loopsEntered = 0;
        int mutationsConsidered = 0;
        long thisAnnealingStepStartTime = 0;

        for (annealingStep = Variables.getInitialAnnealingStep(); annealingStep < Variables.getMaximumTemperatureSteps(); annealingStep++) {
            thisAnnealingStepStartTime = System.currentTimeMillis();

            while (previousPopulation.getSize() < Variables.maxPopMembers()) {
                loopsEntered++;
                //OFFSPRING
                // select new parents and produce offspring
                startTimeForNextCounter = System.currentTimeMillis();
                int parent1Index = rand.nextInt(previousPopulation.getSize()-1);
                int parent2Index = rand.nextInt(previousPopulation.getSize()-1);
                while (parent1Index == parent2Index) {
                    // while: the 2nd parent is the same as the first parent, select random parent 2
                    parent2Index = rand.nextInt(previousPopulation.getSize()-1);
                }
                Double[][] offspringProduced = produceMultipleOffspring(previousPopulation, parent1Index, parent2Index);
                combosGenerated += 2;
                combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;

                int offspring1Result = checkIndividual(offspringProduced[0], loopsEntered, temperature);
                int offspring2Result = checkIndividual(offspringProduced[1], loopsEntered, temperature);

                // MUTATION
                if (rand.nextDouble() < Variables.getCurrentMutationProbability()) {
                    // random which individual to mutate
                    startTimeForNextCounter = System.currentTimeMillis();
                    int mutationPick = rand.nextInt(5);
                    Double[] mutatedOffspring;
                    switch (mutationPick) {
                        case 0:
                            mutatedOffspring = utils.copyDoubleArray(offspringProduced[0]);
                            break;
                        case 1:
                            mutatedOffspring = utils.copyDoubleArray(previousPopulation.getInvididual(parent1Index).getGenes());
                            break;
                        case 2:
                            mutatedOffspring = utils.copyDoubleArray(previousPopulation.getInvididual(parent2Index).getGenes());
                            break;
                        case 3:
                            mutatedOffspring = utils.copyDoubleArray(offspringProduced[1]);
                            break;
                        case 4:
                            mutatedOffspring = utils.copyDoubleArray(previousPopulation.getInvididual(rand.nextInt(previousPopulation.getSize())).getGenes());
                            break;
                        default:
                            logger.error("SHOULD NOT GET HERE");
                            mutatedOffspring = null;
                            System.exit(1);
                    }
                    mutatedOffspring = performMutation(mutatedOffspring);
                    combosGenerated += 1;
                    combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;
                    int result = checkIndividual(mutatedOffspring, loopsEntered, temperature);
                    if (result >= 0)
                        mutationsConsidered++;

                }

            }
            double newTemperature = temperatureFunction(temperature, annealingStep);
            long endAnnealingStepTime = System.currentTimeMillis();
            timeDiff = endAnnealingStepTime - thisAnnealingStepStartTime;
            logger.trace("Taking temperature from {} to {}", temperature, newTemperature);
            logger.info("Completed annealing step {}. Time taken is {}s == {}m == {}h", annealingStep - 1, timeDiff / 1000, timeDiff / 60000, timeDiff / 3600000d);
            long averageAnnealingStepTime = totalTimeForAnnealingSteps / annealingStep;
            logger.info("Average time for annealing steps: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
            logger.info("Models accepted = {} models rejected = {}", decimalFormat.format(acceptedModelIndex), decimalFormat.format(totalModelsEvaluatedByComsol - acceptedModelIndex),
                    acceptedModelIndex, nextPopulation.getSize(), previousPopulation.getSize(),
                    completedPopulationsCounter, Variables.maxPopMembers(), annealingStep, Variables.getMaximumTemperatureSteps());

            totalTimeForAnnealingSteps += (timeDiff); // average time across all annealing steps
            temperature = newTemperature;
            comsolModel.generateFreshModel();
            System.gc(); // force gc after every annealing step - clear out memory
        }
        logger.info("All annealing steps completed");
        logger.info("Time taken by summing diffs = {}s == {}m == {}h", totalTimeForAnnealingSteps/ 1000, totalTimeForAnnealingSteps / 60000, totalTimeForAnnealingSteps / 3600000d);
        long averageAnnealingStepTime = totalTimeForAnnealingSteps / annealingStep;
        logger.info("Average time: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
        timeDiff = System.currentTimeMillis()- annealingSetStartTime;
        logger.info("Time taken by length   time = {}s == {}m == {}h", timeDiff / 1000, timeDiff / 60000, timeDiff / 3600000d);
        averageAnnealingStepTime = (long) (timeDiff / annealingStep);
        logger.info("Average time: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
        return previousPopulation;
    }


    private GeneBank fromPurcellDescription(boolean resumePreviousRun, int previousGenerationCount) throws Exception {
        int annealingStep =0;
        double temperature = Variables.getInitialTemperature();
        double timeDiff = 0;
        long annealingSetStartTime = System.currentTimeMillis();
        long startTimeForNextCounter = 0;
        int loopsEntered = 0;
        int mutationsConsidered = 0;
        long thisAnnealingStepStartTime = 0;
        int maxGeneticRun = 50;
        int geneticStep = 0;
        GeneBank currentlyUsedPopulation = new GeneBank();
        for (Individual individual: previousPopulation.geneScoreBank) {
            currentlyUsedPopulation.addNewIndividual(individual);
        }
        previousPopulation.sortByfScore();
        Individual currentBest = previousPopulation.getInvididual(0);

        for (annealingStep = Variables.getInitialAnnealingStep(); annealingStep < Variables.getMaximumTemperatureSteps(); annealingStep++) {
            thisAnnealingStepStartTime = System.currentTimeMillis();

            // Begin full GA run
            while (geneticStep < Variables.getNumGenerations()) {
                loopsEntered++;

//                currentlyUsedPopulation = cullPopulationProcess(currentlyUsedPopulation);
                //OFFSPRING
                Object[] returned = crossBreedProcess(currentlyUsedPopulation, startTimeForNextCounter);
                currentlyUsedPopulation = (GeneBank) returned[0];
                currentlyUsedPopulation = mutatePopulationProcess(currentlyUsedPopulation, (Individual)returned[1], (Individual)returned[2],startTimeForNextCounter, (Individual)returned[3], (Individual)returned[4], mutationsConsidered );
                geneticStep++;
                if (geneticStep%10 ==0) {
                    logger.info("Moved through {} genetic steps of annealing step {}", geneticStep, annealingStep);
                }
            }
            geneticStep = 0;
            // SA acceptance of new best todo updated with a save clause to save information in here
            if (currentBest.getfScore() < currentlyUsedPopulation.getBestIndividual().getfScore()) {
                currentBest = currentlyUsedPopulation.getBestIndividual();
                logger.trace("Accepted that the new genetics are better than the previous genetics");
                previousPopulation = currentlyUsedPopulation;
            } else {
                double randValue = rand.nextDouble();
                // if random value is less than the calculated probability, then accept it.
                boolean isAccepted = randValue < Math.exp((currentBest.getfScore() - currentlyUsedPopulation.getBestIndividual().getfScore()) / temperature);
                if (isAccepted) {
                    currentBest = currentlyUsedPopulation.getBestIndividual();
                    previousPopulation = currentlyUsedPopulation;
                    logger.trace("Accepted that the new genetics are better than the previous genetics");
                } else {
                    currentlyUsedPopulation = previousPopulation;
                    logger.trace("Rejected the new genetics");
                }
            }
            // save currentlyUsedPopulation and best to csv
            utils.appendGeneHistoryToFile(Variables.getActiveGeneHistoryCSVLocation(), previousPopulation);
            annealingStep+=1;
            double newTemperature = temperatureFunction(temperature, annealingStep);
            long endAnnealingStepTime = System.currentTimeMillis();
            timeDiff = endAnnealingStepTime - thisAnnealingStepStartTime;
            logger.trace("Taking temperature from {} to {}", temperature, newTemperature);
            logger.info("Completed annealing step {}. Time taken is {}s == {}m == {}h", annealingStep - 1, timeDiff / 1000, timeDiff / 60000, timeDiff / 3600000d);
            long averageAnnealingStepTime = totalTimeForAnnealingSteps / annealingStep;
            logger.info("Average time for annealing steps: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
            logger.info("Models accepted = {} models rejected = {}", decimalFormat.format(acceptedModelIndex), decimalFormat.format(totalModelsEvaluatedByComsol - acceptedModelIndex));
//                    acceptedModelIndex, nextPopulation.getSize(), previousPopulation.getSize(),
//                    completedPopulationsCounter, Variables.maxPopMembers(), annealingStep, Variables.getMaximumTemperatureSteps());

            totalTimeForAnnealingSteps += (timeDiff); // average time across all annealing steps
            temperature = newTemperature;


            comsolModel.generateFreshModel();
            System.gc(); // force gc after every annealing step - clear out memory



        }
        logger.info("All annealing steps completed");
        logger.info("Time taken by summing diffs = {}s == {}m == {}h", totalTimeForAnnealingSteps/ 1000, totalTimeForAnnealingSteps / 60000, totalTimeForAnnealingSteps / 3600000d);
        long averageAnnealingStepTime = totalTimeForAnnealingSteps / annealingStep;
        logger.info("Average time: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
        timeDiff = System.currentTimeMillis()- annealingSetStartTime;
        logger.info("Time taken by length   time = {}s == {}m == {}h", timeDiff / 1000, timeDiff / 60000, timeDiff / 3600000d);
        averageAnnealingStepTime = (long) (timeDiff / annealingStep);
        logger.info("Average time: {}s == {}m == {}h", averageAnnealingStepTime / 1000, averageAnnealingStepTime / 60000, averageAnnealingStepTime / 3600000d);
        return previousPopulation;
    }

    private GeneBank cullPopulationProcess(GeneBank population) {
//        population.sortByfScore();
//        populationHistory.addNewIndividual(population.getInvididual(population.getSize()-1));
////        population.removeIndividual(population.getSize()-1); // remove the worst member
//        population.sortByIndex();
        return population;
    }

    private Object[] crossBreedProcess(GeneBank population, long startTimeForNextCounter) {
        // select new parents and produce offspring
        startTimeForNextCounter = System.currentTimeMillis();
        int parent1Index = rand.nextInt(previousPopulation.getSize()-1);
        int parent2Index = rand.nextInt(previousPopulation.getSize()-1);

        // select 2 randomly
        Individual parent1 = population.getInvididual(parent1Index);
        Individual parent2 = population.getInvididual(parent2Index);
        // cull worst pop - will only add the better of the 2 offspring
//        population = cullPopulationProcess(population);
//        population = cullPopulationProcess(population);

        while (parent1Index == parent2Index) {
            // while: the 2nd parent is the same as the first parent, select random parent 2
            parent2Index = rand.nextInt(previousPopulation.getSize()-1);
        }
        Double[][] offspringProduced = produceMultipleOffspring(previousPopulation, parent1Index, parent2Index);
        combosGenerated += 2;
        combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;

        Individual offspring1 = runIndividual(offspringProduced[0]);
        Individual offspring2 = runIndividual(offspringProduced[1]);

        if (offspring1.getfScore() < population.getWorstIndividual().getfScore() || offspring2.getfScore() < population.getWorstIndividual().getfScore()) {
            // if either child is better than existing memebers, then
            population = cullPopulationProcess(population);
            if (offspring1.getfScore() < offspring2.getfScore()) {
                population.addNewIndividual(offspring1);
            } else if (offspring1.getfScore() > offspring2.getfScore()) {
                population.addNewIndividual(offspring2);
            } else {
                // equal score
                logger.info("Individual randomly mutated to the same score");
                logger.info("1=", offspring1.getGenes());
                logger.info("2=", offspring2.getGenes());
                population = cullPopulationProcess(population);
                population.addNewIndividual(offspring1);
                population.addNewIndividual(offspring2);
            }
        } else {
            int breakx = 0;
        }
        return new Object[]{population, offspring1, offspring2, parent1, parent2 };
    }

    private GeneBank mutatePopulationProcess(GeneBank population, Individual offspring1, Individual offspring2, long startTimeForNextCounter, Individual parent1, Individual parent2, int mutationsConsidered) {
        if (rand.nextDouble() < Variables.getCurrentMutationProbability()) {
            // random which individual to mutate
            startTimeForNextCounter = System.currentTimeMillis();
            int mutationPick = rand.nextInt(5);
            Double[] mutatedOffspring;
            switch (mutationPick) {
                case 0:
                    mutatedOffspring = utils.copyDoubleArray(offspring1.getGenes());
                    break;
                case 1:
                    mutatedOffspring = utils.copyDoubleArray(parent1.getGenes());
                    break;
                case 2:
                    mutatedOffspring = utils.copyDoubleArray(parent2.getGenes());
                    break;
                case 3:
                    mutatedOffspring = utils.copyDoubleArray(offspring2.getGenes());
                    break;
                case 4:
                    mutatedOffspring = utils.copyDoubleArray(previousPopulation.getInvididual(rand.nextInt(previousPopulation.getSize())).getGenes());
                    break;
                default:
                    logger.error("SHOULD NOT GET HERE");
                    mutatedOffspring = null;
                    System.exit(1);
            }
            mutatedOffspring = performMutation(mutatedOffspring);
            combosGenerated += 1;
            combinationGenerationTime += System.currentTimeMillis() - startTimeForNextCounter;
            Individual newIndividual = runIndividual(mutatedOffspring);
            mutationsConsidered++;
            if (newIndividual.getfScore() < population.getWorstIndividual().getfScore()) {
                population = cullPopulationProcess(population);
                population.addNewIndividual(newIndividual);
            }
        }
        return population;
    }
}
