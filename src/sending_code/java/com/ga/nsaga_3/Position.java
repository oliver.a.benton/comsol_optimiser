package com.ga.nsaga_3;

import java.util.Objects;

public class Position {

    private static int indexSeed = 0;
    private int index;
    private double theta, radius, x, y;
    boolean inUse;


    public Position(double theta, double radius) {
        this.theta = theta;
        this.radius = radius;
        this.x = radius * Math.cos(theta);
        this.y = radius * Math.sin(theta);
        this.inUse = false;
        this.index = indexSeed;
        indexSeed ++;
    }

    public int getIndex() {
        return index;
    }

    public double getTheta() {
        return theta;
    }

    public double getRadius() {
        return radius;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean positionsAreEqual(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Double.compare(position.theta, theta) == 0 &&
                Double.compare(position.radius, radius) == 0;
    }

    public boolean isZeroLayer() {
        return this.radius == 0;
    }

}
