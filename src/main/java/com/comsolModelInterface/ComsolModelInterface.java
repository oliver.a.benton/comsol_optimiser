package com.comsolModelInterface;

import com.comsol.model.BatchFeature;
import com.comsol.model.Model;
import com.ga.nsaga.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface for running different comsol models
 */
public interface ComsolModelInterface {

	Model generateFreshModel();

	public void updateParams(String[] inputParamNames, String[] paramValues, boolean displayDebug);

	public double runModel(int modelNumber) throws Exception;

	public String getDoubleParameter(String s);

	public void saveImagesInt(String filename, int iteration, Integer[] combination, String startDate, double[][] model360Points);

	public void saveImagesDouble(String filename, int iteration, Double[] combination, String startDate, double[][] model360Points);

	public void deleteAllScatterers();

	public void setCoreOnOff(boolean coreIsOn);

	void addNewScatterer(int circleNumber, double xPosition, double yPosition);

	List<BatchFeature> getBatchJobs();

	/**
	 * Method to set the size of the grid when creatign the raw data. Will create a square grid of the input size.
	 * Recommended is putting in 256 or 512.
	 * @param size
	 * @param model
	 */
	void setRawDataGridSize(int size, Model model);

	ArrayList<String> getAllSquareScatterers();

	ArrayList<String> getAllCircularScatterers();

	double getResult();

    void setAllSquareScatterers(Integer[] testedIndividual, Position[] scattererMap);

	void setAllCircularScatterers(Integer[] testedIndividual, Position[] scattererMap);

	void setAllSquareScatterersDouble(Double[] testedIndividual, Position[] scattererMap);

	void setAllCircularScatterersDouble(Double[] testedIndividual, Position[] scattererMap);

	public void switchToCircularScatterers();

	public void switchToSquareScatterers();

	public void switchToSquareCentralObject();

	public void switchToCircleCentralObject();

	public double[][] getModels360Points();

	/**
	 * Method to automatically run the completely empty model. After completeing it will reinitialise with square scatterers.
	 */
	public void runBlankModel() throws Exception;

	public void saveImagesOneOff(String filename, int iteration, Integer[] combination, double[][] model360Points);

    void saveModel(String filePath);

    void setFitnessWithoutCloak(double fitnessWithoutCloak);

    void setFitnessForSquareMetrics(double fitnessForSquareMetrics);
}

/**
 * DEFAULT/COMMON USEAGE OF OVERRIDE METHODS
 */
//	@Override
//	public Model generateFreshModel() {
//		long startTime = System.currentTimeMillis();
//		Model model = null;
//		try {
//			ModelUtil.initStandalone(false);
//			logger.info("Initialised stand alone");
//			logger.info("Standard time to complete this stage is around 156s - about 5 minutes");
//			model = run();
//			logger.info("run() finished, time taken: {}ms = {}s", (System.currentTimeMillis() - startTime), ((System.currentTimeMillis() - startTime) / 1000));
//			long currentTime = System.currentTimeMillis();
//			model = run2(model);
//			logger.info("run2() finished, time taken: {}ms = {}s", (System.currentTimeMillis() - currentTime), ((System.currentTimeMillis() - currentTime) / 1000));
//			currentTime = System.currentTimeMillis();
//			model = run3(model);
//			logger.info("run3() finished, time taken: {}ms = {}s", (System.currentTimeMillis() - currentTime), ((System.currentTimeMillis() - currentTime) / 1000));
//			model = run4(model);
//			logger.info("All runs(), time taken: {}s", ((System.currentTimeMillis() - startTime) / 1000));
//		} catch (Exception e) {
//			logger.error("{}.  CONTEXT: ", e.getMessage(), e);
//			System.exit(1);
//		}
//		return model;
//	}

//	@Override
//	public void updateParams(String[] inputParamNames, String[] paramValues, boolean displayDebug) {
//		int i;
//		for (i = 0; i < inputParamNames.length; i++) {
//			model.param().set(inputParamNames[i], paramValues[i]);
//		}
//		logger.trace("changed model parameter set to: {}", Arrays.toString(paramValues));
//	}

//	@Override
//	public double runModel() throws Exception {
//		try {
//			long startTime = System.currentTimeMillis();
//			StudyList s = model.study();
//			model.geom("geom1").run("fin"); // run geometry
//			logger.trace("finished geom.run");
//			model.mesh("mesh1").run("map1"); // run mesh
//			logger.trace("finished mesh.run");
//			s.get("std1").run(); // run study
//			logger.trace("finished study.run");
//			model.sol("sol1").runAll(); // run all study solutions
//			logger.trace("Finished runAll()");
//			model.result("pg1").run(); // run result
//			logger.trace("Finished result.run()");
//			model.result().numerical("int1").set("table", "tbl1"); // set result in table
//			model.result().numerical("int1").setResult();
//			double[][] tempArray = model.result().numerical("int1").getReal();
//			logger.trace("Model run completed, obtained value {}, time taken: {}s", tempArray[0][0], ((System.currentTimeMillis() - startTime) / 1000));
//			return tempArray[0][0];
//		} catch (Exception e) {
//			if (e.getMessage().startsWith("A_problem_occurred_when_building_mesh_feature_X")) {
//				logger.error("Problem building the mesh... Params are: {}   CONTEXT: ", Arrays.toString(getParameters()), e);
//			} else {
//				logger.error("{}.  CONTEXT: ", e.getMessage(), e);
//			}
//			throw e;
//		}
//	}

//	@Override
//	public String getDoubleParameter(String s) {
//		String param = "";
//		double paramDouble = 0d;
//		// try to get the parameter
//		try {
//			param = model.param().get(s);
//			paramDouble = model.param().evaluate(s);
//		} catch (Exception e) {
//			// if problem getting parameter, log and quit
//			logger.error("Exception while finding the value of a parameter.  CONTEXT:", e);
//			logger.error("System will freeze with this, shutting down");
//			System.exit(1);
//		}
//		return paramDouble + "";
//	}

//	private String[] getParameters() {
//		String[] returnArray = new String[30];
//		int i = 0;
//		int parameterSet = 0;
//		int threes = 1;
//		// 10 parameter sets
//		for (parameterSet = 1; parameterSet < 11; parameterSet++) {
//			// 3 parameters per set
//			for (threes = 1; threes < 4; threes++) {
//				String starting = "";
//				switch (threes) {
//					case 1:
//						starting = "n";
//						break;
//					case 2:
//						starting = "R";
//						break;
//					case 3:
//						starting = "phi";
//						break;
//					default:
//						logger.error("SERIOUS ERROR WHILE SWITCHING TO FIND THE BREAK");
//						break;
//				}
//				logger.debug("attempting to get {}{}", starting, parameterSet);
//				returnArray[i] = model.param().get(starting + parameterSet);
//
//				// overall location counter
//				i++;
//			}
//		}
//		return returnArray;
//	}

// 		public void saveImagesInt() {
//			// repeat the next line for each image to choose the file name for it.
//			model.result().export("img2").set("pngfilename", (utils.getImageOutputFolder() + "\\audioCloaking_test1_2_server_image_iteration_"+(iteration)+".png"));
//			// run this code after every image has either been disabled or had a save location named
//			model.batch().create("graphics_job", "Batch");
//			model.batch("graphics_job").set("graphics", "on");
//			model.batch("graphics_job").create("export_sequence_task", "Exportseq");
//			model.batch("graphics_job").run();
//		}