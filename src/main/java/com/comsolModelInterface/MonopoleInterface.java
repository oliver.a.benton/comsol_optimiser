package com.comsolModelInterface;

public interface MonopoleInterface extends ComsolModelInterface {

    public double[][][] getMonopoleComplexPoints() ;

    public void switchToMonopoleSource();

    public void switchToPlaneSource();

}
