package com.comsolModelInterface;/*
 */

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.GlobalUtils;
import com.comsol.model.*;
import com.comsol.model.util.*;
import com.comsol.util.exceptions.LicenseException;
import com.Variables;
import com.ga.nsaga.NsagaUtils;
import com.ga.nsaga.Position;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Model exported on Jan 31 2019, 11:06 by COMSOL 5.3.1.348.
 */
public class ComsolModelInterface2_0 implements ComsolModelInterface {

    public static final int MAX_SCATTERERS = 60; // maximum scatterers in a layer
    public static final double MINIMUM_SCATTERER_RADIUS = 0.0005;
    public static final double FIXED_SCATTERER_RADIUS = 0.0006; //2.67mm for original paper - our value is 0.07/lam
    private boolean saveMph = false;
    private Model model;
    private Logger logger = (Logger) LoggerFactory.getLogger(ComsolModelInterface2_0.class.getName());
    private UtilsAndSettings utils = new UtilsAndSettings();
    private int rawGridSize=255; // make sure to make this desired size - 1
    private NsagaUtils nsagaUtils = new NsagaUtils();
    private int freshModelsCreated = 0;

    /**
     * Used to create a new Comsol Model object - this is a wrapper class for the comsol models that are created by comsol
     *
     * @param generate
     * @param level
     */
    public ComsolModelInterface2_0(boolean generate, Level level) {
        if (generate) {
            this.model = generateFreshModel();
        }
        this.logger.setLevel(level);
    }

    /**
     * This is a class to create and store a fresh internal model, while also returing the direct model stored
     *
     * @return the newly created model
     */
    @Override
    public Model generateFreshModel() {
        freshModelsCreated++;
        long startTime = System.currentTimeMillis();
        try {
            if (Variables.waitForLicense()) {
                GlobalUtils.waitForLicense();
            }
            ModelUtil.initStandalone(false); // why is this one breaking? TODO
            logger.trace("Initialised stand alone");
            logger.trace("Setting up new model, estimated time \u2248 70s");
            logger.debug("Setting up model, this could take a while: 4-5 minutes on average.");
            ModelUtil.clear();
            nsagaUtils.setComsolPreferences();
            try {
                model.sol("sol1").clearSolutionData();
                model.sol("sol2").clearSolutionData();
                logger.trace("Clearing solution data");
                model.mesh().clearMeshes();
                logger.trace("Cleared meshes");
            } catch (NullPointerException nE) {
                // if it reaches here, it should be the case that the model doesn't exist anymore
                logger.debug("Generatiing fresh model");
                this.model = Variables.generateFreshComsolModel();
                logger.debug("Fresh model generated");
            } catch (Exception e) {
                int breaxk = 0;
                logger.error("Error occured, keep an eye on it");
                logger.error(e.getMessage());                logger.error("", e.getStackTrace());
            }
            System.gc();
            if (Variables.isClearModelOnGenerate())
                this.model = Variables.generateFreshComsolModel();
            System.gc();
//            nsagaUtils.setComsolPreferences();
            logger.debug("runs() finished, time taken: {}ms = {}s", (System.currentTimeMillis() - startTime), ((System.currentTimeMillis() - startTime) / 1000));
            logger.trace("All model runs(), model created time taken: {}s", ((System.currentTimeMillis() - startTime) / 1000));
            if (Variables.getSanchisDenominator()!= -99999999.9) {
                // if the sanchis denominator is not set to initialised value, then set to the stored value
                setFitnessForSquareMetrics(Variables.getSanchisDenominator());
                runModel(-997);
            }
        } catch (LicenseException e) {
            logger.error("LICENSE Exception when creating model. Need to terminate");
            System.exit(1);
        } catch (Exception e) {
            if (e.getMessage().equals("Model_node_X_is_removed#Model")) {
                this.model = Variables.generateFreshComsolModel();
                System.gc();
                System.out.println("Complete model reset on model");
            }
            else{
                logger.error("{}.  CONTEXT: ", e.getMessage(), e);
                System.exit(1);
            }
        }
        logger.info("Comsol to string, generateModel: " +this.toString() );
        return model;
    }

    /**
     * Used to update a list of parameters for the internal model
     *
     * @param inputParamNames the parameter names to be updated
     * @param paramValues     the values associated with each paramter name
     * @param displayDebug    a boolean control variable on whether or not to display information about what is going on - seemingly redundant for current code
     */
    @Override
    public void updateParams(String[] inputParamNames, String[] paramValues, boolean displayDebug) {
        int i;
        for (i = 0; i < inputParamNames.length; i++) {
            model.param().set(inputParamNames[i], paramValues[i]);
            // if nx == 0
            if (inputParamNames[i].startsWith("n")) {
                // get ax value
                String layerUsedBoolean = "a" + inputParamNames[i].charAt(1);
                // if the input value is 0 (no value in layer), set not to use layer
                if ((Integer.parseInt(paramValues[i]) == 0)) {
                    model.param().set(layerUsedBoolean, "0");
                } else {
                    model.param().set(layerUsedBoolean, "1");
                }
            }
        }
        logger.trace("changed model parameter set to: {}", Arrays.toString(paramValues));
    }

    /**
     * Used to run the existing model
     *
     * @return
     * @throws Exception
     */
    @Override
    public double runModel(int modelNumber) throws Exception {
        if (Variables.waitForLicense()) {
            GlobalUtils.waitForLicense();
        }
        long startTime = System.currentTimeMillis();
        double modelResult = newRunModel(modelNumber);
        if (modelNumber%10 == 0) {
            logger.trace("    Run model {} in time {}s", modelNumber, (System.currentTimeMillis() - startTime) / 1000);
        } else {
            logger.trace("    Run model {} in time {}s", modelNumber, (System.currentTimeMillis() - startTime) / 1000);
        }
//        logger.trace("Run model {} in time {}s", modelNumber, (System.currentTimeMillis() - startTime) / 1000);
        return modelResult;
    }

    /**
     * Actual code that runs the model
     *
     * @return
     */
    private double newRunModel(int modelNumber) throws Exception {
        int stepFailed = 0;
        try {
            if (modelNumber > -900) {
                switch (Variables.getShapeOfObject()) {
                    case "Square":
                        switchToSquareCentralObject();
                        break;
                    case "Circle":
                        switchToCircleCentralObject();
                        break;
                    default:
                        switchToSquareCentralObject();
                        break;
                }
            }
            long startTime = System.currentTimeMillis();
            long currentTime = startTime;
//            currentTime = System.currentTimeMillis();
            model.geom("geom1").run("fin"); // run geometry
            model.component("comp1").geom("geom1").run();
            stepFailed = 1;
//            logger.trace("finished geom.run after {}s", (System.currentTimeMillis() - currentTime) / 1000);
//            currentTime = System.currentTimeMillis();
//            model.mesh("mesh1").run("map1"); // run mesh
//
//            logger.trace("finished mesh.runafter {}s", (System.currentTimeMillis() - currentTime) / 1000);
            model.component("comp1").geom("geom1").feature("c3").getDoubleArray("pos");
            currentTime = System.currentTimeMillis();
//            if (modelNumber == -298)
//                saveModel("D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\output\\breaking_model");
            model.sol("sol1").runAll(); // run all study solutions
            stepFailed = 2;
            logger.trace("Finished runAll() after {}s", (System.currentTimeMillis() - currentTime) / 1000);
            currentTime = System.currentTimeMillis();
            String[] resultList = model.result().tags();
            for (String resultTag: resultList) {
                model.result(resultTag).run();
            }
            stepFailed = 3;
            logger.trace("Finished result.run() after {}s", (System.currentTimeMillis() - currentTime) / 1000);
            currentTime = System.currentTimeMillis();
            model.result().numerical().run();
            stepFailed = 4;
            model.result().numerical("int1").computeResult();
            stepFailed = 5;
            logger.trace("Finished numberical.run after {}s", (System.currentTimeMillis() - currentTime) / 1000); //todo double check running produces a different result each time.
            double[][] tempArray = model.result().numerical("int1").getReal();
            logger.trace("Model {} run completed, obtained value {}, time taken: {}s", modelNumber, tempArray[0][0], ((System.currentTimeMillis() - startTime) / 1000));
            return tempArray[0][0];
        } catch (Throwable e) {
            logger.error("Use stepFailed = "+ stepFailed);
            logger.error("Exception occured after {} models generated", freshModelsCreated);
            if (e.getMessage().startsWith("A_problem_occurred_when_building_mesh_feature_X")) {
                logger.error("Problem building the mesh for model {}...  ", modelNumber);
                int breakx = 0;
            } else if (e.getMessage().contains("Old_model_cannot_be_reconstructed._Do_Clear_Solution_to_expunge")) {
                logger.error("'Do clear solution Error' during model {}\n{}", modelNumber, e);
                int breakx = 0; // potential solution = model.component(<ctag>).mesh(<tag>).clearMesh();
//                throw e;
            } else {
                logger.error("Problem while building model {}\n{}.  ", modelNumber,e);
            }
            try {
                model.sol("sol1").clearSolutionData();
                model.sol("sol2").clearSolutionData();
                saveModel("D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\output\\breaking_model" + Integer.toString(modelNumber));
            } catch (Throwable e2) {
                logger.error("Error while saving a broken model - model number {} with combination \n ", modelNumber, e2);
//                saveCombinationToCSV("D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\output\\breaking_model_combo_" + modelNumber +".csv", com);
                int breakx = 0;
            }
            this.model = generateFreshModel(); //todo move this out of the  line
            throw e;
        }
    }

    /**
     * Used to get the value as a string of the parameter passed in
     *
     * @param s the parameter to find
     * @return the value, in string form
     */
    @Override
    public String getDoubleParameter(String s) {
//        String param = "";
        double paramDouble = 0d;
        // try to get the parameter
        try {
//            param = model.param().get(s);
            paramDouble = model.param().evaluate(s);
        } catch (Exception e) {
            // if problem getting parameter, log and quit
            logger.error("Exception while finding the value of parameter {}.  CONTEXT:", s, e);
            logger.error("System will freeze with this, shutting down");
            System.exit(1);
        }
        return paramDouble + "";
    }

    /**
     * The call code to save images
     *
     * @param filename  the name of the file to save
     * @param iteration the current iteration of the code - gives some slightly more unique identifiers to generated items
     */
    @Override
    public void saveImagesInt(String filename, int iteration, Integer[] combination, String startDate, double[][] model360Points) {
        saveImagesOrigianl(utils, filename, saveMph, iteration, combination, startDate, model360Points);// this one works
//		saveImagesContinuousRun(utils, filename);
    }

    @Override
    public void saveImagesDouble(String filename, int iteration, Double[] combination, String startDate, double[][] model360Points) {
        // empty method, this is a int based interface
    }

    private void saveCombinationToCSV(String path, Integer[] combination) {
        try (PrintWriter writer = new PrintWriter(new File(path))) {

            StringBuilder sb = new StringBuilder();
            // Append strings from array
            for (int element : combination) {
                sb.append(element);
                sb.append(",");
            }

            writer.write(sb.toString());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    private void saveModel360ToCSV(String path, double[] combination) {
        try (PrintWriter writer = new PrintWriter(new File(path))) {

            StringBuilder sb = new StringBuilder();
            // Append strings from array
            for (double element : combination) {
                sb.append(element);
                sb.append(",");
            }

            writer.write(sb.toString());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    private String combineName(String fileName, int combination, int orderValue) {
        String[] fileSplit = fileName.split("\\.");
        String fileType = "";
        if (fileSplit.length >= 2)
            fileType = "."+fileSplit[1];
        return fileSplit[0] + "__combo="+ combination + "__value=" + orderValue + fileType;
    }


    /**
     * Actual code responsible for saving images and CSV's
     *
     * @param utils
     * @param filename
     * @param saveMph   true, if you want an mph file saved for each iteation
     * @param iteration
     */
    private void saveImagesOrigianl(UtilsAndSettings utils, String filename, boolean saveMph, int iteration, Integer[] combination, String startDate, double[][] model360Points) {
        // repeat the next line for each image to choose the file name for it.
        long startTime = System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm");
        Date date = new Date();
        Path path = Paths.get(Variables.getMassImageOutputFolder(startDate, ""), "acousticCloaking_programStart-" + startDate, filename + startTime);
        File directory = new File(path.toAbsolutePath().toString());
        if (!directory.exists()) {
            directory.mkdirs();
        }

        String jobName = "graphics_job_" + filename;
        String taskName = "export_sequence_task_"+iteration;
        saveImagesMainCode(path, iteration);

        logger.trace("Graphics batch created: {}", jobName);
        try {
//            model.batch(jobName).run();
            saveCombinationToCSV(path.toString()+"\\"+combineName("combination_", iteration, 0) +".csv", combination);
            saveModel360ToCSV(path.toString()+"\\"+combineName("360_points_", iteration, 17) +".csv", model360Points[0]);
            saveScatsToOneHot(path.toString()+"\\"+combineName("onehot_coords_", iteration, 20) +".csv");
        } catch (Exception e) {
            if (e.getMessage().equals("Process_status_indicates_that_process_is_running")) {
                logger.error("Graphics job {} is already running: ", jobName, e);
            }
            if (e.getMessage().equals("An_object_with_the_given_name_already_exists")) {
                logger.error("Graphics job {} already exists, problem found: ", taskName, e);
            } else {
                logger.error("Batch job: {}--- {}", jobName, e);
            }
        }
        logger.trace("{} has been started", taskName);
        if (saveMph) {
            try {
                model.save(path + "\\model");
            } catch (IOException e) {
                logger.error("IO error when saving model file", e);
            } catch (Exception e) {
                logger.error("Error when saving model file", e);
            }
        }
    }

    private int[] createZeroArray() {
        int[] xOneHot = new int[354];
        for (int i = 0; i < xOneHot.length; i++) {
            xOneHot[i] = 0;
        }
        return xOneHot;
    }

    private int convertCoordToPixelPosition(double coord) {
        return (int)Math.round(((coord - 0) * (Variables.getPixelWidth()-0))/(Variables.getRegionThickness()-0) + 0);
    }

    private void saveScatsToOneHot(String path) {
        try (PrintWriter writer = new PrintWriter(new File(path))) {

            StringBuilder sb = new StringBuilder();
            ArrayList<String> scatList;
            if (Variables.getShapeOfScatterer().toLowerCase().equals("square"))
                scatList = getAllSquareScatterers();
            else
                scatList = getAllCircularScatterers();
            scatList = new ArrayList<String>(scatList.subList(0, Variables.getMaximumScatterersInAQuadrant()));

            // Append strings from array
            for (String scatName : scatList) {
                double[] coords = model.component("comp1").geom("geom1").feature(scatName).getDoubleArray("pos");
//                int[] xOneHot = createZeroArray();
//                xOneHot[convertCoordToPixelPosition(coords[0])] = 1;
//                int[] yOneHot = createZeroArray();
//                yOneHot[convertCoordToPixelPosition(coords[1])] = 1;
//                for (int element : xOneHot) {
//                    sb.append(element);
//                    sb.append(",");
//                }
                sb.append(convertCoordToPixelPosition(coords[0]));
                sb.append("\n");
                sb.append(convertCoordToPixelPosition(coords[1]));
                sb.append("\n");
            }

            writer.write(sb.toString());
            int breakx = 0;
        } catch (FileNotFoundException fNFE) {
            System.out.println(fNFE.getMessage());
        } catch (Exception e) {
            logger.error("Error during saving the onehot coordianate", e);
        }

    }


    /**
     * Sepcifically for the scatterers that are used with the acoustic cloaking
     *
     * @param circleNumber
     * @param xPosition
     * @param yPosition
     */
    @Override
    public void addNewScatterer(int circleNumber, double xPosition, double yPosition) {
        String name = "c" + circleNumber;
        model.component("comp1").geom("geom1").create(name, "Circle");
        model.component("comp1").geom("geom1").feature(name).set("pos", new double[]{xPosition, -yPosition});
        model.component("comp1").geom("geom1").feature(name).set("r", "radius_scatterer");
//		model.component("comp1").view("view1").hideObjects("hide1").set((name+"(1)"), new int[]{1, 2, 3, 4}); // todo determine if this is needed
    }

    /**
     * Removes every scatterer from the comsol simulation
     */
    @Override
    public void deleteAllScatterers() {
        GeomFeatureList featureList = model.component("comp1").geom("geom1").feature();
        ArrayList<GeomFeature> toDeleteList = new ArrayList<>();
//		model.component("comp1").geom("geom1").create("c2", "Circle");
        Iterator<GeomFeature> iterator = featureList.iterator();
        GeomFeature currentFeature = null;
        // add to new list of "to delete"
        for (GeomFeature feature : featureList) {
            String cur_tag = feature.tag();
            if (cur_tag.charAt(0) == 'c') {
                String numericCode = feature.tag().substring(1);
                try {
                    Integer numberFound = Integer.parseInt(numericCode);
                    if (!(numberFound == 1) && !(numberFound == 3)) {
                        toDeleteList.add(feature);
                        logger.trace("Adding {} to be deleted", feature.tag());
                    }
                } catch (Exception e) {
                    logger.trace("Not found a number");
                }
            }

        }
        // remove from "to delete" list
        Iterator<GeomFeature> iteratorDeleteList = toDeleteList.iterator();
        while (iteratorDeleteList.hasNext()) {
            try {
                currentFeature = iteratorDeleteList.next();
            } catch (Exception e) {
                logger.error("Iterator has found a null object - {}");
            }
            String cur_tag = currentFeature.tag();
            if (cur_tag.charAt(0) == 'c') {
                String numericCode = currentFeature.tag().substring(1);
                String to_remove = "c";
                try {
                    Integer numberFound = Integer.parseInt(numericCode);
                    if (!(numberFound == 1) && !(numberFound == 3)) {
                        to_remove += Integer.toString(numberFound);
                        model.component("comp1").geom("geom1").feature().remove(to_remove);
                        logger.trace("Removed: {}", to_remove);
                    } else {
                        logger.trace("Found: {}", to_remove + numberFound);
                    }
                } catch (Exception e) {
                    logger.trace("Not found a number");
                }
            }
        }

    }

    /**
     * Flip the core to disabled or enabled - this allows creation of models that have the central object or don't
     *
     */
    @Override
    public void setCoreOnOff(boolean setCoreOn) {
        if (setCoreOn)
            model.component("comp1").geom("geom1").feature("c1").active(true);
        else
            model.component("comp1").geom("geom1").feature("c1").active(false);
//
//        if (model.component("comp1").geom("geom1").feature("c1").isActive()){
//
//        } else {
//            model.component("comp1").geom("geom1").feature("c1").active(true);
//        }

    }

    @Override
    public List<BatchFeature> getBatchJobs(){
        return null;
    }

    @Override
    public void setRawDataGridSize(int size, Model model) {
        ExportFeatureList exportFeatureList = model.result().export();
        ArrayList<ExportFeature> dataExportList = new ArrayList<ExportFeature>();
        // get every feature that is data exporter
        for (ExportFeature feature : exportFeatureList) {
            if (feature.tag().length() > 4 && feature.tag().substring(0, 4).equals("data") ) {
                dataExportList.add(feature);
            }
        }
        // for each one, change the size used
        for (ExportFeature feature : exportFeatureList) {
            feature.set("gridx2", "range(-0.1,0.2/"+ size +",0.1)");
            feature.set("gridy2", "range(-0.1,0.2/"+ size +",0.1)");
        }

    }

    @Override
    public ArrayList<String> getAllSquareScatterers() {
        ArrayList<String> returnList = new ArrayList<>();
        GeomFeatureList featureList = model.component("comp1").geom("geom1").feature();
        for (GeomFeature feature : featureList) {
            String cur_tag = feature.tag();
            if (cur_tag.charAt(0) == 's' && cur_tag.charAt(1)=='q') {
                String numericCode = feature.tag().substring(2);
                try {
                    Integer numberFound = Integer.parseInt(numericCode);
                    // ignore square 1, as this is the central object.
                    if (!feature.label().equals("Central square")) {
                        returnList.add(feature.tag());
                    }
                    else {
                        int breakx = 0;
                    }
                } catch (Exception e) {
                    logger.trace("Not found a number");
                }
            }

        }
        return returnList;
    }

    @Override
    public ArrayList<String> getAllCircularScatterers() {
        ArrayList<String> returnList = new ArrayList<>();
        GeomFeatureList featureList = model.component("comp1").geom("geom1").feature();
        for (GeomFeature feature : featureList) {
            String cur_tag = feature.tag();
            if (cur_tag.charAt(0) == 'c') {
                String numericCode = feature.tag().substring(1);
                try {
                    Integer numberFound = Integer.parseInt(numericCode);
                    if (!feature.label().equals("Circle 2_Far_Field_1") && !feature.label().equals("max_radial_distance_scatterers") && !feature.label().equals("central_pipe")) {
                        returnList.add(feature.tag());
                        logger.trace("Adding {} to be found circle scats", feature.tag());
                    } else {
                        int breakx = 0;
                    }
                } catch (Exception e) {
                    logger.trace("Not found a number");
                }
            }

        }
        return returnList;
    }

    @Override
    public double getResult() {
        NumericalFeatureList featureList = model.result().numerical();
        double result = 0.0;
        for (NumericalFeature feature : featureList) {
            if (feature.label().equals("SanchisMetric"))
                result = feature.getReal()[0][0];
        }
        return result;
    }

    private void switchScattererOffIf0Layer(GeomFeature scat, double[] coordinates ) {
        if (coordinates[0] == 0 && coordinates[1] == 0) {
            scat.active(false);
        } else
            scat.active(true);
    }

    @Override
    public void setAllSquareScatterers(Integer[] testedIndividual, Position[] scattererMap) {
        int currentQuadrant = 0;
        int counter = 0;
        int currentSactNumber = 0;
        ArrayList<String> scattererNames = getAllSquareScatterers();
        while (currentQuadrant < 4) {
            switch (currentQuadrant) {
                case 0:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        logger.trace("Starting to place scat "+  counter);
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        logger.trace("Scat label "+  scatLabel + " obtained");
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = scattererMap[testedIndividual[counter]].getY();
                        logger.trace("Coordinates created");

                        scat.set("pos", coordinates);
                        logger.trace("Coordinates set");

                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Coordinate checked for off status");

                        logger.trace("Set scatterer {} (num {}) to have coordinates {}", scatLabel, currentSactNumber, coordinates);
                        currentSactNumber++;
                    }
                    break;
                case 1:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = -scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} (num {}) to have coordinates {}", scatLabel, currentSactNumber, coordinates);
                        currentSactNumber++;
                    }
                    break;
                case 2:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = -scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} (num {}) to have coordinates {}", scatLabel, currentSactNumber, coordinates);
                        currentSactNumber++;
                    }
                    break;
                case 3:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        try {
                            String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                            GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                            double[] coordinates = new double[2];
                            coordinates[0] = -scattererMap[testedIndividual[counter]].getX();
                            coordinates[1] = -scattererMap[testedIndividual[counter]].getY();
                            scat.set("pos", coordinates);
                            switchScattererOffIf0Layer(scat, coordinates);
                            logger.trace("Set scatterer {} (num {}) to have coordinates {}", scatLabel, currentSactNumber, coordinates);
                            currentSactNumber++;
                        } catch (IndexOutOfBoundsException e) {
                            logger.error("{}",e);
                            int breakx = 0;
                        }
                    }
                    break;
            }
            currentQuadrant ++;
        }
    }

    @Override
    public void setAllCircularScatterers(Integer[] testedIndividual, Position[] scattererMap) {
        int currentQuadrant = 0;
        int counter = 0;
        ArrayList<String> scattererNames = getAllCircularScatterers();
        while (currentQuadrant < 4) {
            switch (currentQuadrant) {
                case 0:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} to have coordinates {}", scatLabel, coordinates);
                    }
                    break;
                case 1:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = -scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} to have coordinates {}", scatLabel, coordinates);
                    }
                    break;
                case 2:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = -scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} to have coordinates {}", scatLabel, coordinates);
                    }
                    break;
                case 3:
                    for (counter=0; counter < testedIndividual.length; counter++ ) {
                        // get the scatterer label, set quadrant 0 to be scats 0-29, quad2 = scat 30-59 etc...
                        String scatLabel = scattererNames.get((currentQuadrant * nsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + counter);
                        GeomFeature scat = model.component("comp1").geom("geom1").feature(scatLabel);
                        double[] coordinates = new double[2];
                        coordinates[0] = -scattererMap[testedIndividual[counter]].getX();
                        coordinates[1] = -scattererMap[testedIndividual[counter]].getY();
                        scat.set("pos", coordinates);
                        switchScattererOffIf0Layer(scat, coordinates);
                        logger.trace("Set scatterer {} to have coordinates {}", scatLabel, coordinates);
                    }
                    break;
            }
            currentQuadrant ++;
        }
    }

    @Override
    public void setAllSquareScatterersDouble(Double[] testedIndividual, Position[] scattererMap) {
        // empty method, this is a int based interface
    }

    @Override
    public void setAllCircularScatterersDouble(Double[] testedIndividual, Position[] scattererMap) {
        // empty method, this is a int based interface
    }

    @Override
    public void switchToCircularScatterers() {
        ArrayList<String> allSquares = getAllSquareScatterers();
        ArrayList<String> allCircles = getAllCircularScatterers();
        for (String scatName: allSquares) {
            model.component("comp1").geom("geom1").feature(scatName).active(false);
        }

        for (String scatName: allCircles) {
            model.component("comp1").geom("geom1").feature(scatName).active(true);
        }

    }

    @Override
    public void switchToSquareScatterers() {
        ArrayList<String> allSquares = getAllSquareScatterers();
        ArrayList<String> allCircles = getAllCircularScatterers();
        for (String scatName: allSquares) {
            model.component("comp1").geom("geom1").feature(scatName).active(true);
        }

        for (String scatName: allCircles) {
            model.component("comp1").geom("geom1").feature(scatName).active(false);
        }

    }

    @Override
    public void switchToSquareCentralObject() {
        model.component("comp1").geom("geom1").feature("c1").active(false);
        model.component("comp1").geom("geom1").feature("sq1").active(true);
    }

    @Override
    public void switchToCircleCentralObject() {
        model.component("comp1").geom("geom1").feature("c1").active(true);
        model.component("comp1").geom("geom1").feature("sq1").active(false);
    }

    @Override
    public double[][] getModels360Points() {
        model.result().table("tbl1").clearTableData();
        model.result().numerical("pev1").set("table", "tbl1");
        model.result().numerical("pev1").setResult();
        return model.result().table("tbl1").getReal();
//        return new String[] {""};
    }

    @Override
    public void runBlankModel() throws Exception {
        ArrayList<String> scattererTags = new ArrayList<>(240);
        scattererTags.addAll(getAllSquareScatterers());
        scattererTags.addAll(getAllCircularScatterers());
        GeomFeatureList featureList = model.component("comp1").geom("geom1").feature();
        // disable all the scatterers
        for (String tag : scattererTags) {
            model.component("comp1").geom("geom1").feature(tag).active(false);
        }
        model.component("comp1").geom("geom1").feature("c1").active(false);
        model.component("comp1").geom("geom1").feature("sq1").active(false);
        runModel(-999);

//        scattererTags = getAllSquareScatterers();
//        for (String tag : scattererTags) {
//            model.component("comp1").geom("geom1").feature(tag).active(true);
//        }
//        model.component("comp1").geom("geom1").feature("sq1").active(true);
    }


    @Override
    public void saveImagesOneOff(String filename, int iteration, Integer[] combination, double[][] model360Points) {
// repeat the next line for each image to choose the file name for it.
        Path path = Paths.get(Variables.getOutputFolder(), "MassImages", filename);
        File directory = new File(path.toAbsolutePath().toString());
        if (!directory.exists()) {
            directory.mkdirs();
        }

        String jobName = "graphics_job_" + filename;
        String taskName = "export_sequence_task_"+iteration;
        saveImagesMainCode(path, iteration);
        try {
//            model.batch(jobName).run();
            saveCombinationToCSV(path.toString()+"\\"+combineName("combination_", iteration, 0) +".csv", combination);
            saveModel360ToCSV(path.toString()+"\\"+combineName("360_points_", iteration, 17) +".csv", model360Points[0]);
        } catch (Exception e) {
            if (e.getMessage().equals("Process_status_indicates_that_process_is_running")) {
                logger.error("Graphics job {} is already running: ", jobName, e);
            }
            if (e.getMessage().equals("An_object_with_the_given_name_already_exists")) {
                logger.error("Graphics job {} already exists, problem found: ", taskName, e);
            } else {
                logger.error("Batch job: {}--- {}", jobName, e);
            }
        }
        logger.trace("{} has been started", taskName);
        if (saveMph) {
            try {
                model.save(path + "\\model");
            } catch (IOException e) {
                logger.error("IO error when saving model file", e);
            } catch (Exception e) {
                logger.error("Error when saving model file", e);
            }
        }
    }

    @Override
    public void saveModel(String filePath) {
        try {
            model.save(filePath);
        } catch (IOException e) {
            logger.error("ERROR SAVING MODEL TO MPH file:\n", e);
            e.printStackTrace();
        }
    }

    @Override
    public void setFitnessWithoutCloak(double fitnessWithoutCloak) {
        model.param().set("fitness_w_o_cloak", Double.toString(fitnessWithoutCloak));
    }

    @Override
    public void setFitnessForSquareMetrics(double fitnessForSquareMetrics) {
        model.param().set("fitness_for_squared_metrics", Double.toString(fitnessForSquareMetrics));
    }


    private void saveImagesMainCode(Path path, int iteration) {
        int[] tags_to_save_legened = new int[] {};
        int valueTag = 0;
        for (ExportFeature exportFeature : model.result().export()) {
            if (exportFeature.getType().equals("Image")) {
                int tagNumber = Integer.parseInt(exportFeature.tag().substring(3, exportFeature.tag().length()));
                String imageName = "output_";
                switch (tagNumber) {
                    case 1:
                        imageName += "absolute_scattered";
                        valueTag = 3;
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecoloractive", true);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormax", 2.0);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormin", 0);
                        break;
                    case 2:
                        imageName = "real_scattered";
                        valueTag = 1;
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecoloractive", true);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormax", 1.0);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormin", -1.0);
                        break;
                    case 3:
                        imageName += "real_total";
                        valueTag = 4;
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecoloractive", true);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormax", 1.0);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormin", -1.0);
                        break;
                    case 4:
                        imageName += "absolute_total";
                        valueTag = 5;
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecoloractive", true);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormax", 3.0);
                        model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormin", 0);
                        break;
                    case 5:
                        imageName += "total_blanked";
                        valueTag = 6;
                        break;
                    case 6:
                        imageName = "geometry_quarter";
                        valueTag = 7;
                        break;
                    case 7:
                        imageName += "total_blanked_thermal";
                        valueTag = 8;
                        break;
                    case 8:
                        imageName += "scattered_blanked_pressure";
                        valueTag = 9;
                        break;
                    case 9:
                        imageName += "scattered_blanked_pressure_thermal";
                        valueTag = 10;
                        break;
                    case 10:
                        imageName += "central_object_only";
                        valueTag = 18;
                        break;
                    case 11:
                        imageName += "only_scatterers";
                        valueTag = 19;
                        break;
                    default:
                        System.out.println("FOUND A NEW TAG THAT ISN'T BEING SAVED" + tagNumber);
                        logger.error("FOUND A NEW TAG THAT ISN'T BEING SAVED {}", tagNumber);
                        break;
                }
//                if (valueTag !=7) {
//                    model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecoloractive", true);
//                    model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormax", 1.0);
//                    model.result(exportFeature.getString("plotgroup")).feature("surf1").set("rangecolormin", -1.0);
//                }

                exportFeature.set("pngfilename", (path + "\\" + combineName(imageName, iteration, valueTag)));
                exportFeature.set("imagetype", "png");
                exportFeature.set("options", true);
                exportFeature.set("title", "off");
                exportFeature.set("legend", false);
                exportFeature.set("legend","off");
                if (tags_to_save_legened.length != 0) {
                    for (int tag : tags_to_save_legened) {
                        if (tagNumber == tag) {
                            model.result(exportFeature.getString("plotgroup")).set("showlegends", true);
                            exportFeature.set("legend", true);
                            exportFeature.set("legend", "on");
                        }
                    }
                }
                exportFeature.set("logo", "off");
                exportFeature.set("axes", "off");
//                model.result().export("img7").set("legend", true);
//                if (exportFeature.label().equals("pg5"))
//                    exportFeature.set("legend", "on");
                exportFeature.run();

                // create the export sequence for each image
//                model.batch(jobName).create(imageName, "Exportseq");
//                model.batch(jobName).feature(imageName).set("seq", exportFeature.tag());
//                model.batch(jobName).feature(imageName).set("param", new String[]{"\"No parameters\""});
            }
            else if (exportFeature.getType().equals("Data")) {
                int tagNumber = Integer.parseInt(exportFeature.tag().substring(4, exportFeature.tag().length()));
                String dataName = "raw_data_";
                switch (tagNumber) {
                    case 1:
                        dataName += "total_blanked";
                        valueTag = 11;
                        break;
                    case 2:
                        dataName = "imaginary";
                        valueTag = 12;
                        break;
                    case 3:
                        dataName += "absolute";
                        valueTag = 13;
                        break;
                    case 4:
                        dataName += "gemoetry";
                        valueTag = 2;
                        break;
                    case 5:
                        dataName += "total";
                        valueTag = 14;
                        break;
                    case 6:
                        dataName += "total_blanked";
                        valueTag = 15;
                        break;
                    case 7:
                        dataName += "scattered_blanked";
                        valueTag = 16;
                        break;
                }
                dataName = dataName + ".txt";
                try {
                    exportFeature.set("filename", (path + "\\" + combineName(dataName, iteration, valueTag)));
//                    model.result().export("data" + tagNumber).set("gridx2", "range(-0.1,0.2/" + rawGridSize + ",0.1)");
//                    model.result().export("data" + tagNumber).set("gridy2", "range(-0.1,0.2/" + rawGridSize + ",0.1)");
//                    model.result().export("data" + tagNumber).run();
                } catch (Exception e) {
                    logger.error("PROBLEM WHEN SAVING MODEL RESULTS tag is :{}\n{}", tagNumber, e);
                }


//                model.batch(jobName).create(dataName, "Exportseq");
//                model.batch(jobName).feature(dataName).set("seq", exportFeature.tag());
//                model.batch(jobName).feature(dataName).set("param", new String[]{"\"No parameters\""});
            }
        }
    }

}
