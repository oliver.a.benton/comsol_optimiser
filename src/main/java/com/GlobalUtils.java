package com;

import ch.qos.logback.classic.Logger;
import com.comsol.model.Model;
import com.comsol.model.util.ModelUtil;
import com.comsol.util.exceptions.LicenseException;
import com.ga.nsaga.RunNSAGA;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class GlobalUtils {
    final static Logger logger = (Logger) LoggerFactory.getLogger(GlobalUtils.class.getName());


//    public static String getMassImageOutputFolder(String startTime, String extraFiles) {
//        String userDir = System.getProperty("user.dir");
//        Path path = Paths.get(userDir, "output", "MassImages", extraFiles, startTime);
//        File directory = new File(path.toAbsolutePath().toString());
//        if (!directory.exists()){
//            directory.mkdirs();
//        }
//        return path.toAbsolutePath().toString();
//    }

//    public static String getOutputFolder() {
//        String userDir = System.getProperty("user.dir");
//        Path path = Paths.get(userDir, "output");
//        File directory = new File(path.toAbsolutePath().toString());
//        if (!directory.exists()){
//            directory.mkdirs();
//        }
//        return path.toAbsolutePath().toString();
//    }

    public static ArrayList<File> getComsolTempFolders() {
        String userDir = System.getProperty("user.home");
        Path path = Paths.get(userDir, "AppData", "Local", "Temp");
        File directory = new File(path.toAbsolutePath().toString());
        ArrayList<File> foundComsolTempFiles = new ArrayList<>();
        if (directory.exists()) {
            for (File fileEntry : directory.listFiles()) {
                String name = fileEntry.getName();
                String dateName = "";
                if (name.startsWith("cs")) {
                    foundComsolTempFiles.add(fileEntry);
                }
            }
        }
        path = Paths.get(userDir, ".comsol");
        return foundComsolTempFiles;
    }

    public static String getComsolRecoveryDirectory() {
        String userDir = System.getProperty("user.home");
        Path path = Paths.get(userDir, ".comsol", Variables.getComsolVersion(), "recoveries");
        return path.toAbsolutePath().toString();
    }

    public static void checkIfLicenseIsFreeOrExit() {
        Model model = ModelUtil.create("testModel");
        try {
            logger.debug("Opening model path");
            model.modelPath(Variables.getModelPath());
            logger.debug("Model path opened");
            model.param().set("c0", "343[m/s]");
        } catch (LicenseException le) {
            logger.error("There is a problem when accessing comsol license. Exiting program", le);
            System.exit(-1);
        }
        logger.debug("Clearing model");
        model = null;
        ModelUtil.clear();
        System.gc();
    }

    public static boolean checkIfLicenseIsFree() {
        Model model = ModelUtil.create("testModel");
        try {
            model.modelPath(Variables.getModelPath());
            model.param().set("c0", "343[m/s]");
        } catch (LicenseException le) {
            logger.trace("There is a problem when accessing comsol license. Exiting program", le);
            return false;
        }
        model = null;
        System.gc();
        logger.trace("License is free");
        return true;
    }

    public static void waitForLicense()  {
        while (!GlobalUtils.checkIfLicenseIsFree()) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.error("Error while trying to sleep thread", e);
                System.exit(-1);
            }
        }
    }
}
