package com.ga.nsaga;

/**
 * This class is representative of a data class, which is used to hold the data of each run of an optimizer
 */
public class IndividualsData {
    // todo refactor to give all the metrics as an array or list - saves later refactoring
    public static final int METRIC_COUNT = 10 + 5;
    int index;
    Integer[] genes;
    double fScore;
    int modelNumber;
    double[][] array360Points;
    double mse;
    double sanchisMetric;
    double sanchisMetricH;
    double sanchisMetricSimple;
    double sanchezMetric;
    double sanchezMetricSimple;
    private double normalisedMSE;
    double sanchisPerfectCloak;
    double sanchezPerfectCloak;
    boolean recoreded = false;
    boolean imageSaved = false;
    double rmse;
    double minPressure;
    double maxPressure;
    double averagePressure;
    double absoluteAveragePressure;
    double l2Norm;
    double[] monopoleData;
    double raghuL2Norm;

    public IndividualsData(int index, Integer[] genes, double fScore, int modelNumber, double[][] array360Points,
                           double mse, double sanchisMetric, double sanchisMetricH, double sanchisMetricSimple,
                           double sanchezMetric, double sanchezMetricSimple, double normalisedMSE,
                           double sanchisPerfectCloak, double sanchezPerfectCloak, boolean recoreded, boolean imageSaved,
                           double rmse, double minPressure, double maxPressure, double averagePressure, double absoluteAveragePressure, double l2Norm, double[] monopoleData, double raghuL2Norm) {
        this.index = index;
        this.genes = genes;
        this.fScore = fScore;
        this.modelNumber = modelNumber;
        this.array360Points = array360Points;
        this.mse = mse;
        this.sanchisMetric = sanchisMetric;
        this.sanchisMetricH = sanchisMetricH;
        this.sanchisMetricSimple = sanchisMetricSimple;
        this.sanchezMetric = sanchezMetric;
        this.sanchezMetricSimple = sanchezMetricSimple;
        this.normalisedMSE = normalisedMSE;
        this.sanchisPerfectCloak = sanchisPerfectCloak;
        this.sanchezPerfectCloak = sanchezPerfectCloak;
        this.recoreded = recoreded;
        this.imageSaved = imageSaved;
        this.rmse = rmse;
        this.minPressure = minPressure;
        this.maxPressure = maxPressure;
        this.averagePressure = averagePressure;
        this.absoluteAveragePressure = absoluteAveragePressure;
        this.l2Norm = l2Norm;
        this.monopoleData = monopoleData;
        this.raghuL2Norm = raghuL2Norm;
    }

//    public IndividualsData(int index, Integer[] genes, double fScore, int modelNumber, double[][] array360Points, double mse) {
//        this.index = index;
//        this.genes = genes;
//        this.fScore = fScore;
//        this.modelNumber = modelNumber;
//        this.array360Points = array360Points;
//        this.mse = mse;
//    }

    public int getIndex() {
        return index;
    }

    public Integer[] getGenes() {
        return genes;
    }

    public double getfScore() {
        return fScore;
    }

    public void setfScore(double fScore) {
        this.fScore = fScore;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public double[][] getArray360Points() {
        return array360Points;
    }

    public double getMse() {
        return mse;
    }

    public void setArray360Points(double[][] array360Points) {
        this.array360Points = array360Points;
    }

    public void setMse(double mse) {
        this.mse = mse;
    }

    public double getSanchisMetric() {
        return sanchisMetric;
    }

    public void setSanchisMetric(double sanchisMetric) {
        this.sanchisMetric = sanchisMetric;
    }

    public double getSanchisMetricH() {
        return sanchisMetricH;
    }

    public void setSanchisMetricH(double sanchisMetricH) {
        this.sanchisMetricH = sanchisMetricH;
    }

    public double getSanchisMetricSimple() {
        return sanchisMetricSimple;
    }

    public void setSanchisMetricSimple(double sanchisMetricSimple) {
        this.sanchisMetricSimple = sanchisMetricSimple;
    }

    public double getSanchezMetric() {
        return sanchezMetric;
    }

    public void setSanchezMetric(double sanchezMetric) {
        this.sanchezMetric = sanchezMetric;
    }

    public double getSanchezMetricSimple() {
        return sanchezMetricSimple;
    }

    public void setSanchezMetricSimple(double sanchezMetricSimple) {
        this.sanchezMetricSimple = sanchezMetricSimple;
    }

    public boolean isRecoreded() {
        return recoreded;
    }

    public void setRecoreded(boolean recoreded) {
        this.recoreded = recoreded;
    }

    public boolean isImageSaved() {
        return imageSaved;
    }

    public void setImageSaved(boolean imageSaved) {
        this.imageSaved = imageSaved;
    }

    public void setNormalisedMSE(double normalisedMSE) {
        this.normalisedMSE = normalisedMSE;
    }

    public double getNormalisedMSE() {
        return normalisedMSE;
    }

    public double getSanchisPerfectCloak() {
        return sanchisPerfectCloak;
    }

    public void setSanchisPerfectCloak(double sanchisPerfectCloak) {
        this.sanchisPerfectCloak = sanchisPerfectCloak;
    }

    public double getSanchezPerfectCloak() {
        return sanchezPerfectCloak;
    }

    public void setSanchezPerfectCloak(double sanchezPerfectCloak) {
        this.sanchezPerfectCloak = sanchezPerfectCloak;
    }

    public double getRmse() {
        return rmse;
    }

    public void setRmse(double rmse) {
        this.rmse = rmse;
    }

    public double getMinPressure() {
        return minPressure;
    }

    public void setMinPressure(double minPressure) {
        this.minPressure = minPressure;
    }

    public double getMaxPressure() {
        return maxPressure;
    }

    public void setMaxPressure(double maxPressure) {
        this.maxPressure = maxPressure;
    }

    public double getAveragePressure() {
        return averagePressure;
    }

    public void setAveragePressure(double averagePressure) {
        this.averagePressure = averagePressure;
    }

    public double getAbsoluteAveragePressure() {
        return absoluteAveragePressure;
    }

    public void setAbsoluteAveragePressure(double absoluteAveragePressure) {
        this.absoluteAveragePressure = absoluteAveragePressure;
    }

    public double getL2Norm() {
        return l2Norm;
    }

    public void setL2Norm(double l2Norm) {
        this.l2Norm = l2Norm;
    }

    public double[] getMonopoleData() {
        return monopoleData;
    }

    public void setMonopoleData(double[] monopoleData) {
        this.monopoleData = monopoleData;
    }

    public double getRaghuMetric() {
        return raghuL2Norm;
    }

    public void setRaghuL2Norm(double raghuL2Norm) {
        this.raghuL2Norm = raghuL2Norm;
    }
}
