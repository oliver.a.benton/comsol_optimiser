package com.ga.nsaga;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.GlobalUtils;
import com.comsol.model.util.ModelUtil;
import com.comsolModelInterface.ComsolModelInterface;
import com.comsolModelInterface.ComsolModelInterface2_0;
import com.comsolModelInterface.EmptyModel;
import com.Variables;
import com.comsolModelInterface.MonopoleInterface2_0;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Control code for the optimizers. This is where you can switch out the scatterer types.
 */
public class RunNSAGA {
    /**
     * todo: add a second individual during crossover - the opposite of the one currently generated
     * todo: rate of cooling - use the exponential to increase the
     */
    private static NsagaUtils utils = new NsagaUtils();
    final static Logger logger = (Logger) LoggerFactory.getLogger(RunNSAGA.class.getName());
    final static boolean USE_COMSOL = Variables.isUseComsol(); // control variable, where it can be switched off to give random answers,
final static String GENE_BANK_FILE_TO_USE = Variables.getPreviousRunFilePath(); // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"

    static String optimizerType = Variables.getOptimizerType(); // control variable to choose which of the optimizers to use - SAGA or NSAGAD:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\activeProcessingCSV\geneHistory_renamed.csv
    static boolean resumePreviousRun = Variables.isResumePreviousRun(); // control if using a previous run. If yes, then please do use the previous generationCounter below - use the GENE_BANK_FILE_TO_USE to control which file is loaded
    static int previousGenerationCounter = 0;
    static boolean runCollsionChecks = Variables.isRunCollsionChecks();
//    final static Object[][] SCATTERER_POSITION_INDEX_MAP = utils.loadParamsFromCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv");
    static Position[] SCATTERER_POSITION_INDEX_MAP;
    static Position[] oldMap;
    final static String shapeOfScatterer = Variables.getShapeOfScatterer();
    final static String shapeOfObject = Variables.getShapeOfObject();
    static double radiusScatterer;
    static String comsolReferencesFolder = Paths.get(System.getProperty("user.dir"), "src","main","resources","comsolPreferences").toString();
    static boolean useGeneRandomisation = Variables.isUseGeneRandomisation();
    static boolean saveAllImages = Variables.isSaveAllImages();
    static boolean stillInitialising = true;
    static long programStartTime;
//    static String mapPath = "previousRunFilePath";

    public static void main(String[] args) {
        Variables.setProgramStartTime(System.currentTimeMillis());
//        if (!Variables.getCsvMapPath().equals(mapPath)) {
//            Variables.setCsvMapPath(mapPath);
//        }
//        SCATTERER_POSITION_INDEX_MAP = setUpScattererIndexMap();
//        Variables.setScattererPositionIndexMap(SCATTERER_POSITION_INDEX_MAP);
        long startTime = System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm"); // used in saving the data, once saving is made available.
        Date date = new Date();
        Level loggerLevel;
        if (USE_COMSOL) {
            loggerLevel = Level.TRACE;
        } else {
            loggerLevel = Level.INFO;
        }
        logger.debug("Logger Level set");

        // insert comsol model of choice here
        ComsolModelInterface comsolModel = null;

        if (USE_COMSOL) {
            logger.debug("Beginning comsol");
            // initialise comsol model.
            ModelUtil.initStandalone(false); // method to make comsol work.
            if (Variables.waitForLicense()) {
                GlobalUtils.waitForLicense();
            }
            logger.debug("Checking License");
            GlobalUtils.checkIfLicenseIsFreeOrExit();
            logger.debug("Licesnse Checked");
            File preferenceFile = new File (Paths.get(comsolReferencesFolder, "comsolReferences.txt").toString());
            // if preferneces exists, load file
            if (preferenceFile.exists()) {
                ModelUtil.loadPreferences();
            } else {
                File preferencesDir = new File (comsolReferencesFolder);
//                utils.setComsolPreferences();

//                try {
//                    FileWriter fw = new FileWriter(preferenceFile);
//                    fw.write("Saved preferences");
//                    fw.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }

            if (Variables.getMonopole()) {
                logger.debug("Creating monopole model");
                comsolModel = new MonopoleInterface2_0(USE_COMSOL, loggerLevel);
            } else {
                logger.debug("Creating regular model");
                comsolModel = new ComsolModelInterface2_0(USE_COMSOL, loggerLevel);
            }
        } else {
            comsolModel = new EmptyModel();
        }
        logger.debug("Model created");
        utils.runRecoveryCleanUp();
        File geneHistoryCSV = new File(Paths.get(Variables.getGeneHistoryDir(), "geneHistory.csv").toString());
        // move the old gene history file.
        if (geneHistoryCSV.exists()) {
            String targetFilePath = geneHistoryCSV.getAbsolutePath().substring(0, geneHistoryCSV.getAbsolutePath().length()-4) +"__archive_"+dateFormat.format(date)+".csv";
            try {
                Files.move(Paths.get(geneHistoryCSV.getPath()), Paths.get(targetFilePath));
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("Error copying the old gene history file to archive");
            }
        }
        File completeHistoryCSV = new File(Paths.get(Variables.getGeneHistoryDir(), "geneHistoryAll.csv").toString());
        if (completeHistoryCSV.exists()) {
            String targetFilePath = completeHistoryCSV.getAbsolutePath().substring(0, completeHistoryCSV.getAbsolutePath().length()-4) +"__archive_"+dateFormat.format(date)+".csv";
            try {
                Files.move(Paths.get(completeHistoryCSV.getPath()), Paths.get(targetFilePath));
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("Error copying the old gene history file to archive");
            }
        }
        Variables.setActiveGeneHistoryCSVLocation(geneHistoryCSV.getAbsolutePath());
        Variables.setCompleteHistoryFilePath(completeHistoryCSV.getAbsolutePath());
        logger.info("Comsol to string, setScatRad: " +comsolModel.toString() );
        try {
            Variables.setRadiusScatterer(Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer")));
        } catch (Exception e) {
            logger.error("Failed to find the radius of scatterers - ", e.getMessage(), e.getStackTrace());
            logger.info("Current model interface status ", comsolModel);
//            logger.info("Current model status ", comsolModel.);
        }
//        radiusScatterer = Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer"));
        logger.info("Comsol Model initiated");
        // load in the exisiting storage for coordinates.
//        UtilsAndSettings utils = new UtilsAndSettings();
        GenesAndfScoresBank genesLoaded = null;
        GenesAndfScoresBank genesHistory = null;
        String startDate = dateFormat.format(date);
        logControlVariables(startDate, comsolModel);
        copyCSVAcross(args[0], String.valueOf(Paths.get(Variables.getMassImageOutputFolder(startDate, ""), "configFileApplied"+startDate+".csv")));
        logger.debug("Starting map generation");
        if (!Variables.usePreviousMap() || (Variables.usePreviousMap() && Variables.readOldMapUseNew()) ) {
            IndexMapGenerator mapGenerator = new IndexMapGenerator(comsolModel);
//        mapGenerator.generateMapAndSave(System.getProperty("user.dir") +"\\src\\main\\resources\\indexMaps\\index_map"+dateFormat.format(date)+".csv");
            if (Variables.readOldMapUseNew()) {
                oldMap = setUpScattererIndexMap();
            }
            String map_path = Paths.get(Variables.getMassImageOutputFolder(dateFormat.format(date), ""), "index_map"+dateFormat.format(date)).toString();
            logger.debug("Beginning map.generate");
            mapGenerator.generateMapAndSave(String.valueOf(Paths.get(map_path)));
            logger.debug("Generated map");
            map_path = map_path+".csv";
            Variables.setCsvMapPath(map_path);
            SCATTERER_POSITION_INDEX_MAP = setUpScattererIndexMap();
            logger.debug("Map loaded");
            Variables.setScattererPositionIndexMap(SCATTERER_POSITION_INDEX_MAP);
        } else {
            SCATTERER_POSITION_INDEX_MAP = setUpScattererIndexMap();
            Variables.setScattererPositionIndexMap(SCATTERER_POSITION_INDEX_MAP);
        }
        logger.debug("Maps created");
        if (resumePreviousRun) {
            GenesAndfScoresBank[] loaded = loadGenesFromClass(genesLoaded, genesHistory, Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer")));
            genesLoaded = loaded[0];
            genesHistory = loaded[1];
        } else {
            genesLoaded = utils.generatePopulationSets(Variables.getRadiusScatterer());
            genesHistory = genesLoaded;
        }
        NSAGA_optimizer optimizer = null;
        logger.debug("Beginning optimiser creation");
        try {
            switch (optimizerType) {
                case "SAGA":
                    optimizer = new SAGA(comsolModel, genesLoaded, genesHistory, USE_COMSOL, loggerLevel, SCATTERER_POSITION_INDEX_MAP, shapeOfScatterer, shapeOfObject, Variables.getRadiusScatterer(), startDate);
                    break;
                case "NSAGA":
                    optimizer = new NSAGA(comsolModel, genesLoaded, genesHistory, USE_COMSOL, loggerLevel, SCATTERER_POSITION_INDEX_MAP, shapeOfScatterer, shapeOfObject, Variables.getRadiusScatterer(), startDate);
                    break;
                default:
                    throw new Error("Can't find the optimizer specified, quitting.");
            }
        } catch (Throwable e) {
            logger.error("Error when creating Optimiser:", e);
            System.exit(1);
        }
        logger.debug("Completed initalising optimiser");
//		Optimizer optimizer = new MicrobialGeneticAlgorithm(comsolModel,genesLoaded, USE_COMSOL, loggerLevel);
        GenesAndfScoresBank genesAndfScoresBank = null;
        stillInitialising = false;
        // try to run the optimizer
        try {
            genesAndfScoresBank = optimizer.runOptimizer(resumePreviousRun, previousGenerationCounter);
        } catch (Exception e) {
            // in the event of early finish, copy the current history file to the same place as details
//            String targetFilePath = GlobalUtils.getMassImageOutputFolder(startDate, "" )+("\\geneHistory__started_"+dateFormat.format(date)+".csv");
//            try {
            utils.runRecoveryCleanUp();
            System.gc();
            // clean memory first then try to save it
//            optimizer.saveCombinationHistory(genesAndfScoresBank); // save the current implementation
//                Files.move(Paths.get(geneHistoryCSV.getPath()), Paths.get(targetFilePath));
//                logger.info("Transferred the existing csv data to the output folder");
//            } catch (IOException ioe) {
//                e.printStackTrace();
//                logger.error("Error copying the active gene history to detials", ioe);
//            } catch (Exception e2) {
//                logger.error("Other Error detected when trying to save files", e2);
//            }
            moveCSVAcross(geneHistoryCSV.getAbsolutePath(),Paths.get(Variables.getMassImageOutputFolder(startDate, "" ),("geneHistory__started_"+dateFormat.format(date)+".csv")).toString());
            moveCSVAcross(completeHistoryCSV.getAbsolutePath(),Paths.get(Variables.getMassImageOutputFolder(startDate, "" ),("geneHistoryALL__started_"+dateFormat.format(date)+".csv")).toString());

            if (!e.getMessage().equals("Old_model_cannot_be_reconstructed._Do_Clear_Solution_to_expunge")) {
                logger.error("Broke while running optimiser", e);
                System.exit(1);
            }

        }


        // use the results to make a table, display and save it
        long simulationFinishTime = System.currentTimeMillis();
        logger.info("Time taken to complete simulation: {}s = {}m = {}h", (System.currentTimeMillis()-startTime)/1000, (System.currentTimeMillis()-startTime)/60000, (System.currentTimeMillis()-startTime)/3600000d);
        genesAndfScoresBank.sortByfScore();
        IndividualsData bestIndividual = genesAndfScoresBank.getInvididual(0);
        logger.info("The best individual scored {}% cloak, using an fscore of {} where they used a build of {}", 1- bestIndividual.getfScore(), bestIndividual.getfScore(), bestIndividual.getGenes());

        logger.info("Time taken between end and user response: {}s = {}m = {}h", (System.currentTimeMillis()-simulationFinishTime)/1000, (System.currentTimeMillis()-simulationFinishTime)/60000, (System.currentTimeMillis()-simulationFinishTime)/3600000d);
        try {
            moveCSVAcross(geneHistoryCSV.getAbsolutePath(),Paths.get(Variables.getMassImageOutputFolder(startDate, "" ),("geneHistory__started_"+dateFormat.format(date)+".csv")).toString());
            moveCSVAcross(completeHistoryCSV.getAbsolutePath(),Paths.get(Variables.getMassImageOutputFolder(startDate, "" ),("geneHistoryALL__started_"+dateFormat.format(date)+".csv")).toString());

            geneHistoryCSV = new File(Paths.get(System.getProperty("user.dir") , "src","main","resources","activeProcessingCSV","geneHistory.csv").toString());
            logger.info("Now deleting active processing archive");
            Files.delete(geneHistoryCSV.toPath());
        } catch (IOException e) {
            logger.error("Error while removing the generation file list from the active directory.");
            logger.error("{}", e);
        }


        logger.info("Program completed, using System.exit(0)");
        // clean up the batch directory for comsol running.
        System.exit(0);
    }

    public static Position[] setUpScattererIndexMap() {
        Object[][] initalLoadedMap = utils.loadParamsFromCSV(Variables.getCsvMapPath(), false); // original one
        logger.debug("Map loaded from CSV");
        Position[] positions = new Position[initalLoadedMap.length];
        int i = 0;
        DecimalFormat df = new DecimalFormat("#.#########");
        df.setRoundingMode(RoundingMode.CEILING);
        logger.debug("Setting up position index");
        for (i = 0; i< initalLoadedMap.length; i++) {
            logger.trace("Beginning to set up position {}", i);
            try {
                positions[i] = new Position(Double.parseDouble((String) initalLoadedMap[i][3]), Double.parseDouble((String) initalLoadedMap[i][1]));
            } catch (Throwable e) {
                logger.error("Error when trying to setup positon {}", i, e);
            }
        }
//        NsagaUtils.max_no_scat_pos = positions.length;
        return positions;
    }

    private static GenesAndfScoresBank[] loadGenesFromClass(GenesAndfScoresBank genesLoaded, GenesAndfScoresBank genesHistory, Double radiusScatterer) {
        Object[][] csvRawGenes = utils.loadParamsFromCSV((GENE_BANK_FILE_TO_USE), false);
        logger.debug("CSV Genes loaded in");
        genesLoaded = new GenesAndfScoresBank();
        int index = 0;
        for (Object[] loadedIndividualData: csvRawGenes) {
            if (loadedIndividualData[0].equals("")) {
                logger.trace("Skip over, as found an empty first item of the array");
                continue;
            }
            logger.trace("Pre chromosome load Index = " + index);
            // copy the array from 1 end to the other, ignoring the final element.
            // todo will likely have to parse this information.
            Integer[] chromosome = new Integer[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT];
            String[] stringChromosome = (String[]) Arrays.copyOfRange(loadedIndividualData, 0, utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT);
            for (int i= 0; i< stringChromosome.length; i++) {
                // remove white space
                String s = stringChromosome[i];
                s =s.replaceAll("\\s+", "");
                try {
                    chromosome[i] = Integer.parseInt(s);
                } catch (Exception e) {
                    try {
                        chromosome[i] = Integer.parseInt(s.substring(1));
                    } catch (Exception e2) {
                        logger.error(e2.getMessage());
                        e2.printStackTrace();
                    }
                }
            }
            logger.trace("Post chromosome load Index = " + index);

            // todo write up the code to save and load the information
            // chromosome, fscore, model number, 360 points, mse,  sanchis, sanchisH, sanchisSimple, Sanchez, SanchezSimple, l2Norm, rmse, double minPressure, double maxPressure, double averagePressure, double absoluteAveragePressure, double l2Norm
            if (Variables.readOldMapUseNew()) {
                convertIndividualCoordiantesToNewMap(chromosome, radiusScatterer);
            }
            genesLoaded.addNewData(chromosome, Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]), index,
                    null,
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 1].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 1]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 3].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 3]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 4].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 4]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 5].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 5]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 6].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 6]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 7].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 7]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 8].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 8]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 9].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 9]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 10].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 10]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 11].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 11]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 12].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 12]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 13].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 13]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 14].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 14]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 15].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 15]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 16].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 16]),
                    loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 17].equals("null") ? null : Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT + 17])
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +2]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +3]),
//                        Double.parseDouble((String) loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +1]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +4]),
//                        Double.parseDouble((String)loadedIndividualData[utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT +5])
            );
            logger.trace("Post geneLoaded Index = " + index);
            index++;
        }
        // if the number of imported genes is larger than 70, then filter the best 70 from it
        if (genesLoaded.getSize() > utils.MAX_POPULATION_SIZE) {
            genesLoaded.sortByfScore();
            GenesAndfScoresBank tempGenes = new GenesAndfScoresBank();
            for (index = 0; index < utils.MAX_POPULATION_SIZE; index++) {
                tempGenes.addNewIndividual(genesLoaded.getInvididual(index));
            }
            genesHistory = genesLoaded;
            genesHistory.sortByIndex();
            genesLoaded = tempGenes;
        } else if (csvRawGenes.length < utils.MAX_POPULATION_SIZE) {
            // else if there are less entries than there should be, then add on some new random ones
            IndividualsData[] individualsDataSet = utils.generateXRandomIndividuals(utils.MAX_POPULATION_SIZE - genesLoaded.getSize(), Variables.getRadiusScatterer());
            for (IndividualsData individual : individualsDataSet) {
                genesLoaded.addNewIndividual(individual);
            }
            genesHistory = genesLoaded;
        }
        return new GenesAndfScoresBank[]{genesLoaded, genesHistory};
    }

    private static void logControlVariables(String startDate, ComsolModelInterface comsolModel) {
        logger.info("Confirming before Run:");
        logger.info("    runCollisionChecks = "+ runCollsionChecks);
        logger.info("    optimiserType = "+ optimizerType);
        logger.info("    shapeOfScatterer = "+ shapeOfScatterer);
        logger.info("    shapeOfObject = "+ shapeOfObject);
        logger.info("    radiusScatterer = "+ Variables.getRadiusScatterer());
        logger.info("    resumePreviousRun = "+ resumePreviousRun);
        if (resumePreviousRun)
            logger.info("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
        logger.info("    useGeneRandomisation = " + useGeneRandomisation);
        logger.info("    scattererType = " + shapeOfScatterer);
        logger.info("    central Type = " + shapeOfObject);

        try{
            String newLine = System.getProperty("line.separator");
            FileWriter detailsFile =  new FileWriter(String.valueOf(Paths.get(Variables.getMassImageOutputFolder(startDate, ""), "details.txt")));
            detailsFile.write("Confirming before Run:"+ newLine);
            detailsFile.write("    Version num = "+ Variables.getVersionUsed()+newLine);
            detailsFile.write("    runCollisionChecks = "+ runCollsionChecks+ newLine);
            detailsFile.write("    optimiserType = "+ optimizerType+ newLine);
            detailsFile.write("    shapeOfScatterer = "+ shapeOfScatterer+ newLine);
            detailsFile.write("    shapeOfObject = "+ shapeOfObject+ newLine);
            detailsFile.write("    radiusScatterer = "+ Variables.getRadiusScatterer()+ newLine);
            detailsFile.write("    resumePreviousRun = "+ resumePreviousRun+ newLine);
            if (resumePreviousRun)
                detailsFile.write("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE+ newLine);
            detailsFile.write("    useGeneRandomisation = " + useGeneRandomisation+ newLine);
            detailsFile.write("    scattererType = " + shapeOfScatterer+ newLine);
            detailsFile.write("    central Type = " + shapeOfObject+ newLine);
            detailsFile.write("    Allow 0 switching = " +utils.ALLOW_0_SWITCHING + newLine);
            detailsFile.write("    Optimiser = " + utils.USED_ERROR_METRIC + newLine);
            detailsFile.write("    Inital annealing step = " + (utils.INTIAL_ANNEALING_STEP )+ newLine);
            detailsFile.write("    Max scatts in a quadrant = " + Integer.toString(utils.MAXIMUM_SCATTERERS_IN_A_QUADRANT) + newLine);
            detailsFile.write("    Used scatts in a quadrant = " + Integer.toString(utils.USED_SCATTERERS_IN_A_QUADRANT) + newLine);
            detailsFile.write("    Max pop size = " + Integer.toString(utils.MAX_POPULATION_SIZE) + newLine);
            detailsFile.write("    Max scat positions = " + Integer.toString(utils.MAX_POPULATION_SIZE) + newLine);
            detailsFile.write("    Total annealing steps allowed = " + Integer.toString(utils.totalAnnealingStepsAllowed) + newLine);
            detailsFile.write("    Max num population generations= " + Integer.toString(utils.maxNumPopulationGenerations) + newLine);
            detailsFile.write("    Eliete individuals= " + Integer.toString(utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION) + newLine);
            detailsFile.write("    Equilibirum= " + Integer.toString(utils.EQUILIBRIUM_POSITION_INDEX) + newLine);
            detailsFile.write("    Mutation Prob= " + Double.toString(utils.initialMutationProbability) + newLine);
            detailsFile.write("    Inital temp= " + Double.toString(utils.STARTING_TEMPERATURE) + newLine);
            detailsFile.write("    Max temp steps= " + Integer.toString(utils.MAXIMUM_TEMPERATURE_STEPS) + newLine);
            detailsFile.write("    Gen save counter= " + Integer.toString(utils.GENERATION_SAVE_COUNTER) + newLine);
            detailsFile.write("Radius central pipe= " + comsolModel.getDoubleParameter("radius_central_pipe")+ newLine);
            detailsFile.write("Radius scatterer= " + comsolModel.getDoubleParameter("radius_scatterer")+ newLine);
            detailsFile.write("Scat max distance= " + comsolModel.getDoubleParameter("scatterers_max_distance")+ newLine);

            detailsFile.write("Radius central pipe multiplier= " + Variables.getCentralObjectRadisMultiplier()+ newLine);
            detailsFile.write("Radius scatterer multiplier= " + Variables.getScattererRadiusMultiplier()+ newLine);
            detailsFile.write("Scat max distance multiplier= " + Variables.getCloackThicknesssMultiplier()+ newLine);
            detailsFile.close();
            int breakx = 0;
        } catch (IOException e) {
            logger.error("IO exception when writing details file", e);
        }
    }

    private static void moveCSVAcross(String startPath, String targetPath) {
        try {
            utils.runRecoveryCleanUp();
            System.gc();
            // clean memory first then try to save it
//                optimizer.saveCombinationHistory(genesAndfScoresBank); // save the current implementation
            Files.move(Paths.get(startPath), Paths.get(targetPath));
            logger.info("Transferred the existing csv data to the output folder");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            logger.error("Error copying the active gene history to detials", ioe);
        } catch (Exception e2) {
            logger.error("Other Error detected when trying to save files", e2);
        }
    }

    private static void copyCSVAcross(String startPath, String targetPath) {
        try {
            Files.copy(Paths.get(startPath), Paths.get(targetPath));
            logger.info("Transferred the existing csv data to the output folder");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            logger.error("Error copying the active gene history to detials", ioe);
        } catch (Exception e2) {
            logger.error("Other Error detected when trying to save files", e2);
        }
    }

    private static void convertIndividualCoordiantesToNewMap(Integer[] chromosome, Double radiusScatterer) {
        int newMapIndex = 0;
        double distance = 9999;
        double bestDistance = 9999;
        int bestMatch = newMapIndex;
        Position oldPlace = oldMap[chromosome[0]];
        for (int geneIndex = 0; geneIndex < chromosome.length; geneIndex++) {
            for (newMapIndex = 0; newMapIndex < SCATTERER_POSITION_INDEX_MAP.length-1; newMapIndex++) {
                distance = Math.sqrt( Math.pow(oldMap[chromosome[geneIndex]].getX() -
                        SCATTERER_POSITION_INDEX_MAP[newMapIndex].getX(), 2) +
                        Math.pow(oldMap[chromosome[geneIndex]].getY() - SCATTERER_POSITION_INDEX_MAP[newMapIndex].getY(), 2) );
                if (distance < bestDistance) {
                    bestDistance = distance;
                    bestMatch = newMapIndex;
                }
//                }
            }
            chromosome[geneIndex] = bestMatch;
            logger.trace("Found best match, resetting");
            bestDistance = 9999; // reset best distance.
        }

    }

}
