**NSAGA ReadMe:**

This readme is to help understand NSAGA algorithm written here.

To run the program, simply run the class "RunNSAGA".

The changes that you are likely to make will include:
    RunNSAGA.GENE_BANK_FILE_TO_USE      = a file of stored genes that will be used to resume a previous run.
    RunNSAGA.shapeOfScatterer           = the name of the shape used to represent scatterers. Currently supported are square and circular
    RunNSAGA.shapeOfObject              = the name of the shape used to represent the central object to cloak. Currently supported are square and circular
    RunNSAGA.optimizerType              = The optimizer type to use. Supported values are "SAGA" and "NSAGA"
    RunNSAGA.resumePreviousRun          = Value to control if using a previous run. Set to true if you are
    RunNSAGA.runCollsionChecks          = Value to control if using collision checks. Set to true if you are running collision checks.

    NsagaUtils.MAXIMUM_SCATTERERS_IN_A_QUADRANT     = The maximum number of scatterers that could be placed in a quadrant. Only change when changing the comsol model.
    NsagaUtils.USED_SCATTERERS_IN_A_QUADRANT        = The number of scatterers used in a quadrandt. If it is less than the max, then
    NsagaUtils.MAX_POPULATION_SIZE                  = The maximum number of individuals available in a population
    NsagaUtils.max_no_scat_pos                      = the indexGiven of the maximum number of scatterer positions.
    NsagaUtils.maxNumGenerations                    = The maximum number of generations allowed - usually between 20 and 100.
    NsagaUtils.STARTING_TEMPERATURE                 = Temperature to start at. Set to INTIAL temp by default, but if resuming a previous run, can be set here.


NSAGA_optimizer is the abstract class used to make any SA/GA based optimizer, as it contains the information for
crossover, mutation, carrying pop over to the next generation, temperature control and saving information.

SAGA and NSAGA classes are concrete versions of NSAGA_optimizer that perform their labelled optimisations.

optimiserInterface is the interface classed use to enforce consistent calls between every type of optimiser.

IndividualsData is the data storage class used to store individuals - specifically their "chromosome" or solution,
their indexGiven in a list and the score they have achieved.

GenesAndfScoresBank is used to store sets of IndividualsData - this is what sorts them based on indexGiven vs fscore, and also
what is called to get the individuals.


model run -999 = running the empty model.

The scores it generates are in order:
MSE -> 0 for perfect cloak
Sanchis -> sanchis metric from comsol -> 0 for cloak
Sanchis H -> Calculated from Sanchis without the +1 -> 0 for perfect cloak
Sanchis Simple -> Numerator of Sanchis -> 0 for perfect cloak
Sanchez -> Sanchez example -> 1 for perfect cloak
Sanchez Simple -> Sanchez numerator -> 0 for perfect cloak.