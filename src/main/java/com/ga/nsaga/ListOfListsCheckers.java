package com.ga.nsaga;

import java.util.Collections;
import java.util.List;

public class ListOfListsCheckers {

    /**
     *
     * @param itemToFind the item to find in the list
     * @param list the list to search through
     * @param firstElement the lowest position the list is allowed to search through
     * @param lastElement the highest position the list is allowed to go to for the current level
     * @return index of where the item is. If no item round, returns negative 1
     */
    public int recursiveBinarySearch(List<List<Integer>> list, int firstElement, int lastElement, List<Integer> itemToFind) {
        // termination condition
        if (lastElement >= firstElement) {
            int mid = firstElement + (lastElement - firstElement) / 2;

            // if the middle element is our goal element, return its index
            if (areListsIdentical(itemToFind, list.get(mid)))
                return mid;

            // if middle element is smaller than the goal element
            if (isListEarlier(itemToFind, list.get(mid)))
                return recursiveBinarySearch(list, mid + 1, lastElement, itemToFind);

            return recursiveBinarySearch(list, firstElement, mid - 1, itemToFind);
        }
        // if no element has been found that matches it.
        return -1;
    }

    public int searchIfMissingOrInThere(List<List<Integer>> list, List<Integer>  item, int low, int high) {
        if (high <= low) {
            // only item left in the list should be low, if low is identical return low, else negative of what is there
            if (areListsIdentical(item, list.get(low)))
                return low;
            // if the item belongs before the low point, return low, else return low +1
            return -1 * ((isListEarlier(item, list.get(low))) ? low : (low + 1));
        }
        int mid = (low + high)/2;

        // if mid point is equal, add to after mid point
        if (areListsIdentical(item, list.get(mid)))
            return mid+1;

        // if list is earlier, then search from low to  mid
        if (isListEarlier(item, list.get(mid)))
            return searchIfMissingOrInThere(list, item, low, mid-1);

        // else search from mid to high
        return searchIfMissingOrInThere(list, item, mid+1, high);
    }

    /**
     * Assumes sorted lists
     * @param l1 sorted list 1
     * @param l2 sorted list 2
     * @return true if the lists are identical, false if they aren't
     */
    public boolean areListsIdentical(List<Integer> l1, List<Integer> l2) {
        // look through for duplicates
        int duplicateCounters = 0;
        int i = 0;
        while (i == duplicateCounters) {
            if (l1.get(i) == l2.get(i)) {
                duplicateCounters ++;
            }
            i++;
        }
        return i== duplicateCounters;
    }

    /**
     *
     * @param l1 list to add
     * @param l2 list belonging
     * @return true if l1 should be added before l2 (ie. l1[i] < l2[i]), false if equal or after.
     */
    public boolean isListEarlier(List<Integer> l1, List<Integer> l2) {
        // look through for duplicates
        int duplicateCounters = 0;
        int i = 0;
        while (i == duplicateCounters) {
            if (l1.get(i) == l2.get(i)) {
                duplicateCounters ++;
            }
            i++;
        }
        return l1.get(duplicateCounters) < l2.get(duplicateCounters);
    }

    public void sortListsOfLists(List<List<Integer>> list) {
        sortListOfLists(list, 0, list.size()-1);
    }

    private void sortListOfLists(List<List<Integer>> list, int from, int to) {
        if (from < to) {
            int pivot = from;
            int left = from + 1;
            int right = to;
            List<Integer> pivotValue = list.get(pivot);

            while (left <= right) {
                // left <= to -> limit protection
                while (left <= to && !isListEarlier(pivotValue, list.get(left))) {
                    left++;
                }
                // right > from -> limit protection
                while (right > from && isListEarlier(pivotValue, list.get(right))) {
                    right--;
                }
                if (left < right) {
                    Collections.swap(list, left, right);
                }
            }
            Collections.swap(list, pivot, left - 1);
            sortListOfLists(list, from, right - 1); // <-- pivot was wrong!
            sortListOfLists(list, right + 1, to);   // <-- pivot was wrong!
        }
    }

    public int binarySearchForLocationToAdd(List<List<Integer>> list, List<Integer>  item, int low, int high) {
        if (high <= low)
            // if the item belongs before the low point, return low, else return low +1
            return (isListEarlier(item, list.get(low))) ? low: (low+1);
        int mid = (low + high)/2;

        // if mid point is equal, add to after mid point
        if (areListsIdentical(item, list.get(mid)))
            return mid+1;

        // if list is earlier, then search from low to  mid
        if (isListEarlier(item, list.get(mid)))
            return binarySearchForLocationToAdd(list, item, low, mid-1);

        // else search from mid to high
        return binarySearchForLocationToAdd(list, item, mid+1, high);
    }
}
