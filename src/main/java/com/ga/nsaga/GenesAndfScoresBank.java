package com.ga.nsaga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GenesAndfScoresBank {

    Comparator<IndividualsData> indexComparator = new Comparator<IndividualsData>() {
        @Override
        public int compare(IndividualsData o1, IndividualsData o2) {
            Integer a = o1.getIndex();
            Integer b = o2.getIndex();
            return a.compareTo(b);
        }
    };

    Comparator<IndividualsData> scoreDataComparator = new Comparator<IndividualsData>() {
        @Override
        public int compare(IndividualsData o1, IndividualsData o2) {
            Double a = o1.getfScore();
            Double b = o2.getfScore();
            return a.compareTo(b);
        }
    };

	ArrayList<IndividualsData> geneScoreBank;
	int nextEmptyIndex;

	/**
	 * Empty constructor for a brand new set of scores.
	 */
	public GenesAndfScoresBank() {
		this.geneScoreBank = geneScoreBank;
		this.nextEmptyIndex = 0;
		this.geneScoreBank = new ArrayList<>(70);
	}

	/**
	 * Loading in an existing set of scores.
	 * @param geneScoreBank
	 */
	public GenesAndfScoresBank(ArrayList<IndividualsData> geneScoreBank, boolean sortByIndex) {
		this.geneScoreBank = new ArrayList<>(70);
		if (sortByIndex) {

            geneScoreBank.sort(indexComparator);
        }
		this.nextEmptyIndex = geneScoreBank.get(geneScoreBank.size()-1).getIndex();
		this.geneScoreBank = geneScoreBank;
	}

	public void addNewData(Integer[] genes, double fscore, int modelNumber, double[][] array360Points, double mse,
						   double sanchisMetric, double sanchisMetricH, double sanchisMetricSimple, double sanchezMetric,
						   double sanchisPerfectCloak, double sanchezPerfectCloak, double sanchezMetricSimple, double normalisedMSE,
						   double rmse, double minPressure, double maxPressure, double averagePressure, double absoluteAveragePressure, double l2Norm, double raguMetric) {
		geneScoreBank.add(new IndividualsData(nextEmptyIndex, genes, fscore, modelNumber, array360Points, mse,
				sanchisMetric, sanchisMetricH, sanchisMetricSimple, sanchezMetric, sanchezMetricSimple,
				normalisedMSE,  sanchisPerfectCloak, sanchezPerfectCloak,false, false, rmse, minPressure, maxPressure, averagePressure, absoluteAveragePressure, l2Norm, null, raguMetric));
		nextEmptyIndex++;
	}

	/**
	 * Method to copy over an existing individual. Acts as a semi-shallow copy, as the Individual instance is new,
	 * and the index value, but all other values share the same pointer.
	 * @param individual
	 */
	public void addNewIndividual(IndividualsData individual) {
		geneScoreBank.add(new IndividualsData(nextEmptyIndex, individual.getGenes(), individual.getfScore(),
				individual.getModelNumber(), individual.getArray360Points(), individual.getMse(),
				individual.getSanchisMetric(), individual.getSanchisMetricH(), individual.getSanchisMetricSimple(),
				individual.getSanchezMetric(), individual.getSanchezMetricSimple(), individual.getNormalisedMSE(),
				individual.getSanchisPerfectCloak(), individual.getSanchezPerfectCloak(), individual.isRecoreded(), individual.isImageSaved(),
				individual.getRmse(), individual.getMinPressure(), individual.getMaxPressure(), individual.getAveragePressure(), individual.getAbsoluteAveragePressure(),
				individual.getL2Norm(), null, individual.getRaghuMetric()
		));
        nextEmptyIndex++;
	}

    /**
     * Function to tell if the gene bank has more items than it should
     * @return true if the gene bank contains more than the NsagaUtils.MAX_POPULATION_SIZE
     */
    public boolean isGeneBankFull() {
	    return geneScoreBank.size() >= NsagaUtils.MAX_POPULATION_SIZE;
    }

	/**
	 * Method to return the complete set of scores, in index order.
	 * @return
	 */
	public double[] getScores() {
		double[] scores = new double[geneScoreBank.size()];
		sortByIndex();
		int i = 0;
		for (IndividualsData geneAndfScoreData: geneScoreBank) {
			scores[i] = geneAndfScoreData.getfScore();
			i++;
		}
		return scores;
	}

	public int getSize() {
		return geneScoreBank.size();
	}

	public IndividualsData getInvididual(int indexWanted) {
		return geneScoreBank.get(indexWanted);
	}

	/**
	 * Method to sort the entire gene bank by the order it was added in. Sorts by ascending.
	 */
    public void sortByIndex() {
        Collections.sort(geneScoreBank, indexComparator);
    }

	/**
	 * Method to sort the entire gene bank by the fscore it has stored. Sorts by Ascending.
	 */
	public void sortByfScore() {
        Collections.sort(geneScoreBank, scoreDataComparator);
    }


}
