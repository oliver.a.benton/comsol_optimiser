package com.ga.nsaga_3;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.comsol.model.util.ModelUtil;
import com.comsolModelInterface.ComsolModelInterface;
import com.comsolModelInterface.ComsolModelInterface3_0;
import com.comsolModelInterface.EmptyModel;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static java.nio.file.Paths.get;

public class Run_single_entry {

//    private static Integer[] initalEntry = new Integer[] {388,81,562,650,496,481,371,985,408,779,600,588,263,708,958,295,413,81,884,421,691,257,949,565,925,437,227,139,860,964,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {70,65,642,136,264,138,846,911,912,467,723,278,89,347,799,32,162,99,292,229,102,744,680,682,249,250,314,125,829,702,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {961,35,280,395,161,309,935,1008,150,898,5,702,275,384,310,1007,320,64,125,54,889,854,251,960,601,660,162,1034,264,957,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {704,65,642,136,264,138,846,911,912,467,723,278,89,347,799,32,162,99,292,229,102,744,680,682,249,250,314,125,829,702,0,0,0,0,0};
//    private static Integer[] initalEntry = new Integer[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // the empty arry, just the model
//    private static Integer[] initalEntry = new Integer[] {3020, 2064, 246, 1972, 3270, 3155, 4321, 3120, 2114, 4346, 2435, 156, 2898, 4200, 1658, 414, 653, 1557, 640, 83, 2917, 3385, 2962, 512, 3366, 3143, 191, 2500, 262, 3334, 0, 0, 0, 0, 0};
    private static Integer[] initalEntry = new Integer[] {294, 938, 800, 852, 310, 219, 340, 932, 163, 888, 866, 138, 257, 111, 767, 300, 486, 648, 738, 850, 88, 204, 189, 848, 504, 737, 21, 149, 910, 0, 0, 0, 0, 0 ,0
};
    private static Double[] initialEntryDouble = new Double[] {0.027108421, 0.035329913, 0.022550814, 0.036048063, 0.038425236, 0.027829572, 0.028342134, 0.036325072, 0.03952271, 0.019803411, 0.03874273, 0.03891447, 0.030401369, 0.028897969, 0.025979347, 0.041602214, 0.022529459, 0.039500099, 0.036597343, 0.025762893, 0.037160352, 0.023700869, 0.029949327, 0.04187845, 0.019310117, 0.021345107, 0.038406974, 0.020434079, 0.03724095, 0.030166578, 0.032531146, 0.021341947, 0.030973851, 0.028506894, 0.039149249, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
//    private static Double[] initialEntryDouble = new Double[] {};

    private static NsagaUtils utils = new NsagaUtils();
    final static Logger logger = (Logger) LoggerFactory.getLogger(Run_single_entry.class.getName());
    final static boolean USE_COMSOL = true; // control variable, where it can be switched off to give random answers,
//    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\2019-11-11--11-23_output.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"
    final static String GENE_BANK_FILE_TO_USE = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\src\\main\\resources\\activeProcessingCSV\\geneHistor__archive_2020-06-26--22-48.csv"; // "geneBank_10Layers_v1.2.csv" // "geneBank_10Layers_small_sample_v1.0.csv"

    static String optimizerType = "SAGA"; // control variable to choose which of the optimizers to use - SAGA or NSAGAD:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\activeProcessingCSV\geneHistory_renamed.csv
    static boolean resumePreviousRun = true; // control if using a previous run. If yes, then please do use the previous generationCounter below - use the GENE_BANK_FILE_TO_USE to control which file is loaded
    static int previousGenerationCounter = 0;
    static boolean runCollsionChecks = false;
    //    final static Object[][] SCATTERER_POSITION_INDEX_MAP = utils.loadCSV(System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv");
    static Position[] SCATTERER_POSITION_INDEX_MAP;
    final static String shapeOfScatterer = "Square";
    final static String shapeOfObject = "Square";
    static double radiusScatterer;
    static String comsolReferencesFolder = System.getProperty("user.dir")+"\\src\\main\\resources\\comsolPreferences\\";
    static boolean useGeneRandomisation = false;
    static boolean saveAllImages = true;
    static boolean stillInitialising = true;
    static long programStartTime;
    static ComsolModelInterface comsolModel;
    static double[][]emptyModel360Pressure;
    static double[][]centralObjectOnly360Pressure;
    static String startDate;

    public static void main(String[] args) {
        logger.info("Beginning single individual run");
        programStartTime = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd--H-mm"); // used in saving the data, once saving is made available.
        Date date = new Date();
        Level loggerLevel;
        if (USE_COMSOL) {
            loggerLevel = Level.DEBUG;
        } else {
            loggerLevel = Level.INFO;
        }

        // insert comsol model of choice here
        comsolModel = null;

        if (USE_COMSOL) {
            // initialise comsol model.
            ModelUtil.initStandalone(false); // method to make comsol work.

            File preferenceFile = new File(comsolReferencesFolder + "\\comsolReferences.txt");
            // if preferneces exists, load file
            if (preferenceFile.exists()) {
                ModelUtil.loadPreferences();
            } else {
                File preferencesDir = new File(comsolReferencesFolder);
                utils.setComsolPreferences();

//                try {
//                    FileWriter fw = new FileWriter(preferenceFile);
//                    fw.write("Saved preferences");
//                    fw.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
            comsolModel = new ComsolModelInterface3_0(USE_COMSOL, loggerLevel);
        } else {
            comsolModel = new EmptyModel();
        }
        utils.runRecoveryCleanUp();
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        radiusScatterer = Double.parseDouble(comsolModel.getDoubleParameter("radius_scatterer"));
        logger.info("Comsol Model initiated");

//        GenesAndfScoresBank genesLoaded = null;
//        GenesAndfScoresBank genesHistory = null;
        startDate = dateFormat.format(date);
        logControlVariables(startDate, comsolModel);

        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }

        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }

        try {
            comsolModel.runBlankModel();
            emptyModel360Pressure = comsolModel.getModels360Points();
        } catch (Exception e) {
            e.printStackTrace();
        }
        comsolModel.saveImagesOneOff("emptyModels\\blank_area", utils.getEmptyAreaRunIndex(), new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, emptyModel360Pressure);


        setAllScatterers(utils.getEmpyScattererArray());
        try {
            comsolModel.runModel(utils.getNoScattererRunIndex());
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }
        centralObjectOnly360Pressure = comsolModel.getModels360Points();
        comsolModel.saveImagesOneOff("emptyModels\\only_central_object", utils.getNoScattererRunIndex(), new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, centralObjectOnly360Pressure);
        double denominator = comsolModel.getResult() * Double.parseDouble(comsolModel.getDoubleParameter("fitness_for_squared_metrics"));
        Variables.setSanchisDenominator(denominator);
        comsolModel.setFitnessForSquareMetrics(denominator);
        try {
            comsolModel.runModel(-997);
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }
        Individual individual = new Individual(0,
                initialEntryDouble, -9999.0,
                0,
                null, 9999.0, Double.MAX_VALUE, Double.MAX_VALUE,
                Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, false, false,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE);


        try {
            setAllScatterers(initialEntryDouble);
            logger.info("Running collision checks now");
            boolean collisionOccured = utils.runCollisionCheckRandomIndividual(initialEntryDouble, radiusScatterer);
            if (runCollsionChecks && collisionOccured)
                logger.error("COLLISION OCCURRED while set to prevent collision checks");
            runComsolAndGetMetrics(0, individual);
            comsolModel.saveImagesDouble("savedImages_" + individual.getModelNumber() + "__startTime-", individual.getModelNumber(), individual.getGenes(), startDate, comsolModel.getModels360Points());
            GeneBank geneBank = new GeneBank();
            geneBank.addNewIndividual(individual);
            utils.appendGeneHistoryToFile(String.valueOf(get(Variables.getMassImageOutputFolder(startDate, "")+"\\individual_stats")), geneBank);
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }

//        setAllScatterers(utils.getEmpyScattererArray(), SCATTERER_POSITION_INDEX_MAP);
//        try {
//            comsolModel.runModel(utils.no_scatterer_run);
//        } catch (Exception e) {
//            logger.error("",e);
//            e.printStackTrace();
//        }
//        centralObjectOnly360Pressure = comsolModel.getModels360Points();
//        logger.debug("Setup central object only!");
//        logger.info("Running collision checks now");
//        boolean collisionOccured = utils.runCollisionCheckRandomIndividual(initalEntry, radiusScatterer, SCATTERER_POSITION_INDEX_MAP);
//        logger.info("Collision in given sample? " + Boolean.toString(collisionOccured));

        logger.debug("Completed individual run");
//
        System.exit(0);
    }
//
//
    private static void logControlVariables(String startDate, ComsolModelInterface comsolModel) {
        logger.info("Confirming before Run:");
        logger.info("    runCollisionChecks = "+ Variables.isRunCollsionChecks());
        logger.info("    optimiserType = "+ Variables.getOptimizerType());
        logger.info("    resumePreviousRun = "+ Variables.isResumePreviousRun());
        if (Variables.isResumePreviousRun())
            logger.info("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE);
        logger.info("    useGeneRandomisation = " + Variables.isUseGeneRandomisation());
        logger.info("    useGeneRandomisation = " + Variables.isUseGeneRandomisation());
        logger.info("    scattererType = " +  Variables.getShapeOfScatterer());
        FileWriter detailsFile = null;

        try{
            String newLine = System.getProperty("line.separator");
            detailsFile =  new FileWriter(String.valueOf(get(Variables.getMassImageOutputFolder(startDate, "")+ "\\details.txt")));
            detailsFile.write("Confirming before Run:"+newLine+newLine);
            detailsFile.write("    Version num = "+ Variables.getVersionUsed()+newLine);
            detailsFile.write("    runCollisionChecks = "+ Variables.isRunCollsionChecks()+newLine);
            detailsFile.write("    optimiserType = "+ Variables.getOptimizerType()+newLine);
            detailsFile.write("    radiusScatterer = "+ Variables.getRadiusScatterer()+newLine);
            detailsFile.write("    resumePreviousRun = "+ Variables.isResumePreviousRun()+newLine);
            if (Variables.isResumePreviousRun())
                detailsFile.write("    GENE_BANK_FILE_TO_USE = "+ GENE_BANK_FILE_TO_USE+newLine);
            detailsFile.write("    useGeneRandomisation = " + Variables.isUseGeneRandomisation()+newLine);
            detailsFile.write("    scattererType = " +  Variables.getShapeOfScatterer()+newLine);
            detailsFile.write("    central Type = " + Variables.getShapeOfObject()+newLine);
            detailsFile.write("    Allow 0 switching = " + Variables.isAllow0Switching() +newLine);
            detailsFile.write("    Optimiser = " + Variables.isAllow0Switching() +newLine);
            detailsFile.write("    Inital annealing step = " + (Variables.getInitialAnnealingStep() )+newLine);
            detailsFile.write("    Max scatts in a quadrant = " + Integer.toString(Variables.getMaximumScatterersInAQuadrant()) +newLine);
            detailsFile.write("    Used scatts in a quadrant = " + Integer.toString(Variables.getUsedScatterersInAQuadrant()) +newLine);
            detailsFile.write("    Max pop size = " + Integer.toString(Variables.maxPopMembers()) +newLine);
            detailsFile.write("    Max scat positions = " + "infinite" +newLine);
            detailsFile.write("    Total annealing steps allowed = " + Integer.toString(Variables.getMaximumTemperatureSteps()) +newLine);
            detailsFile.write("    Max num population generations= " + Integer.toString(Variables.maxPopMembers()) +newLine);
    //            detailsFile.write("    Eliete individuals= " + Integer.toString(utils.ELITE_INDIVIDUALS_FOR_NEXT_POPULATION) +newLine);
    //            detailsFile.write("    Equilibirum= " + Integer.toString(utils.EQUILIBRIUM_POSITION_INDEX) +newLine);
            detailsFile.write("    Mutation Prob= " + Double.toString(Variables.getInitialMutationProbability()) +newLine);
            detailsFile.write("    Inital temp= " + Double.toString(Variables.getInitialTemperature()) +newLine);
            detailsFile.write("    Max temp steps= " + Integer.toString(Variables.getMaximumTemperatureSteps()) +newLine);
    //            detailsFile.write("    Gen save counter= " + Integer.toString(V) +newLine);
            detailsFile.write("Radius central pipe= " + comsolModel.getDoubleParameter("radius_central_pipe")+ newLine+newLine);
            detailsFile.write("Radius scatterer= " + comsolModel.getDoubleParameter("radius_scatterer")+ newLine+newLine);
            detailsFile.write("Scat max distance= " + comsolModel.getDoubleParameter("scatterers_max_distance")+ newLine+newLine);
            detailsFile.close();
            int breakx = 0;
        } catch (IOException e) {
            logger.error("IO exception when writing details file", e);
        }
        finally {
            if (detailsFile!= null) {
                try {
                    detailsFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
//
    protected static void setAllScatterers(Double[] testedIndividual ) {
    switch (shapeOfScatterer) {
        case "Square":
            comsolModel.setAllSquareScatterersDouble(testedIndividual, null);
            break;
        case "Circle":
            comsolModel.setAllSquareScatterersDouble(testedIndividual, null);
            break;
        default:
            comsolModel.setAllSquareScatterersDouble(testedIndividual, null);
            break;

    }

}

    protected static double sum(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += (model360[0][i]);
        }
        return sum;
    }

    protected static double sumAbsolutes(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.abs(model360[0][i]);
        }
        return sum;
    }

    protected static double sumSquares(double[][] model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.pow((model360[0][i]), 2);
        }
        return sum;
    }

    /**
     * https://rem.jrc.ec.europa.eu/RemWeb/atmes2/20b.htm
     * @param model360
     * @return
     */
    protected static double normalisedMSE(double[][] model360) {
        double averageBase = sum(emptyModel360Pressure)/emptyModel360Pressure.length;
        double averageOfModel = sum(model360)/model360.length;
        double squareSumOfDifferencesOverMeans = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            // sum, [(P-m)**2 / (Paverage * MAverage)]
            squareSumOfDifferencesOverMeans += Math.pow(model360[0][i] - emptyModel360Pressure[0][i],2) / (averageBase * averageOfModel);
        }
        return (1/model360.length) * squareSumOfDifferencesOverMeans;
    }

    protected static double getMinOfModel360(double[] model360) {
        double min = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] < min) {
                min = model360[i];
            }
        }
        return min;
    }

    protected static double getMaxOfModel360(double[] model360) {
        double max = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] > max) {
                max = model360[i];
            }
        }
        return max;
    }

    protected static  double runComsolAndGetMetrics(int modelNumber, Individual individual) {
        double fscore = Double.MAX_VALUE;
        try {
            setAllScatterers(individual.getGenes());
            comsolModel.runModel(modelNumber);
            if (individual.getModelNumber() != modelNumber) {
                individual.modelNumber = modelNumber;
            }
            double[][] model360 = comsolModel.getModels360Points();
            double sanchisMetric = comsolModel.getResult();
            double mse = calculateError(comsolModel.getModels360Points());
            double rmse = Math.sqrt(mse);
            double sanchisMetric2 = 1 - sumSquares(model360) / sumSquares(centralObjectOnly360Pressure);
            double sanchisMetric3 = sumSquares(model360) / sumSquares(centralObjectOnly360Pressure); // sum of 360 points with cloak / sum of 360 points with the central object.
            double sanchisMetric4 = sumSquares(model360);
            double sanchisMetricVsPerfect = 1 - sumSquares(model360) / sumSquares(emptyModel360Pressure);
            double sanchezMetric1 = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(centralObjectOnly360Pressure)) ), -1);
            double sanchezMetricVsperfect = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(emptyModel360Pressure)) ), -1);
            double sanchezMetric2 = sumAbsolutes(model360);
            double normalisedMSE = normalisedMSE(model360);
            individual.setArray360Points(comsolModel.getModels360Points());
            individual.setMse(calculateError(comsolModel.getModels360Points()));
            individual.setSanchisMetric(sanchisMetric);
            individual.setSanchisMetricH(sanchisMetric3);
            individual.setSanchisMetricSimple(sanchisMetric4);
            individual.setSanchezMetric(sanchezMetric1);
            individual.setSanchezMetricSimple(sanchezMetric2);
            individual.setNormalisedMSE(normalisedMSE);
            individual.setSanchisPerfectCloak(sanchisMetricVsPerfect);
            individual.setSanchezPerfectCloak(sanchezMetricVsperfect);
            individual.setfScore(selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2));
            individual.setRmse(rmse);
            double[] model360SubArray = Arrays.copyOfRange(model360[0], 1, model360[0].length);
            double[][] model360SubArray2D = new double[1][360];
            model360SubArray2D[0] = model360SubArray;
            individual.setMinPressure(getMinOfModel360(model360SubArray));
            individual.setMaxPressure(getMaxOfModel360(model360SubArray));
            individual.setAveragePressure(sum(model360)/360);
            individual.setAbsoluteAveragePressure(sumAbsolutes(model360)/360);
//            System.out.println("    MSE metric       -> 0 (1) = " + mse);
//            System.out.println("Sanchis metric   -> 1 (2) = " + sanchisMetric2);
//            System.out.println("Sanchis metric   -> 0 (3) = " + sanchisMetric3);
//            System.out.println("Sanchis metric R -> 0 (3) = " + sanchisMetric);
//            System.out.println("Sanchis metric   -> 0 (4) = " + sanchisMetric4);
//            System.out.println("Sanchez metric   -> 1 (5) = " + sanchezMetric1);
//            System.out.println("Sanchez metric   -> 0 (6) = " + sanchezMetric2 + "\n\n");
            fscore = selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fscore;
    }

    protected  static double calculateError(double[][] model360) {
        int i;
        double meanSquareError = 0.0;
        double sumOfDifferences = 0.0;
        double sumOfDifferencesSquared = 0.0;//i = 1, as the first column of the table is set to the frequency, not needed. However it should be independant of it - 20k - 20k = 0, therefore should have no impact on error rate
        for (i=1; i<model360[0].length; i++) {
            sumOfDifferences += (model360[0][i] - emptyModel360Pressure[0][i]);
            sumOfDifferencesSquared += Math.pow(model360[0][i] - emptyModel360Pressure[0][i], 2);
        }
        meanSquareError = sumOfDifferencesSquared/model360.length;

        return meanSquareError;
    }

    public static  double selectErrorMetric(double mse, double sanchisMetric, double sanchisMetric2, double sanchisMetric3, double sanchisMetric4, double sanchezMetric1, double sanchezMetric2) {
        switch (Variables.getUsedErrorMetric().toLowerCase()) {
            case "mse":
                return mse;
            case "sanchis":
                return sanchisMetric;
            case "sanchisMetric2":
                return sanchisMetric2;
            case "sanchezMetric3":
                return sanchisMetric3;
            case "sanchezMetric4":
                return sanchisMetric4;
            case "sanchezMetric1":
                return sanchezMetric1;
            case "sanchezMetric2":
                return sanchezMetric2;
            default:
                return mse;
        }
    }

}
