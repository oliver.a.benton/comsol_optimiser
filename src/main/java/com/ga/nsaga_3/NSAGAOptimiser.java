package com.ga.nsaga_3;

import com.GlobalUtils;
import com.comsolModelInterface.ComsolModelInterface;
import com.ga.SimulatedAnnealingTechniques;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.Variables;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.*;

public abstract class NSAGAOptimiser implements optimiserInterface {

    ComsolModelInterface comsolModel;
    NsagaUtils utils;
    GeneBank previousPopulation;
    GeneBank nextPopulation;
    protected double currentEquilibirumScore = Double.MAX_VALUE;
    Logger logger = (Logger) LoggerFactory.getLogger(NSAGAOptimiser.class.getName());

    Position[] scattererToIndexMap;

    protected final String saveFilePath = Variables.getOutputFolder() +"\\NSAGA_out";
    protected String activeGeneLogFile = System.getProperty("user.dir") +"\\src\\main\\resources\\activeProcessingCSV\\geneHistory";
    protected CopyOption[] copyOptions = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
    };


    private SimulatedAnnealingTechniques simulatedAnnealingTechniques = new SimulatedAnnealingTechniques();
    private String shapeOfScatterer;
    private String shapeOfObject;
    private HashSet<List<Integer>> combinationHistory;
    private HashSet<List<Double>> rejectedHistory;
    private boolean scanRejectedHistory = true; // control variable for scanning the rejected history for duplicates.
    protected double[][] emptyModel360Pressure;
    protected double[][] centralObjectOnly360Pressure;
    protected DecimalFormat decimalFormat = new DecimalFormat("#.##");
    protected int numberModelsRun = 0;
    private long totalTimeForEachModel = 0;
    protected boolean useComsol;
    protected int numberOfCheckhistoryRegjections = 0;
//    private ListOfListsCheckers listOfListsChecker = new ListOfListsCheckers();
    protected String startDate;
    protected long totalTimeForEachHistoryCheck = 0;
    protected int lastReportedIndex = 0;
    protected long totalTimeForAnnealingSteps = 0;
    protected int timeSinceLastModel = 0;
    protected int historyRejections = 0;
    protected int mutationHistoryRejects = 0;
    protected int historyRejectionsFromRejectedList = 0;
    protected int totalHistoryChecks = 0;
    protected int acceptedModelIndex;
    protected int totalModelsEvaluatedByComsol = 0;
    protected int consideredAndRejected = 0;
    protected long timeStartedNsagaLoop;
    protected long totalTimeSpentSaving = 0;
    protected int collisionRejects = 0;
    protected int collisionTime = 0;
    protected long combinationGenerationTime = 0;
    protected int numberOfTimesSaved = 0;
    protected long populationTotalTime = 0;
    protected long timeSpentSavingImages = 0;
    protected int numberOfImagesSaved = 0;
    protected int combosGenerated = 0;
    protected int numCollisionChecks = 0;
    protected long timeSpentCopyingNextPopulationOver = 0;
    protected long garbageCollectionChecksTime = 0;
    protected  long timeSpentLogging = 0;
    protected int completedPopulationsCounter = 0;


    NSAGAOptimiser (ComsolModelInterface comsolModel, GeneBank genesLoaded, GeneBank geneHistory, boolean useComsol, Level loggerLevel, String shapeOfScatterer, String shapeOfObject, String startDate) throws Exception {
        this.startDate = startDate;
        this.comsolModel = comsolModel;
        this.previousPopulation = genesLoaded;
        this.utils = new NsagaUtils();
        this.logger.setLevel(loggerLevel);
        this.shapeOfScatterer = shapeOfScatterer;
        this.shapeOfObject = shapeOfObject;
        this.useComsol = useComsol;
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setGroupingSize(3);

        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }

        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        File directory = new File(activeGeneLogFile.substring(0, activeGeneLogFile.length()-12));
        if (!directory.exists()) {
            directory.mkdirs();
        }
        combinationHistory = new HashSet<>();
        rejectedHistory = new HashSet<>();

        try {
            comsolModel.runBlankModel();
            emptyModel360Pressure = comsolModel.getModels360Points();
        } catch (Exception e) {
            e.printStackTrace();
        }
        comsolModel.saveImagesOneOff("emptyModels\\blank_area", utils.getEmptyAreaRunIndex(), new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, emptyModel360Pressure);

        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }
        setAllScatterers(utils.getEmpyScattererArray());
        try {
            this.comsolModel.runModel(utils.getNoScattererRunIndex());
        } catch (Exception e) {
            logger.error("",e);
            e.printStackTrace();
        }
        centralObjectOnly360Pressure = comsolModel.getModels360Points();
        comsolModel.saveImagesOneOff("emptyModels\\only_central_object", utils.getNoScattererRunIndex(), new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, centralObjectOnly360Pressure);
        double denominator = comsolModel.getResult() * Double.parseDouble(comsolModel.getDoubleParameter("fitness_for_squared_metrics"));
        Variables.setSanchisDenominator(denominator);
        comsolModel.setFitnessForSquareMetrics(denominator);
        comsolModel.runModel(-997);

        nextPopulation = null;
        int currentModel = -previousPopulation.getSize();
        int i;
        switch (shapeOfObject) {
            case "Square":
                comsolModel.switchToSquareCentralObject();
                break;
            case "Circle":
                comsolModel.switchToCircleCentralObject();
                break;
            default:
                comsolModel.switchToSquareCentralObject();
                break;
        }

        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.switchToSquareScatterers();
                break;
            case "Circle":
                comsolModel.switchToCircularScatterers();
                break;
            default:
                comsolModel.switchToSquareScatterers();
                break;
        }
        for (i = 0; i < previousPopulation.getSize(); i++) {
            setAllScatterers(previousPopulation.getInvididual(i).getGenes());
            if (i % 10 == 0) {
                logger.debug("Initial run set, checking member {}", i);
            } else {
                logger.trace("Initial run set, checking member {}", i);
            }
            // evaluate individual
            double tempMetric = previousPopulation.getInvididual(i).getfScore();
            runComsolAndGetMetrics(currentModel, previousPopulation.getInvididual(i));
            double newMetric = previousPopulation.getInvididual(i).getfScore();
            double diff = newMetric - tempMetric;
            GeneBank tempGeneBank = new GeneBank();
            tempGeneBank.addNewIndividual(previousPopulation.getInvididual(i));
            saveAnIndividual(tempGeneBank);
            if (!Variables.isSaveNoImages())
                comsolModel.saveImagesDouble("savedImages_" + previousPopulation.getInvididual(i).getModelNumber() + "__startTime-", previousPopulation.getInvididual(i).getModelNumber(), previousPopulation.getInvididual(i).getGenes(), startDate, comsolModel.getModels360Points());
            currentModel++;
        }
        try {
            utils.appendGeneHistoryToFile(Variables.getActiveGeneHistoryCSVLocation(), previousPopulation);
        } catch (Exception e) {
            logger.error("Error setting up gene History file, ", e);
            System.exit(-1);
        }
        int breakx = 0;
    }


    /**
     * Method to randomly mutate the offered individual
     * @param originalIndividual
     * @return
     */
    protected Double[] performMutation(Double[] originalIndividual){

        Double[] mutatedIndividual = utils.copyDoubleArray(originalIndividual);
        Random rand = new Random();
        int mutationLocation = rand.nextInt(Variables.getUsedScatterersInAQuadrant()); //select location from amongst scatterers
        mutationLocation = mutationLocation * 2; // There are twice as many coordiantes as scatterers.
        // setup mutation to move it between 1/4 a scatterer width and 1 scatterer width
        Double min = Variables.getRadiusScatterer()/2;
        Double max = Variables.getRadiusScatterer()*2;
        double difference1 = (Math.random()/ 1.0) * (max-min) + min;
        double difference2 = (Math.random()/ 1.0) * (max-min) + min;
        if (rand.nextDouble() > 0.5)  // 50% chance of negative
            difference1 = -1 * difference1;
        if (rand.nextDouble() > 0.5)  // 50% chance of negative
            difference2 = -1 * difference2;

        if (mutationLocation % 2 != 0)
            mutationLocation = mutationLocation -1;

        logger.trace("Individual is being mutated - gene {} being changed from {} to {}", mutationLocation, originalIndividual[mutationLocation], originalIndividual[mutationLocation] + difference1);
        logger.trace("Individual is being mutated - gene {} being changed from {} to {}", mutationLocation+1, originalIndividual[mutationLocation+1], originalIndividual[mutationLocation+1] + difference2);
        double newX = mutatedIndividual[mutationLocation] + difference1;
        double newY = mutatedIndividual[mutationLocation+1] + difference2;

        double minDistance = Double.parseDouble(comsolModel.getDoubleParameter("radius_central_pipe"));
        double maxDistance = Double.parseDouble(comsolModel.getDoubleParameter("scatterers_max_distance"));

        // if x is less than min
//        if (newX < minDistance && newY < minDistance)
//            newX = 0;
//        // if x is greater than max
//        if (newX > maxDistance )
//            newX = maxDistance - Variables.getRadiusScatterer();
//        // if y is less than min
//        if (newY < minDistance )
//            newY = 0;
//        // if y is greater than may
//        if (newY > maxDistance )
//            newY = maxDistance - Variables.getRadiusScatterer();

        double radiusOfNewCoord = Math.sqrt(newX*newX + newY*newY );
        // if combined coordiantes are outside of area, reduce larger coordinate by half a scatterer radius until
        while (radiusOfNewCoord > maxDistance) {
            double[] ab = reduceLarger(newX, newY, Variables.getRadiusScatterer()/2);
            newX = ab[0];
            newY = ab[1];
            radiusOfNewCoord = Math.sqrt(newX*newX + newY*newY );
        }

        // if combined coordiantes are too low
        if (radiusOfNewCoord < minDistance) {
            if (mutatedIndividual[mutationLocation] == 0 && mutatedIndividual[mutationLocation+1]==0) {
                // if the coordiantes are mutating to larger, increase coordes until outside the minimum
                while (radiusOfNewCoord < minDistance) {
                    double[] ab = increaseSmaller(newX, newY, Variables.getRadiusScatterer()/2);
                    newX = ab[0];
                    newY = ab[1];
                    radiusOfNewCoord = Math.sqrt(newX*newX + newY*newY );
                }
            } else if (Variables.isAllow0Switching()){
                // else, coords have mutated into minimum area, so set to 0 and 0 switching it allowed
                newX = 0;
                newY = 0;
            } else {
                // else coordinates have moved outside allowed zones and are not allowed to go to 0 and back.
                while (radiusOfNewCoord < minDistance+ (2*Variables.getRadiusScatterer())) {
                    double[] ab = increaseSmaller(newX, newY, Variables.getRadiusScatterer()/2);
                    newX = ab[0];
                    newY = ab[1];
                    radiusOfNewCoord = Math.sqrt(newX*newX + newY*newY );
                }
            }
        }
//        while (radiusOfNewCoord < minDistance) {
//            double[] ab = increaseSmaller(newX, newY, Variables.getRadiusScatterer()/2);
//            newX = ab[0];
//            newY = ab[1];
//            radiusOfNewCoord = Math.sqrt(newX*newX + newY*newY );
//        }

        mutatedIndividual[mutationLocation] = newX;
        mutatedIndividual[mutationLocation+1] = newY;
        return mutatedIndividual;
    }

    private double[] reduceLarger(double a, double b, double amount) {
        if (a > b)
            a -= amount;
        else
            b -= amount;
        return new double[] {a, b};
    }

    private double[] increaseSmaller(double a, double b, double amount) {
        if (a < b)
            a += amount;
        else
            b += amount;
        return new double[] {a, b};
    }

    protected double temperatureFunction(double temperature, int currentAnnealingStep) {
//        return simulatedAnnealingTechniques.decreaseByControlAmount(temperature, currentAnnealingStep, MAXIMUM_TEMPERATURE_STEPS, utils.INITIAL_TEMPERATURE);
//        return simulatedAnnealingTechniques.decreaseByLogs(NsagaUtils.INITIAL_TEMPERATURE, currentAnnealingStep);
//        return simulatedAnnealingTechniques.initialTempToStepOverMax(currentAnnealingStep, com.ga.nsaga.NsagaUtils.MAXIMUM_TEMPERATURE_STEPS, com.ga.nsaga.NsagaUtils.INITIAL_TEMPERATURE);
        double newTemp = simulatedAnnealingTechniques.decreaseByPurcellFactor(Variables.getInitialTemperature(), currentAnnealingStep, Variables.getMaximumTemperatureSteps());
        // prevent true 0 values for dividing by 0 errors
        if (newTemp == 0.0)
            newTemp = 0.00000000001;
        return newTemp;

    }


    /**
     * Method to produce a new offspring via random crossover points
     * @param inputPopulation the population to select parents from
     * @param parent1Index indexGiven of parent 1 to crossover
     * @param parent2Index indexGiven of parent 2 to crossover
     * @return the chromosome of the new child
     */
    protected Double[][] produceMultipleOffspring(GeneBank inputPopulation, int parent1Index, int parent2Index) {
        Random rand = new Random();
        // to ensure there is at least 1 element from both parents, the crossover indexGiven has to be between 1 and n-2 inclusive.
        // croosover nextEmptyIndex has to be between 1 and the penultimate - aka in array[0: n-1], mutation in range has to be [1:n-2]
        // rand(n-3) +1 == [0:n-3] + 1 = [1:n-2]   --- ensures there will always be at least one element from both parents
        int crossoverIndex = rand.nextInt(Variables.getUsedScatterersInAQuadrant() - 3)+1;
        logger.trace("Selected gene {} to mutate", crossoverIndex);
        Double[] parent1Data = inputPopulation.getInvididual(parent1Index).getGenes();
        Double[] parent2Data = inputPopulation.getInvididual(parent2Index).getGenes();;
        Double[] offspringIndividual = new Double[Variables.getUsedScatterersInAQuadrant()];
        Double[] offspringIndividual2 = new Double[Variables.getUsedScatterersInAQuadrant()];
        int geneIndex = 0;
        // copy parent 1 and parent 2's data
        while (geneIndex < Variables.getUsedScatterersInAQuadrant()) {
            // if the crossover nextEmptyIndex is for parent 1
            if (geneIndex <= crossoverIndex) {
                offspringIndividual[geneIndex] = parent1Data[geneIndex];
                offspringIndividual2[geneIndex] = parent2Data[geneIndex];
                // else for parent 2
            } else {
                offspringIndividual[geneIndex] = parent2Data[geneIndex];
                offspringIndividual2[geneIndex] = parent1Data[geneIndex];
            }
            geneIndex ++;
        }
        offspringIndividual = utils.setChromosomeEndToZero(offspringIndividual);
        offspringIndividual2 = utils.setChromosomeEndToZero(offspringIndividual2);
        return new Double[][]{offspringIndividual, offspringIndividual2};
    }



    /**
     *
     * @param individual
     * @param loopsEntered
     * @param temperature
     * @return -3 if rejected due to collision, 0 if considered but SA rejected, 1 if successfully added
     */
    protected int checkIndividual(Double[] individual, int loopsEntered, double temperature) {
        long startTimeForNextCounter = 0;
        startTimeForNextCounter = System.currentTimeMillis();
        if (Variables.isRunCollsionChecks() && utils.runCollisionCheckRandomIndividual(individual, Variables.getRadiusScatterer())) {
            // collision has occured and not allowed, thus reject solution, without any increment.
            logger.trace("Collision checks have failed, there are collisions, rejecting");
            timeSinceLastModel++;
            collisionRejects++;
            numCollisionChecks++;
            collisionTime += System.currentTimeMillis() - startTimeForNextCounter;
            return -3;
        }
        collisionTime += System.currentTimeMillis() - startTimeForNextCounter;

        startTimeForNextCounter = System.currentTimeMillis();
        // offspring has passed anti-collision code
        Individual newIndividual = simulatedAnnealingGate(individual, temperature, acceptedModelIndex, numberModelsRun);
        totalModelsEvaluatedByComsol++;
        // if offspring is considered good by SA, then add to next population, else ignore it
        if (newIndividual == null) {
            logger.trace("Generated individual was rejected by the SA gate");
            consideredAndRejected ++;
            return 0;
        } else {
            addIndividual(newIndividual);
            timeSinceLastModel = 0;
            //outputCoordinates(newIndividual); // method to check for coordinates output
            acceptedModelIndex++;
            return 1;
        }
    }

    protected Individual runIndividual(Double[] newIndividualGenes) {
        Individual newIndividual = new Individual(acceptedModelIndex, newIndividualGenes,
                -9999.0,
                numberModelsRun,
                null, 9999.0, Double.MAX_VALUE, Double.MAX_VALUE,
                Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, false, false,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE);

        // set up simulation
//        getAllScatterers();
        setAllScatterers(newIndividualGenes);
        long time = System.currentTimeMillis();
        runComsolAndGetMetrics(numberModelsRun, newIndividual);
        GeneBank tempGeneBank = new GeneBank();
        tempGeneBank.addNewIndividual(newIndividual);
        saveAnIndividual(tempGeneBank);
        time = System.currentTimeMillis() - time;
        numberModelsRun++;
        totalTimeForEachModel += time;
        long startTimeImages = System.currentTimeMillis();
        if (Variables.isSaveAllImages()) {
            comsolModel.saveImagesDouble("savedImages_" + newIndividual.getModelNumber() + "__startTime-", newIndividual.getModelNumber(), newIndividual.getGenes(), startDate, comsolModel.getModels360Points());
            numberOfImagesSaved++;
        }
        timeSpentSavingImages+= System.currentTimeMillis() - startTimeImages;
        return newIndividual;
    }

    /**
     *
     * @param testedIndividual
     * @param temperature
     * @param acceptedModelIndex
     * @param numberModelsRun
     * @return null if the individual has been rejected by the system, else it returns the individual with updated score values
     */
    protected Individual simulatedAnnealingGate(Double[] testedIndividual, double temperature, int acceptedModelIndex, int numberModelsRun) {
        Double[] newIndividualGenes = utils.copyDoubleArray(testedIndividual);
        newIndividualGenes = utils.setChromosomeEndToZero(newIndividualGenes);
        Individual newIndividual = runIndividual(newIndividualGenes);
        // if the fscore is less than the current equilibrium, it is a better cloak, so accept
        double fscore = newIndividual.getfScore();
        GeneBank tempBank = new GeneBank();
        tempBank.addNewIndividual(newIndividual);
        saveAnIndividual(tempBank);

        if (fscore < currentEquilibirumScore) {
            logger.trace("New individual is better, score={}, equilibrium={} - temp={}", fscore, currentEquilibirumScore, temperature, Math.exp((currentEquilibirumScore - fscore) / temperature));
            return newIndividual;
        } else { // else fscore is equal to or worse - consider acceptance or rejection.

            Random random = new Random();
            double randValue = random.nextDouble();


            // if random value is less than the exponential of the (current "best" - current/temp), then accept it
            if (randValue < Math.exp((currentEquilibirumScore - fscore) / temperature)) {
                if (numberModelsRun %100 ==0)
                    logger.debug("Accepted the new individual, score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                else
                    logger.trace("Accepted the new individual, score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                return newIndividual;
            } else {
                if (numberModelsRun %100 ==0)
                    logger.debug("Rejected the new individual score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                else
                    logger.trace("Rejected the new individual score={}, equilibrium={} - temp={}, rand={}, temp function to get under ={}", fscore, currentEquilibirumScore, temperature, randValue, Math.exp((currentEquilibirumScore - fscore) / temperature));
                List<Double> sortedIndividual = sortGenome(newIndividualGenes);
                if (rejectedHistory.size() == 0)
                    rejectedHistory.add(sortedIndividual);
                else {
//                    int location = listOfListsChecker.binarySearchForLocationToAdd(rejectedHistory, sortedIndividual, 0, rejectedHistory.size() - 1);
                    rejectedHistory.add(sortedIndividual);
                }

//                rejectedHistory.add(sortedIndividual);
//                sortHistory(rejectedHistory);
                return null;
            }
        }
        // save every image if needed, but only save the accepted models

    }

    protected void addIndividual(Individual individual) {
        // if  there are no duplicates found throughtout the history, then accept
        nextPopulation.addNewIndividual(individual);
    }


    /**
     * Used as a one off run for a comsol model individual. This allows it to bypass the Simulated annealing gate.
     * @param modelNumber
     * @param individual
     */
    protected double runComsolAndGetMetrics(int modelNumber, Individual individual) {
        double fscore = Double.MAX_VALUE;
        try {
            setAllScatterers(individual.getGenes());
            comsolModel.runModel(modelNumber);
            if (individual.getModelNumber() != modelNumber) {
                individual.modelNumber = modelNumber;
            }
            double[][] model360 = comsolModel.getModels360Points();
            double sanchisMetric = comsolModel.getResult();
            double mse = calculateError(comsolModel.getModels360Points());
            double rmse = Math.sqrt(mse);
            double sanchisMetric2 = 1 - sumSquares(model360) / sumSquares(centralObjectOnly360Pressure);
            double sanchisMetric3 = sumSquares(model360) / sumSquares(centralObjectOnly360Pressure); // sum of 360 points with cloak / sum of 360 points with the central object.
            double sanchisMetric4 = sumSquares(model360);
            double sanchisMetricVsPerfect = 1 - sumSquares(model360) / sumSquares(emptyModel360Pressure);
            double sanchezMetric1 = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(centralObjectOnly360Pressure)) ), -1);
            double sanchezMetricVsperfect = Math.pow((1+ (sumAbsolutes(model360)/sumAbsolutes(emptyModel360Pressure)) ), -1);
            double sanchezMetric2 = sumAbsolutes(model360);
            double normalisedMSE = normalisedMSE(model360);
            double l2Norm = calculateL2Norm(model360[0]);
            individual.setArray360Points(comsolModel.getModels360Points());
            individual.setMse(calculateError(comsolModel.getModels360Points()));
            individual.setSanchisMetric(sanchisMetric);
            individual.setSanchisMetricH(sanchisMetric3);
            individual.setSanchisMetricSimple(sanchisMetric4);
            individual.setSanchezMetric(sanchezMetric1);
            individual.setSanchezMetricSimple(sanchezMetric2);
            individual.setNormalisedMSE(normalisedMSE);
            individual.setSanchisPerfectCloak(sanchisMetricVsPerfect);
            individual.setSanchezPerfectCloak(sanchezMetricVsperfect);
            individual.setfScore(selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm));
            individual.setRmse(rmse);
            double[] model360SubArray = Arrays.copyOfRange(model360[0], 1, model360[0].length);
            double[][] model360SubArray2D = new double[1][360];
            model360SubArray2D[0] = model360SubArray;
            individual.setMinPressure(getMinOfModel360(model360SubArray));
            individual.setMaxPressure(getMaxOfModel360(model360SubArray));
            individual.setAveragePressure(sum(model360)/360);
            individual.setAbsoluteAveragePressure(sumAbsolutes(model360)/360);
            individual.setL2Norm(l2Norm);
//            System.out.println("    MSE metric       -> 0 (1) = " + mse);
//            System.out.println("Sanchis metric   -> 1 (2) = " + sanchisMetric2);
//            System.out.println("Sanchis metric   -> 0 (3) = " + sanchisMetric3);
//            System.out.println("Sanchis metric R -> 0 (3) = " + sanchisMetric);
//            System.out.println("Sanchis metric   -> 0 (4) = " + sanchisMetric4);
//            System.out.println("Sanchez metric   -> 1 (5) = " + sanchezMetric1);
//            System.out.println("Sanchez metric   -> 0 (6) = " + sanchezMetric2 + "\n\n");
            fscore = selectErrorMetric(mse, sanchisMetric, sanchisMetric2, sanchisMetric3, sanchisMetric4, sanchezMetric1, sanchezMetric2, l2Norm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fscore;
    }

    /**
     * Method to set all the scatterer coordinates, regardless of which kind of scatterer it is.
     * @param testedIndividual
     */
    protected void setAllScatterers(Double[] testedIndividual ) {
        switch (shapeOfScatterer) {
            case "Square":
                comsolModel.setAllSquareScatterersDouble(testedIndividual, null);
                break;
            case "Circle":
                comsolModel.setAllCircularScatterersDouble(testedIndividual, null);
                break;
            default:
                comsolModel.setAllSquareScatterersDouble(testedIndividual, null);
                break;

        }

    }

    protected double calculateError(double[][] model360) {
        int i;
        double meanSquareError = 0.0;
        double sumOfDifferences = 0.0;
        double sumOfDifferencesSquared = 0.0;//i = 1, as the first column of the table is set to the frequency, not needed. However it should be independant of it - 20k - 20k = 0, therefore should have no impact on error rate
        for (i=1; i<model360[0].length; i++) {
            sumOfDifferences += (model360[0][i] - centralObjectOnly360Pressure[0][i]);
            sumOfDifferencesSquared += Math.pow(model360[0][i] - centralObjectOnly360Pressure[0][i], 2);
        }
        meanSquareError = sumOfDifferencesSquared/model360.length;

        return meanSquareError;
    }

    protected double sum(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += (model360[0][i]);
        }
        return sum;
    }

    protected double sumAbsolutes(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.abs(model360[0][i]);
        }
        return sum;
    }

    protected double sumSquares(double[][]model360) {
        double sum = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            sum += Math.pow((model360[0][i]), 2);
        }
        return sum;
    }

    /**
     * https://rem.jrc.ec.europa.eu/RemWeb/atmes2/20b.htm
     * @param model360
     * @return
     */
    protected double normalisedMSE(double[][]model360) {
        double averageBase = sum(centralObjectOnly360Pressure)/centralObjectOnly360Pressure.length;
        double averageOfModel = sum(model360)/model360.length;
        double squareSumOfDifferencesOverMeans = 0;
        int i;
        for (i=1; i<model360[0].length; i++) {
            // sum, [(P-m)**2 / (Paverage * MAverage)]
            squareSumOfDifferencesOverMeans += Math.pow(model360[0][i] - centralObjectOnly360Pressure[0][i],2) / (averageBase * averageOfModel);
        }
        return (1/model360.length) * squareSumOfDifferencesOverMeans;
    }

    public double selectErrorMetric(double mse, double sanchisMetric, double sanchisMetric2, double sanchisMetric3, double sanchisMetric4, double sanchezMetric1, double sanchezMetric2, double l2Norm) {
        switch (Variables.getUsedErrorMetric().toLowerCase()) {
            case "mse":
                return mse;
            case "sanchis":
                return sanchisMetric;
            case "sanchisMetric2":
                return sanchisMetric2;
            case "sanchezMetric3":
                return sanchisMetric3;
            case "sanchezMetric4":
                return sanchisMetric4;
            case "sanchezMetric1":
                return sanchezMetric1;
            case "sanchezMetric2":
                return sanchezMetric2;
            case "l2Norm":
                return l2Norm;
            default:
                return mse;
        }
    }

    protected double getMinOfModel360(double[] model360) {
        double min = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] < min) {
                min = model360[i];
            }
        }
        return min;
    }

    protected double getMaxOfModel360(double[] model360) {
        double max = model360[0];
        for (int i = 1; i< model360.length; i++) {
            if (model360[i] > max) {
                max = model360[i];
            }
        }
        return max;
    }

    protected List<Double> sortGenome(Double[] inputGenome) {
        inputGenome = utils.setChromosomeEndToZero(inputGenome);
        List<Double> sortedIndividual = new ArrayList<>(inputGenome.length);
        // add each gene to sorted
        for (Double doublei : inputGenome) {
            sortedIndividual.add(doublei);
        }
        //sort
        Collections.sort(sortedIndividual);
        return sortedIndividual;
    }

    private int savedNumberOfTimes = 0;

    protected boolean saveAnIndividual(GeneBank population) {
        boolean savedFile = false;
        try {
            savedFile = utils.appendGeneHistoryToFile(Variables.getCompleteHistoryFile().substring(0, Variables.getCompleteHistoryFile().length()-4), population);
        } catch (Exception e) {
            e.printStackTrace();
        }
        utils.runRecoveryCleanUp();
        if (!savedFile) {
            logger.error("Problem while appending the data");
        }
        savedNumberOfTimes++;
        return savedFile;
    }

    private double calculateL2Norm(double[] model360) {
        // F = ||W-U||/||V-U|| where U = empty region vector, V = object field vector, W = cloak pressure vector
        // U = emptyModel360Pressure, V = Central object only 360 pressure, W = input model 360 vector
        double[] numeratorArray = subtractArrays(model360, emptyModel360Pressure[0]);
        double[] denominatorArray = subtractArrays(centralObjectOnly360Pressure[0], emptyModel360Pressure[0]);

        double numerator = abosoluteValueDoubleArray(numeratorArray);
        double denominator = abosoluteValueDoubleArray(denominatorArray);

        return numerator/denominator;
    }

    private double[] subtractArrays(double[] array1, double[] array2) {
        double[] tempArray  = new double[array1.length];
        for (int i=0; i < array1.length; i++) {
            tempArray[i] = array1[i]-array2[i];
        }
        return  tempArray;
    }

    private double abosoluteValueDoubleArray(double[] array) {
        double sum = 0;
        for (int i=0; i < array.length; i++) {
            sum += array[i] * array[i];
        }
        return Math.sqrt(sum);
    }


}
