package com.ga.nsaga_3;

import com.Variables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GeneBank {

    Comparator<Individual> indexComparator = new Comparator<Individual>() {
        @Override
        public int compare(Individual o1, Individual o2) {
            Integer a = o1.getIndexGiven();
            Integer b = o2.getIndexGiven();
            return a.compareTo(b);
        }
    };

    Comparator<Individual> scoreDataComparator = new Comparator<Individual>() {
        @Override
        public int compare(Individual o1, Individual o2) {
            Double a = o1.getfScore();
            Double b = o2.getfScore();
            return a.compareTo(b);
        }
    };
    
    int nextEmptyIndex = 0;
    ArrayList<Individual> geneScoreBank;

    /**
     * Empty constructor for a brand new set of scores.
     */
    public GeneBank() {
        this.nextEmptyIndex = 0;
        this.geneScoreBank = new ArrayList<>();
    }

    /**
     * Loading in an existing set of scores.
     * @param geneScoreBank
     */
    public GeneBank(ArrayList<Individual> geneScoreBank, boolean sortByIndex) {
        this.geneScoreBank = new ArrayList<>(70);
        if (sortByIndex) {

            geneScoreBank.sort(indexComparator);
        }
        this.nextEmptyIndex = geneScoreBank.get(geneScoreBank.size()-1).getIndexGiven();
        this.geneScoreBank = geneScoreBank;
    }

    public void addNewData(Double[] genes, double fscore, int modelNumber, double[][] array360Points, double mse,
                           double sanchisMetric, double sanchisMetricH, double sanchisMetricSimple, double sanchezMetric,
                           double sanchisPerfectCloak, double sanchezPerfectCloak, double sanchezMetricSimple, double normalisedMSE,
                           double rmse, double minPressure, double maxPressure, double averagePressure, double absoluteAveragePressure, double l2Norm) {
        geneScoreBank.add(new Individual(nextEmptyIndex, genes, fscore, modelNumber, array360Points, mse,
                sanchisMetric, sanchisMetricH, sanchisMetricSimple, sanchezMetric, sanchezMetricSimple,
                normalisedMSE,  sanchisPerfectCloak, sanchezPerfectCloak,false, false, rmse, minPressure, maxPressure, averagePressure, absoluteAveragePressure, l2Norm));
        nextEmptyIndex++;
    }

    /**
     * Method to copy over an existing individual. Acts as a semi-shallow copy, as the Individual instance is new,
     * and the indexGiven value, but all other values share the same pointer.
     * @param individual
     */
    public void addNewIndividual(Individual individual) {
        geneScoreBank.add(new Individual(nextEmptyIndex, individual.getGenes(), individual.getfScore(),
                individual.getModelNumber(), individual.getArray360Points(), individual.getMse(),
                individual.getSanchisMetric(), individual.getSanchisMetricH(), individual.getSanchisMetricSimple(),
                individual.getSanchezMetric(), individual.getSanchezMetricSimple(), individual.getNormalisedMSE(),
                individual.getSanchisPerfectCloak(), individual.getSanchezPerfectCloak(), individual.isRecoreded(), individual.isImageSaved(),
                individual.getRmse(), individual.getMinPressure(), individual.getMaxPressure(), individual.getAveragePressure(), individual.getAbsoluteAveragePressure(),
                individual.getL2Norm()
        ));
        nextEmptyIndex++;
    }

    /**
     * Function to tell if the gene bank has more items than it should
     * @return true if the gene bank contains more than the NsagaUtils.MAX_POPULATION_SIZE
     */
    public boolean isGeneBankFull() {
        return geneScoreBank.size() >= Variables.maxPopMembers();
    }

    /**
     * Method to return the complete set of scores, in indexGiven order.
     * @return
     */
    public double[] getScores() {
        double[] scores = new double[geneScoreBank.size()];
        sortByIndex();
        int i = 0;
        for (Individual geneAndfScoreData: geneScoreBank) {
            scores[i] = geneAndfScoreData.getfScore();
            i++;
        }
        return scores;
    }

    public int getSize() {
        return geneScoreBank.size();
    }

    public Individual getInvididual(int indexWanted) {
        return geneScoreBank.get(indexWanted);
    }

    /**
     * Method to sort the entire gene bank by the order it was added in. Sorts by ascending.
     */
    public void sortByIndex() {
        Collections.sort(geneScoreBank, indexComparator);
    }

    /**
     * Method to sort the entire gene bank by the fscore it has stored. Sorts by Ascending.
     */
    public void sortByfScore() {
        Collections.sort(geneScoreBank, scoreDataComparator);
    }

    public void removeIndividual(int i) {
        geneScoreBank.remove(i);
    }

    public Individual getBestIndividual(){
        this.sortByfScore();
        Individual toReturn = this.getInvididual(0);
        this.sortByIndex();
        return toReturn;
    }

    public Individual getWorstIndividual(){
        this.sortByfScore();
        Individual toReturn = this.getInvididual(this.geneScoreBank.size()-1);
        this.sortByIndex();
        return toReturn;
    }
}
