package com.ga.nsaga_3;

import com.ga.nsaga.GenesAndfScoresBank;

/**
 * Optimiser Interface, made to ensure that all optimisers will have the same common method to call, allowing the control
 * class RunNSAGA to switch out whichever optimiser is needed, only having to change which concrete class is built.
 */
public interface optimiserInterface {

    public GeneBank runOptimizer(boolean resumePreviousRun, int previousGenerationCount) throws Exception;

}
