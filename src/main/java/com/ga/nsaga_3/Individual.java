package com.ga.nsaga_3;

import java.util.ArrayList;

public class Individual {

    int indexGiven;
    Double[] genes; // going to be number of scatterers * 2 larger
    double fScore;
    int modelNumber;
    double[][] array360Points;
    Double[] metricsList;
    double mse;
    double sanchisMetric;
    double sanchisMetricH;
    double sanchisMetricSimple;
    double sanchezMetric;
    double sanchezMetricSimple;
    private double normalisedMSE;
    double sanchisPerfectCloak;
    double sanchezPerfectCloak;
    boolean recoreded = false;
    boolean imageSaved = false;
    double rmse;
    double minPressure;
    double maxPressure;
    double averagePressure;
    double absoluteAveragePressure;
    double l2Norm;


    public Individual(int indexGiven, Double[] genes, double fScore, int modelNumber, double[][] array360Points,
                      double mse, double sanchisMetric, double sanchisMetricH, double sanchisMetricSimple,
                      double sanchezMetric, double sanchezMetricSimple, double normalisedMSE,
                      double sanchisPerfectCloak, double sanchezPerfectCloak, boolean recoreded, boolean imageSaved,
                      double rmse, double minPressure, double maxPressure, double averagePressure, double absoluteAveragePressure, double l2Norm) {
        this.indexGiven = indexGiven;
        this.genes = genes;
        this.fScore = fScore;
        this.modelNumber = modelNumber;
        this.array360Points = array360Points;
        ArrayList<Double> metricsList = new ArrayList<>();
        metricsList.add(mse);
        metricsList.add(sanchisMetric);
        metricsList.add(sanchisMetricH);
        metricsList.add(sanchisMetricSimple);
        metricsList.add(sanchezMetric);
        metricsList.add(sanchezMetricSimple);
        metricsList.add(normalisedMSE);
        metricsList.add(sanchisPerfectCloak);
        metricsList.add(sanchezPerfectCloak);
        metricsList.add(rmse);
        metricsList.add(l2Norm);
        metricsList.add(minPressure);
        metricsList.add(maxPressure);
        metricsList.add(averagePressure);
        metricsList.add(absoluteAveragePressure);
        this.mse = mse;
        this.sanchisMetric = sanchisMetric;
        this.sanchisMetricH = sanchisMetricH;
        this.sanchisMetricSimple = sanchisMetricSimple;
        this.sanchezMetric = sanchezMetric;
        this.sanchezMetricSimple = sanchezMetricSimple;
        this.normalisedMSE = normalisedMSE;
        this.sanchisPerfectCloak = sanchisPerfectCloak;
        this.sanchezPerfectCloak = sanchezPerfectCloak;
        this.recoreded = recoreded;
        this.imageSaved = imageSaved;
        this.rmse = rmse;
        this.minPressure = minPressure;
        this.maxPressure = maxPressure;
        this.averagePressure = averagePressure;
        this.absoluteAveragePressure = absoluteAveragePressure;
        this.l2Norm = l2Norm;
        this.metricsList = metricsList.toArray(new Double[0]);
        int breakx = 0;
    }

//    public IndividualsData(int indexGiven, Integer[] genes, double fScore, int modelNumber, double[][] array360Points, double mse) {
//        this.indexGiven = indexGiven;
//        this.genes = genes;
//        this.fScore = fScore;
//        this.modelNumber = modelNumber;
//        this.array360Points = array360Points;
//        this.mse = mse;
//    }

        public int getIndexGiven() {
            return indexGiven;
        }

        public Double[] getGenes() {
            return genes;
        }

        public double getfScore() {
            return fScore;
        }

        public void setfScore(double fScore) {
            this.fScore = fScore;
        }

        public void setIndexGiven(int indexGiven) {
            this.indexGiven = indexGiven;
        }

        public int getModelNumber() {
            return modelNumber;
        }

        public double[][] getArray360Points() {
            return array360Points;
        }

        public double getMse() {
            return mse;
        }

        public void setArray360Points(double[][] array360Points) {
            this.array360Points = array360Points;
        }

        public void setMse(double mse) {
            this.mse = mse;
        }

        public double getSanchisMetric() {
            return sanchisMetric;
        }

        public void setSanchisMetric(double sanchisMetric) {
            this.sanchisMetric = sanchisMetric;
        }

        public double getSanchisMetricH() {
            return sanchisMetricH;
        }

        public void setSanchisMetricH(double sanchisMetricH) {
            this.sanchisMetricH = sanchisMetricH;
        }

        public double getSanchisMetricSimple() {
            return sanchisMetricSimple;
        }

        public void setSanchisMetricSimple(double sanchisMetricSimple) {
            this.sanchisMetricSimple = sanchisMetricSimple;
        }

        public double getSanchezMetric() {
            return sanchezMetric;
        }

        public void setSanchezMetric(double sanchezMetric) {
            this.sanchezMetric = sanchezMetric;
        }

        public double getSanchezMetricSimple() {
            return sanchezMetricSimple;
        }

        public void setSanchezMetricSimple(double sanchezMetricSimple) {
            this.sanchezMetricSimple = sanchezMetricSimple;
        }

        public boolean isRecoreded() {
            return recoreded;
        }

        public void setRecoreded(boolean recoreded) {
            this.recoreded = recoreded;
        }

        public boolean isImageSaved() {
            return imageSaved;
        }

        public void setImageSaved(boolean imageSaved) {
            this.imageSaved = imageSaved;
        }

        public void setNormalisedMSE(double normalisedMSE) {
            this.normalisedMSE = normalisedMSE;
        }

        public double getNormalisedMSE() {
            return normalisedMSE;
        }

        public double getSanchisPerfectCloak() {
            return sanchisPerfectCloak;
        }

        public void setSanchisPerfectCloak(double sanchisPerfectCloak) {
            this.sanchisPerfectCloak = sanchisPerfectCloak;
        }

        public double getSanchezPerfectCloak() {
            return sanchezPerfectCloak;
        }

        public void setSanchezPerfectCloak(double sanchezPerfectCloak) {
            this.sanchezPerfectCloak = sanchezPerfectCloak;
        }

        public double getRmse() {
            return rmse;
        }

        public void setRmse(double rmse) {
            this.rmse = rmse;
        }

        public double getMinPressure() {
            return minPressure;
        }

        public void setMinPressure(double minPressure) {
            this.minPressure = minPressure;
        }

        public double getMaxPressure() {
            return maxPressure;
        }

        public void setMaxPressure(double maxPressure) {
            this.maxPressure = maxPressure;
        }

        public double getAveragePressure() {
            return averagePressure;
        }

        public void setAveragePressure(double averagePressure) {
            this.averagePressure = averagePressure;
        }

        public double getAbsoluteAveragePressure() {
            return absoluteAveragePressure;
        }

        public void setAbsoluteAveragePressure(double absoluteAveragePressure) {
            this.absoluteAveragePressure = absoluteAveragePressure;
        }

        public int getLength() {
            return metricsList.length;
        }

    public double getL2Norm() {
        return l2Norm;
    }

    public void setL2Norm(double l2Norm) {
        this.l2Norm = l2Norm;
    }
}


