package com;

import com.comsol.model.Model;
import com.comsolModelInterface.comsolModels.Acoustic_Cloaking_20KHz_monopole_5point4;
import com.comsolModelInterface.comsolModels.Acoustic_Cloaking_20KHz_thin_cloak_add_pml_explicit4_with_parametric_sweep;
import com.comsolModelInterface.comsolModels.Acoustic_Cloaking_20KHz_monopole;
import com.comsolModelInterface.comsolModels.Acoustic_Cloaking_20KHz_monopole_working;
//import com.comsolModelInterface.comsolModels.Acous;
import com.comsolModelInterface.comsolModels.legacy.Acoustic_Cloaking_20KHz_thin_cloak;
import com.comsolModelInterface.comsolModels.monopole.CircularWavefront_SqObjectCloak_java;
import com.ga.SimulatedAnnealingTechniques;
import com.ga.nsaga.Position;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class Variables {
    // control variables / flags
    private static boolean useGeneRandomisation = false;
    static boolean runCollsionChecks = true;
    static String optimizerType = "SAGA"; // control variable to choose which of the optimizers to use - SAGA or NSAGAD:\dev_D\comsol_optimiser\comsol_optimiser\src\main\resources\activeProcessingCSV\geneHistory_renamed.csv
    static String shapeOfScatterer = "Circle";
    static String shapeOfObject = "Circle";
    static boolean saveAllImages = false;
    static boolean saveNoImages = true;
    static boolean useOldSystem = true; // use 2.0 = True, 3.0 = false
    private static boolean save360Points = true;
    private static String USED_ERROR_METRIC = "sanchis";
    private static boolean ALLOW_0_SWITCHING = true;  // value to control if scatterers are allowed to switch down to the hidden layer or not - set to true to allow them to
    private static boolean USE_COMSOL = true; // control variable, where it can be switched off to give random answers,

    /** Begin values to control thickness of cloak and other comsol settings!*/
    private static String cloackThicknesssMultiplier = "1.52"; // How far out from the central scatterer the cloak can go maximum
    private static String centralObjectRadisMultiplier = "1"; // How far out from the central scatterer the cloak can go maximum
    private static String scattererRadiusMultiplier = "1/15"; // How far out from the central scatterer the cloak can go maximum
    private static String csvMapPath = System.getProperty("user.dir")+"\\output\\MassImages\\2020-11-09--16-09\\index_map2020-11-09--16-09.csv";
    private static boolean usePreviousMap = false; // adjust to
    // "\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv" -- ORIGINAL MAP
    // "\\src\\main\\resources\\sactterer_index_coordinate_map.csv -- NEW MAP
    // "\\output\\MassImages\\2020-10-22--15-06\\index_map2020-10-22--15-06.csv" -- trialing one
    private static Position[] SCATTERER_POSITION_INDEX_MAP;
    // ultrathin = System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_mesh_ultrathin.csv", true
    // thicker   = System.getProperty("user.dir")+"\\src\\main\\resources\\scatterer_dense_mesh_120_with_coordinates.csv"
    private static int pixelWidth = 354; // the number of pixels used to generate an image - 354 was base value
    private static double regionThickness = 0.0605; // width of region that is examined -- 0.045 was base value -- in larger zones, e.g. 2x thickness,
    // if cloak thickness is increased, then need to use a larger region thickness - 2x object, 1.52 cloak = 0.060368
    private static double mappedLayerDensity = 0.25; // default is 0.25
    private static double mappedScatterRadiusMultiplier = 1.5; // default is 1.5
    /** End values to control thickness of cloak!*/


    // resuming previous run:
    static int previousGenerationCounter = 0;
    private static boolean resumePreviousRun = true;
    private static String previousRunFilePath = "D:\\dev_D\\comsol_optimiser\\comsol_optimiser\\output\\MassImages\\2020-12-17--16-59\\geneHistoryAll.csv" ;
    private static int INITIAL_ANNEALING_STEP = 16; // variable to control the intial starting step - can be changed to resume previous runs.
    private static double STARTING_TEMPERATURE = initialTemperatureFinder();  // the starting temperature is the temperature that the algorithm will start at. This is the temperature to adjust when resuming a previous run.

    // general variables
    private static int initalPopSize = 30;
    private static int numGenerations = 50;
    private static double radiusScatterer;
    static long programStartTime;
    private static long timeSpentClearingRecoveries = 0;
    private static double INITIAL_TEMPERATURE = 0.7; // the initial temperature is the starting temperature that every simulation starts at
    private static int MAXIMUM_TEMPERATURE_STEPS = 50;
    private static double initialMutationProbability = 0.2;
    private static int MAXIMUM_SCATTERERS_IN_A_QUADRANT = 35; // variable to show the maximum number of scatterers in a quadrant - 30 = 120 scat total, 35 = 140 scat total
    private static double currentMutationProbability = initialMutationProbability;
    private static double radiusCentralObject;
    private static double max_scatterer_placement_radius;
    private static double min_scatterer_placement_radius;
    private static String versionUsed;
    private static double sanchisDenominator=-99999999.9;
    private static String activeGeneHistoryCSVLocation;
    private static boolean clearModelOnGenerate = false;
    private static boolean runFreshModel = false;
    private static String completeHistoryFilePath;
    private static String massImageOutputFolder;
    private static String logFolderUsed;
    private static String comsolVersion;
    private static int reannealingStep;
    private static double tempRaiseAmount;
    private static String geneHistoryDir;
    private static int numberOfCores;
    private static String modelPath;
    private static boolean readOldMapUseNew;
    private static boolean checkoutLicense;
    private static boolean waitForLicense;
    private static String tempFunction;
    private static double addtionalMutationDecrease;
    private static double mutateAddtional;
    private static boolean monpole;
    private static String contourFilePath;

    public static double getRadiusCentralObject() {
        return radiusCentralObject;
    }

    public static void setRadiusCentralObject(double radiusCentralObject) {
        Variables.radiusCentralObject = radiusCentralObject;
    }

    public static boolean isSaveAllImages() {
        return saveAllImages;
    }

    public static boolean isSave360Points() {
        return save360Points;
    }

    public static String getUsedErrorMetric() {
        return USED_ERROR_METRIC;
    }

    public static boolean isAllow0Switching() {
        return ALLOW_0_SWITCHING;
    }

    public static int getInitialAnnealingStep() {
        return INITIAL_ANNEALING_STEP;
    }

    public static double getStartingTemperature() {
        return STARTING_TEMPERATURE;
    }

    public static double getInitialTemperature() {
        return INITIAL_TEMPERATURE;
    }

    public static int getMaximumTemperatureSteps() {
        return MAXIMUM_TEMPERATURE_STEPS;
    }

    public static double getInitialMutationProbability() {
        return initialMutationProbability;
    }

    public static int getMaximumScatterersInAQuadrant() {
        return MAXIMUM_SCATTERERS_IN_A_QUADRANT;
    }

    public static int getUsedScatterersInAQuadrant() {
        return USED_SCATTERERS_IN_A_QUADRANT;
    }

    public static int USED_SCATTERERS_IN_A_QUADRANT = MAXIMUM_SCATTERERS_IN_A_QUADRANT; // used to control the active number of scatterers

    public static int maxPopMembers() {
        return numGenerations + initalPopSize;
    }

    public static boolean isResumePreviousRun() {
        return resumePreviousRun;
    }

    public static int getInitalPopSize() {
        return initalPopSize;
    }

    public static int getNumGenerations() {
        return numGenerations;
    }

    public static double getRadiusScatterer() {
        return radiusScatterer;
    }

    public static void setRadiusScatterer(double radiusScatterer) {
        Variables.radiusScatterer = radiusScatterer;
    }

    public static boolean isUseGeneRandomisation() {
        return useGeneRandomisation;
    }

    public static boolean isRunCollsionChecks() {
        return runCollsionChecks;
    }

    public static String getOptimizerType() {
        return optimizerType;
    }

    public static String getShapeOfScatterer() {
        return shapeOfScatterer;
    }

    public static String getShapeOfObject() {
        return shapeOfObject;
    }

    public static int getPreviousGenerationCounter() {
        return previousGenerationCounter;
    }

    public static long getProgramStartTime() {
        return programStartTime;
    }

    public static void setProgramStartTime(long programStartTime) {
        Variables.programStartTime = programStartTime;
    }

    public static void increseTimeSpentCleaningRevoveries(long increase) {
        timeSpentClearingRecoveries += increase;
    }

    public static long getTimeSpentClearingRecoveries() {
        return timeSpentClearingRecoveries;
    }

    public static boolean isUseOldSystem() {
        return useOldSystem;
    }

    private static double initialTemperatureFinder() {
        SimulatedAnnealingTechniques simulatedAnnealingTechniques = new SimulatedAnnealingTechniques();
        double temperature = INITIAL_TEMPERATURE;
        for (int i = 0; i < INITIAL_ANNEALING_STEP; i++ ) {
            temperature = simulatedAnnealingTechniques.decreaseByCoolingRate(temperature, 0.25);
        }
        return temperature;
    }

    public static double getCurrentMutationProbability() {
        return currentMutationProbability;
    }

    public static void setCurrentMutationProbability(double currentMutationProbability) {
        Variables.currentMutationProbability = currentMutationProbability;
    }

    public static double getMax_scatterer_placement_radius() {
        return max_scatterer_placement_radius;
    }

    public static void setMax_scatterer_placement_radius(double max_scatterer_placement_radius) {
        Variables.max_scatterer_placement_radius = max_scatterer_placement_radius;
    }

    public static double getMin_scatterer_placement_radius() {
        return min_scatterer_placement_radius;
    }

    public static void setMin_scatterer_placement_radius(double min_scatterer_placement_radius) {
        Variables.min_scatterer_placement_radius = min_scatterer_placement_radius;
    }

    public static boolean isUseComsol() {
        return USE_COMSOL;
    }

    public static String getVersionUsed() {
        return versionUsed;
    }

    public static void setVersionUsed(String versionUsed) {
        Variables.versionUsed = versionUsed;
    }

    public static String getCloackThicknesssMultiplier() {
        return cloackThicknesssMultiplier;
    }

    public static String getCentralObjectRadisMultiplier() {
        return centralObjectRadisMultiplier;
    }

    public static String getScattererRadiusMultiplier() {
        return scattererRadiusMultiplier;
    }

    public static String getCsvMapPath() {
        return csvMapPath;
    }

    public static void setCsvMapPath(String csvMapPath) {
        Variables.csvMapPath = csvMapPath;
    }

    public static Position[] getScattererPositionIndexMap() {
        return SCATTERER_POSITION_INDEX_MAP;
    }

    public static void setScattererPositionIndexMap(Position[] scattererPositionIndexMap) {
        SCATTERER_POSITION_INDEX_MAP = scattererPositionIndexMap;
    }

    public static void setSanchisDenominator(double denominator) {
        sanchisDenominator = denominator;
    }

    public static double getSanchisDenominator() {
        return sanchisDenominator;
    }

    public static String getActiveGeneHistoryCSVLocation() {
        return activeGeneHistoryCSVLocation;
    }

    public static void setActiveGeneHistoryCSVLocation(String activeGeneHistoryCSVLocation) {
        Variables.activeGeneHistoryCSVLocation = activeGeneHistoryCSVLocation;
    }

    public static Model generateFreshComsolModel() {
        System.out.println("Generating first comsol model");
        if (Variables.getModelPath().endsWith("explicit4_with_parametric_sweep.mph")) {
            System.out.println("Generating First model, trying traditional");
            return Acoustic_Cloaking_20KHz_thin_cloak_add_pml_explicit4_with_parametric_sweep.createModel();
        } else if (Variables.getModelPath().endsWith("Acoustic_Cloaking_20KHz_monopole.mph")) {
            if (comsolVersion.equals("v54")) {
                try {
                    System.out.println("Generating First model, trying monopol 5.4");
                    return Acoustic_Cloaking_20KHz_monopole_5point4.createModel();
                } catch (Exception e) {
                    System.out.println("ERROR when running comsol 54 - ");
                    e.printStackTrace();
                }
            } else if (comsolVersion.equals("v56")) {
                try {
                    System.out.println("Generating First model, trying monopol 5.6");
                    return Acoustic_Cloaking_20KHz_monopole_working.createModel();
                } catch (Exception e){
                    System.out.println("ERROR when running comsol 56 - ");
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("ERROR WHEN MAKING COMSOL MODEL - tried to find model "+Variables.getModelPath());
            System.exit(-1);
        }
        return null;
//        return Acoustic_Cloaking_20KHz_thin_cloak_add_pml_explicit3.createModel();
//        return CircularWavefront_SqObjectCloak_java.createModel();
    }

    public static int getScatMapLength() {
        return SCATTERER_POSITION_INDEX_MAP.length;
    }

    public static double getPixelWidth() {
        return pixelWidth;
    }

    /**
     *
     * @return the size of the region that will be examined by the algorithm - e.g. 0.045 = 0.045m square region
     */
    public static double getRegionThickness() {
        return regionThickness;
    }

    public static String getPreviousRunFilePath() {
        return previousRunFilePath;
    }

    public static boolean isClearModelOnGenerate() {
        return clearModelOnGenerate;
    }

    public static String getCompleteHistoryFile() {
        return completeHistoryFilePath;
    }

    public static void setCompleteHistoryFilePath(String completeHistoryFilePath) {
        Variables.completeHistoryFilePath = completeHistoryFilePath;
    }

    public static boolean usePreviousMap() {
        return usePreviousMap;
    }

    public static boolean isSaveNoImages() {
        return saveNoImages;
    }

    public static void setUseGeneRandomisation(boolean useGeneRandomisation) {
        Variables.useGeneRandomisation = useGeneRandomisation;
    }

    public static void setRunCollsionChecks(boolean runCollsionChecks) {
        Variables.runCollsionChecks = runCollsionChecks;
    }

    public static void setOptimizerType(String optimizerType) {
        Variables.optimizerType = optimizerType;
    }

    public static void setSaveAllImages(boolean saveAllImages) {
        Variables.saveAllImages = saveAllImages;
    }

    public static void setSaveNoImages(boolean saveNoImages) {
        Variables.saveNoImages = saveNoImages;
    }

    public static void setUseOldSystem(boolean useOldSystem) {
        Variables.useOldSystem = useOldSystem;
    }

    public static void setCloackThicknesssMultiplier(String cloackThicknesssMultiplier) {
        Variables.cloackThicknesssMultiplier = cloackThicknesssMultiplier;
    }

    public static void setCentralObjectRadisMultiplier(String centralObjectRadisMultiplier) {
        Variables.centralObjectRadisMultiplier = centralObjectRadisMultiplier;
    }

    public static void setScattererRadiusMultiplier(String scattererRadiusMultiplier) {
        Variables.scattererRadiusMultiplier = scattererRadiusMultiplier;
    }

    public static void setUsePreviousMap(boolean usePreviousMap) {
        Variables.usePreviousMap = usePreviousMap;
    }

    public static void setPixelWidth(int pixelWidth) {
        Variables.pixelWidth = pixelWidth;
    }

    public static void setRegionThickness(double regionThickness) {
        Variables.regionThickness = regionThickness;
    }

    public static void setPreviousGenerationCounter(int previousGenerationCounter) {
        Variables.previousGenerationCounter = previousGenerationCounter;
    }

    public static void setResumePreviousRun(boolean resumePreviousRun) {
        Variables.resumePreviousRun = resumePreviousRun;
    }

    public static void setPreviousRunFilePath(String previousRunFilePath) {
        Variables.previousRunFilePath = previousRunFilePath;
    }

    public static void setStartingTemperature(double startingTemperature) {
        STARTING_TEMPERATURE = startingTemperature;
    }

    public static void setInitalPopSize(int initalPopSize) {
        Variables.initalPopSize = initalPopSize;
    }

    public static void setNumGenerations(int numGenerations) {
        Variables.numGenerations = numGenerations;
    }

    public static void setTimeSpentClearingRecoveries(long timeSpentClearingRecoveries) {
        Variables.timeSpentClearingRecoveries = timeSpentClearingRecoveries;
    }

    public static void setClearModelOnGenerate(boolean clearModelOnGenerate) {
        Variables.clearModelOnGenerate = clearModelOnGenerate;
    }

    public static void setRunFreshModel(boolean runFreshModel) {
        Variables.runFreshModel = runFreshModel;
    }

    public static void setShapeOfScatterer(String shapeOfScatterer) {
        Variables.shapeOfScatterer = shapeOfScatterer;
    }

    public static void setShapeOfObject(String shapeOfObject) {
        Variables.shapeOfObject = shapeOfObject;
    }

    public static void setSave360Points(boolean save360Points) {
        Variables.save360Points = save360Points;
    }

    public static void setUsedErrorMetric(String usedErrorMetric) {
        USED_ERROR_METRIC = usedErrorMetric;
    }

    public static void setAllow0Switching(boolean allow0Switching) {
        ALLOW_0_SWITCHING = allow0Switching;
    }

    public static void setUseComsol(boolean useComsol) {
        USE_COMSOL = useComsol;
    }

    public static void setInitialAnnealingStep(int initialAnnealingStep) {
        INITIAL_ANNEALING_STEP = initialAnnealingStep;
    }

    public static void setInitialTemperature(double initialTemperature) {
        INITIAL_TEMPERATURE = initialTemperature;
    }

    public static void setMaximumTemperatureSteps(int maximumTemperatureSteps) {
        MAXIMUM_TEMPERATURE_STEPS = maximumTemperatureSteps;
    }

    public static void setInitialMutationProbability(double initialMutationProbability) {
        Variables.initialMutationProbability = initialMutationProbability;
    }

    public static void setMaximumScatterersInAQuadrant(int maximumScatterersInAQuadrant) {
        MAXIMUM_SCATTERERS_IN_A_QUADRANT = maximumScatterersInAQuadrant;
    }

    public static String getMassImageOutputFolder(String startDate, String extraFiles) {
        Path path = Paths.get(massImageOutputFolder, "output", "MassImages", extraFiles, startDate);
        File directory = new File(path.toAbsolutePath().toString());
        if (!directory.exists()){
            directory.mkdirs();
        }
        return path.toString();
    }

    public static String getOutputFolder() {
        return Paths.get(massImageOutputFolder,"output").toString();
    }

    public static void setMassImageOUtputFolder(String massImageOUtputFolder) {
        Variables.massImageOutputFolder = massImageOUtputFolder;
    }


    public static String getLogFolderUsed() {
        return logFolderUsed;
    }

    public static void setLogFolderUsed(String logFolderUsed) {
        Variables.logFolderUsed = logFolderUsed;
    }

    public static double getMappedLayerDensity() {
        return mappedLayerDensity;
    }

    public static void setMappedLayerDensity(double mappedLayerDensity) {
        Variables.mappedLayerDensity = mappedLayerDensity;
    }

    public static double getMappedScatterRadiusMultiplier() {
        return mappedScatterRadiusMultiplier;
    }

    public static void setMappedScatterRadiusMultiplier(double mappedScatterRadiusMultiplier) {
        Variables.mappedScatterRadiusMultiplier = mappedScatterRadiusMultiplier;
    }

    public static String getComsolVersion() {
        return Variables.comsolVersion;
    }

    public static void setComsolVersion(String comsolVersion) {
        Variables.comsolVersion = comsolVersion;
    }

    /**
     * Reannealing is used to determine if the program should raise temperature agian after a certain number of steps.
     * a value of 0 means not to, any higher values are applied after that many steps
     * @return
     */
    public static int getReAnnealingStep() {
        return reannealingStep;
    }

    public static void setReannealingStep(int reannealingStep) {
        Variables.reannealingStep = reannealingStep;
    }

    public static double getTempRaiseAmount() {
        return tempRaiseAmount;
    }

    public static void setTempRaiseAmount(double tempRaiseAmount) {
        Variables.tempRaiseAmount = tempRaiseAmount;
    }

    public static String getGeneHistoryDir() {
        return Variables.geneHistoryDir;
    }

    public static void setGeneHistoryDir(String geneHistoryDir) {
        Variables.geneHistoryDir = geneHistoryDir;
    }

    public static int getNumberCores() {
        return numberOfCores;
    }

    public static void setNumberOfCores(int numberOfCores) {
        Variables.numberOfCores = numberOfCores;
    }

    public static String getModelPath() {
        return modelPath;
    }

    public static void setModelPath(String modelPath) {
        Variables.modelPath = modelPath;
    }

    public static boolean readOldMapUseNew() {
        return readOldMapUseNew;
    }

    public static void setReadOldMapUseNew(boolean readOldMapUseNew) {
        Variables.readOldMapUseNew = readOldMapUseNew;
    }

    public static boolean checkoutLicense() {
        return checkoutLicense;
    }

    public static void setCheckoutLicense(boolean checkoutLicense) {
        Variables.checkoutLicense = checkoutLicense;
    }

    public static boolean waitForLicense() {
        return waitForLicense;
    }

    public static void setWaitForLicense(boolean waitForLicense) {
        Variables.waitForLicense = waitForLicense;
    }

    public static String getTempFunction() {
        return tempFunction;
    }

    public static void setTempFunction(String tempFunction) {
        Variables.tempFunction = tempFunction;
    }


    public static void setAddtionalMutationDecrease(double addtionalMutationDecrease) {
        Variables.addtionalMutationDecrease = addtionalMutationDecrease;
    }

    public static double getAddtionalMutationDecrease() {
        return addtionalMutationDecrease;
    }

    public static void setMutateAddtional(double mutateAddtional) {
        Variables.mutateAddtional = mutateAddtional;
    }

    public static double getMutateAddtional() {
        return mutateAddtional;
    }

    public static void setUsedScatterersInAQuadrant(int int1) {
        USED_SCATTERERS_IN_A_QUADRANT = int1;
    }

    public static void setMonopole(boolean b) {
        monpole = b;
    }

    public static boolean getMonopole() {
        return monpole;
    }

    public static void setContourFilePath(String contourFilePath) {
        Variables.contourFilePath = contourFilePath;
    }

    public static String getContourFilePath() {
        return contourFilePath;
    }
}
