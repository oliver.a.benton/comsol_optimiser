Acoustic Cloaking inverse problem with comsol

**Prerequisites**:
Comsol, Maven

**Installing:**
When using Intellij, import this project from exsiting sources as a Maven file
When running this project through eclipse...? UNKOWN.

When running this project for the first time, make sure to set up the comsol jar files. Look up your IDE method to add jars
to a project and direct it to the plugins folder of your comsol install, usually:
C:\Program Files\COMSOL\COMSOL<VERSION>\Multiphysics\plugins




**IMPORTANT:**
Comsol licenses are included in java, but done by machine. If you have comsol open, java will use the same licence. If you don't
have comsol open, you will be fighting with other liscences.

When exporting a comsol model, remember this one **SERIOUSLY IMPORTANT** -
go file, compact history. Wait for this process to finish, then export to java. This will prevent the entire simulation
history from being stored with it.

**Files**

src.main.java.com.comsolModels:
    Stores the interface and implementation comsol models that are used. This is used in other
    locations to be able to control comsol models - it is our custom api surrounding the comsol model.

src.main.java.com.GA:
    the code for the genetic algorithms.
    The packages includes are labelled:
         "mine" for a very simple microbial GA, that was never fully implemented.
         "nsaga" for the NN SAGA code
         "raghu" for the SAGA code developed by Raghu.
    Further readmes on their uses can be found within their packages.


src.main.java.com.massImageSaving:
    this is the code for saving large numbers of images during data generation. Run the combination generator to create a
    random set of combinations, that should be correct.
    Then run the "RunSearchSpace" file to run this combination set. It will then save the outputs to "target.out.MassImages.{}"
    where {} is the date and time that you started running the project.

src.main.resources.batchJobFiles:
    This stores the batch jobs during a data gen run. If it isn't there for you, don't worry, it's desinged to delte itself
    after usage, as a batch job that is still there and labelled as "scheduling" will stop future batch jobs.
    If you at any point stop this program mid run, you will need to manuall delete the batch jobs folder. This may require
    specific order of deletion for certain files inside, if they are still in use.
    It can also be used for debugging combinations of coordinates, if there is no

src.main.resources.comsolParamters:
    This stores a file containing the paramters used during comsol, such as scatterer radius etc... If there have been
    no changes to this since the last data generation, it can speed up data generation, and remove the need to access
    comsol for the production. This means that it is easier to use.

src.main.resources.comsolProgram:
    This stores a copy of the comsol program used, as well as a pre-made java based copy

src.main.resources.comsolProgram.backup_working_comsol:
    This folder simply stores a last known working backup of the comsol file in the folder above, in the event of inadvertent changes.
    While git makes this slightly reduendant, it is useful for those less adept at Git.

src.main.resources.geneBankStorage:
    While still labelled geneBankStorage, this folder actually also stores all the combinations that are used during random
    generation.


**Changes to COMSOL file:**
If any changes are made to the comsol file, then you should:
1. immediately compact the history
2. save a copy as the java version. 
3. Take this copy, replace the existing src.main.java.com.comsolModel.acoustic_scatterers file with it.
4. add the following code snippet, making sure to update as many "runX" methods as needed: 
`public static Model createModel() {
    Model model = run();
    model = run2(model);
    return model;
  }`



***COMSOL specific info:***

**Outputs:**
Line Integration 1 = the main output - normalised so 1.0 = as good as no scatterers, >1.0 = worse than no scatterers, 0.0 = "perfect cloak" 
    - recommend simulating in comsol actual to see if 0.0 values actually are perfect cloak

**CSV OUTPUTS:**
0-34 == the coordinate indexs A-AI
35 (AJ) = used_score
36 (AK) = mse
37 (AL) = sanchis -- 1- comsol output -- tend to 1
38 (AM) = sanchis -- comsol output H -- tend to 0
39 (AN) = sanchis -- H mine -- tend to 0
40 (AO) = sanchis numerator -- tend to 0
41 (AP) = sanchez full -- tend to 1
42 (AQ) = sanchez numerator -- tend to 0
43 (AR) = normalised MSE -- tend to 0
44 (AS) = sanchis 2011 using perfect cloak (i.e. 37 but with perfect cloak as normaliser) -- tend to 1
45 (AT) = sanchez 2018 using perfect cloak (i.e. 41 but with perfect cloak as normaliser) -- tend to 1
46 (AU) = RMSE -- tend to 0
47 (AV) = minimum pressure of 360 points
48 (AW) = maximum pressure of 360 points
49 (AX) = average pressure of 360 points
50 (AY) = average of absolute pressures of 360 points


**Rules**:
	- No collision between center object and scatteres:  (R1 + r1) - R >= 0  ---> R+r1 < R1 ---> R+r1-R1 > 0
	- phi should be below 360
	- Scatterers in layer should try not to overlap - empirical evidence says that it doesnt work as well: R1 * pi /n1 > r1 ---> (R1 * pi /n1) -r1 > 0
	- Scatterers between layers should also try not to overlap - formula pending on using more than 1 layer
	- R1 < 2R - upper limit for how far out the radius can be 
	- 
	


**Extra checks for it:**
phi must be 0-359 - 360 = 0 so no need - if random generates something above 360, should mod (%) down to a value between 0 and 359 - phi= phi%360

line 100, 115: 115 (model.geom("geom1").feature("rot1").set("rot", "range(0,360/(n1-1),360)");) is set to run /(n1-1). n1 is initialised at line 100 and is set to 1.
This means /(1-1) = /0 --> impossible - fastest fix is to change line 100 to declare to value of 2 - (/2-1) which allows it to continue


**Taking a picture:**
To save an image, you need to run a batch job as well as the saving code created for teh model.export().
This is:
        model.batch().create("graphics_job", "Batch");
		model.batch("graphics_job").set("graphics", "on");
		model.batch("graphics_job").create("export_sequence_task", "Exportseq");

This code, run after the image set ups allows the image to be saved at an appropriate location. This is recommended for saving the very last one

**HELPFUL LINKS
Tutorials to read for tips:
For saving images using a sequence/batch job:
https://uk.comsol.com/blogs/how-to-export-images-automatically-after-solving-your-model/
For using the java API for comsol:
https://uk.comsol.com/blogs/automate-modeling-tasks-comsol-api-use-java/

** Observations **:
It seems to run faster if you create a new model on each iteration. This will therefore require garbage collection every so often.